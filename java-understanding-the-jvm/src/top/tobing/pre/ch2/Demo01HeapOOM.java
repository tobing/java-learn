package top.tobing.pre.ch2;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author tobing
 * @Date 2020/11/18 10:52
 * @Description Java堆内存溢出异常测试
 * VM Args：-Xms20m -Xmx20m -XX:HeapDumpOnOutOfMemoryError
 */
public class Demo01HeapOOM {
    static class OOMObject {

    }

    public static void main(String[] args) {

        List<OOMObject> list = new ArrayList<>();
        while (true) {
            list.add(new OOMObject());
        }
    }

      //Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
    //at java.util.Arrays.copyOf(Arrays.java:3210)
    //at java.util.Arrays.copyOf(Arrays.java:3181)
    //at java.util.ArrayList.grow(ArrayList.java:265)
    //at java.util.ArrayList.ensureExplicitCapacity(ArrayList.java:239)
    //at java.util.ArrayList.ensureCapacityInternal(ArrayList.java:231)
    //at java.util.ArrayList.add(ArrayList.java:462)
    //at top.tobing.pre.ch2.Demo01HeapOOM.main(Demo01HeapOOM.java:21)

}
