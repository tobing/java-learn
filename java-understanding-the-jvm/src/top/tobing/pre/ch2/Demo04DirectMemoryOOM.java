package top.tobing.pre.ch2;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * @Author tobing
 * @Date 2020/11/18 11:17
 * @Description 直接内存溢出-收银unsafe分配本机内存
 * VM Args:-Xms20m -Xmx20m -XX:MaxDirectMemorySize=10m
 */
public class Demo04DirectMemoryOOM {

    private static long _1MB = 1024 * 1024;

    public static void main(String[] args) throws IllegalAccessException {
        Field unsafeField = Unsafe.class.getDeclaredFields()[0];
        unsafeField.setAccessible(true);
        Unsafe unsafe = (Unsafe) unsafeField.get(null);
        while (true) {
            unsafe.allocateMemory(_1MB);
        }
    }
}
