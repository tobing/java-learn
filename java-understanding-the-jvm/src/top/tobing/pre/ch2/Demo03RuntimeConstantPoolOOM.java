package top.tobing.pre.ch2;

/**
 * @Author tobing
 * @Date 2020/11/18 11:13
 * @Description String.intern测试
 */
public class Demo03RuntimeConstantPoolOOM {
    public static void main(String[] args) {
        String str1 = new StringBuilder("to").append("tobing").toString();
        System.out.println(str1.intern() == str1);

        // str2在StringBuilder中toString方法中创建时new
        // str2.intern，在Version类创建时“java”被加载到常量池，引用肯定不一样
        String str2 = new StringBuilder("ja").append("va").toString();
        System.out.println(str2.intern() == str2);
    }
}
