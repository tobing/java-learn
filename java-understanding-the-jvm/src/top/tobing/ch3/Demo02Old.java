package top.tobing.ch3;

/**
 * @Author tobing
 * @Date 2020/11/19 20:29
 * @Description 大对象直接进入老年代
 */
public class Demo02Old {
    public static void main(String[] args) {
        testPretenureSizeThreshold();
    }

    /**
     * VM Args:-verbose:gc -Xms20M -Xmx20m -Xmn10m -XX:+PrintGCDetails -XX:SurvivorRatio=8 -XX:PretenureSizeThreshold=3145728 -XX:+UseConcMarkSweepGC
     * -XX:PretenureSizeThreshold 超过该设置的限度直接在老年代生成，这里设置的3MB
     * 注意以上参数在Parallel Scavenge模式下并不会生效，只能在Parnew+CMS模式下
     */
    private static void testPretenureSizeThreshold() {
        // 分配4M空间
        byte[] allocation = new byte[4 * 1024 * 1024];
    }
    /**
     * Heap
     *  par new generation   total 9216K, used 1976K [0x00000000fec00000, 0x00000000ff600000, 0x00000000ff600000)
     *   eden space 8192K,  24% used [0x00000000fec00000, 0x00000000fedee1b0, 0x00000000ff400000)
     *   from space 1024K,   0% used [0x00000000ff400000, 0x00000000ff400000, 0x00000000ff500000)
     *   to   space 1024K,   0% used [0x00000000ff500000, 0x00000000ff500000, 0x00000000ff600000)
     *  concurrent mark-sweep generation total 10240K, used 4096K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
     *  Metaspace       used 3192K, capacity 4496K, committed 4864K, reserved 1056768K
     *   class space    used 346K, capacity 388K, committed 512K, reserved 1048576K
     */
}
