package top.tobing.ch3;

/**
 * @Author tobing
 * @Date 2020/11/19 20:43
 * @Description 长期存活的对象进入老年代
 */
public class Demo03MaxTenuring {
    public static void main(String[] args) {
        testTenuringThreshold1();
    }

    /**
     * VM Args:-verbose:gc -Xms20M -Xmx20m -Xmn10m -XX:+PrintGCDetails -XX:SurvivorRatio=8  -XX:MaxTenuringThreshold=1
     * 设置升入老年代时间为：1
     */
    private static void testTenuringThreshold1() {

        byte[] allocation1 = new byte[1024 / 4];
        byte[] allocation2 = new byte[4 * 1024 * 1024];
        byte[] allocation3 = new byte[4 * 1024 * 1024];
        allocation3 = null;
        allocation3 = new byte[4 * 1024 * 1024];

        /**
         *Heap
         *  PSYoungGen      total 9216K, used 6328K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
         *   eden space 8192K, 77% used [0x00000000ff600000,0x00000000ffc2e1f8,0x00000000ffe00000)
         *   from space 1024K, 0% used [0x00000000fff00000,0x00000000fff00000,0x0000000100000000)
         *   to   space 1024K, 0% used [0x00000000ffe00000,0x00000000ffe00000,0x00000000fff00000)
         *  ParOldGen       total 10240K, used 8192K [0x00000000fec00000, 0x00000000ff600000, 0x00000000ff600000)
         *   object space 10240K, 80% used [0x00000000fec00000,0x00000000ff400020,0x00000000ff600000)
         *  Metaspace       used 3212K, capacity 4496K, committed 4864K, reserved 1056768K
         *   class space    used 349K, capacity 388K, committed 512K, reserved 1048576K
         */
    }

    /**
     * VM Args:-verbose:gc -Xms20M -Xmx20m -Xmn10m -XX:+PrintGCDetails -XX:SurvivorRatio=8  -XX:MaxTenuringThreshold=15
     * 设置升入老年代时间为：1
     */
    private static void testTenuringThreshold15() {

        byte[] allocation1 = new byte[1024 * 1024 / 4];
        byte[] allocation2 = new byte[4 * 1024 * 1024];
        byte[] allocation3 = new byte[4 * 1024 * 1024];
        allocation3 = null;
        allocation3 = new byte[4 * 1024 * 1024];

        /**
         * Heap
         *  PSYoungGen      total 9216K, used 6072K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
         *   eden space 8192K, 74% used [0x00000000ff600000,0x00000000ffbee1e8,0x00000000ffe00000)
         *   from space 1024K, 0% used [0x00000000fff00000,0x00000000fff00000,0x0000000100000000)
         *   to   space 1024K, 0% used [0x00000000ffe00000,0x00000000ffe00000,0x00000000fff00000)
         *  ParOldGen       total 10240K, used 8192K [0x00000000fec00000, 0x00000000ff600000, 0x00000000ff600000)
         *   object space 10240K, 80% used [0x00000000fec00000,0x00000000ff400020,0x00000000ff600000)
         *  Metaspace       used 3212K, capacity 4496K, committed 4864K, reserved 1056768K
         *   class space    used 349K, capacity 388K, committed 512K, reserved 1048576K
         */
    }
}
