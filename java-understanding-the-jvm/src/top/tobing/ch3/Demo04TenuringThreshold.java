package top.tobing.ch3;

/**
 * @Author tobing
 * @Date 2020/11/19 20:52
 * @Description 动态对象年龄判定
 * 并不是永远要求对象年龄大到MaxTenuringThreshold才会进入老年代
 */
public class Demo04TenuringThreshold {

    private static int _1M = 1024 * 1024;

    public static void main(String[] args) {
        testTenuringThreshold();
    }

    /**
     * VM Args:-verbose:gc -Xms20M -Xmx20m -Xmn10m -XX:+PrintGCDetails -XX:SurvivorRatio=8  -XX:MaxTenuringThreshold=15
     */
    private static void testTenuringThreshold() {
        byte[] allocation1 = new byte[_1M / 4];
        byte[] allocation2 = new byte[_1M / 4];
        byte[] allocation3 = new byte[4 * _1M];
        byte[] allocation4 = new byte[4 * _1M];
        allocation4 = null;
        allocation4 = new byte[4 * _1M];
    }

    /**
     * Heap
     *  PSYoungGen      total 9216K, used 6584K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
     *   eden space 8192K, 80% used [0x00000000ff600000,0x00000000ffc6e218,0x00000000ffe00000)
     *   from space 1024K, 0% used [0x00000000fff00000,0x00000000fff00000,0x0000000100000000)
     *   to   space 1024K, 0% used [0x00000000ffe00000,0x00000000ffe00000,0x00000000fff00000)
     *  ParOldGen       total 10240K, used 8192K [0x00000000fec00000, 0x00000000ff600000, 0x00000000ff600000)
     *   object space 10240K, 80% used [0x00000000fec00000,0x00000000ff400020,0x00000000ff600000)
     *  Metaspace       used 3212K, capacity 4496K, committed 4864K, reserved 1056768K
     *   class space    used 349K, capacity 388K, committed 512K, reserved 1048576K
     */
}
