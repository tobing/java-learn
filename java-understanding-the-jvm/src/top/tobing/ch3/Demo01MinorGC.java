package top.tobing.ch3;

/**
 * @Author tobing
 * @Date 2020/11/19 20:20
 * @Description 新生代Minor GC
 */
public class Demo01MinorGC {

    private static int _1MB = 1024 * 1024;

    public static void main(String[] args) {
        testAllocation();
    }

    /**
     * VM Args:-Xms20m -Xmx20m -Xmn10m -XX:+PrintGCDetails
     * 设置堆空间20m、堆最大空间20m、新生代空间10m
     */
    private static void testAllocation() {
        byte[] allocation1, allocation2, allocation3, allocation4;
        allocation1 = new byte[2 * _1MB];
        allocation2 = new byte[2 * _1MB];
        allocation3 = new byte[2 * _1MB];
        allocation4 = new byte[2 * _1MB];
    }
    /**
     * [GC (Allocation Failure) [PSYoungGen: 7792K->808K(9216K)] 7792K->6960K(19456K), 0.0038392 secs] [Times: user=0.00 sys=0.00, real=0.00 secs]
     * [Full GC (Ergonomics) [PSYoungGen: 808K->0K(9216K)] [ParOldGen: 6152K->6740K(10240K)] 6960K->6740K(19456K), [Metaspace: 3188K->3188K(1056768K)], 0.0067996 secs] [Times: user=0.05 sys=0.00, real=0.01 secs]
     * Heap
     *  PSYoungGen      total 9216K, used 2377K [0x00000000ff600000, 0x0000000100000000, 0x0000000100000000)
     *   eden space 8192K, 29% used [0x00000000ff600000,0x00000000ff852738,0x00000000ffe00000)
     *   from space 1024K, 0% used [0x00000000ffe00000,0x00000000ffe00000,0x00000000fff00000)
     *   to   space 1024K, 0% used [0x00000000fff00000,0x00000000fff00000,0x0000000100000000)
     *  ParOldGen       total 10240K, used 6740K [0x00000000fec00000, 0x00000000ff600000, 0x00000000ff600000)
     *   object space 10240K, 65% used [0x00000000fec00000,0x00000000ff2950c8,0x00000000ff600000)
     *  Metaspace       used 3198K, capacity 4496K, committed 4864K, reserved 1056768K
     *   class space    used 347K, capacity 388K, committed 512K, reserved 1048576K
     *
     *   ========================================================================
     *   堆：
     *   【新生代】
     *      eden：8M左右
     *      from：1M左右
     *      to：1M左右
     *      新生代可用 = eden + from = 9M ，因此total = 9126KB
     *   【老年代】
     *      total：10M
     *   【元空间】
     *      total：3M左右
     *
     */
}
