package top.tobing.other;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Author tobing
 * @Date 2021/4/8 10:47
 * @Description
 */
public class LRUCache<K, V> extends LinkedHashMap<K, V> {

    // Cache最大空间
    private static final int MAX_ENTRIES = 3;

    // 移除最老元素的时机
    @Override
    protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
        return this.size() > MAX_ENTRIES;
    }

    public LRUCache() {
        super(MAX_ENTRIES, 0.75F, true);
    }

    public static void main(String[] args) {
        LRUCache<Integer, String> cache = new LRUCache<>();
        cache.put(1, "a");
        cache.put(2, "b");
        cache.put(3, "c");
        cache.get(1);
        cache.put(4, "d");
        System.out.println(cache.keySet());
    }
}
