package java.lang;

/**
 * @Author tobing
 * @Date 2020/11/22 21:55
 * @Description
 */
public class StringD {
    static {
        System.out.println("DIY String is called.");
    }

    public static void main(String[] args) {
        System.out.println("DIY String is executing.");
    }
    /**
     * 错误: 在类 java.lang.String 中找不到 main 方法, 请将 main 方法定义为:
     *    public static void main(String[] args)
     * 否则 JavaFX 应用程序类必须扩展javafx.application.Application
     */
}
