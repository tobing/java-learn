package top.tobing.pre.ch17;

import java.util.ArrayList;

/**
 * @Author tobing
 * @Date 2020/12/31 11:11
 * @Description GC日志相关
 * -XX:+PrintGC
 * -XX:+PrintGCDetails
 *
 *
 * 额外参数：
 * -Xmx6m -Xms6m
 */
public class Demo01GCLogParam {
    public static void main(String[] args) {
        ArrayList<byte[]> list = new ArrayList<>();
        for (int i = 0; i < 500; i++) {
            // 每次添加100KB
            list.add(new byte[100 * 1024]);
        }
    }
}
