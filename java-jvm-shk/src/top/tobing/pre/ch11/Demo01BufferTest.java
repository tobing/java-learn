package top.tobing.pre.ch11;

import java.nio.ByteBuffer;
import java.util.Scanner;

/**
 * @Author tobing
 * @Date 2020/12/22 10:17
 * @Description 直接内存的使用
 * IO                   NIO(New IO / Non-Blocking)
 * byte[] / char[]      Buffer(ByteBuffer)
 * Stream               Channel
 */
public class Demo01BufferTest {

    private static final int _1GB = 1024 * 1024 * 1024;

    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocateDirect(_1GB);

        System.out.println("内存分配完毕！");

        Scanner scanner = new Scanner(System.in);
        scanner.next();

        System.out.println("内存开始释放！");
        buffer = null;
        System.gc();
        scanner.next();
    }


}
