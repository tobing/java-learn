package top.tobing.pre.ch11;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * @Author tobing
 * @Date 2020/12/22 10:22
 * @Description
 */
public class Demo02BufferError {
    private static final int _20M = 1024 * 1024 * 20;

    public static void main(String[] args) {
        ArrayList<ByteBuffer> list = new ArrayList<>();

        int count = 0;
        while (true) {
            try {
                list.add(ByteBuffer.allocateDirect(_20M));
                count++;
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                System.out.println(count);
            }
        }
    }
}
