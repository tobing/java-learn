package top.tobing.pre.ch12;

/**
 * @Author tobing
 * @Date 2020/12/23 21:53
 * @Description 测试不同模式（解析器、编译器）下的性能
 * 参数：
 * -Xint：纯解析器   6549
 * -Xcomp：纯编译器  797 ms
 * -Xmixed：混合模式 908 ms
 */
public class Demo01IntComp {
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        testPrimeNumber(1000000);
        long end = System.currentTimeMillis();
        System.out.println("花费时间为：" + (end - start) + " ms");
    }

    // 用于测试
    public static void testPrimeNumber(int count) {
        for (int i = 0; i < count; i++) {
            //计算100以内的质数
            label:
            for (int j = 2; j <= 100; j++) {
                for (int k = 2; k <= Math.sqrt(j); k++) {
                    if (j % k == 0) {
                        continue label;
                    }
                }
                //System.out.println(j);
            }

        }
    }
}
