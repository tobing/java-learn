package top.tobing.pre.ch9;

/**
 * @Author tobing
 * @Date 2020/12/19 22:59
 * @Description
 */
public class Demo02NonFinal {
    public static void main(String[] args) {
        Order order = null;
        // 以下两句会报NullPointException吗？ 不会。 为什么呢？
        order.hello();
        System.out.println(order.count);
    }
}

class Order {
    public static int count = 1;
    public static final int finalCount = 2333;
    public static void hello() {
        System.out.println("hello!");
    }
}
