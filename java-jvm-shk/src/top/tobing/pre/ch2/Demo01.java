package top.tobing.pre.ch2;

/**
 * @Author tobing
 * @Date 2020/11/22 19:01
 * @Description
 */
public class Demo01 {
    private static int num = 1;

    static {
        num = 2;
        a = 20;
        System.out.println(num);
        // 下面语句会报非法的前置引用
        //System.out.println(a);
    }

    private static int a = 10;

    public static void main(String[] args) {
        System.out.println(Demo01.num);
        System.out.println(Demo01.a);
    }
}
