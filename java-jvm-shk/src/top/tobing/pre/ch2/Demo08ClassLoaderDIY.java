package top.tobing.pre.ch2;

import java.io.FileNotFoundException;

/**
 * @Author tobing
 * @Date 2020/11/22 21:50
 * @Description
 */
public class Demo08ClassLoaderDIY extends ClassLoader {
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {

        try {
            byte[] result = getClassFromCustomPath(name);
            if (result == null) {
                throw new FileNotFoundException();
            } else {
                return defineClass(name, result, 0, result.length);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        throw new ClassNotFoundException(name);
    }

    private byte[] getClassFromCustomPath(String name) {
        // 从自定义路径中加载指定类
        // 如果指定路径的字节码文件进行了加密，则需要此方法中进行解密
        return null;
    }

    public static void main(String[] args) {
        Demo08ClassLoaderDIY demo08ClassLoaderDIY = new Demo08ClassLoaderDIY();
        try {
            Class<?> clazz = Class.forName("One", true, demo08ClassLoaderDIY);
            Object newInstance = clazz.newInstance();
            System.out.println(newInstance.getClass().getClassLoader());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
