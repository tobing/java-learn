package top.tobing.pre.ch2;

/**
 * @Author tobing
 * @Date 2020/11/22 20:30
 * @Description
 */
public class Demo04Clinit2 {
    static class Father {
        public static int A = 1;

        static {
            A = 2;
        }
    }

    static class Son extends Father {
        public static int B = A;
    }

    public static void main(String[] args) {
        // 执行Son的clinit之前先自行Father的clinit
        // clinit会将静态变量和静态代码块合并执行
        // 异常A = 2 ，而B=A，故B=2
        System.out.println(Son.B);
    }
}
