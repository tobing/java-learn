package top.tobing.pre.ch2;

/**
 * @Author tobing
 * @Date 2020/11/22 20:38
 * @Description
 */
public class Demo06ClassLoader1 {
    public static void main(String[] args) {
        // 获取系统类加载器
        // sun.misc.Launcher$AppClassLoader@18b4aac2
        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
        System.out.println(systemClassLoader);

        // 获取系统类加载器的上层
        // sun.misc.Launcher$ExtClassLoader@1b6d3586
        ClassLoader parent = systemClassLoader.getParent();
        System.out.println(parent);

        // 获取扩展类加载器的上层
        // null
        // Bootstrap ClassLoader
        ClassLoader parentParent = parent.getParent();
        System.out.println(parentParent);

        // 输出自定义类的类加载器
        // sun.misc.Launcher$AppClassLoader@18b4aac2
        ClassLoader classLoader = Demo06ClassLoader1.class.getClassLoader();
        System.out.println(classLoader);

        // 输出核心类库的类加载器
        // null
        // Bootstrap ClassLoader
        ClassLoader stringClassLoader = String.class.getClassLoader();
        System.out.println(stringClassLoader);
    }
}
