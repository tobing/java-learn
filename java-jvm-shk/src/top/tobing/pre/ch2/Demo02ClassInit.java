package top.tobing.pre.ch2;

/**
 * @Author tobing
 * @Date 2020/11/22 20:20
 * @Description 类的初始化
 */
public class Demo02ClassInit {
    private static int a = 1;
    static {
        a = 2;
        b = 20;
        System.out.println(a);
        // 下面的语句是非法的
        // 报错：非法的前置引用
        // System.out.println(b);
    }
    private static int b = 10;

    public static void main(String[] args) {
        System.out.println(Demo02ClassInit.a);
        System.out.println(Demo02ClassInit.b);
    }

    /** <clinit>()方法
     *  此方法不需要定义，是javac编译器自动收集类中的所有类变量的赋值动作和静态代码块合并而来
     *  构造器方法中的指令按语句出现在源文件中出现的顺序
     *  ==================================================
     *  0 iconst_1
     *  1 putstatic #3 <top/tobing/ch2/Demo02ClassInit.a>
     *  4 iconst_2
     *  5 putstatic #3 <top/tobing/ch2/Demo02ClassInit.a>
     *  8 bipush 20
     * 10 putstatic #5 <top/tobing/ch2/Demo02ClassInit.b>
     * 13 getstatic #2 <java/lang/System.out>
     * 16 getstatic #3 <top/tobing/ch2/Demo02ClassInit.a>
     * 19 invokevirtual #4 <java/io/PrintStream.println>
     * 22 bipush 10
     * 24 putstatic #5 <top/tobing/ch2/Demo02ClassInit.b>
     * 27 return
     */
}
