package top.tobing.pre.ch2;

/**
 * @Author tobing
 * @Date 2020/11/22 20:34
 * @Description 虚拟机必须保证一个类的<clinint>()方法在多线程下会被同步加锁
 */
public class Demo05ClinitDeatThread {
    public static void main(String[] args) {
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "开始");
            ThreadDemo threadDemo = new ThreadDemo();
            System.out.println(Thread.currentThread().getName() + "结束");
        }).start();
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "开始");
            ThreadDemo threadDemo = new ThreadDemo();
            System.out.println(Thread.currentThread().getName() + "结束");
        }).start();
    }
}

class ThreadDemo {
    static {
        if (true) {
            System.out.println(Thread.currentThread().getName() + "初始化当前类");
            while (true) {
            }
        }
    }
}