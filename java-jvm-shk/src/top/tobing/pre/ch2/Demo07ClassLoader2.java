package top.tobing.pre.ch2;

import sun.misc.Launcher;

import java.net.URL;

/**
 * @Author tobing
 * @Date 2020/11/22 20:43
 * @Description ClassLoader URL
 */
public class Demo07ClassLoader2 {
    public static void main(String[] args) {
        // 输出BootstrapClassLoader的类加载路径
        System.out.println("===================启动类加载器===================");
        URL[] urLs = Launcher.getBootstrapClassPath().getURLs();
        for (URL urL : urLs) {
            System.out.println(urL.toExternalForm());
        }

//        System.out.println("===================扩展类加载器===================");
//        String extDirs = System.getProperty("java.ext.dirs");
//        for (String s : extDirs.split(";")) {
//            System.out.println(s);
//        }
//

        /**
         * ===================启动类加载器===================
         * file:/D:/Java/jdk1.8.0_191/jre/lib/resources.jar
         * file:/D:/Java/jdk1.8.0_191/jre/lib/rt.jar
         * file:/D:/Java/jdk1.8.0_191/jre/lib/sunrsasign.jar
         * file:/D:/Java/jdk1.8.0_191/jre/lib/jsse.jar
         * file:/D:/Java/jdk1.8.0_191/jre/lib/jce.jar
         * file:/D:/Java/jdk1.8.0_191/jre/lib/charsets.jar
         * file:/D:/Java/jdk1.8.0_191/jre/lib/jfr.jar
         * file:/D:/Java/jdk1.8.0_191/jre/classes
         * ===================扩展类加载器===================
         * D:\Java\jdk1.8.0_191\jre\lib\ext
         * C:\WINDOWS\Sun\Java\lib\ext
         */
    }
}
