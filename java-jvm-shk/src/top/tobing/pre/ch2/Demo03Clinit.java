package top.tobing.pre.ch2;

/**
 * @Author tobing
 * @Date 2020/11/22 20:26
 * @Description Clinit
 */
public class Demo03Clinit {
    private int a = 1;
    private static int b = 2;


    public static void main(String[] args) {
        int c = 3;
    }

    public Demo03Clinit() {
        a = 10;
        int d = 20;
    }
    /**
     * 0 iconst_2
     * 1 putstatic #3 <top/tobing/ch2/Demo03Clinit.b>
     * 4 return
     */
}
