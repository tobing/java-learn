package top.tobing.pre.ch8;

/**
 * @Author tobing
 * @Date 2020/12/15 11:34
 * @Description
 */
public class Demo03SpaceInit {
    public static void main(String[] args) {
        long initialMemory = Runtime.getRuntime().totalMemory() / 1024 / 1024;
        long maxMemory = Runtime.getRuntime().maxMemory() / 1024 / 1024;
        System.out.println("-Xms :" + initialMemory);
        System.out.println("-Xmx :" + maxMemory);

//        System.out.println("系统内存大小为：" + initialMemory * 64.0 / 1024);
//        System.out.println("系统内存大小为：" + maxMemory * 4.0 / 1024);
//      配置Xms Xmx参数前
//        -Xms :243
//        -Xmx :3605
//        系统内存大小为：15.1875
//        系统内存大小为：14.08203125

//        配置Xms、Xmx参数后
//        -Xms :19
//        -Xmx :19
    }
}
