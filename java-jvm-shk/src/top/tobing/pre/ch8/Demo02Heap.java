package top.tobing.pre.ch8;

/**
 * @Author tobing
 * @Date 2020/12/15 11:26
 * @Description -Xms20m -Xmx20m
 */
public class Demo02Heap {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Start...");
        Thread.sleep(Integer.MAX_VALUE);
        System.out.println("End.....");
    }
}
