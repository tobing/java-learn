package top.tobing.pre.ch8;

/**
 * @Author tobing
 * @Date 2020/12/17 22:03
 * @Description 栈上分配
 * -Xms1G -Xmx1G -XX:+PrintGCDetails -XX:-DoEscapeAnalysis
 */
public class Demo06StackAllocation {
    public static void main(String[] args) throws InterruptedException {
        long startTime = System.currentTimeMillis();
        // 调用方法创建一千万个对象
        for (int i = 0; i < 10000000; i++) {
            allocate();
        }
        long endTime = System.currentTimeMillis();
        System.out.println("花费总时间：" + (endTime - startTime) + "ms");
        // 相当于阻塞main线程
        Thread.sleep(Integer.MAX_VALUE);
    }

    // 注意：此方法中user对象没有发生逃逸，因此很有可能会栈上分配
    public static void allocate() {
        User user = new User();
    }
}

class User {

}
