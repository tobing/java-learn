package top.tobing.pre.ch8;

/**
 * @Author tobing
 * @Date 2020/12/17 21:41
 * @Description 逃逸分析
 * 可以通过判断new的对象实体有没有可能在方法外被使用来判断。
 */
public class Demo05EscapeAnalysis {

    public Demo05EscapeAnalysis escapeAnalysis;

    /**
     * 将变量作为返回值返回，对象发生了方法逃逸
     *
     * @return R
     */
    public Demo05EscapeAnalysis getInstance() {
        return escapeAnalysis == null ? new Demo05EscapeAnalysis() : escapeAnalysis;
    }

    /**
     * 变量是成员变量，并非局部变量，发生了逃逸
     */
    public void setObject() {
        this.escapeAnalysis = new Demo05EscapeAnalysis();
    }

    /**
     * 变量在方法中被分配而且只在方法内使用，没有发生逃逸，可以栈上分配
     */
    public void useEscapeAnalysis() {
        Demo05EscapeAnalysis obj = new Demo05EscapeAnalysis();
    }

    /**
     * 方法中生成的对象可能被外部方法使用，发生了逃逸
     */
    public void demo() {
        Demo05EscapeAnalysis obj = getInstance();
    }

}
