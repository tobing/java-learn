package top.tobing.pre.ch8;

/**
 * @Author tobing
 * @Date 2020/12/15 10:56
 * @Description -Xms10 -Xmx10
 */
public class Demo01Heap {
    public static void main(String[] args) {
        System.out.println("start.....");
        try {
            Thread.sleep(Integer.MAX_VALUE);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("end....");
    }
}
