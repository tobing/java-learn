package top.tobing.pre.ch8;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author tobing
 * @Date 2020/12/15 11:40
 * @Description -Xms600m -Xmx600m
 */
public class Demo04OOM {
    public static void main(String[] args) throws InterruptedException {
        List<Picture> list = new ArrayList<>();
        while (true) {
            Thread.sleep(20);
            // java.lang.OutOfMemoryError: Java heap space
            list.add(new Picture(new byte[1024 * 512]));
        }
    }
}

class Picture {
    byte[] pixels;

    public Picture(byte[] pixels) {
        this.pixels = pixels;
    }
}
