package top.tobing.pre.ch8;

/**
 * @Author tobing
 * @Date 2020/12/18 10:44
 * @Description 标量替换
 * -Xms100m -Xmx100m -XX:+PrintGC -XX:+DoEscapeAnalysis -XX:-EliminateAllocations
 */
public class Demo07ScalarReplace {
    public static class User {
        public int id;
        public String name;
    }
    public static void alloc() {
        User user = new User();
        user.id = 1000;
        user.name = "tobing";
    }
    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {       
            alloc();
        }
        long end = System.currentTimeMillis();
        System.out.println("花费总时间为： " + (end - start) + "ms");
    }
}
