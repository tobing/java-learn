package top.tobing.pre.ch13;

/**
 * @Author tobing
 * @Date 2020/12/24 16:40
 * @Description Intern相关问题
 */
public class Demo09StringInternProblemPlus {
    public static void main(String[] args) {
        String s3 = new String("1") + new String("1");
        String s4 = "11";
        s3.intern();
        System.out.println(s3 == s4);
    }
}
