package top.tobing.pre.ch13;

import java.util.HashSet;
import java.util.Set;

/**
 * @Author tobing
 * @Date 2020/12/24 12:21
 * @Description 常量池
 * -XX:MetaspaceSize=6m -XX:MaxMetaspaceSize=6m -Xms6m -Xmx6m
 */
public class Demo03StringPool {
    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        int i = 0;
        while (true) {
            set.add(String.valueOf(i++).intern());
        }
    }
}
