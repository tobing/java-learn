package top.tobing.pre.ch13;

/**
 * @Author tobing
 * @Date 2020/12/24 14:23
 * @Description 字符串的拼接
 */
public class Demo05StringConcat {

    public static void main(String[] args) {
        Demo05StringConcat concat = new Demo05StringConcat();
        concat.test04();
    }

    public void test01() {
        String s1 = "a" + "b" + "c"; // 编译期优化，编译期就将前转换为：String s1 = "abc";
        String s2 = "abc";

        System.out.println(s1 == s2);// true
        System.out.println(s1.equals(s2));
    }

    public void test02() {
        String s1 = "JavaEE";
        String s2 = "Hadoop";

        String s3 = "JavaEEHadoop";
        String s4 = "JavaEE" + "Hadoop";
        String s5 = s1 + "Hadoop";
        String s6 = "JavaEE" + s2;
        String s7 = s1 + s2;
        String s8 = s7.intern();

        System.out.println(s3 == s4);// true
        System.out.println(s3 == s5);// false
        System.out.println(s3 == s6);// false
        System.out.println(s3 == s7);// false
        System.out.println(s5 == s6);// false
        System.out.println(s5 == s7);// false
        System.out.println(s6 == s7);// false
        System.out.println(s3 == s8);// true

        // 如果拼接前后出现变量，相当于在堆空间中new String()，具体内容为拼接的结果
        // intern：判断字符串常量池中是否存在该变量，如果存在，返回所在常量池中的地址
        // 如果不存在，则在常量池中创建，并返回地址。
    }

    public void test03() {
        String s1 = "a";
        String s2 = "b";
        String s3 = "ab";
        String s4 = s1 + s2;
        System.out.println(s3 == s4); // false
        // 通过字节码我们可以发现本质是
        // new StringBuilder().append(s1).append(s2).toString();
    }

    /**
     * 字符串的拼接不一定是使用StringBuilder
     * 如果拼接左右两边都是字符串常量（"abc"）或者常量引用（final String），仍然采用编译器优化。
     * 因此针对于final修饰的类、方法、基本数据类型、引用数据类型的量的结构时，能使用上final尽量使用final。
     */
    public void test04() {
        final String s1 = "a";
        final String s2 = "b";
        String s3 = "ab";
        String s4 = s1 + s2;
        System.out.println(s3 == s4);// true
    }

}
