package top.tobing.pre.ch13;

/**
 * @Author tobing
 * @Date 2020/12/24 17:36
 * @Description
 */
public class Demo10StringInternExam {
    public static void main(String[] args) {
        String x = "ab";        // 将“ab”放入字符串常量池中，并返回执行该的引用
        String s = new String("a") + new String("b");   // 堆中创建，相当于，new String("ab")
        String s2 = s.intern();         // 执行字符串常量池
        System.out.println(s2 == "ab"); // true
        System.out.println(s == "ab");  // false
    }
}
