package top.tobing.pre.ch13;

/**
 * @Author tobing
 * @Date 2020/12/24 16:05
 * @Description 不同字符串拼接方式的对比
 */
public class Demo07StringCreateCompare {
    public static void main(String[] args) {
        Demo07StringCreateCompare compare = new Demo07StringCreateCompare();
        long start = System.currentTimeMillis();
        //compare.concatByAdd(100000);        // 4022ms
        compare.concatByStringBuilder(100000); // 5 ms
        long end = System.currentTimeMillis();
        System.out.println("一共花费： " + (end - start) + " ms.");
    }

    // 加号+拼接
    public void concatByAdd(int times) {
        String src = "";
        for (int i = 0; i < times; i++) {
            src = src + "a";
        }
    }

    // StringBuilder拼接
    public void concatByStringBuilder(int times) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < times; i++) {
            sb.append("a");
        }
    }
}
