package top.tobing.pre.ch13;


/**
 * @Author tobing
 * @Date 2020/12/23 22:57
 * @Description 字符串不可变
 */
public class Demo02StringImmutablePlus {

    String str = new String("good");
    char[] ch = {'t', 'e', 's', 't'};

    public void change(String str, char[] ch) {
        str = "test change";
        str = new String("gogogog0");
//        System.out.println(str);
        ch[0] = 'u';
    }

    public static void main(String[] args) {
        Demo02StringImmutablePlus plus = new Demo02StringImmutablePlus();
        plus.change(plus.str, plus.ch);
        System.out.println(plus.str);
        System.out.println(plus.ch);
    }
}
