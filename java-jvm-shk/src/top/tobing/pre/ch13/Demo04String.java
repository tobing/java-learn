package top.tobing.pre.ch13;

/**
 * @Author tobing
 * @Date 2020/12/24 12:27
 * @Description
 */
public class Demo04String {
    public static void main(String[] args) {

        System.out.println();
        System.out.println("1");// 2125
        System.out.println("2");// 2126
        System.out.println("3");
        System.out.println("4");
        System.out.println("5");
        System.out.println("6");
        System.out.println("7");
        System.out.println("8");
        System.out.println("9");
        System.out.println("10");

        System.out.println("1");//2135
        System.out.println("2");//2135
        System.out.println("3");
        System.out.println("4");
        System.out.println("5");
        System.out.println("6");
        System.out.println("7");
        System.out.println("8");
        System.out.println("9");
    }
}
