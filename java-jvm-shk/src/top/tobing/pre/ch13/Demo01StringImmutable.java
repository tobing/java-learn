package top.tobing.pre.ch13;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @Author tobing
 * @Date 2020/12/24 12:11
 * @Description 测试StringTableSize对性能影响
 * -XX:StringTableSize=1009
 */
public class Demo01StringImmutable {
    public static void main(String[] args) {
        //测试StringTableSize参数
//        System.out.println("我来打个酱油");
//        try {
//            Thread.sleep(1000000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader("words.txt"));
            long start = System.currentTimeMillis();
            String data;
            while ((data = br.readLine()) != null) {
                data.intern(); //如果字符串常量池中没有对应data的字符串的话，则在常量池中生成
            }
            long end = System.currentTimeMillis();
            System.out.println("花费的时间为：" + (end - start));//1009:206ms  默认:
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}

