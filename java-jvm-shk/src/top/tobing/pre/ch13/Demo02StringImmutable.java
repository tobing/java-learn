package top.tobing.pre.ch13;

/**
 * @Author tobing
 * @Date 2020/12/23 22:51
 * @Description 字符串的不可变性
 */
public class Demo02StringImmutable {
    public static void main(String[] args) {
        Demo02StringImmutable demo02 = new Demo02StringImmutable();
        // demo02.test01();
        // demo02.test02();
        demo02.test03();


    }

    // 测试重新赋值
    public void test01() {
        String s1 = "abc";
        String s2 = "abc";
        // s1 = "hello";
        System.out.println(s1 == s2);
        System.out.println(s1);
        System.out.println(s2);
    }

    // 测试拼接
    public void test02() {
        String s1 = "abc";
        String s2 = "abc";
        s2 += "tobing";
        System.out.println(s1 == s2);
        System.out.println(s1);
        System.out.println(s2);
    }

    // 测试replace
    public void test03() {
        String s1 = "abc";
        String s2 = s1.replace('o', 'u');
        String s3 = s1.replace('a', 'u');
        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println(s1 == s2);
        System.out.println(s1 == s3);

    }
}
