package top.tobing.pre.ch13;

/**
 * @Author tobing
 * @Date 2020/12/24 17:45
 * @Description
 */
public class Demo10StringInternExam2 {
    public static void main(String[] args) {
        // String s1 = new String("ab");    // 会在字符串常量池中创建“ab”，但是返回堆中的String对象
        String s1 = new String("a") + new String("b");  // 不会在字符串常量池中创建“ab”，返回堆中的String对象
        s1.intern();
        String s2 = "ab";
        System.out.println(s1 == s2);
    }
}
