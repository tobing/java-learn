package top.tobing.pre.ch15;

/**
 * @Author tobing
 * @Date 2020/12/25 18:04
 * @Description finalize方法实现方法复活
 */
public class Demo01FinalizeRelive {
    // 类变量，属于GC Roots
    private static Object object;

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("重写finalize方法");
        // 重新执行GC Root，完成自救
        object = this;
    }

    public static void main(String[] args) {
        try {
            // 第一次GC
            object = new Demo01FinalizeRelive();
            object = null;
            System.gc();
            System.out.println("第一次 GC");
            Thread.sleep(2000);
            // 判断是否存活
            if (object == null) {
                System.out.println("object 已经销毁。");
            } else {
                System.out.println("object 仍然存活。");
            }
            // 第二次GC
            object = null;
            System.gc();
            Thread.sleep(2000);
            // 判断是否自救成功
            if (object == null) {
                System.out.println("object 已经销毁。");
            } else {
                System.out.println("object 仍然存活。");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
