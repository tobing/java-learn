package top.tobing.pre.ch15;

import java.util.ArrayList;

/**
 * @Author tobing
 * @Date 2020/12/25 23:10
 * @Description dump文件分析
 * <p>
 * -Xms8m -Xmx8m -XX:+HeapDumpOnOutOfMemoryError
 */
public class Demo02HeapDump {

    // 1M字节数组
    byte[] buffer = new byte[1024 * 1024 * 1];

    public static void main(String[] args) {
        ArrayList<Demo02HeapDump> list = new ArrayList<>();
        int count = 0;
        try {
            while (true) {
                list.add(new Demo02HeapDump());
            }
        } catch (Throwable e) {
            System.out.println("count = " + count);
            e.printStackTrace();
        }
    }
}
