package top.tobing.pre.ch4;

/**
 * @Author tobing
 * @Date 2020/12/12 21:48
 * @Description 绑定机制测试
 */
public class Demo06BindingMechan {
    public static void main(String[] args) {

    }
    //   invokeinterface 晚期绑定
    public void showAnimal(Animal animal) {
        animal.eat();
    }
    //  invokeinterface 晚期绑定
    public void showHuntable(Huntable huntable) {
        huntable.hunt();
    }
}


class Animal {
    public void eat() {
        System.out.println("动物进食");
    }
}

interface Huntable {
    void hunt();
}

class Dog extends Animal implements Huntable {

    // invokevirtual
    @Override
    public void hunt() {
        System.out.println("狗捕耗子，多管闲事");
    }

    // invokevirtual
    @Override
    public void eat() {
        super.eat();
        System.out.println("狗啃骨头");
    }
}

class Cat extends Animal implements Huntable {
    // invokespecial
    public Cat() {
        super();
    }
    // invokespecial
    public Cat(String username) {
        this();
    }

    @Override
    public void hunt() {
        super.eat();
        System.out.println("帽不耗子，天经地义");
    }

    @Override
    public void eat() {
        System.out.println("猫吃鱼");
    }
}
