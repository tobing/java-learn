package top.tobing.pre.ch4;

/**
 * @Author tobing
 * @Date 2020/11/28 16:58
 * @Description 验证栈的大小命令可行性
 */
public class Demo02StackError2 {
    private static int count = 0;

    public static void main(String[] args) {
        count++;
        System.out.println(count);
        main(args);
    }


    /**
     * 默认参数下-Xss1024k：count=11412
     * 修改参数-Xss256k：count=2459
     */
}
