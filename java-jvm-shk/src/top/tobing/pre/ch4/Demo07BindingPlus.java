package top.tobing.pre.ch4;

/**
 * @Author tobing
 * @Date 2020/12/12 22:05
 * @Description 虚方法和非虚方法
 * invokestatic、invokespecial指令调用的都是非虚方法
 */
public class Demo07BindingPlus {
}

class Father {
    public Father() {
        System.out.println("Father的构造器");
    }

    public static void showStatic(String str) {
        System.out.println("Father " + str);
    }

    public final void showFinal() {
        System.out.println("Father final 方法");
    }

    public void showCommon() {
        System.out.println("Father 普通方法");
    }
}

class Son extends Father {
    public Son() {
        // invokespecial 非虚方法
        super();
    }

    public Son(int age) {
        // invokespecial 非虚方法
        this();
    }

    // 此处不是重写父类的方法，因为静态方法不能被重写
    public static void showStatic(String str) {
        System.out.println("Son " + str);
    }

    private void showPrivate(String str) {
        System.out.println("Son private" + str);
    }

    public void info() {

    }

    public void display(Father f) {
        // invokevirtual 虚方法
        f.showCommon();
    }

    public void show() {
        // invokestatic 非虚方法
        showStatic("hello");
        // invokestatic 非虚方法
        super.showStatic("World");
        // invokespecial 虚方法
        showPrivate("hello!");
        // invokespecial 虚方法
        super.showCommon();
        // invokevirtual 【非虚方法】
        // 尽管这里是invokevirtual ，但是由于final不能被子类重写，因此是非虚方法
        showFinal();
        // invokevirtual 虚方法
        showCommon();

        GO go = null;
        // invokeinterface 虚方法
        go.methodA();
    }

    public static void main(String[] args) {
        Son son = new Son();
        son.show();
    }
}

interface GO {
    void methodA();
}


