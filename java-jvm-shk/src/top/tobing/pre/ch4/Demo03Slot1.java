package top.tobing.pre.ch4;

/**
 * @Author tobing
 * @Date 2020/11/28 17:02
 * @Description 局部变量表-变量槽slot
 */
public class Demo03Slot1 {
    public static void main(String[] args) {

    }

    public static void testStatic() {
        // slot index 0 : 非实例方法，不会持有this
        int a = 10;
        {
            // slot index 2
            int b = 20;
            a = b + 10;
        }
        // slot index 2 ： slot复用
        // 此时b已经处理作用域，无用，这是c可用复用b的位置，达到节省空间
        int c = 30;
    }

    public void testNonStatic() {
        // 实例方法会留有this引用
        // slot index0

        // slot index：1
        String name = "tobing";

        // slot index: 2
        int a = 10;

        // slot index: 3
        char c = 30;

        // slot index: 4 long会占用2个slot
        long d = 1000L;

        // slot index: 6  因为long占用2个slot
        double f = 10.0;
    }


}
