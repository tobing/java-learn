package top.tobing.pre.ch4;

/**
 * @Author tobing
 * @Date 2020/11/28 17:55
 * @Description 操作数栈演示
 */
public class Demo04OperandStack {

    public static void main(String[] args) {

    }

    public void testAdd() {
        int a = 10;
        int b = 20;
        int c = a + b;
    }

    /**
     * testAdd
     * 0 bipush 10   将10入栈
     * 2 istore_1    将10出栈并放到局部变量表1的位置
     * 3 bipush 20   将20入栈
     * 5 istore_2    将20出栈并放到局部变量表2的位置
     * 6 iload_1     读取局部变量表1的变量 入栈
     * 7 iload_2     读取局部变量表2的变量 入栈
     * 8 iadd        执行相加
     * 9 istore_3    将结果放到局部变量表3的位置
     * 10 return
     */


    public int testAddReturn() {
        int a = 1000;
        int c = 10;
        int d = a + c;
        return d;
    }

    /**
     * testAddReturn
     * 0 sipush 1000 将1000入栈
     * 3 istore_1    将栈顶元素出栈放到局部变量表1中
     * 4 bipush 10   将10入栈
     * 6 istore_2    将栈顶元素出栈放到局部变量表2中
     * 7 iload_1     读取局部变量表1变量将其入栈
     * 8 iload_2     读取局部变量表2变量将其入栈
     * 9 iadd        将栈中元素执行相加操作
     * 10 istore_3    将结果放到局部变量表3中
     * 11 iload_3     读取局部变量表3
     * 12 ireturn     返回
     */

    public void testReturn() {
        int res = testAddReturn();
        int f = 10;
    }
    /**testReturn
     * 0 aload_0     读取this变量，下面是执行方法
     * 1 invokevirtual #2 <top/tobing/ch4/Demo04OperandStack.testAddReturn>
     * 4 istore_1    将方法返回值放到局部变量表1
     * 5 bipush 10   操作数栈入10：f
     * 7 istore_2    放到局部变量表2位置
     * 8 return
     */


}
