package top.tobing.pre.ch4;

/**
 * @Author tobing
 * @Date 2020/12/10 22:26
 * @Description
 */
public class Demo05DynamicLinking {

    public static void main(String[] args) {

    }

    int age = 10;

    public void testA() {
        System.out.println(10);
        testB();
    }

    public void testB() {
        System.out.println(age);
    }

}
