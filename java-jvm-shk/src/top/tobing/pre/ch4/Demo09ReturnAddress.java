package top.tobing.pre.ch4;

import java.util.Date;

/**
 * @Author tobing
 * @Date 2020/12/13 9:23
 * @Description 方法返回地址
 * + 一个方法在正常调用完成之3后究竟需要使用哪一个返回地址指令，还需要通过方法返回值的实际数据类型而定。
 * + 在字节码指令中，返回指令包含ireturn、lreturn、freturn、dreturn以及areturn；另外void方法、实例初始化方法、类和接口的初始化方法使用的return指令
 */
public class Demo09ReturnAddress {
    public char testChar() {
        return 'a'; // ireturn
    }

    public boolean testBoolean() {
        return false;// ireturn
    }

    public int testInt() {
        return 1;   // ireturn
    }

    public short testShort() {
        return 1;   // ireturn
    }

    public long testLong() {
        return 1L;  // lreturn
    }

    public double testDouble() {
        return 1.1; // dreturn
    }

    public float testFloat() {
        return 1.1F;// freturn
    }

    public String testString() {
        return new String(); // areturn
    }

    public Date testDate() {
        return new Date();  // areturn
    }

    static {
        // return
    }
}

class DemoException {
    public void testExec() {
        try {
            exec();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    public void exec() throws RuntimeException {
        System.out.println("即将抛出异常");
        throw new RuntimeException("抛出测试异常");   // athorw 无return
    }
}
