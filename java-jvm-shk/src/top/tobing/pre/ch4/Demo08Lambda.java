package top.tobing.pre.ch4;

/**
 * @Author tobing
 * @Date 2020/12/13 8:46
 * @Description
 */
public class Demo08Lambda {
    public void lambda(Fun fun) {
        return;
    }
    public static void main(String[] args) {
        Demo08Lambda lambda = new Demo08Lambda();
        // invokedynamic
        lambda.lambda((str) -> {
            if (str.equals("tobing")) {
                return true;
            } else {
                return false;
            }
        });
    }
}
@FunctionalInterface
interface Fun {
    public boolean fun(String str);
}
