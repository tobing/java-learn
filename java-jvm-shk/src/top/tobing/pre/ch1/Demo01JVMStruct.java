package top.tobing.pre.ch1;

/**
 * @Author tobing
 * @Date 2020/11/20 20:06
 * @Description JVM的指令采用栈来设计
 */
public class Demo01JVMStruct {


    public static void main(String[] args) {
        int i = 1;
        int j = 2;
        int res = i + j;
    }

    /**
     * Compiled from "Demo01JVMStruct.java"
     * public class top.tobing.pre.ch1.Demo01JVMStruct {
     *   public top.tobing.pre.ch1.Demo01JVMStruct();
     *     Code:
     *        0: aload_024
     *        1: invokespecial #1                  // Method java/lang/Object."<init>":()V
     *        4: return
     *
     *   public static void main(java.lang.String[]);
     *     Code:
     *        0: iconst_1
     *        1: istore_1
     *        2: iconst_2
     *        3: istore_2
     *        4: iload_1
     *        5: iload_2
     *        6: iadd
     *        7: istore_3
     *        8: return
     * }
     */

    /**
     * 由于跨平台的设计、Java指令根据栈来设计
     * 不同平台CPU架构不同、所以不能设计基于寄存器。
     * 栈模式优点是：
     * 跨平台性、指令集小、指令多；执行性能比寄存器差
     */

}
