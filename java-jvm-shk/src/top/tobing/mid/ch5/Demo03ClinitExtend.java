package top.tobing.mid.ch5;

/**
 * @Author tobing
 * @Date 2021/1/7 22:37
 * @Description
 */
public class Demo03ClinitExtend extends Father {
    static {
        number = 30;
        System.out.println("Son:");
        System.out.println("Son:" + number);
    }

    public static void main(String[] args) {

    }
}

class Father {
    public static int number = 0;

    static {
        System.out.println("Father");
        System.out.println("Father:" + number);
    }
}
