package top.tobing.mid.ch5;

/**
 * @Author tobing
 * @Date 2021/1/8 15:31
 * @Description 被动使用情况（不会执行clinit）
 * 1. 当访问一个静态字段时，只有真正声明这字段的类才会被初始化。因此通过子类引用父类的静态i被拿来不会导致子类初始化。
 * 2. 通过数组定义类引用，不会触发此类的初始化。
 * 3. 引用常量不会触发类或接口的初始化，因为常量在链接的过程已经被显式赋值。
 * 4. 调用ClassLoader雷丹loadClass()方法加载一个类，不是对于类主动使用，不会导致类的初始化。
 */
public class Demo09PassiveUse01 {

    public static void main(String[] args) {
        testStaticFinal();
    }

    // 方式1：继承
    // 子类调用父类静态的变量，父类会被初始化，子类不会
    // 子类不初始化不意味着不被加载
    public static void testExtend() {
        Child.number = 20;
    }

    // 方式2：数组类型
    public static void testArr() {
        Person[] people = new Person[10];
        System.out.println(people.getClass());
        System.out.println(people.getClass().getSuperclass());
    }

    // 方式3：ClassLoad
    public static void testClassLoad() {
        try {
            ClassLoader.getSystemClassLoader().loadClass("top.tobing.mid.ch5.Parent");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void testStaticFinal() {
        int number = Person.number;
    }
}

class Parent {
    public static int number = 10;

    static {
        System.out.println("Parent");
    }
}

class Child extends Parent {
    static {
        System.out.println("Cild");
    }
}


class Person {
    public static final int number = 10;

    static {
        System.out.println("Person");
    }
}