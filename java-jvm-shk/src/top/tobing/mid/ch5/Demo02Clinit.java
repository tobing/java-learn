package top.tobing.mid.ch5;

/**
 * @Author tobing
 * @Date 2021/1/7 22:32
 * @Description
 */
public class Demo02Clinit {
    // 编译时处理
    public static final int HOUR = 10;

    // 类加载时赋值
    public static int id = 1000;
    // 此处直接赋值和在静态代码块中直接赋值在字节码中并无区别
    public static int name = 20;

    static {
        // name = 20;
        System.out.println("Static block exec!");
    }
}
