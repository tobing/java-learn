package top.tobing.mid.ch5;

/**
 * @Author tobing
 * @Date 2021/1/8 10:54
 * @Description static 与 final 修饰字段显示赋值问题，到底在哪个阶段
 * 情况1：在链接阶段的准备环节
 * 情况2：在初始化阶段的<clinit>()中赋值
 * 【结论】
 * 在链接阶段准备阶段的情况：
 * 1. 对于基本数据类型，使用显式赋值，直接赋值常量，而非调用方法；
 * 2. 对于String类型，使用字面量方式创建。
 *
 * 除了链接准备阶段，其余都是<clinit>()阶段
 */
public class Demo05NonClinintPlus {
//    public static int a1 = 1;           // 在<clinit>()期间赋值
//    public static final int a2 = 1;     // 在链接准备期间赋值

//    public static int a3 = Integer.valueOf(1);                // 在<clinit>()期间赋值
//    public static final int a4 = Integer.valueOf(2);          // 在<clinit>()期间赋值
//    public static final Integer a5 = Integer.valueOf(3);      // 在<clinit>()期间赋值

//    public static final String s1 = "String1";                // 在链接准备期间赋值
//    public static final String s2 = new String("String2");    // 在<clinit>()期间赋值
}
