package top.tobing.mid.ch5;

/**
 * @Author tobing
 * @Date 2021/1/8 15:00
 * @Description 类的主动使用
 * <p>
 * 4. 使用了java.lang.reflect包中的方法反射类的方法时，如`Class.forName("top.tobing.Main")`，称主动使用Main。
 * 5. 一个子类初始化是，发现父类还没进行过初始化，先触发父类初始化。
 * 6. 一个接口定义了default方法，直接或间接实现该类的类的初始化，该接口在之前被初始化。
 * 7. 虚拟机启动时，用户需要指定执行的主类，虚拟机会先初始化该类（带main那个类）。
 * 8. 当初次调用MethodHandle实例时，处死话MethodHandle指向的方法所在的类，涉及解析REF_getStatic/REF_putStatic/REF_invokeStatic方法句柄对应的类。
 */
public class Demo08ActiveUse02 {
    static {
        System.out.println("Demo08ActiveUser02");
    }
    public static void main(String[] args) {
        testInterfaceDefalut();
    }

    // 方式4：反射
    public static void testReflect() {
        try {
            Class.forName("top.tobing.mid.ch5.Animal");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    // 方式5：继承
    public static void testExtend() {
        new Dog();
    }

    // 方式6：default接口
    public static void testInterfaceDefalut() {
        // new Animal();
        new Dog();
    }
}

class Animal implements Show {
    static {
        System.out.println("Animal 静态代码块执行。");
    }
}

class Dog extends Animal {
    static {
        System.out.println("Dog 静态代码块执行。");
    }
}

interface Show {
    public static final Thread t = new Thread() {
        {
            System.out.println("Show初始化");
        }
    };


    public default void show() {
        System.out.println("show......");
    }
}