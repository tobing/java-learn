package top.tobing.mid.ch5;

import java.io.*;
import java.util.Random;

/**
 * @Author tobing
 * @Date 2021/1/8 12:17
 * @Description 主动使用的情况
 * 1. 创建一个类的实例，包括new、反射、克隆、反序列化。
 * 2. 调用类的静态方法，即执行了invokestatic指令。
 * 3. 使用了类、接口静态字段，如使用了getstatic、putstatic指令，final修饰的静态字段要特殊处理。
 * 4. 使用了java.lang.reflect包中的方法反射类的方法时，如`Class.forName("top.tobing.Main")`，称主动使用Main。
 * 5. 一个子类初始化是，发现父类还没进行过初始化，先触发父类初始化。
 * 6. 一个接口定义了default方法，直接或间接实现该类的类的初始化，该接口在之前被初始化。
 * 7. 虚拟机启动时，用户需要指定执行的主类，虚拟机会先初始化该类（带main那个类）。
 * 8. 当初次调用MethodHandle实例时，处死话MethodHandle指向的方法所在的类，涉及解析REF_getStatic/REF_putStatic/REF_invokeStatic方法句柄对应的类。
 */
public class Demo07ActiveUse01 {
    public static void main(String[] args) throws Exception {
        testStaticFinalOtherFiled();
    }

    // 方式1：new
    // 执行clinit
    public static void testNew() {
        Demo demo = new Demo();
    }

    // 序列化
    public static void helper() {
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(new FileOutputStream("temp.dat"));
            oos.writeObject(new Demo());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // 方式1：反序列化
    // 执行clinit
    public static void testSerializable() {
        // 方式1：反序列化
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(new FileInputStream("temp.dat"));
            Demo temp = (Demo) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // 方式2：执行类的静态方法
    // 执行clinit
    public static void testStaticMethod() {
        Demo.method();
    }

    // 方式3：执行类的静态字段
    // 执行clinit
    public static void testStaticField() {
        System.out.println(Demo.number);
    }

    // final static
    // 不执行clinit
    public static void testStaticFinalFiled() {
        System.out.println(Demo.number2);
    }

    // final static
    // 执行clinit
    public static void testStaticFinalOtherFiled() {
        System.out.println(Demo.number3);
    }
}

class Demo implements Serializable {

    public static int number;
    public static final int number2 = 10;
    public static final int number3 = new Random(10).nextInt();

    static {
        System.out.println("<clinit>() 被执行了！");
    }

    public static void method() {
        System.out.println("method 被执行了！");
    }
}
