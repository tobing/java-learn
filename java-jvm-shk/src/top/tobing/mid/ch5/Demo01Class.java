package top.tobing.mid.ch5;

import java.lang.reflect.Method;

/**
 * @Author tobing
 * @Date 2021/1/5 12:14
 * @Description
 */
public class Demo01Class {
    public static void main(String[] args) throws ClassNotFoundException {
        // 获取String类的Class对象
        // Class持有String.class在方法区中的引用
        // 因此Class可以获取String.class在方法区中的数据结构
        Class<?> clazz = Class.forName("java.lang.String");
        // 获取String类中的方法信息
        Method[] methods = clazz.getMethods();
        StringBuilder sb = new StringBuilder();
        for (Method method : methods) {
            // 返回值类型 方法名 (参数);
            sb.append(method.getReturnType().getSimpleName());
            sb.append(" ");
            sb.append(method.getName());
            sb.append(" ");
            sb.append("(");
            Class<?>[] parameterTypes = method.getParameterTypes();
            if (parameterTypes != null && parameterTypes.length > 0) {
                for (Class<?> parameterType : parameterTypes) {
                    sb.append(parameterType.getSimpleName());
                    sb.append(" ");
                }
            }
            sb.append(")");
            sb.append(";");
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }
}
