package top.tobing.mid.ch5;

/**
 * @Author tobing
 * @Date 2021/1/8 11:12
 * @Description
 */
public class Demo06StaticDeadLock {
    public static void main(String[] args) {
        new Thread(() -> {
            try {
                Class.forName("top.tobing.mid.ch5.ClassA");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }).start();

        new Thread(() -> {
            try {
                Class.forName("top.tobing.mid.ch5.ClassB");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }).start();
    }
}

class ClassA {
    static {
        try {
            Thread.sleep(1000);
            Class.forName("top.tobing.mid.ch5.ClassB");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("ClassA初始化成功！");
    }
}

class ClassB {
    static {
        try {
            Thread.sleep(1000);
            Class.forName("top.tobing.mid.ch5.ClassA");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("ClassB初始化成功！");
    }
}
