package top.tobing.mid.ch4;

import java.io.File;

/**
 * @Author tobing
 * @Date 2021/1/4 8:50
 * @Description 对象创建与访问指令
 */
public class Demo01ObjectCreate {

    public void testNewInstance() {
        Object obj = new Object();
        File file = new File("tobing.mp4");
        /**
         *  0 new #2 <java/lang/Object>                     // 在堆中创建Object对象，并把指向该对象的地址压入栈中
         *  3 dup                                           // 复制一份引用（对象地址），便于后面使用
         *  4 invokespecial #1 <java/lang/Object.<init>>    // 将栈顶引用（对象地址）出栈，通过引用地址访问init方法
         *  7 astore_1                                      // 将剩下引用（对象地址）出栈，保存到局部变量索引为1的位置
         *  8 new #3 <java/io/File>                         // 在堆中创建File对象，并把执行该对象的地址压入栈中
         * 11 dup                                           // 复制地址
         * 12 ldc #4 <tobing.mp4>                           // 将“tobing.mp4”入栈
         * 14 invokespecial #5 <java/io/File.<init>>        // 将之前复制的一个地址出栈，将字符串传入，执行init
         * 17 astore_2                                      // 将剩下的File对象地址保存的局部变量表中索引为2的位置
         * 18 return
         */
    }

    public void testNewArray() {
        Object obj = new Object();
        int[] intArray = new int[20];
        Object[] objArray = new Object[30];
        int[][] multiArray = new int[40][50];
        String[][] mutliStrArray = new String[60][];
        /**
         *  0 new #2 <java/lang/Object>                     // 创建Object，地址压栈
         *  3 dup                                           // 复制操作数栈的地址
         *  4 invokespecial #1 <java/lang/Object.<init>>    // 出栈执行init
         *  7 astore_1                                      // 地址出栈保存到局部变量表
         *  8 bipush 20                                     // 20 入栈
         * 10 newarray 10 (int)                             // 堆内存中创建int类数组，将地址入栈
         * 12 astore_2                                      // 将地址出栈并保存到局部变量表中
         * 13 bipush 30                                     // 30入栈，创建数组的时候出栈
         * 15 anewarray #2 <java/lang/Object>               // 堆内存中创建引用类型数组，并将地址入栈
         * 18 astore_3                                      // 将地址出栈，保存到局部变量表中
         * 19 bipush 40                                     // 40 入栈
         * 21 bipush 50                                     // 50 入栈
         * 23 multianewarray #6 <[[I> dim 2                 // 堆内存中创建二维int类型数组，分别是上面的[40][50]，地址入栈
         * 27 astore 4                                      // 地址出栈保存
         * 29 bipush 60                                     // 将60入栈
         * 31 anewarray #7 <[Ljava/lang/String;>            // 堆中创建引用类型数组，地址入栈
         * 34 astore 5                                      // 地址出栈保存到局部变量表
         * 36 return
         */
    }

    public void testPrintHello() {
        System.out.println("Hello");
        /**
         * 0 getstatic #8 <java/lang/System.out>            // 将静态属性：System.out 入栈
         * 3 ldc #9 <Hello>                                 // "Hello" 入栈
         * 5 invokevirtual #10 <java/io/PrintStream.println>// 调用println，“Hello”出栈输出
         * 8 return
         */
    }

    public void testSetOrderId() {
        Order order = new Order();
        order.id = 1001;
        System.out.println(order.id);

        Order.name = "tobing";
        System.out.println(Order.name);
        /**
         *  0 new #11 <top/tobing/mid/ch5/Order>            // 同上
         *  3 dup
         *  4 invokespecial #12 <top/tobing/mid/ch5/Order.<init>>
         *  7 astore_1
         *  8 aload_1
         *  9 sipush 1001                                   // 1001 入栈
         * 12 putfield #13 <top/tobing/mid/ch5/Order.id>    // 将1001出栈，并放到order.id指向的堆内存的地方
         * 15 getstatic #8 <java/lang/System.out>           // System.out静态属性入栈
         * 18 aload_1                                       // 将System.out保存
         * 19 getfield #13 <top/tobing/mid/ch5/Order.id>    // 将order.id入栈
         * 22 invokevirtual #14 <java/io/PrintStream.println>// 出栈执行println
         * 25 ldc #15 <tobing>                               // “tobing”入栈
         * 27 putstatic #16 <top/tobing/mid/ch5/Order.name>  // 将“tobing”出栈赋值给Order.name
         * 30 getstatic #8 <java/lang/System.out>            // System.out静态属性入栈
         * 33 getstatic #16 <top/tobing/mid/ch5/Order.name>  // Order.name静态属性入栈
         * 36 invokevirtual #10 <java/io/PrintStream.println>// println调用，Order.name出栈
         * 39 return
         */
    }

    public void testSetArray() {
        int[] intArray = new int[20];
        intArray[3] = 11;
        System.out.println(intArray[1]);

        boolean[] arr = new boolean[10];
        arr[1] = true;

        /**
         *  0 bipush 20             // 20 入栈
         *  2 newarray 10 (int)     // 创建int数组、长度20（20出栈），将数组地址压栈
         *  4 astore_1              // 数组地址出栈保存到局部变量表
         *  5 aload_1               // 局部变量表中数组地址入栈
         *  6 iconst_3              // 3 入栈
         *  7 bipush 11             // 11 入栈
         *  9 iastore               // 数组地址、索引地址、值出栈，实现 intArray[3] = 11
         * 10 getstatic #8 <java/lang/System.out>
         * 13 aload_1               // 局部变量表索引为1【intArray】入栈
         * 14 iconst_1              // 1 入栈
         * 15 iaload                // 【intArray】、1出栈 --> intArray[1]取值，压栈
         * 16 invokevirtual #14 <java/io/PrintStream.println>
         * 19 bipush 10
         * 21 newarray 4 (boolean)
         * 23 astore_2
         * 24 aload_2
         * 25 iconst_1
         * 26 iconst_1
         * 27 bastore               // 注意：使用的bastore
         * 28 return
         */
    }

    public void testArrayLength() {
        double[] arr = new double[10];
        System.out.println(arr.length);
    }

    public String testCheckCast(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        return null;
        /**
         *  0 aload_1                           // obj 入栈
         *  1 instanceof #17 <java/lang/String> // 判断obj是否为String
         *  4 ifeq 12 (+8)
         *  7 aload_1
         *  8 checkcast #17 <java/lang/String>  // 强制类型转换 String
         * 11 areturn
         * 12 aconst_null
         * 13 areturn
         */
    }
}

class Order {
    int id;
    static String name;
}
