package top.tobing.mid.ch4;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * @Author tobing
 * @Date 2021/1/4 18:33
 * @Description 异常跳转相关指令
 */
public class Demo08ExceptionJump {
    public void throwZero(int i) {
        if (i == 0) {
            throw new RuntimeException("值不能为0");
        }
        /** 假设i=0
         *  0 iload_1           // i=0 入栈
         *  1 ifne 14 (+13)     // i != 0 ? 不正确，直接
         *  4 new #2 <java/lang/RuntimeException>   // 堆空间创建RuntimeException并将地址压栈
         *  7 dup               // 复制引用
         *  8 ldc #3 <值不能为0> // “值不能为0”引用
         * 10 invokespecial #4 <java/lang/RuntimeException.<init>>  // 构造方法
         * 13 athrow            // 手动抛出
         * 14 return
         */
    }

    // 声明throws的时候，会在Exceptions显示
    public void throwOne(int i) throws RuntimeException {
        if (i == 1) {
            throw new RuntimeException("值不能为1");
        }
        /** 假设i=1
         *  0 iload_1               //i=1 压栈
         *  1 iconst_1              // 1 压栈
         *  2 if_icmpne 15 (+13)    // 1!=0 ? 正确，继续执行
         *  5 new #2 <java/lang/RuntimeException>   // 堆内存创建RE，地址压栈
         *  8 dup                   // 地址复制
         *  9 ldc #5 <值不能为1>     // “值不能为1”压栈
         * 11 invokespecial #4 <java/lang/RuntimeException.<init>>  // RE构造方法
         * 14 athrow                // 手动抛出
         * 15 return
         */
    }

    public void nonCatchException() {
        int i = 10;
        int j = i / 0;
        System.out.println(j);
        /** 下面字节码指令没有看到与异常相关的，虚拟机会自动抛出
         *  0 bipush 10
         *  2 istore_1
         *  3 iload_1
         *  4 iconst_0
         *  5 idiv
         *  6 istore_2
         *  7 getstatic #6 <java/lang/System.out>
         * 10 iload_2
         * 11 invokevirtual #7 <java/io/PrintStream.println>
         * 14 return
         */
    }

    public void tryCatch() {
        try {
            File file = new File("a.txt");
            FileInputStream fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        /**
         *  0 new #8 <java/io/File>     // 堆内存创建File，地址压栈
         *  3 dup                       // 地址复制
         *  4 ldc #9 <a.txt>            // "a.txt"压栈
         *  6 invokespecial #10 <java/io/File.<init>>   // File 出栈
         *  9 astore_1                  // File赋值给file
         * 10 new #11 <java/io/FileInputStream> // 堆内存创建FileInputStream，！！！！此时抛出了异常
         * 13 dup                               // 由于在异常表中记录了该异常，会跳转到30处理
         * 14 aload_1
         * 15 invokespecial #12 <java/io/FileInputStream.<init>>
         * 18 astore_2
         * 19 goto 35 (+16)
         * 22 astore_1
         * 23 aload_1
         * 24 invokevirtual #14 <java/io/FileNotFoundException.printStackTrace>
         * 27 goto 35 (+8)
         * 30 astore_1                  // 将异常信息保存到局部变量表下标为1的位置
         * 31 aload_1                   // 异常加载到操作数栈
         * 32 invokevirtual #15 <java/lang/RuntimeException.printStackTrace>    // 执行异常堆栈信息输出
         * 35 return
         */
    }

    public String testFinally() {
        String str = "tobing";
        try {
            return str;
        } finally {
            str = "gogog";
        }
        /**
         *  0 ldc #16 <tobing>  // “tobing”入栈            ///top{tobing}bottom
         *  2 astore_1          // 保存到str               ///top{}bottom
         *  3 aload_1           // str入栈                ///top{tobing}bottom
         *  4 astore_2          // 保存到局部变量表索引2的位置【拷贝】   ///top{}bottom
         *  5 ldc #17 <gogog>   // 没有异常，继续gogog入栈   ///top{gogog}bottom
         *  7 astore_1          // 保存           ///top{}bottom
         *  8 aload_2           // 替换           ///top{tobing}bottom
         *  9 areturn           // 返回栈顶       ///top{}bottom
         * 10 astore_3          // !!!!!!!出现问题!!!!!!!!
         * 11 ldc #17 <gogog>
         * 13 astore_1
         * 14 aload_3
         * 15 athrow
         */
    }
}
