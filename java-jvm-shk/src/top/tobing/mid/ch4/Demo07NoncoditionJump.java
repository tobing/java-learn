package top.tobing.mid.ch4;

/**
 * @Author tobing
 * @Date 2021/1/4 17:14
 * @Description
 */
public class Demo07NoncoditionJump {

    public void whileInt() {
        int i = 0;
        while (i < 100) {
            String s = "tobing";
            i++;
        }

        /**
         *  0 iconst_0          // 0 入栈
         *  1 istore_1          // 0 出栈 保存到局部变量表i
         *  2 iload_1           // i=0 入栈 v1                    // i=100 入栈 v1
         *  3 bipush 100        // 100 入栈 v2                    // 100 入栈   v2
         *  5 if_icmpge 17 (+12)    // v1 >= v2 ? 不大于，继续执行   // v1 >= v2 ？ 等于，跳转
         *  8 ldc #2 <tobing>       // "tobing" 入栈
         * 10 astore_2              // 出栈保存到局部变量表 s
         * 11 iinc 1 by 1           // i++
         * 14 goto 2 (-12)          // goto 2.....
         * 17 return                                               // 循环出来了
         */
    }

    public void whileDouble() {
        double d = 0.0;
        while (d < 100.1) {
            String s = "tobing";
            d++;
        }
        /**
         *  0 dconst_0
         *  1 dstore_1
         *  2 dload_1
         *  3 ldc2_w #3 <100.1>
         *  6 dcmpg
         *  7 ifge 20 (+13)
         * 10 ldc #2 <tobing>
         * 12 astore_3
         * 13 dload_1
         * 14 dconst_1
         * 15 dadd
         * 16 dstore_1
         * 17 goto 2 (-15)
         * 20 return
         */
    }

    public void doWhileTest() {
        int i = 1;
        do {
            i++;
        } while (i <= 100);
        /**
         *  0 iconst_1
         *  1 istore_1
         *  2 iinc 1 by 1       // 至少先执行一次
         *  5 iload_1
         *  6 bipush 100
         *  8 if_icmple 2 (-6)
         * 11 return
         */
    }

    public void whileTest() {
        int i = 1;
        while (i <= 50) {
            i++;
        }
        /**
         *  0 iconst_1
         *  1 istore_1
         *  2 iload_1
         *  3 bipush 50
         *  5 if_icmpgt 14 (+9)
         *  8 iinc 1 by 1
         * 11 goto 2 (-9)
         * 14 return
         */
    }

    // for和while的字节码指令一模一样
    public void forTest() {
        for (int i = 1; i <= 50; i++) {

        }
        /**
         *  0 iconst_1
         *  1 istore_1
         *  2 iload_1
         *  3 bipush 50
         *  5 if_icmpgt 14 (+9)
         *  8 iinc 1 by 1
         * 11 goto 2 (-9)
         * 14 return
         */
    }
}
