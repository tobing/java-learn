package top.tobing.mid.ch4;

/**
 * @Author tobing
 * @Date 2021/1/4 21:51
 * @Description
 */
public class Demo09Synchronized {

    private int i = 0;

    public synchronized void add() {
        i++;
        /** 字节码指令中并没有体现监视器指令：monitorexit或monitorenter
         *  0 aload_0
         *  1 dup
         *  2 getfield #2 <top/tobing/mid/ch5/Demo09Synchronized.i>
         *  5 iconst_1
         *  6 iadd
         *  7 putfield #2 <top/tobing/mid/ch5/Demo09Synchronized.i>
         * 10 return
         */
    }
    public void test() {
        synchronized (this) {
            i--;
        }
        /**
         *  0 aload_0               // this入栈               ###top{this}bottom
         *  1 dup                   // 复制this               ###top{this|this}bottom
         *  2 astore_1              // this出栈保存到局部变量表 ###top{this}bottom
         *  3 monitorenter          // 进入进入对象监视器       ###top{}bottom
         *  4 aload_0               // this入栈               ###top{this}bottom
         *  5 dup                   // 复制this压栈            ###top{this|this}bottom
         *  6 getfield #2 <top/tobing/mid/ch5/Demo09Synchronized.i> // this出栈，获取i
         *  9 iconst_1              // 1 入栈
         * 10 isub                  // i-- this出栈、1出栈
         * 11 putfield #2 <top/tobing/mid/ch5/Demo09Synchronized.i> // 设置i
         * 14 aload_1               // 加载索引为1 this
         * 15 monitorexit           // 释放对象监视器
         * 16 goto 24 (+8)          // 跳转到return语句
         * 19 astore_2              !!!!!!!!!!!!!以上过程如果发生异常将会在异常表中被捕获，执行以下流程保证会释放锁!!!!!!!!!!!!!
         * 20 aload_1
         * 21 monitorexit
         * 22 aload_2
         * 23 athrow
         * 24 return
         */
    }
}
