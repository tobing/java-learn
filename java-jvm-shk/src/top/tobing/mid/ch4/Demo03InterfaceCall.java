package top.tobing.mid.ch4;

/**
 * @Author tobing
 * @Date 2021/1/4 10:32
 * @Description
 */
public class Demo03InterfaceCall {
    public static void main(String[] args) {
        BB bb = new BB();
        bb.testDefault();
        AA.testStatic();
        /**
         *  0 new #2 <top/tobing/mid/ch5/BB>
         *  3 dup
         *  4 invokespecial #3 <top/tobing/mid/ch5/BB.<init>>       // <init>
         *  7 astore_1
         *  8 aload_1
         *  9 invokevirtual #4 <top/tobing/mid/ch5/BB.testDefault>  // interface -> default
         * 12 invokestatic #5 <top/tobing/mid/ch5/AA.testStatic>    // interface -> static
         * 15 return
         */
    }
}

interface AA {
    static void testStatic() {
    }

    default void testDefault() {
    }
}

class BB implements AA {

}
