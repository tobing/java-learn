package top.tobing.mid.ch4;

/**
 * @Author tobing
 * @Date 2021/1/4 12:42
 * @Description 操作数栈的操作指令
 */
public class Demo05StackOperate {

    public void print() {
        Object o = new Object();
        // System.out.println(o.toString());
        o.toString();
        /**
         *  0 new #3 <java/lang/Object>                     // 堆中创建Object并将地址压栈
         *  3 dup                                           // 将Object的地址复制在压栈
         *  4 invokespecial #1 <java/lang/Object.<init>>    // Object地址出栈，执行<init>方法
         *  7 astore_1                                      // 将剩下的Object保存到局部变量表【o】
         *  8 aload_1                                       // 从局部变量表加载【o】
         *  9 invokevirtual #4 <java/lang/Object.toString>  // 执行toString
         * 12 pop                                           // 由于返回值没有被使用，直接pop
         * 13 return
         */
    }

    public void foo() {
        bar();
        /**
         * 0 aload_0        // 加载this
         * 1 invokevirtual #5 <top/tobing/mid/ch5/Demo05StackOperate.bar>   // this出栈执行bar方法，结果压栈
         * 4 pop2           // 由于的long占用2个slot，而且没有被使用，因此pop2
         * 5 return
         */
    }

    public long bar() {
        return 0;
    }

    private long index = 0;

    public long nextIndex() {
        return index++;
        /**
         *  0 aload_0           // 加载this
         *  1 dup               // 复制this                               ///top{this|this}bottom
         *  2 getfield #2 <top/tobing/mid/ch5/Demo05StackOperate.index> // this出栈，调用字段index【long类型】，压栈 ///top{long    |this}bottom
         *  5 dup2_x1           // 将结果long，复制并移动到this以下的位置。  ///top{long    |this|long    }bottom
         *  6 lconst_1          // 1 入栈                                 ///top{1|long    |this|long    }bottom
         *  7 ladd              // 1 与index相加                          ///top{this|long    }bottom
         *  8 putfield #2 <top/tobing/mid/ch5/Demo05StackOperate.index>  ///top{}bottom
         * 11 lreturn
         */
    }
}
