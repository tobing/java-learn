package top.tobing.mid.ch4;

/**
 * @Author tobing
 * @Date 2021/1/4 15:44
 * @Description 条件跳转
 */
public class Demo06ConditionJump {
    public void compare01() {
        int a = 0;
        if (a != 0) {
            a = 10;
        } else {
            a = 20;
        }
        /**
         *  0 iconst_0          // 0 入栈
         *  1 istore_1          // 0 出栈 保存到局部变量表a
         *  2 iload_1           // a=0 入栈
         *  3 ifeq 12 (+9)      // 0==0 ？ 跳转到12
         *  6 bipush 10
         *  8 istore_1
         *  9 goto 15 (+6)
         * 12 bipush 20         // 20 入栈
         * 14 istore_1          // 20 出栈 保存到局部变量表a
         * 15 return
         */
    }

    public void compare02() {
        float f1 = 9;
        float f2 = 10;
        System.out.println(f1 < f2);
        /**
         *  0 ldc #2 <9.0>                  // 9.0 入栈
         *  2 fstore_1                      // 9.0 出栈保存到局部变量表 f1
         *  3 ldc #3 <10.0>                 // 10.0 入栈
         *  5 fstore_2                      // 10.0 出栈保存到局部变量表 f2
         *  6 getstatic #4 <java/lang/System.out>   // System.out入栈
         *  9 fload_1                       // f1=10.0 入栈 v1
         * 10 fload_2                       // f2=20.0 入栈 v2
         * 11 fcmpg                         // v1 < v2  ==> -1
         * 12 ifge 19 (+7)                  // -1 > 0 ? 不大于，直接继续执行
         * 15 iconst_1                      // 1 入栈
         * 16 goto 20 (+4)                  // 跳转到20
         * 19 iconst_0
         * 20 invokevirtual #5 <java/io/PrintStream.println>    // 输出1 true
         * 23 return
         */
    }

    public void compare03() {
        int i1 = 10;
        long l1 = 20;
        System.out.println(i1 < l1);
        /**
         *  0 bipush 10                 // 10 入栈
         *  2 istore_1                  // 10 出栈保存到局部变量表i1
         *  3 ldc2_w #6 <20>            // 20 入栈
         *  6 lstore_2                  // 20 出栈保存到局部变量表l2
         *  7 getstatic #4 <java/lang/System.out>   // System.out 入栈
         * 10 iload_1                   // i1=10 入栈
         * 11 i2l                       // 10 int->long v1
         * 12 lload_2                   // 20(long) 入栈 v2
         * 13 lcmp                      // v1 < v2  ==> -1
         * 14 ifge 21 (+7)              // -1 > 0 ? 不大于，继续执行
         * 17 iconst_1                  // 1 入栈
         * 18 goto 22 (+4)              // 跳转到22
         * 21 iconst_0
         * 22 invokevirtual #5 <java/io/PrintStream.println>    // 输出1 true
         * 25 return
         */
    }

    public int compare04(double d) {
        if (d > 50.0) {
            return 1;
        } else {
            return -1;
        }
        /** 假设 d = 40
         *  0 dload_1           // d=40 入栈 v1
         *  1 ldc2_w #8 <50.0>  // 50.0 入栈 v2
         *  4 dcmpl             // v1 < v2  ==> -1
         *  5 ifle 10 (+5)      // -1 < 0 ? 小于，跳转到10
         *  8 iconst_1
         *  9 ireturn
         * 10 iconst_m1         // -1 入栈
         * 11 ireturn
         */
    }

    public boolean compareNull(String str) {
        if (str == null) {
            return true;
        } else {
            return false;
        }
        /** 假设str=“tobing”
         * 0 aload_1            // str="tobing" 入栈
         * 1 ifnonnull 6 (+5)   // str != null ? 对，直接跳转到6
         * 4 iconst_1
         * 5 ireturn
         * 6 iconst_0           // 0 入栈
         * 7 ireturn            // 返回 false
         */
    }

    public void ifCompare1() {
        int i = 10;
        int j = 20;
        System.out.println(i < j);
        /**
         *  0 bipush 10
         *  2 istore_1
         *  3 bipush 20
         *  5 istore_2
         *  6 getstatic #4 <java/lang/System.out>
         *  9 iload_1       // 10 入栈 v1
         * 10 iload_2       // 20 入栈 v2
         * 11 if_icmpge 18 (+7)     // v1 > v2 ? 不对，继续执行
         * 14 iconst_1      // 1 r入栈
         * 15 goto 19 (+4)  // 跳转到19
         * 18 iconst_0
         * 19 invokevirtual #5 <java/io/PrintStream.println>    // 1 出栈输出 true
         * 22 return
         */
    }

    public void ifCompare2() {
        short s1 = 9;
        byte b1 = 10;
        System.out.println(s1 > b1);
        /**
         *  0 bipush 9
         *  2 istore_1
         *  3 bipush 10
         *  5 istore_2
         *  6 getstatic #4 <java/lang/System.out>
         *  9 iload_1   // s1 入栈 v1
         * 10 iload_2   // b1 入栈 v2
         * 11 if_icmple 18 (+7)     // v1 < v2 ？ 正确，跳转18
         * 14 iconst_1
         * 15 goto 19 (+4)
         * 18 iconst_0              // 0 入栈
         * 19 invokevirtual #5 <java/io/PrintStream.println>    // 输出false
         * 22 return
         */
    }

    public void ifCompare3() {
        Object obj1 = new Object();
        Object obj2 = new Object();
        System.out.println(obj1 == obj2);
        System.out.println(obj1 != obj2);
        /**
         *  0 new #10 <java/lang/Object>
         *  3 dup
         *  4 invokespecial #1 <java/lang/Object.<init>>
         *  7 astore_1
         *  8 new #10 <java/lang/Object>
         * 11 dup
         * 12 invokespecial #1 <java/lang/Object.<init>>
         * 15 astore_2
         * 16 getstatic #4 <java/lang/System.out>
         * 19 aload_1           // obj1 入栈  v1 0x111
         * 20 aload_2           // obj2 入栈  v2 0x222
         * 21 if_acmpne 28 (+7)     // v1 != v2 ？ 正确，跳转28
         * 24 iconst_1
         * 25 goto 29 (+4)
         * 28 iconst_0              // 0 入栈
         * 29 invokevirtual #5 <java/io/PrintStream.println>    // 输出false
         * 32 getstatic #4 <java/lang/System.out>
         * 35 aload_1
         * 36 aload_2
         * 37 if_acmpeq 44 (+7)
         * 40 iconst_1
         * 41 goto 45 (+4)
         * 44 iconst_0
         * 45 invokevirtual #5 <java/io/PrintStream.println>
         * 48 return
         */
    }

    public void switch01(int select) {
        int num;
        switch (select) {
            case 100:
                num = 10;
                break;
            case 500:
                num = 50;
                break;
            case 200:
                num = 20;
                break;
            default:
                num = 0;
                break;
        }
        /** 不连续，假设select= 500
         *  0 iload_1               // select=500 入栈
         *  1 lookupswitch 3        // 500 出栈比较 【值得注意的是，此处的顺序以及帮我们按顺序排好】
         * 	    100:  36 (+35)
         * 	    200:  48 (+47)
         * 	    500:  42 (+41)      // 来到这里 跳转到42
         * 	    default:  54 (+53)
         * 36 bipush 10
         * 38 istore_2
         * 39 goto 56 (+17)
         * 42 bipush 50             // 50 入栈
         * 44 istore_2              // 赋值
         * 45 goto 56 (+11)         // 跳转 56  【break的作用，如果没有break，则不会有goto这一行】
         * 48 bipush 20
         * 50 istore_2
         * 51 goto 56 (+5)
         * 54 iconst_0
         * 55 istore_2
         * 56 return                // 跳转到此处，返回
         */
    }

    public void switch02(int select) {
        int num;
        switch (select) {
            case 1:
                num = 10;
                break;
            case 2:
                num = 20;
                break;
            case 3:
                num = 30;
                break;
            default:
                num = 0;
                break;
        }
        /** 连续，假设select=3
         *  0 iload_1
         *  1 tableswitch 1 to 3	1:  28 (+27)    // select=3 出栈，因为连续跳转3
         * 	                        2:  34 (+33)
         * 	                        3:  40 (+39)    // 跳转40
         * 	                  default:  46 (+45)
         * 28 bipush 10
         * 30 istore_2
         * 31 goto 48 (+17)
         * 34 bipush 20
         * 36 istore_2
         * 37 goto 48 (+11)
         * 40 bipush 30         // 30 压栈
         * 42 istore_2          // 储存
         * 43 goto 48 (+5)      // 跳转48
         * 46 iconst_0
         * 47 istore_2
         * 48 return            // 返回
         */
    }

    // JDK1.7新特性：引入String
    public void switch03(String season) {
        switch (season) {
            case "SPRING":
                break;
            case "SUMMER":
                break;
            case "AUTUMN":
                break;
            case "WINTER":
                break;
            default:
                break;
        }
        /**
         *   0 aload_1
         *   1 astore_2
         *   2 iconst_m1
         *   3 istore_3
         *   4 aload_2
         *   5 invokevirtual #11 <java/lang/String.hashCode> // 将参数的字符串，获取其hash，并压栈
         *   8 lookupswitch 4                      // 对比，假设是1734407483
         * 	                -1842350579:  52 (+44)
         * 	                -1837878353:  66 (+58)
         * 	                -1734407483:  94 (+86)  // 跳转到94
         *          	     1941980694:  80 (+72)
         * 	default:  105 (+97)
         *  52 aload_2
         *  53 ldc #12 <SPRING>
         *  55 invokevirtual #13 <java/lang/String.equals>
         *  58 ifeq 105 (+47)
         *  61 iconst_0
         *  62 istore_3
         *  63 goto 105 (+42)
         *  66 aload_2
         *  67 ldc #14 <SUMMER>
         *  69 invokevirtual #13 <java/lang/String.equals>
         *  72 ifeq 105 (+33)
         *  75 iconst_1
         *  76 istore_3
         *  77 goto 105 (+28)
         *  80 aload_2
         *  81 ldc #15 <AUTUMN>
         *  83 invokevirtual #13 <java/lang/String.equals>
         *  86 ifeq 105 (+19)
         *  89 iconst_2
         *  90 istore_3
         *  91 goto 105 (+14)
         *  94 aload_2                  // 加载参数的值
         *  95 ldc #16 <WINTER>         // WINTER 入栈
         *  97 invokevirtual #13 <java/lang/String.equals>  // 再次比较
         * 100 ifeq 105 (+5)
         * 103 iconst_3
         * 104 istore_3
         * 105 iload_3
         * 106 tableswitch 0 to 3	0:  136 (+30)
         * 	1:  139 (+33)
         * 	2:  142 (+36)
         * 	3:  145 (+39)
         * 	default:  145 (+39)
         * 136 goto 145 (+9)
         * 139 goto 145 (+6)
         * 142 goto 145 (+3)
         * 145 return
         */
    }


}
