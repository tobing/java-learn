package top.tobing.mid.ch4;

/**
 * @Author tobing
 * @Date 2021/1/4 12:08
 * @Description
 */
public class Demo04MethodReturn {
    public int testIReturn() {
        int i = 10;
        return i;   // ireturn

    }

    public double testDReturn() {
        return 0.0; // dreturn
    }

    public String testString() {
        return "tobing";    // areturn
    }

    public int[] testArr() {
        return null;        // areturn
    }

    // 存在类型转换
    public float testFloat() {
        int i = 10;
        return i;           // i2f freturn
    }

    public byte testByte() {
        return 0;           // ireturn
    }

}
