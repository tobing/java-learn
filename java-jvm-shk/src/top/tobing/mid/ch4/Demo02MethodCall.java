package top.tobing.mid.ch4;

import java.util.Date;

/**
 * @Author tobing
 * @Date 2021/1/4 10:24
 * @Description 方法调用
 */
public class Demo02MethodCall {

    // invokespecial：静态分配
    // 以下的三种情况，方法不可能被重写
    public void testInvokeSpecial() {
        // 情况1：类的实例构造器方法：<init>()
        Date date = new Date();
        Thread thread = new Thread();
        // 情况2：父类的方法
        super.toString();
        // 情况3：私有方法
        testPrivate();
        /**
         *  0 new #2 <java/util/Date>
         *  3 dup
         *  4 invokespecial #3 <java/util/Date.<init>>      // init
         *  7 astore_1
         *  8 new #4 <java/lang/Thread>
         * 11 dup
         * 12 invokespecial #5 <java/lang/Thread.<init>>    // init
         * 15 astore_2
         * 16 aload_0
         * 17 invokespecial #6 <java/lang/Object.toString>  // 父类方法
         * 20 pop
         * 21 aload_0
         * 22 invokespecial #7 <top/tobing/mid/ch5/Demo02MethodCall.testPrivate>    // private
         * 25 return
         */
    }

    private void testPrivate() {
    }

    // invokestatic：静态分配
    public void testInvokeStatic() {
        testStatic();
        /**
         * 0 invokestatic #8 <top/tobing/mid/ch5/Demo02MethodCall.testStatic>       // static
         * 3 return
         */
    }

    public static void testStatic() {
    }

    // invokeinterface
    public void testInvokeInterface() {
        Runnable t = new Thread();
        t.run();

        Comparable<Integer> com = null;
        com.compareTo(123);
        /**
         *  0 new #4 <java/lang/Thread>
         *  3 dup
         *  4 invokespecial #5 <java/lang/Thread.<init>>
         *  7 astore_1
         *  8 aload_1
         *  9 invokeinterface #9 <java/lang/Runnable.run> count 1   // interface
         * 14 aconst_null
         * 15 astore_2
         * 16 aload_2
         * 17 bipush 123
         * 19 invokestatic #10 <java/lang/Integer.valueOf>
         * 22 invokeinterface #11 <java/lang/Comparable.compareTo> count 2  // interface
         * 27 pop
         * 28 return
         */
    }

    // invokevirtual
    public void testInvokeVirtual() {
        System.out.println("Hello");
        Thread t1 = null;
        t1.run();
        /**
         *  0 getstatic #12 <java/lang/System.out>
         *  3 ldc #13 <Hello>
         *  5 invokevirtual #14 <java/io/PrintStream.println>
         *  8 aconst_null
         *  9 astore_1
         * 10 aload_1
         * 11 invokevirtual #15 <java/lang/Thread.run>
         * 14 return
         */
    }

}
