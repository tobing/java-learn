package top.tobing.mid.ch0;

/**
 * @Author tobing
 * @Date 2020/12/31 16:11
 * @Description
 */
public class Demo03FartherAndSon {
    public static void main(String[] args) {
        // Father f = new Father();    // Father.x = 10
        // System.out.println(f.x);    // 20
        Father f = new Son();
        System.out.println(f.x);
    }
}

class Father {
    int x = 10;

    public Father() {
        this.print();
        x = 20;
    }

    public void print() {
        System.out.println("Father.x = " + x);
    }
}


class Son extends Father {
    int x = 30;

    public Son() {
        this.print();
        x = 40;
    }

    @Override
    public void print() {
        System.out.println("Son.x = " + x);
    }
}
