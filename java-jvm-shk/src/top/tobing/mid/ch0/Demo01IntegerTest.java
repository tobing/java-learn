package top.tobing.mid.ch0;

/**
 * @Author tobing
 * @Date 2020/12/31 16:02
 * @Description
 */
public class Demo01IntegerTest {
    public static void main(String[] args) {
//        Integer i1 = 11;
//        Integer i2 = 11;
//        System.out.println(i1 == i2);   // True
//
//        Integer i3 = 128;
//        Integer i4 = 128;
//        System.out.println(i3 == i4);   // False

        // Integer i3 = 128 ===> Integer i3 = Integer.valueOf(128);
        /**    （-128, 127)则直接返回缓存的内容，复制创建一个新的Integer对象返回。
         *     public static Integer valueOf(int i) {
         *         if (i >= IntegerCache.low && i <= IntegerCache.high)
         *             return IntegerCache.cache[i + (-IntegerCache.low)];
         *         return new Integer(i);
         *     }
         */

        Integer i5 = 10;
        int i6 = 10;        // ==> Integer i6 = Integer.valueOf(10)
        System.out.println(i5 == i6);
    }
}
