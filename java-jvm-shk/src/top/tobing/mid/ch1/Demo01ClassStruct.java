package top.tobing.mid.ch1;

/**
 * @Author tobing
 * @Date 2020/12/31 17:09
 * @Description
 */
public class Demo01ClassStruct {
    private int num = 1;
    public int add() {
        num = num + 1;
        return num;
    }
}
