package top.tobing.mid.ch2;

/**
 * @Author tobing
 * @Date 2021/1/2 19:58
 * @Description 算术运算指令
 */
public class Demo02Arithemetic {

    public static void main(String[] args) {
        Demo02Arithemetic demo02Arithemetic = new Demo02Arithemetic();
        demo02Arithemetic.testNaN();
    }

    public void testInf() {
        int i = 10;
        double j = i / 0.0;
        System.out.println(j);    // Infinity
    }

    public void testNaN() {
        double i = 0.0;
        double j = i / 0.0;
        System.out.println(j);   // NaN
    }

    public void testSub() {
        float i = 10;
        float j = -i;
        i = -j;
        /**
         * 0 ldc #7 <10.0>  // 从堆中加载【常量10.0】到操作数栈中
         * 2 fstore_1       // 将【常量10.0】出栈存储到局部变量表为1的位置【i】
         * 3 fload_1        // 加载局部变量表中中索引为1的值【i】到操作数栈中
         * 4 fneg           // 将【i=10】出栈进行fneg取反操作，并将结果压入栈中
         * 5 fstore_2       // 将结果-10出栈并存储到局部变量表索引为2的位置【j】
         * 6 fload_2        // 将【j】值压入栈
         * 7 fneg           // 将【j】值出栈并取反，存储会栈中
         * 8 fstore_1       // 将结果存储到局部变量表索引为1 的位置【i】
         * 9 return
         */
    }

    public void testAdd() {
        int i = 100;
        i = i + 5;
        i += 5;
        /**
         *  0 bipush 100    // 将100压入操作数栈中
         *  2 istore_1      // 将100存储到局部变量表索引为1的位置【i】
         *  3 iload_1       // 将i=100入操作数栈
         *  4 iconst_5      // 将常量5压入操作数栈
         *  5 iadd          // 将100和5出栈，执行add操作，并将结果压入栈中
         *  6 istore_1      // 将结果105当做int保存到索引为1的位置
         *  7 iinc 1 by 5   // 将索引为1的变量【i】自增5
         * 10 return
         */
    }

    public int testAri() {
        int a = 80;
        int b = 7;
        int c = 10;
        return (a + b) * c;
        /**
         *  0 bipush 80 // 80入操作数栈
         *  2 istore_1  // a = 80
         *  3 bipush 7  // 7入操作数栈
         *  5 istore_2  // b = 7
         *  6 bipush 10 // 10入操作数栈
         *  8 istore_3  // c =10；
         *  9 iload_1   // a = 80 入栈
         * 10 iload_2   // b = 7 入栈
         * 11 iadd      // 80、7出栈并执行add，将结果压入栈
         * 12 iload_3   // c = 10 入栈
         * 13 imul      // 10 与 87执行imul，将结果压栈
         * 14 ireturn   // 返回栈顶结果
         */
    }

    public int testAriPlus(int i, int j) {
        return ((i + j - 1) & ~(j - 1));
        /**
         *  0 iload_1       // i 入栈
         *  1 iload_2       // j 入栈
         *  2 iadd          // add(i,j)结果入栈
         *  3 iconst_1      // 1入栈
         *  4 isub          // isub(1,add(i,j) 结果入栈
         *  5 iload_2       // j 入栈
         *  6 iconst_1      // 1 入栈
         *  7 isub          // sub(1,j) 结果入栈
         *  8 iconst_m1     // -1 入栈
         *  9 ixor          // 将 -1 与之前运算的结果执行异或操作 ==> 实现取反 ， 最后将讲过入栈
         * 10 iand          // and 操作
         * 11 ireturn       // 返回and操作结果
         */
    }

    public void testAddAndGet() {
        int i = 100;
        ++i;
        // i++;
        /** 无论是 ++i 还是 ++i 都是一下字节码指令
         * 0 bipush 100     // 100 入栈
         * 2 istore_1       // 100 出栈 保存到 i
         * 3 iinc 1 by 1    // i 直接 +1
         * 6 return         // 返回
         */
    }


    public void testAddPlus() {
        int i = 10;
        int a = i++;

        int j = 20;
        int b = ++j;
        /**
         *  0 bipush 10     // 10 入栈
         *  2 istore_1      // 10出栈保存到i中
         *  3 iload_1       // 加载i的值入栈
         *  4 iinc 1 by 1   // i值 +1【相加不需要经过操作数栈】
         *  7 istore_2      // 将栈顶元素10【之前i的值】出栈并保存到a中
         *  =======================================================
         *  8 bipush 20     // 20 入栈
         * 10 istore_3      // 20出栈保存到j
         * 11 iinc 3 by 1   // j值 +1
         * 14 iload_3       // 加载j的值入栈
         * 15 istore 4      // 将栈顶值出栈保存到b
         * 17 return        // 返回
         */
    }

    public void testAddPlusPlus() {
        int i = 10;
        i = i++;
        System.out.println(i);
        /**
         *  0 bipush 10     // 10 入栈
         *  2 istore_1      // 10 出栈保存到i
         *  3 iload_1       // i 的值入栈
         *  4 iinc 1 by 1   // i 的值加1
         *  7 istore_1      // 将操作数栈的栈顶数组赋值给i
         *  8 getstatic #5 <java/lang/System.out>
         * 11 iload_1
         * 12 invokevirtual #8 <java/io/PrintStream.println>
         * 15 return
         */
    }

}
