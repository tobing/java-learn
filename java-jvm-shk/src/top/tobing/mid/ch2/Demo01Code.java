package top.tobing.mid.ch2;

import java.util.Date;

/**
 * @Author tobing
 * @Date 2021/1/2 17:47
 * @Description
 */
public class Demo01Code {

    // 变量入栈
    public void load(int num, Object obj, long count, boolean flag, short[] arr) {
        System.out.println(num);
        System.out.println(obj);
        System.out.println(count);
        System.out.println(flag);
        System.out.println(arr);
        /**
         *  0 getstatic #2 <java/lang/System.out>
         *  3 iload_1       // 将局部变量表序号为1【num】 压入 操作数栈
         *  4 invokevirtual #3 <java/io/PrintStream.println>
         *  7 getstatic #2 <java/lang/System.out>
         * 10 aload_2       // 将局部变量表序号为2【obj】 压入 操作数栈
         * 11 invokevirtual #4 <java/io/PrintStream.println>
         * 14 getstatic #2 <java/lang/System.out>
         * 17 lload_3       // 将局部变量表序号为3【count】 压入 操作数栈
         * 18 invokevirtual #5 <java/io/PrintStream.println>
         * 21 getstatic #2 <java/lang/System.out>
         * 24 iload 5       // 将局部变量表序号为4【flag】 压入 操作数栈
         * 26 invokevirtual #6 <java/io/PrintStream.println>
         * 29 getstatic #2 <java/lang/System.out>
         * 32 aload 6       // 将局部变量表序号为6【arr】 压入 操作数栈
         * 34 invokevirtual #4 <java/io/PrintStream.println>
         * 37 return
         */
    }

    // 常量入栈
    public void loadPushConstLdc() {
        int i = -1;
        int a = 5;
        int b = 6;
        int c = 127;
        int d = 128;
        int e = 32767;
        int f = 32768;
        /**
         *  0 iconst_m1     // -1 入栈
         *  1 istore_1
         *  2 iconst_5      // 5 入栈
         *  3 istore_2
         *  4 bipush 6      // 6 入栈
         *  6 istore_3
         *  7 bipush 127    // 127 入栈
         *  9 istore 4
         * 11 sipush 128    // 128 入栈
         * 14 istore 5
         * 16 sipush 32767  // 32767 入栈
         * 19 istore 6
         * 21 ldc #7 <32768>  // 32768 入栈
         * 24 istore 7
         * 26 return
         */
    }

    public void loadConstLdc() {
        long a1 = 1;
        long a2 = 2;
        float b1 = 2;
        float b2 = 3;
        double c1 = 1;
        double c2 = 2;
        Date d = null;
        /**
         *  0 lconst_1      // 1 入栈
         *  1 lstore_1
         *  2 ldc2_w #8 <2> // 2 入栈
         *  5 lstore_3
         *  6 fconst_2      // 2 入栈
         *  7 fstore 5
         *  9 ldc #10 <3.0> // 3.0 入栈
         * 11 fstore 6
         * 13 dconst_1      // 1 入栈
         * 14 dstore 7
         * 16 ldc2_w #11 <2.0>// 2.0 入栈
         * 19 dstore 9
         * 21 aconst_null   // null 入栈
         * 22 astore 11
         * 24 return
         */
    }

    // 出栈装入局部变量表指令
    public void store(int k, double d) {
        int m = k + 2;
        long l = 12;
        String str = "tobing";
        float f = 10.0F;
        d = 10;
        /**
         *  0 iload_1       // 从局部变量表中加载索引为1【k=1】到操作数栈
         *  1 iconst_2      // 将【常量2】加载到操作数栈
         *  2 iadd          // 将变量【k】与【常量2】出栈，执行add操作，并将结果保存在操作数栈中
         *  3 istore 4      // 将结果出栈储存在局部变量表索引为4的位置【m】
         *  5 ldc2_w #13 <12>   // 将【常量12】加载到操作数栈中
         *  8 lstore 5          // 将【常量12】出栈存储到局部变量表索引5的位置【l】
         * 10 ldc #15 <tobing>  // 将【字符串tobing】加载到操作数中
         * 12 astore 7          // 将【字符串tobing】出栈存储到局部变量表索引为7的位置【str】
         * 14 ldc #16 <10.0>    // 将【常量10.0】加载到操作数栈中
         * 16 fstore 8          // 将【常量10.0】出栈存储到局部变量表索引为8的位置【f】
         * 18 ldc2_w #17 <10.0> // 将【常量10.0】加载到操作数栈中
         * 21 dstore_2          // 将【常量10.0】出栈存储到局部变量表索引为2的位置【d】
         * 22 return
         */
    }

    public void foo(long l, float f) {
        {
            int i = 0;
        }
        {
            String s = "Hello world";
        }
        /**
         * 0 iconst_0   // 将【常量0】压入操作数栈中
         * 1 istore 4   // 将【常量0】出栈存储到局部变量表索引为4的位置【i】
         * 3 ldc #19 <Hello world> // 将【常量Hello world】压入操作数栈中
         * 5 astore 4              // 将【常量Hello world】出栈存储到索引为4的位置【f】（此处存在slot复用）
         * 7 return
         */
    }
}
