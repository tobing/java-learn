package top.tobing.mid.ch3;

/**
 * @Author tobing
 * @Date 2021/1/3 17:35
 * @Description 类型转换
 */
public class Demo01UpperTypeCast {

    public static void main(String[] args) {
        Demo01UpperTypeCast typeCast = new Demo01UpperTypeCast();
        typeCast.upperCast02();
    }

    public void upperCast01() {
        int i = 10;
        long l = i;
        float f = i;
        double d = i;

        float f1 = l;
        double d1 = l;
        double d2 = f1;
        /**
         *  0 bipush 10
         *  2 istore_1
         *  3 iload_1
         *  4 i2l           // int --> long
         *  5 lstore_2
         *  6 iload_1
         *  7 i2f           // int --> float
         *  8 fstore 4
         * 10 iload_1
         * 11 i2d           // int --> double
         * 12 dstore 5
         * 14 lload_2
         * 15 l2f           // long --> float
         * 16 fstore 7
         * 18 lload_2
         * 19 l2d           // long --> double
         * 20 dstore 8
         * 22 fload 7
         * 24 f2d           // float--> double
         * 25 dstore 10
         * 27 return
         */
    }

    public void upperCast02() {
        int i = 123123123;
        float f = i;
        System.out.println(f);  // 1.2312312E8 精度丢失

        long l = 123123123123123123L;
        double d = l;
        System.out.println(d);  // 1.2312312312312312E17 精度丢失
    }

    public void upperCast03(byte b) {
        int i = b;
        long l = b;
        double d = b;
        /**
         * 0 iload_1
         * 1 istore_2
         * 2 iload_1
         * 3 i2l        // int --> long
         * 4 lstore_3
         * 5 iload_1
         * 6 i2d        // int --> double
         * 7 dstore 5
         * 9 return
         */
    }

    public void upperCast04(short s) {
        int i = s;
        long l = s;
        double d = s;
        /**
         * 0 iload_1
         * 1 istore_2
         * 2 iload_1
         * 3 i2l        // int --> long
         * 4 lstore_3
         * 5 iload_1
         * 6 i2d        // int --> double
         * 7 dstore 5
         * 9 return
         */
    }


}
