package top.tobing.mid.ch3;

/**
 * @Author tobing
 * @Date 2021/1/3 18:15
 * @Description 向下类型转换
 */
public class Demo02DownTypeCast {

    public static void main(String[] args) {
        Demo02DownTypeCast cast = new Demo02DownTypeCast();
        // cast.downCast4();
        cast.downCast5();
    }

    public void downCast1() {
        int i = 10;
        byte b = (byte) i;      // i2b
        short s = (short) i;    // i2s
        char c = (char) i;      // i2c
    }

    public void downCast2() {
        float f = 10;
        long l = (long) f;  // f2l
        int i = (int) f;    // f2i
        byte b = (byte) f;  // f2i i2b
        double d = 10;
        byte b1 = (byte) d; // d2i i2b
    }

    public void downCast3() {
        short s = 10;
        byte b = (byte) s;  // i2b
    }

    public void downCast4() {
        int i = 128;
        byte b = (byte) i;  // i2b
        System.out.println(b);  // -128
    }

    public void downCast5() {
        double d1 = Double.NaN;
        int i = (int) d1;
        System.out.println(i);  // 0

        double d2 = Double.POSITIVE_INFINITY;
        long l = (long) d2;
        int i2 = (int) d2;
        System.out.println(l);  // 与下面相等
        System.out.println(Long.MAX_VALUE);
        System.out.println(i2); // 与下面相等
        System.out.println(Integer.MAX_VALUE);
    }


}
