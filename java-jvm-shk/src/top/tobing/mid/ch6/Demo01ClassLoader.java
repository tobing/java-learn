package top.tobing.mid.ch6;

/**
 * @Author tobing
 * @Date 2021/1/8 20:53
 * @Description
 */
public class Demo01ClassLoader {
    public static void main(String[] args) {
        testSpecialClassLoader();
    }

    // 不同的类加载器
    public static void testClassLoaderType() {
        try {
            // 引导类加载器
            // null
            Class<?> clazz1 = Class.forName("java.lang.String");
            System.out.println(clazz1.getClassLoader());

            // 应用程序加载器
            // sun.misc.Launcher$AppClassLoader@18b4aac2
            Class<?> clazz2 = Class.forName("top.tobing.mid.ch6.Demo01ClassLoader");
            System.out.println(clazz2.getClassLoader());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    // 获取类加载器的方法
    public static void testClassLoaderPath() {
        // 获取当前类加载器
        // null -> 引导类加载器
        ClassLoader clazz1 = StringBuilder.class.getClassLoader();
        System.out.println(clazz1);

        // 获取当前线程上下文类加载器
        // sun.misc.Launcher$AppClassLoader 应用程序类加载器
        ClassLoader clazz2 = Thread.currentThread().getContextClassLoader();
        System.out.println(clazz2);

        // 获取系统的类加载器
        // sun.misc.Launcher$AppClassLoader 应用程序类加载器
        ClassLoader clazz3 = ClassLoader.getSystemClassLoader();
        System.out.println(clazz3);
    }

    // 一些特殊的类加载器
    public static void testSpecialClassLoader() {
        // 字符串数组
        // null:代表是引导类加载器
        String[] str = new String[10];
        System.out.println("String[]: " + str.getClass().getClassLoader());

        // 自定义类数组
        // sun.misc.Launcher$AppClassLoader：应用程序类加载器
        Demo01ClassLoader[] classLoaders = new Demo01ClassLoader[10];
        System.out.println("Demo01ClassLoader[]: " + classLoaders.getClass().getClassLoader());

        // 基本数据类型数组
        // null：代表该类不用加载器，有JVM自动加载
        int[] ints = new int[10];
        System.out.println("ini[]:" + ints.getClass().getClassLoader());
    }
}

