package top.tobing.mid.ch6;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.ResultSet;

/**
 * @Author tobing
 * @Date 2021/1/9 12:16
 * @Description
 */
public class Demo02DIYClassLoader {
    public static void main(String[] args) {
        System.out.println(ResultSet.class.getClassLoader());

        MyClassLoader myClassLoader = new MyClassLoader("d:/");
        try {
            Class<?> clazz = myClassLoader.findClass("Demo01Code");
            System.out.println(clazz.getClassLoader());
            System.out.println(clazz.getClassLoader().getParent());
            System.out.println(clazz.getClassLoader().getClass().getName());
            System.out.println(clazz.getClassLoader().getParent().getClass().getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}


class MyClassLoader extends ClassLoader {


    private String path;

    public MyClassLoader(String path) {
        this.path = path;
    }

    public MyClassLoader(ClassLoader parent, String path) {
        super(parent);
        this.path = path;
    }

    // findClass        ---->   加载类的字节码流
    // defineClass      ---->   将类的字节码流转换为Class
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        BufferedInputStream bis = null;
        ByteArrayOutputStream baos = null;
        try {
            // 拼接字节码文件路径
            String classPath = this.path + name + ".class";
            // 将.class文件通过输入流加载，在通过输出流输出到byteArray
            bis = new BufferedInputStream(new FileInputStream(classPath));
            baos = new ByteArrayOutputStream();
            int len = 0;
            byte[] buf = new byte[1024];
            while ((len = bis.read(buf)) != -1) {
                baos.write(buf, 0, len);
            }
            byte[] byteCodes = baos.toByteArray();
            // 将byteArray转换为Class对象
            return defineClass(null, byteCodes, 0, byteCodes.length);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bis != null)
                    bis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (baos != null)
                    baos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}