package top.tobing.next;

/**
 * @Author tobing
 * @Date 2021/1/10 18:28
 * @Description
 */
public class Demo {
    public volatile static int i = 10;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            for (int j = 0; j < 100000; j++) {
                i++;
            }
        }).start();

        new Thread(() -> {
            i+=1;
        }).start();

        new Thread(() -> {
            for (int j = 0; j < 100000; j++) {
                i += 1;
            }
        }).start();


        Thread.sleep(4000);
        System.out.println(i);
    }
}
