package top.tobing.ch4;

/**
 * @Author tobing
 * @Date 2021/1/12 22:31
 * @Description
 */
public class Demo09Synchronized {
    public static void main(String[] args) {
        // 同步代码块
        synchronized (Demo09Synchronized.class) {
        }
        // 同步方法
        testSynchronized();
    }
    public static synchronized void testSynchronized() {
    }
}
