package top.tobing.ch4;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.util.LinkedList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author tobing
 * @Date 2021/1/13 16:20
 * @Description 等待超时模型实现简单数据库连接池
 */
public class Demo13ConnectionPool {
    static ConnectionPool pool = new ConnectionPool(10);
    static CountDownLatch start = new CountDownLatch(1);
    static CountDownLatch end;

    // 模拟客户端使用连接池
    public static void main(String[] args) throws InterruptedException {
        int threadCount = 30;
        end = new CountDownLatch(threadCount);
        int count = 20;
        AtomicInteger got = new AtomicInteger();
        AtomicInteger noGot = new AtomicInteger();
        for (int i = 0; i < threadCount; i++) {
            Thread thread = new Thread(new ConnectionRunner(count, got, noGot), "ConnectionRunnerThread");
            thread.start();
        }
        start.countDown();
        end.await();
        System.out.println("total invoke:" + (threadCount * count));
        System.out.println("got connection " + got);
        System.out.println("not got connection " + noGot);

    }

    // 模拟用户线程使用连接，统计申请成功以及不成功的次数
    static class ConnectionRunner implements Runnable {
        int count;
        AtomicInteger got;
        AtomicInteger noGot;

        public ConnectionRunner(int count, AtomicInteger got, AtomicInteger noGot) {
            this.count = count;
            this.got = got;
            this.noGot = noGot;
        }

        @Override
        public void run() {
            try {
                start.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            while (count > 0) {
                try {
                    Connection connection = pool.getConnection(1000);
                    if (connection != null) {
                        try {
                            connection.createStatement();
                            connection.commit();
                        } finally {
                            pool.releaseConnection(connection);
                            got.incrementAndGet();
                        }
                    } else {
                        noGot.incrementAndGet();
                    }
                } catch (Exception e) {

                } finally {
                    count--;
                }
            }
            end.countDown();
        }
    }
}

// 自定义连接池
class ConnectionPool {
    // 利用一个双向队列来存储连接
    private LinkedList<Connection> pool = new LinkedList<>();

    // 初始化一个大小为initialSize的连接池
    public ConnectionPool(int initialSize) {
        if (initialSize > 0) {
            for (int i = 0; i < initialSize; i++) {
                pool.addLast(ConnectionDriver.createConnection());
            }
        }
    }

    // 释放一个数据库连接
    public void releaseConnection(Connection connection) {
        if (connection != null) {
            synchronized (pool) {
                pool.addLast(connection);
                pool.notifyAll();
            }
        }
    }

    // 获取一个数据库连接
    public Connection getConnection(long mills) throws InterruptedException {
        synchronized (pool) {
            if (mills < 0) {    // 不设置超时时间
                // 如果数据库为空，则循环等待直到拿到连接
                while (pool.isEmpty()) {
                    pool.wait();
                }
                return pool.removeFirst();
            } else {            // 设置超时时间
                // 计算超时时间
                long future = System.currentTimeMillis() + mills;
                // 保存可接受等待时间
                long remaining = mills;
                while (pool.isEmpty() && remaining > 0) {
                    // 等待直到被唤醒或者超时
                    pool.wait(remaining);
                    // 防止虚假唤醒，即唤醒了但是没有得到资源，重新计算等待时间
                    remaining = future - System.currentTimeMillis();
                }
                Connection result = null;
                if (!pool.isEmpty()) {
                    result = pool.removeFirst();
                }
                return result;
            }
        }
    }
}

// java.sql.Connection是一个接口，最终实现的是有数据库驱动提供方来实现
// 此处为了简化反射创建要给Connection，该Connection内部实现仅仅是commint()方法调用时休眠100ms
class ConnectionDriver {
    static class ConnectionHandler implements InvocationHandler {
        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if ("commit".equals(method.getName())) {
                TimeUnit.MILLISECONDS.sleep(100);
            }
            return null;
        }
    }
    public static final Connection createConnection() {
        return (Connection) Proxy.newProxyInstance(
                ConnectionDriver.class.getClassLoader(),
                new Class[]{Connection.class},
                new ConnectionHandler());
    }
}
