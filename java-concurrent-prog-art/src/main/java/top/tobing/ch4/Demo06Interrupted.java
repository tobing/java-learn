package top.tobing.ch4;

import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2021/1/12 21:40
 * @Description
 */
public class Demo06Interrupted {
    public static void main(String[] args) throws InterruptedException {
        // 初始化线程
        Thread sleepRunner = new Thread(new SleepRunner(), "SleepRunner");
        Thread busyRunner = new Thread(new BusyRunner(), "BusyRunner");
        // 线程线程为守护线程
        sleepRunner.setDaemon(true);
        busyRunner.setDaemon(true);
        // 启动线程
        sleepRunner.start();
        busyRunner.start();
        System.out.println("SleepThread isInterrupted() : " + sleepRunner.isInterrupted()); // false
        System.out.println("BusyThread isInterrupted() : " + busyRunner.isInterrupted());   // false
        // 休眠2s，让两个线程充分执行
        TimeUnit.SECONDS.sleep(2);
        // 分别尝试中断两个线程
        sleepRunner.interrupt();
        busyRunner.interrupt();
        System.out.println("SleepThread isInterrupted() : " + sleepRunner.isInterrupted()); // false
        System.out.println("BusyThread isInterrupted() : " + busyRunner.isInterrupted());   // true
        TimeUnit.SECONDS.sleep(1);
    }

    static class SleepRunner implements Runnable {
        @Override
        public void run() {
            while (true) {
                try {
                    TimeUnit.SECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class BusyRunner implements Runnable {
        @Override
        public void run() {
            while (true) {
            }
        }
    }
}
