package top.tobing.ch4;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2021/1/12 21:57
 * @Description suspend/resume和stop使用
 */
public class Demo07Deprecated {
    public static void main(String[] args) throws InterruptedException {
        DateFormat format = new SimpleDateFormat("HH:mm:ss");
        Thread thread = new Thread(() -> {
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            while (true) {
                System.out.println(Thread.currentThread().getName() + "Run at" + sdf.format(new Date()));
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "PrintThread");
        thread.setDaemon(true);
        thread.start();
        // 睡眠3s
        TimeUnit.SECONDS.sleep(3);
        // 挂起、暂停
        thread.suspend();
        System.out.println(Thread.currentThread().getName() + "Run at" + format.format(new Date()));
        // 睡眠3s
        TimeUnit.SECONDS.sleep(3);
        // 恢复
        thread.resume();
        System.out.println(Thread.currentThread().getName() + "Run at" + format.format(new Date()));
        // 睡眠3s
        TimeUnit.SECONDS.sleep(3);
        // 终止
        thread.stop();
        System.out.println(Thread.currentThread().getName() + "Run at" + format.format(new Date()));
        TimeUnit.SECONDS.sleep(3);

    }
}
