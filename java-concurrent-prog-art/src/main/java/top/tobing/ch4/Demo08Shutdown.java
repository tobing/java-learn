package top.tobing.ch4;

import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2021/1/12 22:06
 * @Description 标志位控制线程的停止
 */
public class Demo08Shutdown {
    public static void main(String[] args) throws InterruptedException {
        // 中断方式终止
        Runner interrupt = new Runner();
        Thread runner1 = new Thread(interrupt, "Runner_1");
        runner1.start();
        TimeUnit.SECONDS.sleep(1);
        runner1.interrupt();
        // 标志位方式终止
        Runner flag = new Runner();
        Thread runner2 = new Thread(flag, "Runner_2");
        runner2.start();
        TimeUnit.SECONDS.sleep(1);
        flag.cancel();


    }

    private static class Runner implements Runnable {
        private long i;
        private volatile boolean on = true;

        @Override
        public void run() {
            // 当 on == true 且当前线程未被中断，则一直运行
            while (on && !Thread.currentThread().isInterrupted()) {
                i++;
            }
            System.out.println("Count: i = " + i);
        }

        public void cancel() {
            on = false;
        }
    }
}
