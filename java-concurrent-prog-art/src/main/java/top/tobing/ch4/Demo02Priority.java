package top.tobing.ch4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2021/1/12 17:08
 * @Description 线程优先级
 */
public class Demo02Priority {

    private static volatile boolean notStart = true;
    private static volatile boolean notEnd = true;

    public static void main(String[] args) throws InterruptedException {
        List<Job> jobList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            int priority = i > 5 ? Thread.MAX_PRIORITY : Thread.MIN_PRIORITY;
            Job job = new Job(priority);
            jobList.add(job);
            Thread thread = new Thread(job);
            thread.setPriority(priority);
            thread.start();
        }

        notStart = false;
        TimeUnit.SECONDS.sleep(5);
        notEnd = false;
        for (Job job : jobList) {
            System.out.println(job.priority + ":" + job.jobCount);
        }

    }


    // 任务
    static class Job implements Runnable {
        private int priority;
        private long jobCount;
        public Job(int priority) {
            this.priority = priority;
        }
        @Override
        public void run() {
            while (notStart) {
                Thread.yield();
            }
            while (notEnd) {
                Thread.yield();
                jobCount++;
            }
        }
    }
}
