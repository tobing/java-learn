package top.tobing.ch4;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;

/**
 * @Author tobing
 * @Date 2021/1/12 17:05
 * @Description 一个Java程序运行时有多个辅助线程在执行
 */
public class Demo01MulitThread {
    public static void main(String[] args) {
        // 获取Java线程管理MXBean
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        // 不需要获取同步 monitor 和 synchronizer 信息，仅仅获取线程和线程堆栈信息
        ThreadInfo[] threadInfos = threadMXBean.dumpAllThreads(false, false);
        // 编码，输出线程ID和名称
        for (ThreadInfo threadInfo : threadInfos) {
            System.out.println(threadInfo.getThreadId() + " : " + threadInfo.getThreadName());
        }
    }
}
