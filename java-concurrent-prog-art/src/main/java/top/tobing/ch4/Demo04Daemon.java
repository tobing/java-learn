package top.tobing.ch4;

import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2021/1/12 21:04
 * @Description
 */
public class Demo04Daemon {
    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    TimeUnit.SECONDS.sleep(2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    System.out.println("守护线程退出");
                }
            }
        }, "Runner");
        // 将现场设置为守护线程
        thread.setDaemon(true);
        thread.start();
    }
}
