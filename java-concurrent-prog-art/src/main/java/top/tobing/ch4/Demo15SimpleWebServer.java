package top.tobing.ch4;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Author tobing
 * @Date 2021/1/13 18:23
 * @Description 使用线程池技术实现简单的Web服务器
 */
public class Demo15SimpleWebServer {
    public static void main(String[] args) throws IOException {
        SimpleHttpServer.setBasePath("E:\\Dev\\DevTool\\AdminLTE\\AdminLTE汉化\\adminlte2-itcast\\release\\dist");
        SimpleHttpServer.start();
    }
}

class SimpleHttpServer {
    // HTTP请求的线程池
    static ThreadPool<HttpRequestHandler> pool = new DefaultThreadPool<>(10);
    // SimpleHTTPServer根垃圾
    static String basePath;
    static ServerSocket serverSocket;
    // 服务器端口【默认】
    static int port = 8080;

    // 设置服务器端口
    public static void setPort(int port) {
        if (port > 0) {
            SimpleHttpServer.port = port;
        }
    }

    // 设置服务器根路径
    public static void setBasePath(String basePath) {
        SimpleHttpServer.basePath = basePath;
    }

    public static void start() throws IOException {
        serverSocket = new ServerSocket(port);
        Socket socket = null;
        while ((socket = serverSocket.accept()) != null) {
            pool.execute(new HttpRequestHandler(socket));
        }
        serverSocket.close();
    }


    static class HttpRequestHandler implements Runnable {
        // 一个线程绑定一个Socket
        private Socket socket;

        // 一个线程处理一个Socket
        public HttpRequestHandler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            String line = null;
            BufferedReader br = null;
            BufferedReader read = null;
            PrintWriter out = null;
            InputStream in = null;
            try {
                read = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String header = read.readLine();
                String filePath = basePath + header.split(" ")[1];
                out = new PrintWriter(socket.getOutputStream());
                if (filePath.endsWith("jpg") || filePath.endsWith("ico")) { // 对于图片资源使用字节流
                    in = new FileInputStream(filePath);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    int i = 0;
                    while ((i = in.read()) != -1) {
                        baos.write(i);
                    }
                    byte[] array = baos.toByteArray();
                    out.println("HTTP/1.1 200 OK");
                    out.println("Server:Tobing");
                    out.println("Content-Type: image/jepg");
                    out.println("Content-Length: " + array.length);
                    out.println("");
                    socket.getOutputStream().write(array, 0, array.length);
                } else {    // 对于文本资源使用字符流
                    br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));
                    out = new PrintWriter(socket.getOutputStream());
                    out.println("HTTP/1.1 200 OK");
                    out.println("Server:Tobing");
                    out.println("Content-Type: text/html; charset=UTF-8");
                    out.println("");
                    while ((line = br.readLine()) != null) {
                        out.println(line);
                    }
                }
                out.flush();
            } catch (Exception e) {
                out.println("HTTP/1.1 500");
                out.println("");
                out.flush();
            } finally {
                close(br, in, read, out, socket);
            }
        }
    }

    // 批量关闭资源
    private static void close(Closeable... closeables) {
        if (closeables != null) {
            for (Closeable closeable : closeables) {
                try {
                    if (closeable != null)
                        closeable.close();
                } catch (IOException e) {

                }
            }
        }
    }
}
