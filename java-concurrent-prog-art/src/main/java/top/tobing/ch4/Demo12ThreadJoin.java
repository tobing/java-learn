package top.tobing.ch4;

/**
 * @Author tobing
 * @Date 2021/1/13 11:55
 * @Description Thread.join
 */
public class Demo12ThreadJoin {

    public static void main(String[] args) {
        Thread pre = Thread.currentThread();
        for (int i = 0; i < 10; i++) {
            Thread cur = new Thread(new Domino(pre), String.valueOf(i));
            cur.start();
            pre = cur;
        }
    }

    static class Domino implements Runnable {
        private Thread thread;
        public Domino(Thread thread) {
            this.thread = thread;
        }
        @Override
        public void run() {
            try {
                thread.join();
                System.out.println(thread.getName() + "执行结束....");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " terminate");
        }
    }
}
