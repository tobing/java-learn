package top.tobing.ch4;

import java.io.*;

/**
 * @Author tobing
 * @Date 2021/1/13 11:40
 * @Description 管道流
 */
public class Demo11PipedModel {
    public static void main(String[] args) throws IOException {
        PipedReader in = new PipedReader();
        PipedWriter out = new PipedWriter();
        // 将输出流绑定输入流
        out.connect(in);
        new Thread(new Print(in), "Print").start();
        // 接受系统输入流，将流写入到管道流中
        int receive = 0;
        try {
            while ((receive = System.in.read()) != -1) {
                out.write(receive);
            }
        } finally {
            out.close();
        }
    }

    static class Print implements Runnable {
        private PipedReader in;
        public Print(PipedReader in) {
            this.in = in;
        }
        @Override
        public void run() {
            int receive = 0;
            try {
                while ((receive = in.read()) != -1) {
                    System.out.print((char) receive);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
