package top.tobing.ch4;

import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2021/1/13 12:08
 * @Description
 */
public class Demo12ThreadLocal {
    private static final ThreadLocal<Long> TIME_THREADLOCAL = new ThreadLocal<Long>() {
        @Override
        protected Long initialValue() {
            return System.currentTimeMillis();
        }
    };

    public static final void begin() {
        TIME_THREADLOCAL.set(System.currentTimeMillis());
    }

    public static final long end() {
        return System.currentTimeMillis() - TIME_THREADLOCAL.get();
    }

    public static void main(String[] args) throws InterruptedException {
        Demo12ThreadLocal.begin();
        TimeUnit.SECONDS.sleep(2);
        System.out.println("时间花费：" + Demo12ThreadLocal.end());
    }
}
