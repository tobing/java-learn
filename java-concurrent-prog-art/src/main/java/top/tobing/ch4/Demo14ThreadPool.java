package top.tobing.ch4;


import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @Author tobing
 * @Date 2021/1/13 17:20
 * @Description
 */
public class Demo14ThreadPool {
    public static void main(String[] args) {
    }
}

// 线程池接口定义
interface ThreadPool<Job extends Runnable> {

    // 执行一个Job，Job需要实现Runnable
    void execute(Job job);

    // 关闭线程池
    void shutdown();

    // 增加工作线程
    void addWorkers(int num);

    // 减少工作线程
    void removeWorker(int num);

    // 查看正在等待执行的工作线程
    int getJobSize();
}

// 自定义线程池实现
class DefaultThreadPool<Job extends Runnable> implements ThreadPool<Job> {
    // 最大线程数
    private static final int MAX_WORKER_NUMBERS = 10;
    // 默认线程数
    private static final int DEFAULT_WORKER_NUMBERS = 5;
    // 最小线程数
    private static final int MIN_WORKER_NUMBER = 1;
    // 任务Job队列
    private final LinkedList<Job> jobs = new LinkedList<>();
    // 工作者队列
    private final List<Worker> workers = Collections.synchronizedList(new ArrayList<Worker>());
    // 工作者线程数量
    private int workerNum = DEFAULT_WORKER_NUMBERS;
    // 线程编号生成器
    private AtomicLong threadNum = new AtomicLong();

    public DefaultThreadPool() {
        initializeWorkers(DEFAULT_WORKER_NUMBERS);
    }

    public DefaultThreadPool(int num) {
        if (num < MIN_WORKER_NUMBER) {
            num = MIN_WORKER_NUMBER;
        } else if (num > MAX_WORKER_NUMBERS) {
            num = MAX_WORKER_NUMBERS;
        }
        initializeWorkers(num);
    }

    // 初始化线程工作者
    private void initializeWorkers(int num) {
        for (int i = 0; i < num; i++) {
            Worker worker = new Worker();
            workers.add(worker);
            Thread thread = new Thread(worker, "ThreadPool-Worker-" + threadNum.incrementAndGet());
            thread.start();
        }
    }

    @Override
    public void execute(Job job) {
        if (job != null) {
            synchronized (jobs) {
                jobs.addLast(job);  // 添加一个Job后，对工作队列调用其他notify方法
                jobs.notify();      // 而不是notifyAll，因为只需要确保有工作者线程
                                    // 唤醒就可以，notify比notifyAll花费更小的开销。
            }
        }
    }

    @Override
    public void shutdown() {
        for (Worker worker : workers) {
            worker.shutdown();
        }
    }

    @Override
    public void addWorkers(int num) {
        synchronized (jobs) {
            if (num + this.workerNum > MAX_WORKER_NUMBERS) {
                num = MAX_WORKER_NUMBERS - this.workerNum;
            }
            initializeWorkers(num);
            this.workerNum += num;
        }
    }

    @Override
    public void removeWorker(int num) {
        synchronized (jobs) {
            if (num >= this.workerNum) {
                throw new IllegalArgumentException("beyond workNum");
            }
            int count = 0;
            while (count < num) {
                Worker worker = workers.get(count);
                if (workers.remove(worker)) {
                    worker.shutdown();
                    count++;
                }
            }
        }
    }

    @Override
    public int getJobSize() {
        return jobs.size();
    }

    // 一个Worker对应一个线程
    class Worker implements Runnable {
        // 是否工作
        private volatile boolean running = true;
        @Override
        public void run() {
            while (running) {
                // 从工作列表中取出一个Job执行
                Job job = null;
                synchronized (jobs) {
                    while (jobs.isEmpty()) {
                        try {
                            jobs.wait();
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                            return;
                        }
                    }
                    job = jobs.removeFirst();
                }
                if (job != null) {
                    job.run();
                }
            }
        }

        public void shutdown() {
            running = false;
        }
    }
}
