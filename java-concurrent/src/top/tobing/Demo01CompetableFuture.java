package top.tobing;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author tobing
 * @Date 2021/4/11 20:47
 * @Description
 */
public class Demo01CompetableFuture {
    private static ExecutorService executorService = Executors.newFixedThreadPool(10);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        test03();

    }

    private static void teset05() throws InterruptedException, ExecutionException {
        CompletableFuture<String> task1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            String str = "原始车架";
            System.out.println("运行结果：" + str);
            return str;
        }, executorService);

        CompletableFuture<String> finish = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            String str = "原始车轮";
            System.out.println("运行结果：" + str);
            return str;
        }, executorService).thenCombineAsync(task1, (res1, res2) -> {
            System.out.println("组装车子与车轮");
            return res1 + " : " + res2;
        }, executorService);

        String res = finish.get();
        System.out.println(res);
    }

    private static void test03() throws InterruptedException, ExecutionException {
        CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询商品数据库信息");
            return "商品信息";
        }, executorService);

        CompletableFuture<String> future2 = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询库存数据库信息");
            return "库存信息";
        }, executorService);

        CompletableFuture<String> future3 = CompletableFuture.supplyAsync(() -> {
            System.out.println("查询商品图片数据库信息");
            return "图片信息";
        }, executorService);

        CompletableFuture<Void> res = CompletableFuture.allOf(future1, future2, future3);
        res.get();
        System.out.println("全部执行完毕！！！");

    }

    private static void test04() throws InterruptedException, ExecutionException {
        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
            System.out.println("【第一步】");
            System.out.println("当前线程：" + Thread.currentThread().getId());
            String str = "原始车架";
            System.out.println("运行结果：" + str);
            return str;
        }, executorService).thenApplyAsync((prev) -> {  // 这个方法会接收上一步的结果，并会返回结果用于下一步
            System.out.println("【第二步】");
            System.out.println("当前线程：" + Thread.currentThread().getId());
            prev = "真皮车座[" + prev + "]蓝色车灯";
            System.out.println("运行结果：" + prev);
            return prev;
        }, executorService).thenApplyAsync((prev) -> {
            System.out.println("【第三步】");
            System.out.println("当前线程：" + Thread.currentThread().getId());
            prev = "跑车车胎[" + prev + "]暗黑油漆";
            System.out.println("运行结果：" + prev);
            return prev;
        }, executorService);
        String res = completableFuture.get();
        System.out.println("最终执行结果：" + res);
    }

    private static void test02() throws InterruptedException, ExecutionException {
        CompletableFuture<Object> handle = CompletableFuture.supplyAsync(() -> {
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 1;
            System.out.println("运行结果：" + i);
            return i;
        }, executorService).handle((res, throwable) -> {
            // 发生异常则返回默认值，并提示信息，否则直接返回结果
            if (throwable != null) {
                System.out.println("执行发生异常：" + throwable);
                return -1;
            }
            return res;
        });
        Object res = handle.get();
        System.out.println("异步执行结果为：" + res);
    }

    private static void test01() throws InterruptedException, ExecutionException {
        // 异步
        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {   // 提供一个供给型接口，可以返回一个结果
            System.out.println("当前线程：" + Thread.currentThread().getId());
            int i = 10 / 0;
            System.out.println("运行结果：" + i);
            return i;
        }).whenComplete((res, throwable) -> {  // 提供一个消费型接口实例，可以接受若干参数
            System.out.println("执行结果是：" + res + " ;抛出的异常信息为：" + throwable);
        }).exceptionally(throwable -> {         // 提供一个函数型接口实例，将结果出来并返回【发生异常时执行】
            System.out.println("异常信息为：" + throwable);
            return 10;
        });

        Integer rest = completableFuture.get();
        System.out.println("执行结果");
    }
}
