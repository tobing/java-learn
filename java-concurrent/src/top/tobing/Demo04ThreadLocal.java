package top.tobing;

import java.util.Properties;

/**
 * @Author tobing
 * @Date 2021/6/19 10:02
 * @Description ThreadLocal source code analysis.
 *
 */
public class Demo04ThreadLocal {
    private static final int HASH_INCREMENT = 0x61c88647;

    public static void main(String[] args) throws Exception {
        String myTC1 = System.getProperty("myTC");
        System.out.println("mytc = " + myTC1);
        int n = 5;
        int max = 2 << (n - 1);
        for (int i = 0; i < max; i++) {
            System.out.print(i * HASH_INCREMENT & (max - 1));
            System.out.print(" ");
        }
    }
}
