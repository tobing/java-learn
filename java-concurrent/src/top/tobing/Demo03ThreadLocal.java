package top.tobing;

/**
 * @Author tobing
 * @Date 2021/6/18 21:39
 * @Description InheritableThreadLocal
 * InheritableThreadLocal 是 ThreadLocal 的一个子类，与ThreadLocal中每个线程都有读一个的变量不同，
 * InheritableThreadLocal 允许当前线程的所有子线程访问到其定义的变量。
 */
public class Demo03ThreadLocal {
    public static void main(String[] args) {
        ThreadLocal<String> threadLocal = new ThreadLocal<>();
        InheritableThreadLocal<String> inheritableThreadLocal = new InheritableThreadLocal<>();

        new Thread(() -> {
            System.out.println("======== Thread-1 ========");
            threadLocal.set("Thread-1 threadLocal");
            inheritableThreadLocal.set("Thread-1 inheritableThreadLocal");
            // 显然此处可以获取到
            System.out.println(threadLocal.get());
            System.out.println(inheritableThreadLocal.get());

            new Thread(() -> {
                System.out.println("======== childThread ========");
                // 由于子线程和父线程不同，因此此处无法获取
                System.out.println(threadLocal.get());
                // 子线程可以获取到父线程的InheritableThreadLocal
                System.out.println(inheritableThreadLocal.get());
            }).start();
        }).start();

        new Thread(() -> {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("======== Thread-2 ========");
            // 显然，下面无论如何都无法获取得到
            System.out.println(threadLocal.get());
            System.out.println(inheritableThreadLocal.get());
        }).start();
    }
}
