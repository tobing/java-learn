package top.tobing.pattern.factory.demo03;

/**
 * @Author tobing
 * @Date 2020/12/30 20:58
 * @Description 工厂方法模式
 * 定义一个用于创建对象的接口，让子类决定实例化哪个产品对象。工厂方法是一个产品的实例化延迟到其工厂的子类。
 * 工厂方法模式中有以下角色：
 * 【抽象工厂】定义创建产品的规范，调用者通过此角色来访问具体的工厂 【多态】
 * 【具体工厂】实现了抽象工厂里面定义的规范，完成具体的产品的创建
 * 【抽象产品】定义产品的规范，描述了产品的主要特征和功能。
 * 【具体产品】实现了抽象产品中定义规范，有具体工厂来创建，同具体工厂一一对应。
 *
 */
public class FactoryMethodPatternDemo {

    // 【优点】
    // 用户只需要知道具体的工厂名称就可以得到就提的产品，无需知道产品的具体创建过程；
    // 当需要添加新的产品的时候，只需要添加【具体产品】以及与具体产品对应的【具体工厂】就行，无需对原有的代码做任何修改，满足开闭原则。
    // 【缺点】
    // 每添加一个产品就要添加一个【具体产品】类和与之对应的【具体工厂】，增加了系统复制度，容易类爆炸。
    public static void main(String[] args) {
        CoffeeStore coffeeStore = new CoffeeStore();
        Coffee latte = coffeeStore.orderCoffee(new LatteCoffeeFactory());
        Coffee american = coffeeStore.orderCoffee(new AmericanCoffeeFactory());
        System.out.println(latte);
        System.out.println(american);
    }
}
