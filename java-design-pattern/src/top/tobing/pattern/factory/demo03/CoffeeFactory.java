package top.tobing.pattern.factory.demo03;

/**
 * @Author tobing
 * @Date 2020/12/30 21:03
 * @Description 抽象工厂-CoffeeFactory
 * 定义了Coffee工厂的规范
 */
public interface CoffeeFactory {
    // 创建Coffee实体
    Coffee createCoffee();
}
