package top.tobing.pattern.factory.demo03;

/**
 * @Author tobing
 * @Date 2020/12/30 21:04
 * @Description 具体产品-AmericanCoffee
 */
public class AmericanCoffee implements Coffee {
    @Override
    public String getName() {
        return "美式";
    }

    @Override
    public void addMilk() {
        System.out.println("美式Coffee添加牛奶");
    }

    @Override
    public void addSugar() {
        System.out.println("美式Coffee添加糖");
    }
}
