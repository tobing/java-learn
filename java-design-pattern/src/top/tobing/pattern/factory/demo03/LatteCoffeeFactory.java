package top.tobing.pattern.factory.demo03;

/**
 * @Author tobing
 * @Date 2020/12/30 21:06
 * @Description 具体工厂-LatteCoffeeFactory
 */
public class LatteCoffeeFactory implements CoffeeFactory {
    @Override
    public Coffee createCoffee() {
        // 创建LatteCoffee并完成初始化
        LatteCoffee latteCoffee = new LatteCoffee();
        latteCoffee.addMilk();
        latteCoffee.addSugar();
        return latteCoffee;
    }
}
