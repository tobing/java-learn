package top.tobing.pattern.factory.demo03;

/**
 * @Author tobing
 * @Date 2020/12/30 21:12
 * @Description
 */
public class CoffeeStore {

    Coffee orderCoffee(CoffeeFactory coffeeFactory) {
        return coffeeFactory.createCoffee();
    }
}
