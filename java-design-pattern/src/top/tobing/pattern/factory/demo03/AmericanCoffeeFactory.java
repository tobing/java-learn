package top.tobing.pattern.factory.demo03;

/**
 * @Author tobing
 * @Date 2020/12/30 21:05
 * @Description 具体工厂-AmericanCoffeeFactory
 */
public class AmericanCoffeeFactory implements CoffeeFactory {
    @Override
    public Coffee createCoffee() {
        // 创建AmericanCoffee并完成初始化
        AmericanCoffee americanCoffee = new AmericanCoffee();
        americanCoffee.addMilk();
        americanCoffee.addSugar();
        return americanCoffee;
    }
}
