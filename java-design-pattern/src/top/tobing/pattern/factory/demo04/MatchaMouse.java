package top.tobing.pattern.factory.demo04;

/**
 * @Author tobing
 * @Date 2020/12/30 21:33
 * @Description 甜点-抹茶慕斯
 */
public class MatchaMouse implements Dessert {

    @Override
    public void show() {
        System.out.println("抹茶慕斯");
    }
}
