package top.tobing.pattern.factory.demo04;

/**
 * @Author tobing
 * @Date 2020/12/30 21:35
 * @Description
 */
public interface Factory {
    // 创建咖啡
    Coffee createCoffee();

    // 创建甜点
    Dessert createDessert();
}
