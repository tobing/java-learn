package top.tobing.pattern.factory.demo04;

/**
 * @Author tobing
 * @Date 2020/12/30 21:35
 * @Description 美式：美式咖啡+抹茶慕斯
 */
public class AmericanDessertFactory implements Factory {
    @Override
    public Coffee createCoffee() {
        AmericanCoffee americanCoffee = new AmericanCoffee();
        americanCoffee.addMilk();
        americanCoffee.addSugar();
        americanCoffee.getName();
        return americanCoffee;
    }

    @Override
    public Dessert createDessert() {
        MatchaMouse matchaMouse = new MatchaMouse();
        matchaMouse.show();
        return matchaMouse;
    }
}
