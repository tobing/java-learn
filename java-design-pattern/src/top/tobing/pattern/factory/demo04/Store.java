package top.tobing.pattern.factory.demo04;

/**
 * @Author tobing
 * @Date 2020/12/30 21:40
 * @Description
 */
public class Store {
    void order(Factory factory) {
        factory.createCoffee();
        factory.createDessert();
    }
}
