package top.tobing.pattern.factory.demo04;

/**
 * @Author tobing
 * @Date 2020/12/30 21:03
 * @Description 抽象产品-Coffee
 */
public interface Coffee {
    // 获取Coffee名称
    String getName();
    // 加牛奶
    void addMilk();
    // 加糖
    void addSugar();
}
