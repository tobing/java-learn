package top.tobing.pattern.factory.demo04;

/**
 * @Author tobing
 * @Date 2020/12/30 21:33
 * @Description
 */
public class Triamisu implements Dessert {
    @Override
    public void show() {
        System.out.println("提拉米苏");
    }
}
