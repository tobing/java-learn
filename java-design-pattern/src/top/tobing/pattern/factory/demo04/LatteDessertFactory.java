package top.tobing.pattern.factory.demo04;

/**
 * @Author tobing
 * @Date 2020/12/30 21:36
 * @Description 拿铁：提拉米苏+拿铁咖啡
 */
public class LatteDessertFactory implements Factory {
    @Override
    public Coffee createCoffee() {
        LatteCoffee latteCoffee = new LatteCoffee();
        latteCoffee.addMilk();
        latteCoffee.addSugar();
        return latteCoffee;
    }

    @Override
    public Dessert createDessert() {
        Triamisu triamisu = new Triamisu();
        triamisu.show();
        return triamisu;
    }
}
