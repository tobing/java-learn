package top.tobing.pattern.factory.demo04;

/**
 * @Author tobing
 * @Date 2020/12/30 21:32
 * @Description 甜点接口
 */
public interface Dessert {
    void show();
}
