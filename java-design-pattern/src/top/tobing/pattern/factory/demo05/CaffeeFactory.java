package top.tobing.pattern.factory.demo05;


import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @Author tobing
 * @Date 2020/12/30 21:50
 * @Description Coffee工厂
 * 实现动态加载指定配置文件（bean.properties）中指定的类
 * 并将反射生成的类添加到容器中，便于选取
 */
public class CaffeeFactory {

    private static Map<String, Coffee> coffees = new HashMap<>();


    // 加载配置文件读取配置文件
    static {
        Properties properties = new Properties();
        InputStream is = CaffeeFactory.class.getClassLoader().getResourceAsStream("bean.properties");
        try {
            // 加载配置文件信息
            properties.load(is);
            // 遍历配置文件信息
            for (Object key : properties.keySet()) {
                // 获取一个对于的全限定类名，通过反射创建对象
                // System.out.println(o + properties.getProperty((String) o));

                Class clazz = Class.forName(properties.getProperty((String) key));
                Coffee coffee = (Coffee) clazz.newInstance();
                coffees.put((String) key, coffee);
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    public static Coffee createCoffee(String name) {
        return coffees.get(name);
    }
}
