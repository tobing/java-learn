package top.tobing.pattern.factory.demo05;

/**
 * @Author tobing
 * @Date 2020/12/30 21:44
 * @Description 配置文件 + 简单工厂实现灵活配置
 */
public class PropertiesAndSimpleFactoryDemo {
    public static void main(String[] args) {
        Coffee latte = CaffeeFactory.createCoffee("latte");
        Coffee american = CaffeeFactory.createCoffee("american");
        System.out.println(latte);
        System.out.println(american);
    }
}
