package top.tobing.pattern.factory.demo05;

/**
 * @Author tobing
 * @Date 2020/12/30 21:04
 * @Description 具体产品-LatteCoffee
 */
public class LatteCoffee implements Coffee {
    @Override
    public String getName() {
        return "拿铁";
    }

    @Override
    public void addMilk() {
        System.out.println("拿铁Coffee添加牛奶");
    }

    @Override
    public void addSugar() {
        System.out.println("拿铁Coffee添加糖");
    }
}
