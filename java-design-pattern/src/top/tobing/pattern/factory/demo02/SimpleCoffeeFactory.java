package top.tobing.pattern.factory.demo02;

/**
 * @Author tobing
 * @Date 2020/12/30 20:49
 * @Description 简单Coffee工厂
 * 之前的程序中，如果添加了一种新的Coffee种类，就要去修改CoffeeStore的编码。
 * 使用工厂可以使得CoffeeStore与Coffee的耦合度降低，但是会产生CoffeeStore与SimpleCoffeeFactory、
 * Coffee与SimpleCoffeeFactory之间的新耦合
 */
public class SimpleCoffeeFactory {

    public Coffee createCoffee(String type) {
        Coffee coffee = null;
        if ("latte".equalsIgnoreCase(type)) {
            coffee = new LatteCoffee();
        } else if ("american".equalsIgnoreCase(type)) {
            coffee = new AmericanCoffee();
        } else {
            throw new IllegalArgumentException("没有这种咖啡！");
        }
        return coffee;
    }
}
