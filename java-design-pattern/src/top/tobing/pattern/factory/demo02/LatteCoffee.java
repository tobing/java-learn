package top.tobing.pattern.factory.demo02;

/**
 * @Author tobing
 * @Date 2020/12/30 20:37
 * @Description 拿铁咖啡
 */
public class LatteCoffee extends Coffee {
    @Override
    public String name() {
        System.out.println("拿铁");
        return "拿铁";
    }
}
