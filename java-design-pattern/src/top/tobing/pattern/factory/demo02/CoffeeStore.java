package top.tobing.pattern.factory.demo02;

/**
 * @Author tobing
 * @Date 2020/12/30 20:40
 * @Description
 */
public class CoffeeStore {

    public Coffee orderCoffee(String type) {
        // 不再直接判断创建Coffee，而是通过SimpleCoffeeFactory来创建Coffee
        // 可以降低CoffeeStore与Coffee之间的耦合
        // 【存在问题】
        // 使用Factory模式，可以封装创建某种Coffee创建的细节，可以通过参数直接获取对象。
        // 把对象的创建于业务逻辑分类，避免修改用户代码，当有新的Coffee的时候，修改Factory即可。

        SimpleCoffeeFactory factory = new SimpleCoffeeFactory();
        return factory.createCoffee(type);
    }

}
