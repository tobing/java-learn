package top.tobing.pattern.factory.demo06;

import sun.util.BuddhistCalendar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Author tobing
 * @Date 2020/12/30 22:07
 * @Description JDK中实现了【工厂方法模型】的实例
 */
public class JDKFactoryImpl {
    public static void main(String[] args) {
        // Collection.iterator()方法
        // Collection的每个子类都具有Iterator行为
        // Collection       --->    抽象工厂
        // Iterator         --->    抽象产品
        // ArrayList        --->    具体工厂【Collection的实现】
        // ArrayList.Itr    --->    具体产品【Iterator的实现】
        // 通过工厂模式，使得Collection每天一个子类（容器类），就要创建一个对应的遍历器（Iterator）
        // 因为不同容器编译的方式不同，因此这样也很合理
        ArrayList<Object> list = new ArrayList<>();
        list.add("风清扬");
        list.add("令狐冲");
        list.add("逍遥子");
        Iterator<Object> iterator = list.iterator();

        // DateFormat同样使用了工厂模式-【简单工厂模式】
        DateFormat instance = DateFormat.getInstance();
        // DateFormat   -->     工厂
        /** DateFormat部分源码
         *     private static DateFormat get(LocaleProviderAdapter adapter, int timeStyle, int dateStyle, Locale loc) {
         *         DateFormatProvider provider = adapter.getDateFormatProvider();
         *         DateFormat dateFormat;  // 此处通过工厂模式来创建不同的DataFormat
         *         if (timeStyle == -1) {
         *             dateFormat = provider.getDateInstance(dateStyle, loc);
         *         } else {
         *             if (dateStyle == -1) {
         *                 dateFormat = provider.getTimeInstance(timeStyle, loc);
         *             } else {
         *                 dateFormat = provider.getDateTimeInstance(dateStyle, timeStyle, loc);
         *             }
         *         }
         *         return dateFormat;
         *     }
         */


        // Calendar同样使用了工厂模式----【简单工厂模式】
        Calendar calendar = Calendar.getInstance();
        /**Calendar源码
         *         Calendar cal = null;
         *
         *         if (aLocale.hasExtensions()) {
         *             String caltype = aLocale.getUnicodeLocaleType("ca"); // 不同的caltype创建不同的对象
         *             if (caltype != null) {
         *                 switch (caltype) {
         *                 case "buddhist":
         *                 cal = new BuddhistCalendar(zone, aLocale);
         *                     break;
         *                 case "japanese":
         *                     cal = new JapaneseImperialCalendar(zone, aLocale);
         *                     break;
         *                 case "gregory":
         *                     cal = new GregorianCalendar(zone, aLocale);
         *                     break;
         *                 }
         *             }
         *         }
         */
    }
}
