package top.tobing.pattern.factory.demo01.before;

/**
 * @Author tobing
 * @Date 2020/12/30 20:35
 * @Description 咖啡类
 */
public abstract class Coffee {

    // 获取咖啡名称
    public abstract String name();

    // 加牛奶
    public void addMilk() {
        System.out.println("加牛奶");
    }

    // 加糖
    public void addSugar() {
        System.out.println("加糖");
    }
}
