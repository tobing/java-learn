package top.tobing.pattern.factory.demo01.before;

/**
 * @Author tobing
 * @Date 2020/12/30 20:40
 * @Description
 */
public class CoffeeStore {

    public Coffee orderCoffee(String type) {
        Coffee coffee = null;
        if ("latte".equalsIgnoreCase(type)) {
            coffee = new LatteCoffee();
        } else if ("american".equalsIgnoreCase(type)) {
            coffee = new AmericanCoffee();
        } else {
            throw new IllegalArgumentException("没有这种咖啡！");
        }
        return coffee;
    }

}
