package top.tobing.pattern.factory.demo01.before;

/**
 * @Author tobing
 * @Date 2020/12/30 20:35
 * @Description 咖啡店点餐系统
 * Java中，万物皆对象，这些对象都要创建，如果创建的时候直接使用new的方式，将会导致程序的对象间的耦合度提高，
 * 假如我们要更换对象的时候，所有使用new的地方都要重新编码修改，这显然违背了软件设计的【开闭原则】。
 * 如果我们使用工厂来帮助我们来修改不同的对象，达到与对象解耦合的目的。
 * 工厂模式的最大优点就是：：：：解耦
 */
public class CoffeeDemo {
    public static void main(String[] args) {
        CoffeeStore coffeeStore = new CoffeeStore();
        // 创建拿铁
        Coffee latte = coffeeStore.orderCoffee("latte");
        latte.addMilk();
        latte.addSugar();
        latte.name();

        System.out.println("===============================");
        // 创建美式
        Coffee american = coffeeStore.orderCoffee("american");
        american.addSugar();
        american.addMilk();
        american.name();

        // 不存在的咖啡
        Coffee coffee = coffeeStore.orderCoffee("coffee");
        coffee.addSugar();


    }
}
