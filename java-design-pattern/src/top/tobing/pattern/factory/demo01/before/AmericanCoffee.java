package top.tobing.pattern.factory.demo01.before;

/**
 * @Author tobing
 * @Date 2020/12/30 20:37
 * @Description 美式咖啡
 */
public class AmericanCoffee extends Coffee {
    @Override
    public String name() {
        System.out.println("美式咖啡");
        return "美式";
    }
}
