package top.tobing.pattern.decorator;

/**
 * @Author tobing
 * @Date 2021/1/3 16:38
 * @Description 快餐
 */
public abstract class FastFood {
    // 快餐价格
    private float price;
    // 快餐描述
    private String desc;

    public FastFood() {
    }

    public FastFood(float price, String desc) {
        this.price = price;
        this.desc = desc;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    // 快餐价格
    public abstract float cost();
}
