package top.tobing.pattern.decorator;

/**
 * @Author tobing
 * @Date 2021/1/3 16:39
 * @Description 炒饭
 */
public class FiredRice extends FastFood {


    public FiredRice() {
        super(10, "炒饭");
    }

    @Override
    public float cost() {
        return getPrice();
    }
}
