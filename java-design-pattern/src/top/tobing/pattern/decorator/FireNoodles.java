package top.tobing.pattern.decorator;

/**
 * @Author tobing
 * @Date 2021/1/3 16:42
 * @Description 炒面
 */
public class FireNoodles extends FastFood {


    public FireNoodles() {
        super(12, "炒面");
    }

    @Override
    public float cost() {
        return getPrice();
    }
}
