package top.tobing.pattern.decorator;

/**
 * @Author tobing
 * @Date 2021/1/3 16:31
 * @Description 装饰者模式
 *  一快餐店有炒面、炒饭两种快餐。可以额外添加鸡蛋、火腿、培根这些配菜，加配菜需要加钱。
 *  每个配菜的价钱通常不一样。如果每加一样配菜就添加一个子类，那么容易出现类爆炸。
 *
 *  装饰者模式：在不改变现有对象的情况下，动态地给这些对象增加一些额外的功能。
 *   抽象构件（Component）：顶一个抽象接口规范准备接受附加的对象
 *   具体构件（Concrete Component）；实现抽象构建，通过装饰爵士为其添加一些职责
 *   抽象修饰（Decorator）；继承或实现抽象构件，并包含具体的构建实例，可以通过其子类扩展具体构件的功能。
 *   具体修饰（Concrete Decorator）：实现抽象装饰的相关方法，给具体构建对象添加附加的责任。
 */
public class DecoratorDemo {
}
