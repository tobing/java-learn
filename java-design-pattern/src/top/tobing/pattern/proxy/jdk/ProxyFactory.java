package top.tobing.pattern.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @Author tobing
 * @Date 2021/1/3 12:31
 * @Description
 */
public class ProxyFactory {

    private TrainStation station = new TrainStation();

    // 通过JDKProxy类动态生成代理对象并返回
    public SaleTicket getProxyObject() {
        /** newProxyInstance 方法参数
         * loader – the class loader to define the proxy class                  // 被代理类的ClassLoader
         * interfaces – the list of interfaces for the proxy class to implement // 被代理类实现的接口
         * h – the invocation handler to dispatch method invocations to         // 调用处理程序
         */
        // 通过newProxyInstance方法动态生成代理对象
        SaleTicket proxyInstance = (SaleTicket) Proxy.newProxyInstance(station.getClass().getClassLoader(),
                station.getClass().getInterfaces(), (proxy, method, args) -> {
                    System.out.println("JDK:动态代理增强前");
                    // 执行被代理对象的方法
                    // the object the underlying method is invoked from
                    String ret = (String) method.invoke(station, args);
                    System.out.println("JDK:动态代理增强前");
                    return "JDK: " + ret;
                }
        );
        return proxyInstance;
    }
}
