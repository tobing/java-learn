package top.tobing.pattern.proxy.jdk;

/**
 * @Author tobing
 * @Date 2021/1/3 12:17
 * @Description 抽象主题类（Subject）
 */
public interface SaleTicket {

    // 售票
    public String sell();
}
