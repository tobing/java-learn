package top.tobing.pattern.proxy.jdk;


/**
 * @Author tobing
 * @Date 2021/1/3 12:27
 * @Description 代理模式 -- JDK动态代理
 * JDK提供了一个动态代理类Proxy，Proxy可以在程序运行的时候通过newProxyInstance方法动态创建代理对象{ProxyObject}并返回。
 */
public class Main {
    public static void main(String[] args) {
        ProxyFactory factory = new ProxyFactory();
        SaleTicket prox = factory.getProxyObject();
        String res = prox.sell();
        System.out.println(res);
        System.out.println(prox.getClass().getName()      );
        while (true) {
        }
    }
}
