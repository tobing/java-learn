package top.tobing.pattern.proxy.cglib;

/**
 * @Author tobing
 * @Date 2021/1/3 13:57
 * @Description
 */
public class Main {
    public static void main(String[] args) {
        ProxyFactory factory = new ProxyFactory();
        TrainStation proxyObject = factory.getProxyObject();
        proxyObject.sell();
    }
}
