package top.tobing.pattern.proxy.cglib;


import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @Author tobing
 * @Date 2021/1/3 14:00
 * @Description
 *
 */
public class ProxyFactory implements MethodInterceptor {

    private TrainStation station = new TrainStation();

    public TrainStation getProxyObject() {
        Enhancer enhancer = new Enhancer();
        // 设置父类字节码对象
        enhancer.setSuperclass(TrainStation.class);
        // 设置回调
        enhancer.setCallback(this);
        TrainStation station = (TrainStation) enhancer.create();
        return station;
    }


    @Override
    public String intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("CGLib:代理增强前");
        String res = (String) methodProxy.invokeSuper(o, objects);
        System.out.println("CGLib:代理增强后");
        return "CGLib:" + res;
    }
}
