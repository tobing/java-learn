package top.tobing.pattern.proxy.stat;

/**
 * @Author tobing
 * @Date 2021/1/3 12:20
 * @Description 代理类 -- 代理点类
 */
public class ProxyPoint implements SaleTicket {

    private TrainStation station = new TrainStation();

    @Override
    public String sell() {
        System.out.println("ProxyPoint:增强前");
        String ret = station.sell();
        System.out.println("ProxyPoint:增强后");
        return "ProxyPoint:" + ret;
    }
}
