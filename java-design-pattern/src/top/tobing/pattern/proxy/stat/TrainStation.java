package top.tobing.pattern.proxy.stat;

/**
 * @Author tobing
 * @Date 2021/1/3 12:19
 * @Description 具体主题类 -- 火车站类
 */
public class TrainStation implements SaleTicket{
    @Override
    public String sell() {
        System.out.println("TrainStation:售票！！！");
        return "TrainStation:B112";
    }
}
