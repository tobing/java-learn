package top.tobing.pattern.proxy.stat;

/**
 * @Author tobing
 * @Date 2021/1/3 12:22
 * @Description 代理模式 -- 静态代理
 * 当我们去买票的时候不直接去火车站{TrainStation}，而是通过代理点{ProxyPoint}买票；
 * 代理点{ProxyPoint}持有火车站{TrainStation}的引用，可以帮我们去买票{sell}；
 * 在帮我们买票的时候可以直接，代理点{ProxyPoint}可以增强火车站{TrainStation}买票的功能，比如：收一下手续费
 * <p>
 * 存在问题：
 * 当是实现的SaleTicket接口添加方法的时候，无论是火车站，还是代理点，都要重新实现。
 */
public class Main {
    public static void main(String[] args) {
        ProxyPoint proxyPoint = new ProxyPoint();
        String sell = proxyPoint.sell();
        System.out.println(sell);
    }
}
