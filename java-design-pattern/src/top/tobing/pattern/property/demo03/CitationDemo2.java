package top.tobing.pattern.property.demo03;

/**
 * @Author tobing
 * @Date 2021/1/2 21:51
 * @Description
 */
public class CitationDemo2 {
    public static void main(String[] args) throws CloneNotSupportedException {
        Citation c1 = new Citation();
        Citation c2 = c1.clone();

        Student stu1 = new Student();
        stu1.setName("tobing");

        c1.setStudent(stu1);
        c2.setStudent(stu1);
        c2.getStudent().setName("zenyet");

        c1.show();
        c2.show();
        //zenyet同学：在2021学年的第一学期中表现优秀，被评为三好学生。特发此状！
        //zenyet同学：在2021学年的第一学期中表现优秀，被评为三好学生。特发此状！

        System.out.println(c1.getStudent());
        System.out.println(c2.getStudent());
        //top.tobing.pattern.property.demo03.Student@1b6d3586
        //top.tobing.pattern.property.demo03.Student@1b6d3586

        /**
         * 从输出结果分析可以知道:当被克隆对象属性包含引用数据类型的时候，是直接复制引用地址。
         * 这就会导致被复制的对象修改了引用的对象是，原对象也会被影响。我们称这种方式为「浅拷贝」
         */

        // 「浅拷贝」创建一个新对象，新对象的属性和原来对象完全一样，对于非基本数据类型，仍指向原来对象属性所指向的对象的内存地址
        // 「深拷贝」创建一个新对象，属性中引用的其他对象也会被克隆，不再指向原有对象地址。
    }
}
