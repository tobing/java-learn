package top.tobing.pattern.property.demo04;

import java.io.*;

/**
 * @Author tobing
 * @Date 2021/1/2 21:51
 * @Description 深拷贝方式1 -- 对象序列化
 */
public class CitationDemo3 {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Citation c1 = new Citation();
        Student stu = new Student();
        c1.setStudent(stu);

        // 将对象序列化到本地文件中 「要保证对象实现了Serializable」
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("D:\\a.txt"));
        oos.writeObject(c1);
        // 关闭资源
        oos.close();

        // 将本地文件反序列为对象
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("D:\\a.txt"));
        Citation c2 = (Citation) ois.readObject();
        // 关闭资源
        ois.close();


        c1.getStudent().setName("tobing");
        c2.getStudent().setName("zenyet");

        c1.show();
        c2.show();

        System.out.println(c1);
        System.out.println(c2);

        System.out.println(c1.getStudent());
        System.out.println(c2.getStudent());

        // 输出
        //tobing同学：在2021学年的第一学期中表现优秀，被评为三好学生。特发此状！
        //zenyet同学：在2021学年的第一学期中表现优秀，被评为三好学生。特发此状！
        //top.tobing.pattern.property.demo04.Citation@7f31245a
        //top.tobing.pattern.property.demo04.Citation@7ef20235
        //top.tobing.pattern.property.demo04.Student@6d6f6e28
        //top.tobing.pattern.property.demo04.Student@27d6c5e0
    }
}
