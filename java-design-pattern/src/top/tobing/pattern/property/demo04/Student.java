package top.tobing.pattern.property.demo04;

import java.io.Serializable;

/**
 * @Author tobing
 * @Date 2021/1/2 21:49
 * @Description
 */
public class Student implements Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
