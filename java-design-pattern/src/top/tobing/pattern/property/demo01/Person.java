package top.tobing.pattern.property.demo01;

/**
 * @Author tobing
 * @Date 2021/1/2 21:38
 * @Description 具体原型类-被复制的类
 */
public class Person implements Cloneable {

    public Person() {
        System.out.println("Person构造方法被调用！！！");
    }

    @Override
    public Person clone() throws CloneNotSupportedException {
        System.out.println("clone方法被调用");
        // clone是native方法
        return (Person) super.clone();
    }
}
