package top.tobing.pattern.property.demo01;

/**
 * @Author tobing
 * @Date 2021/1/2 21:35
 * @Description 原型模式
 * 用一个已经创建的实例作为原型，通过复制（clone）该原型对象来创建一个和原型对象相同的新兑现。
 * 原型模式中包含以下角色：
 * 抽象原型类：具有clone()方法的接口 --> Cloneable
 * 具体原型类：实现了抽象原型类clone()方法，是可以被复制的类。
 * 访问类：使用具体原型类中的clone()方法来复制新的对象。
 */
public class PropertyDemo {
    public static void main(String[] args) throws CloneNotSupportedException {
        Person p1 = new Person();
        System.out.println("===========执行克隆前==========");
        // 从输出可以知道，clone并不调用构造方法
        Person p2 = p1.clone();
        // 可以看到以下两个对象是不同的对象
        System.out.println(p1); // top.tobing.pattern.property.demo01.Person@1b6d3586
        System.out.println(p2); // top.tobing.pattern.property.demo01.Person@4554617c

    }
}
