package top.tobing.pattern.property.demo02;

/**
 * @Author tobing
 * @Date 2021/1/2 21:47
 * @Description
 */
public class CitationDemo {
    public static void main(String[] args) throws CloneNotSupportedException {
        Citation c1 = new Citation();
        Citation c2 = c1.clone();

        c1.setName("tobing");
        c2.setName("zenyet");

        System.out.println(c1);
        System.out.println(c2);
        c1.show();
        c2.show();
    }
}
