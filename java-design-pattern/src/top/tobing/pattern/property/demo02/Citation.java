package top.tobing.pattern.property.demo02;

/**
 * @Author tobing
 * @Date 2021/1/2 21:45
 * @Description 原型模式使用场景--奖状模板
 */
public class Citation implements Cloneable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void show() {
        System.out.println(name + "同学：在2021学年的第一学期中表现优秀，被评为三好学生。特发此状！");
    }

    @Override
    protected Citation clone() throws CloneNotSupportedException {
        return (Citation) super.clone();
    }
}
