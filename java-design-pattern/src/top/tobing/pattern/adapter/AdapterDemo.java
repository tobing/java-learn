package top.tobing.pattern.adapter;

/**
 * @Author tobing
 * @Date 2021/1/3 15:07
 * @Description 结构型模式 -- 适配器模式
 * 适配器模式主要有以下三种角色：
 * 目标（target）：要去被适配的对象，可能是第三方。如电脑的USB接口。
 * 适配者（Adaptee）：要取和目标适配的对象，如SD卡。
 * 适配器（Adapter）：能够将适配者通过转换，使其能够适配目标，如读卡器。
 *
 * 如：一台电脑只能读取 SD 卡中的数据，而想要读取 TF 中的数据，则需要通过适配器模式。
 */
public class AdapterDemo {

}
