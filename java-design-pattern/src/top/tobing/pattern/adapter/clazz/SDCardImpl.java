package top.tobing.pattern.adapter.clazz;

/**
 * @Author tobing
 * @Date 2021/1/3 15:13
 * @Description
 */
public class SDCardImpl implements SDCard {
    @Override
    public void writeSD(String msg) {
        System.out.println("SD卡写数据:" + msg);
    }

    @Override
    public String readSD() {
        return "SD: { Hello world }";
    }
}
