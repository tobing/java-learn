package top.tobing.pattern.adapter.clazz;

/**
 * @Author tobing
 * @Date 2021/1/3 15:18
 * @Description 适配器模式1 -- 类适配器模式
 * 类适配器模式违反了合成服用原则，即：尽量使用组合、依赖来代替继承。
 * 在类适配器模式下，如果目标无实现接口，那么适配器只能继承目标接口，此时适配器无法再去继承适配者，因为Java是单继承的
 */
public class Main {
    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.readSD(new SDCardImpl());
        System.out.println("=============================");
        computer.readSD(new TF2SDAdapter());
    }
}
