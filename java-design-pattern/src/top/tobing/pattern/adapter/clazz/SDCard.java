package top.tobing.pattern.adapter.clazz;

/**
 * @Author tobing
 * @Date 2021/1/3 15:12
 * @Description
 */
public interface SDCard {
    // 写数据
    public void writeSD(String msg);

    // 读数据
    public String readSD();
}
