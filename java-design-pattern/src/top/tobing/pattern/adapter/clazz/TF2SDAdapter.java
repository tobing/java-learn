package top.tobing.pattern.adapter.clazz;

/**
 * @Author tobing
 * @Date 2021/1/3 15:17
 * @Description TF 卡转 SD 适配器
 */
public class TF2SDAdapter extends TFCardImpl implements SDCard {


    @Override
    public void writeSD(String msg) {
        writeTF(msg);
    }

    @Override
    public String readSD() {
        return readTF();
    }
}
