package top.tobing.pattern.adapter.clazz;

/**
 * @Author tobing
 * @Date 2021/1/3 15:14
 * @Description
 */
public interface TFCard {

    // 读TF卡
    public String readTF();

    // 写TF卡
    public void writeTF(String msg);
}
