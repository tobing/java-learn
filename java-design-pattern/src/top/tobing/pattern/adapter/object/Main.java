package top.tobing.pattern.adapter.object;

/**
 * @Author tobing
 * @Date 2021/1/3 15:23
 * @Description 适配者模式 -- 对象适配模式
 */
public class Main {
    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.readSD(new SDCardImpl());
        System.out.println("===============================");
        computer.readSD(new TF2SDAdapter(new TFCardImpl()));
    }

    /**
     * JDK源码中对适配器模式的运用
     * InputStreamReader ： 将字节流转化为字符流
     * ---------------------------------------------
     * InputStream ------StreamDecoder--------> Reader
     *      |                 |                  |
     *      |                 |                  |
     *    适配者            适配器               目标
     * ---------------------------------------------
     * public class StreamDecoder extends Reader {
     *     ......
     *     private InputStream in;
     *     ......
     * }
     */
}
