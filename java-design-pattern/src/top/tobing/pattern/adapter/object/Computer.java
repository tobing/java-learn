package top.tobing.pattern.adapter.object;

/**
 * @Author tobing
 * @Date 2021/1/3 15:16
 * @Description Computer只会读取SD卡数据，不能读取TF卡的数据
 */
public class Computer {
    public void readSD(SDCard sdCard) {
        String msg = sdCard.readSD();
        System.out.println(msg);
    }
}
