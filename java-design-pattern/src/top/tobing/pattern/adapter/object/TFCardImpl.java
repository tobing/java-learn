package top.tobing.pattern.adapter.object;

/**
 * @Author tobing
 * @Date 2021/1/3 15:15
 * @Description
 */
public class TFCardImpl implements TFCard {
    @Override
    public String readTF() {
        return "TF: { Hello Tobing }";
    }

    @Override
    public void writeTF(String msg) {
        System.out.println("TF卡写数据：" + msg);
    }
}
