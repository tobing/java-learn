package top.tobing.pattern.adapter.object;

/**
 * @Author tobing
 * @Date 2021/1/3 15:17
 * @Description TF 卡转 SD 适配器
 */
public class TF2SDAdapter extends SDCardImpl {

    private TFCard tfCard;

    public TF2SDAdapter(TFCard tfCard) {
        this.tfCard = tfCard;
    }

    @Override
    public void writeSD(String msg) {
        if (tfCard == null) {
            throw new IllegalArgumentException("TFCard不能为空");
        }
        tfCard.writeTF(msg);
    }

    @Override
    public String readSD() {
        if (tfCard == null) {
            throw new IllegalArgumentException("TFCard 不能为空");
        }
        return tfCard.readTF();
    }
}
