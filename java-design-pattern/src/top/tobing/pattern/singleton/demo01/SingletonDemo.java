package top.tobing.pattern.singleton.demo01;

/**
 * @Author tobing
 * @Date 2020/12/30 16:44
 * @Description
 */
public class SingletonDemo {
    public static void main(String[] args) {
        Singleton singleton = Singleton.getInstance();
        Singleton instance1 = Singleton.getInstance();

        System.out.println(singleton == instance1);
    }
}
