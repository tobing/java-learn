package top.tobing.pattern.singleton.demo01;

/**
 * @Author tobing
 * @Date 2020/12/30 16:42
 * @Description 单例设计模式实现1
 * 饿汉式：静态初始化
 */
public class Singleton {
    // 1. 私有构造器
    private Singleton() {
    }

    // 2. 静态初始化
    private static Singleton instance = new Singleton();

    // 3. 公共方法
    public static Singleton getInstance() {
        return instance;
    }
}
