package top.tobing.pattern.singleton.demo10;

import java.io.IOException;
import java.io.InputStream;

/**
 * @Author tobing
 * @Date 2020/12/30 20:25
 * @Description JDK-Runtime
 * Runtime中对单例设计模式进行了实现，使用的是饿汉式静态初始化
 */
public class RuntimeDemo {
    public static void main(String[] args) throws IOException {
        // 分别获取两次Runtime，判断是否为单例
//        Runtime runtime = Runtime.getRuntime();
//        Runtime runtime1 = Runtime.getRuntime();
//        System.out.println(runtime == runtime1);

        // 使用Runtime
        Runtime runtime = Runtime.getRuntime();
        Process process = runtime.exec("ping www.baidu.com");
        InputStream is = process.getInputStream();
        // 定义1MB缓冲器储存输出的信息
        byte[] buffer = new byte[100 * 1024 * 1024];
        int len = is.read(buffer);
        String res = new String(buffer,0,len,"GBK");
        System.out.println(len);
        System.out.println(res);
    }
}
