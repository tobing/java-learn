package top.tobing.pattern.singleton.demo05;

/**
 * @Author tobing
 * @Date 2020/12/30 16:52
 * @Description 单例设计模式
 * 饿汉式：双重校验锁机制
 */
public class Singleton {

    // 解决：通过反射的方式破坏设计模式
    private Singleton() {
        synchronized (Singleton.class) {
            if (instance != null) {
                throw new RuntimeException("不能重复创建对象");
            }
        }
    }

    // volatile解释
    // 对象在创建的时候分为三个步骤
    // 1)  memory = allocate        分配物理内存
    // 2) instance(memory)          初始化对象
    // 3) instance = memory         将对象地址赋值给instance
    // 由于JVM底层存在对上面做的指令重排序的问题，
    // 可能会导致2）先发生于3）
    // 这在单线程的模式下是没有问题的，但是多线程环境下，当3）发生的时候，instance已经!=null，但是instance还未初始化成功。
    // 这是就会导致return的instance还未执行3），即还未初始化完成。
    // 通过volatile可以避免JVM对instance初始化过程的指令重排序，从而保证了对象分配的安全
    private static volatile Singleton instance;

    public static Singleton getInstance() {
        if (instance == null) {
            synchronized (Singleton.class) {
                if (instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }
}
