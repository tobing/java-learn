package top.tobing.pattern.singleton.demo05;


/**
 * @Author tobing
 * @Date 2020/12/30 17:12
 * @Description
 */
public class SingletonDemo {
    public static void main(String[] args) {

        long start = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            Singleton singleton = Singleton.getInstance();
        }
        long end = System.currentTimeMillis();
        // 7
        System.out.println("一共花费：" + (end - start));
    }
}
