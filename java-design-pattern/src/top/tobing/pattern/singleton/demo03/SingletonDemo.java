package top.tobing.pattern.singleton.demo03;

/**
 * @Author tobing
 * @Date 2020/12/30 16:49
 * @Description
 */
public class SingletonDemo {
    public static void main(String[] args) {
//        Singleton instance1 = Singleton.getInstance();
//        Singleton instance2 = Singleton.getInstance();
//
//        System.out.println(instance1 == instance2);


        new Thread(() -> {
            Singleton instance = Singleton.getInstance();
            System.out.println(instance);
        }).start();

        new Thread(() -> {
            Singleton instance = Singleton.getInstance();
            System.out.println(instance);
        }).start();

        new Thread(() -> {
            Singleton instance = Singleton.getInstance();
            System.out.println(instance);
        }).start();

        new Thread(() -> {
            Singleton instance = Singleton.getInstance();
            System.out.println(instance);
        }).start();


        // 在多线程环境下，创建了多个对象，因此是不安全的。
        //top.tobing.pattern.singleton.demo03.Singleton@710a52b
        //top.tobing.pattern.singleton.demo03.Singleton@377839d0
        //top.tobing.pattern.singleton.demo03.Singleton@456a2b6
        //top.tobing.pattern.singleton.demo03.Singleton@456a2b6
    }
}
