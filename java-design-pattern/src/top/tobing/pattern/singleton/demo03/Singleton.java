package top.tobing.pattern.singleton.demo03;

/**
 * @Author tobing
 * @Date 2020/12/30 16:47
 * @Description 单例设计模式实现3
 * 懒汉式：线程不安全
 */
public class Singleton {

    private Singleton() {
    }

    private static Singleton singleton;

    public static Singleton getInstance() {
        if (singleton == null) {
            singleton = new Singleton();
        }
        return singleton;
    }

}
