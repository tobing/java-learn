package top.tobing.pattern.singleton.demo04;

/**
 * @Author tobing
 * @Date 2020/12/30 17:00
 * @Description 单例设计模式实现4
 * 懒汉式：同步方法
 */
public class Singleton {
    private Singleton() {
    }

    private static Singleton instance;

    // 同步方法的缺点
    // 同步方法将整个方法都加锁，降低了并发的性能
    // 分析可以知道，当读取Singleton的时候，是不会影响线程的安全的。
    // 而且，在对象创建之后，大部分的场景都是读。
    // 通过同步方法的方式，会阻塞读、写线程，使得性能大大下降
    // 有什么方法可以进行改进呢？双重校验锁机制
    public synchronized static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
        }
        return instance;
    }
}
