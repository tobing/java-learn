package top.tobing.pattern.singleton.demo04;


/**
 * @Author tobing
 * @Date 2020/12/30 17:07
 * @Description
 */
public class SingletonDemo {
    public static void main(String[] args) {

        long start = System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            Singleton singleton = Singleton.getInstance();
        }
        long end = System.currentTimeMillis();
        // 211
        System.out.println("一共花费：" + (end - start));
    }
}
