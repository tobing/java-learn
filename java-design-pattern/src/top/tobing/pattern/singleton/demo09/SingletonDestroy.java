package top.tobing.pattern.singleton.demo09;

import top.tobing.pattern.singleton.demo06.Singleton;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @Author tobing
 * @Date 2020/12/30 18:03
 * @Description 单例设计模式
 * 反射方式
 */
public class SingletonDestroy {
    public static void main(String[] args) throws Exception {
        // 获取Singleton字节码对象
        Class clazz = Singleton.class;
        // 获取Singleton类的所有无参构造方法
        Constructor cons = clazz.getDeclaredConstructor();
        // 取消访问修饰访问
        cons.setAccessible(true);
        // 创建对象
        Singleton instance1 = (Singleton) cons.newInstance();
        Singleton instance2 = (Singleton) cons.newInstance();

        // 判断是否同一个对象
        System.out.println(instance1);
        System.out.println(instance2);
        // top.tobing.pattern.singleton.demo06.Singleton@1b6d3586
        // top.tobing.pattern.singleton.demo06.Singleton@4554617c
    }
}
