package top.tobing.pattern.singleton.demo06;

import java.io.Serializable;

/**
 * @Author tobing
 * @Date 2020/12/30 17:21
 * @Description 单例设计模式实现6
 * 饿汉式：静态内部类
 */
public class Singleton implements Serializable {

    // 解决：反射方式破坏单例设计模式
    private Singleton() {
        throw new RuntimeException("不能通过此方式创建对象");
    }

    // 静态内部类
    // JVM在加载外部类的时候，不会加载静态内部类，只有内部类的属性、方法被调用的时候，才会被加载，并初始化静态属性。
    // 静态属性被static修饰确保只会被实例化一次，并且严格保证实例化顺序（静止指令重排序）
    private static class SingletonHolder {
        private static final Singleton INSTANCE = new Singleton();
    }

    // 第一次加载Singleton类时，不会取初始化INSTACNCE，只有第一次调用getInstance方法时，
    // 虚拟机会加载SigletonHolder，并初始化INSTANCE，这样可以保证线程安全，也可以保证Singleton类唯一性。
    // 这种方式在开源项目中比较常见，在没有加任何锁的情况下，保证了多线程的安全。没有任何的性能影响和空间浪费。
    public static Singleton getInstance() {
        return SingletonHolder.INSTANCE;
    }

    // 可用用来解决：序列化与反序列化破坏单例设计模式的问题
    private Object readResolve() {
        return SingletonHolder.INSTANCE;
    }
}
