package top.tobing.pattern.singleton.demo07;

/**
 * @Author tobing
 * @Date 2020/12/30 17:38
 * @Description 单例设计模式方式7
 * 饿汉式：枚举
 *
 * 枚举实现是极力推荐的一种，因为枚举类型是线程安全的，并且只会装载一次，设计者充分利用了枚举的这特性来实现单例设计模式。
 * 并且枚举类型是唯一一种不会被破坏的单例设计模式。
 */
public enum Singleton {
    INSTANCE;
}
