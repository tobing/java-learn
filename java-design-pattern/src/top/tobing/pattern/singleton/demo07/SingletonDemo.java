package top.tobing.pattern.singleton.demo07;

/**
 * @Author tobing
 * @Date 2020/12/30 17:40
 * @Description
 */
public class SingletonDemo {
    public static void main(String[] args) {
        Singleton instance1 = Singleton.INSTANCE;
        Singleton instance2 = Singleton.INSTANCE;

        System.out.println(instance1 == instance2);
    }
}
