package top.tobing.pattern.singleton.demo02;

/**
 * @Author tobing
 * @Date 2020/12/30 16:45
 * @Description 单例设计模式实现2
 * 饿汉式：静态代码块初始化
 */
public class Singleton {
    private Singleton() {
    }

    private static Singleton instance;

    static {
        instance = new Singleton();
    }

    public static Singleton getInstance() {
        return instance;
    }
}
