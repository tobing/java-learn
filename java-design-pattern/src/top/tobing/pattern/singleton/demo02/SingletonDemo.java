package top.tobing.pattern.singleton.demo02;

/**
 * @Author tobing
 * @Date 2020/12/30 16:46
 * @Description
 */
public class SingletonDemo {
    public static void main(String[] args) {
        Singleton singleton1 = Singleton.getInstance();
        Singleton singleton2 = Singleton.getInstance();

        System.out.println(singleton1 == singleton2);
    }
}
