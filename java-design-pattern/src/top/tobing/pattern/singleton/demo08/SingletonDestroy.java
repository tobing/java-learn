package top.tobing.pattern.singleton.demo08;

import top.tobing.pattern.singleton.demo06.Singleton;

import java.io.*;

/**
 * @Author tobing
 * @Date 2020/12/30 17:53
 * @Description 单例设计模式的破坏方式1
 * 序列化与反序列化方式
 */
public class SingletonDestroy {
    public static void main(String[] args) throws Exception{
        // 将对象序列化为文件
        // writeObject2File();
        // 从文件中读取对象
        Singleton instance1 = readObject2File();
        Singleton instance2 = readObject2File();

        System.out.println(instance1);
        System.out.println(instance2);

    }

    private static Singleton readObject2File() throws Exception {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("D:\\a.txt"));
        Singleton singleton = (Singleton)ois.readObject();
        return singleton;
    }

    // 将Singleton序列化到本地
    private static void writeObject2File() throws IOException {
        Singleton instance = Singleton.getInstance();
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("D:\\a.txt"));
        oos.writeObject(instance);
    }
}
