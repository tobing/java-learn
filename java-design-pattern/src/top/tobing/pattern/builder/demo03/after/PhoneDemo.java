package top.tobing.pattern.builder.demo03.after;

/**
 * @Author tobing
 * @Date 2021/1/2 23:09
 * @Description
 */
public class PhoneDemo {
    public static void main(String[] args) {
        // 这种方式参数多，可读性差，使用成本高
        Phone phone = new Phone("Intel", "三星", "金士顿", "技嘉");
        System.out.println(phone);
    }
}
