package top.tobing.pattern.builder.demo03.before;

/**
 * @Author tobing
 * @Date 2021/1/2 23:09
 * @Description
 */
public class PhoneDemo {
    public static void main(String[] args) {
        // 通过添加Build内部类，提高创建的可读性
        // 链式编程
        Phone phone = new Phone.Builder()
                .cpu("骁龙")
                .mainboard("台积电")
                .memory("镁光")
                .screen("三星")
                .build();
        System.out.println(phone);
    }
}
