package top.tobing.pattern.builder.demo01;

/**
 * @Author tobing
 * @Date 2021/1/2 22:56
 * @Description
 */
public class Director {
    private Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }

    // 将各个组件组装为Bike
    public Bike construct() {
        builder.buildFrame();
        builder.buildSeat();
        return builder.createBike();
    }
}
