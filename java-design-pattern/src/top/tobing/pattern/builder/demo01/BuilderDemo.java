package top.tobing.pattern.builder.demo01;

/**
 * @Author tobing
 * @Date 2021/1/2 22:48
 * @Description 建造者模式
 * 将一个复杂对象的关键与表示分离，使得同样的构建过程可以创建不同的对象。
 * <p>
 * 建造者模式分离了部件的构建{builder}与装配{Director}，从而可以构建出复杂的对象。
 * 构建者模式适合于某个对象的构建过程比较复制。
 * 构建者模式实现了构建与装配的解耦合，不同的构建器，相同的装配，可以得出不同的对象；相同的构建器，不同的装配顺序也可以得出不同对象。
 * <p>
 * 构建者模式主要有以下角色：
 * 「抽象建造者」Builder，规定实现复杂对象的那些部分的创建，不涉及具体部件的对象创建。
 * 「具体建造者」ConcreteBuilder，实现builder接口，完成复杂产品各个部件的具体创建方法。
 * 「产品类」Product，要被创建的复杂对象。
 * 「指挥者类」Director，调用具体的建造者了创建复杂对象的各个部分，只负责保证对象各部分完整创建或按照某种顺序创建。
 */
public class BuilderDemo {
    public static void main(String[] args) {
        showBike(new OfoBuilder());
        showBike(new MobikeBuilder());
    }

    private static void showBike(Builder builder) {
        Director director = new Director(builder);
        Bike bike = director.construct();
        System.out.println(bike.getFrame());
        System.out.println(bike.getSeat());
    }
}
