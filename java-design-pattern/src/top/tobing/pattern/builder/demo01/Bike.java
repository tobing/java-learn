package top.tobing.pattern.builder.demo01;

/**
 * @Author tobing
 * @Date 2021/1/2 22:55
 * @Description
 */
public class Bike {
    // 车架
    private String frame;
    // 座位
    private String seat;

    public String getFrame() {
        return frame;
    }

    public void setFrame(String frame) {
        this.frame = frame;
    }

    public String getSeat() {
        return seat;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }
}
