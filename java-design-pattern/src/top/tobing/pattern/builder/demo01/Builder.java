package top.tobing.pattern.builder.demo01;

/**
 * @Author tobing
 * @Date 2021/1/2 22:55
 * @Description
 */
public abstract class Builder {
    protected Bike bike = new Bike();

    // 构建车架
    public abstract void buildFrame();

    // 构建车座
    public abstract void buildSeat();

    // 构建整车
    public abstract Bike createBike();

}
