package top.tobing.pattern.builder.demo01;

/**
 * @Author tobing
 * @Date 2021/1/2 22:56
 * @Description
 */
public class OfoBuilder extends Builder {
    @Override
    public void buildFrame() {
        System.out.println("碳纤维车架");
    }

    @Override
    public void buildSeat() {
        System.out.println("橡胶车座");
    }

    @Override
    public Bike createBike() {
        return bike;
    }
}
