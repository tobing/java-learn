package top.tobing.pattern.builder.demo02;

/**
 * @Author tobing
 * @Date 2021/1/2 22:55
 * @Description
 */
public class MobikeBuilder extends Builder {
    @Override
    public void buildFrame() {
        System.out.println("铝合金车架");
    }

    @Override
    public void buildSeat() {
        System.out.println("真皮车座");
    }

    @Override
    public Bike createBike() {
        return bike;
    }
}
