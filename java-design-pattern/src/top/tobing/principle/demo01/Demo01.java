package top.tobing.principle.demo01;

/**
 * @Author tobing
 * @Date 2020/12/29 21:12
 * @Description 开闭原则
 * 对修改关闭，对扩展打开
 */
public class Demo01 {
    public static void main(String[] args) {
        SougouInput input = new SougouInput();
        AbstractSkin skin = new TobingDefalut();
        input.setSkin(skin);
        skin.display();
    }
}
