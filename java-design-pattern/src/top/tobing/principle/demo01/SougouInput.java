package top.tobing.principle.demo01;

/**
 * @Author tobing
 * @Date 2020/12/29 21:13
 * @Description
 */
public class SougouInput {

    private AbstractSkin skin;

    public void setSkin(AbstractSkin skin) {
        this.skin = skin;
    }

    public void display() {
        this.skin.display();
    }
}
