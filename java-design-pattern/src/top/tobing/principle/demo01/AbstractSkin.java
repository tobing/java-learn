package top.tobing.principle.demo01;

/**
 * @Author tobing
 * @Date 2020/12/29 21:09
 * @Description 抽象皮肤类
 */
public abstract class AbstractSkin {

    //显示
    public abstract void display();
}
