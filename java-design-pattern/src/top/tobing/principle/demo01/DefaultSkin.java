package top.tobing.principle.demo01;

/**
 * @Author tobing
 * @Date 2020/12/29 21:11
 * @Description 默认皮肤
 */
public class DefaultSkin extends AbstractSkin{

    @Override
    public void display() {
        System.out.println("默认皮肤");
    }
}
