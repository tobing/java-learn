package top.tobing.principle.demo04.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:33
 * @Description 防水接口
 */
public interface WaterProof {
    // 防水功能
    public void waterProof();
}
