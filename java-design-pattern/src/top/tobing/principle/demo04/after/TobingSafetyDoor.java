package top.tobing.principle.demo04.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:35
 * @Description Tobing牌防盗门
 * 防火、防水、防盗
 */
public class TobingSafetyDoor implements AntiTheft, WaterProof, FireProof {
    @Override
    public void antiTheft() {
        System.out.println("Tobing牌安全门防盗");
    }

    @Override
    public void fireProof() {
        System.out.println("Tobing牌安全门防火");
    }

    @Override
    public void waterProof() {
        System.out.println("Tobing牌安全门防水");
    }
}
