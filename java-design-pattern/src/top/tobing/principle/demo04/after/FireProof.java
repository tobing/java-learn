package top.tobing.principle.demo04.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:34
 * @Description 防火功能
 */
public interface FireProof {
    public void fireProof();
}
