package top.tobing.principle.demo04.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:36
 * @Description Zenyet 安全门
 * 防火、防盗
 */
public class ZenyetSafetyDoor implements FireProof, AntiTheft {
    @Override
    public void antiTheft() {
        System.out.println("Zenyet牌安全门防盗");
    }

    @Override
    public void fireProof() {
        System.out.println("Zenyet牌安全门防火");
    }
}
