package top.tobing.principle.demo04.after;


/**
 * @Author tobing
 * @Date 2020/12/29 22:37
 * @Description TODO 接口隔离原则
 * 客户端不应该被迫依赖它不使用的方法；
 * 一个类对另一个类的依赖应该建立在【最小接口】上。
 */
public class SafetyDoorDemo {
    public static void main(String[] args) {
        TobingSafetyDoor tobingSafeDoor = new TobingSafetyDoor();
        ZenyetSafetyDoor zenyetSafetyDoor = new ZenyetSafetyDoor();

        tobingSafeDoor.antiTheft();
        tobingSafeDoor.fireProof();
        tobingSafeDoor.waterProof();

        zenyetSafetyDoor.antiTheft();
        zenyetSafetyDoor.fireProof();
    }
}
