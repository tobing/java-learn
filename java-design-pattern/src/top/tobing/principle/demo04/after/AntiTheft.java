package top.tobing.principle.demo04.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:34
 * @Description 防盗接口
 */
public interface AntiTheft {
    public void antiTheft();
}
