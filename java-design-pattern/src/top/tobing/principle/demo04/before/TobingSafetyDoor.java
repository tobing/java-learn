package top.tobing.principle.demo04.before;

/**
 * @Author tobing
 * @Date 2020/12/29 22:31
 * @Description Tobing牌安全门
 * 防水、防火、范小偷
 */
public class TobingSafetyDoor implements SafetyDoor {
    @Override
    public void antiTheft() {
        System.out.println("防盗");
    }

    @Override
    public void fireProof() {
        System.out.println("防火");
    }

    @Override
    public void waterPoorf() {
        System.out.println("防水");
    }
}
