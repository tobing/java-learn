package top.tobing.principle.demo04.before;

/**
 * @Author tobing
 * @Date 2020/12/29 22:30
 * @Description 安全门接口
 */
public interface SafetyDoor {
    // 防盗
    public void antiTheft();

    // 防火
    public void fireProof();

    // 防水
    public void waterPoorf();
}
