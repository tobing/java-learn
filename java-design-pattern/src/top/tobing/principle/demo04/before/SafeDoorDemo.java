package top.tobing.principle.demo04.before;

/**
 * @Author tobing
 * @Date 2020/12/29 22:32
 * @Description
 */
public class SafeDoorDemo {
    public static void main(String[] args) {
        TobingSafetyDoor safetyDoor = new TobingSafetyDoor();
        safetyDoor.antiTheft();
        safetyDoor.fireProof();
        safetyDoor.waterPoorf();
    }
}
