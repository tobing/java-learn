package top.tobing.principle.demo02.after;

/**
 * @Author tobing
 * @Date 2020/12/29 21:47
 * @Description 正方形
 */
public class Square implements Quadrilateral {
    private double side;

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    @Override
    public double getWidth() {
        return side;
    }

    @Override
    public double getLength() {
        return side;
    }
}
