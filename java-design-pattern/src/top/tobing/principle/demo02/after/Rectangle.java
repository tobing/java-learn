package top.tobing.principle.demo02.after;

/**
 * @Author tobing
 * @Date 2020/12/29 21:46
 * @Description
 */
public class Rectangle implements Quadrilateral {

    private double width;
    private double length;

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getLength() {
        return length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }
}
