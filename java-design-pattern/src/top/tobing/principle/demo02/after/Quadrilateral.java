package top.tobing.principle.demo02.after;

/**
 * @Author tobing
 * @Date 2020/12/29 21:45
 * @Description 四边形
 */
public interface Quadrilateral {

    double getWidth();

    double getLength();
}
