package top.tobing.principle.demo02.after;


/**
 * @Author tobing
 * @Date 2020/12/29 21:49
 * @Description
 */
public class RectangleDemo {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.setLength(100);
        rectangle.setWidth(50);
        printLengthAndWidth(rectangle);
        resize(rectangle);
        printLengthAndWidth(rectangle);
    }

    // 调整长宽，当宽 < 长，宽++
    public static void resize(Rectangle rectangle) {
        while (rectangle.getLength() >= rectangle.getWidth()) {
            rectangle.setWidth(rectangle.getWidth() + 1);
        }
    }

    public static void printLengthAndWidth(Quadrilateral quadrilateral) {
        System.out.println("长：" + quadrilateral.getLength());
        System.out.println("宽：" + quadrilateral.getWidth());
    }
}
