package top.tobing.principle.demo02.before;

/**
 * @Author tobing
 * @Date 2020/12/29 21:28
 * @Description 长方形
 */
public class Rectangle {
    private double length;
    private double width;

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }
}
