package top.tobing.principle.demo02.before;

/**
 * @Author tobing
 * @Date 2020/12/29 21:29
 * @Description 正方形
 */
public class Square extends Rectangle {

    // 重写父类Rectangle的setWidth用来设置长宽
    @Override
    public void setWidth(double width) {
        super.setWidth(width);
        super.setLength(width);
    }
}
