package top.tobing.principle.demo02.before;

/**
 * @Author tobing
 * @Date 2020/12/29 21:30
 * @Description 里氏代换原则
 * 子类可以扩展父类的功能，不能改变父类原有的功能；
 * 换句话说，子类继承父类的时候，除了添加的方法完成功能外，尽量不要重写父类的方法
 * 如果重写父类的方法来完成新功能，写起来虽然简单，但是会使得整个继承体系的可复用性降低，特别是多态比较频繁的时候。
 */
public class RectangleDemo {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.setWidth(100);
        rectangle.setLength(200);
        // printLengthAndWidth(rectangle);
        resize(rectangle);
        printLengthAndWidth(rectangle);
        System.out.println("============================");
        Square square = new Square();
        // 存在问题，square重写了父类的setWidth，会导致死循环
        resize(square);
        printLengthAndWidth(square);
    }

    // 调整长宽，当宽 < 长，宽++
    public static void resize(Rectangle rectangle) {
        while (rectangle.getLength() >= rectangle.getWidth()) {
            rectangle.setWidth(rectangle.getWidth() + 1);
        }
    }

    public static void printLengthAndWidth(Rectangle rectangle) {
        System.out.println("长：" + rectangle.getLength());
        System.out.println("宽：" + rectangle.getWidth());
    }

}
