package top.tobing.principle.demo05;

/**
 * 粉丝类
 */
public class Fans {
    private  String name;

    public Fans(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
