package top.tobing.principle.demo05;

/**
 * 中间人代理
 */
public class Agent {
    private Stars stars;
    private Company company;
    private Fans fans;

    public Stars getStars() {
        return stars;
    }

    public void setStars(Stars stars) {
        this.stars = stars;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Fans getFans() {
        return fans;
    }

    public void setFans(Fans fans) {
        this.fans = fans;
    }

    // 粉丝和明星见面
    public void meeting() {
        System.out.println(stars.getName() + "与" + fans.getName() + "见面");
    }

    // 明星和公司洽谈
    public void business() {
        System.out.println(stars.getName() + "与" + company.getName() + "洽谈");
    }
}
