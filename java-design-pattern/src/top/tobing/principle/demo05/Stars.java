package top.tobing.principle.demo05;

/**
 * 明星类
 */
public class Stars {
    private String name;

    public Stars(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
