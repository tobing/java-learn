package top.tobing.principle.demo05;

/**
 * 公司类
 */
public class Company {
    private String name;

    public Company(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
