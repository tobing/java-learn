package top.tobing.principle.demo05;

/**
 * 迪米特法则
 * 迪米特法则又称最少知识原则。
 * 直接和朋友交谈，不跟“陌生人”说话
 * 其含义是：如果两个软件实体无须直接通信，那么就不应该直接的调用，可以通过第三方转发该调用。其目的是降低类的耦合度。提供模块的相对独立性。
 * 迪米特法则中的朋友是指：当对象本身/当前对象的成员对象/当前对象所创建的对象/当前对象的方法参数等，这些对象同当前对象存在关联/聚合或组合关系，
 * 可以直接访问这些对象的方法
 */
public class AgentDemo {
    public static void main(String[] args) {
        // 创建中间人
        Agent agent = new Agent();
        // 创建公司
        Company company = new Company("Tobing传媒公司");
        // 创建明星
        Stars stars = new Stars("张三");
        // 创建粉丝
        Fans fans = new Fans("李四粉丝");

        agent.setCompany(company);
        agent.setFans(fans);
        agent.setStars(stars);

        agent.meeting();
        agent.business();

    }
}
