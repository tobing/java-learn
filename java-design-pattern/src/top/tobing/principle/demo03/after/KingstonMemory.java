package top.tobing.principle.demo03.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:05
 * @Description 金士顿内存条
 */
public class KingstonMemory implements Memory {
    @Override
    public void save() {
        System.out.println("使用金士顿内存条加载数据");
    }
}
