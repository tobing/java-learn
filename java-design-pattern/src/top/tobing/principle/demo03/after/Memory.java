package top.tobing.principle.demo03.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:24
 * @Description 内存条接口
 */
public interface Memory {

    // 保存数据
    public void save();
}
