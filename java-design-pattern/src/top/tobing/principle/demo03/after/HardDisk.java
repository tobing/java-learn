package top.tobing.principle.demo03.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:24
 * @Description 硬盘接口
 */
public interface HardDisk {
    // 保存数据
    public void save(String data);

    // 获取数据
    public String get();
}
