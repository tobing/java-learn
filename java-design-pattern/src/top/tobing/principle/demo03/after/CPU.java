package top.tobing.principle.demo03.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:24
 * @Description CPU接口
 */
public interface CPU {

    // CPU运行
    public void run();
}
