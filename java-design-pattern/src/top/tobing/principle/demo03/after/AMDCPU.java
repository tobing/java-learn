package top.tobing.principle.demo03.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:29
 * @Description AMD CPU
 */
public class AMDCPU implements CPU {
    @Override
    public void run() {
        System.out.println("AMD CPU正在运行");
    }
}
