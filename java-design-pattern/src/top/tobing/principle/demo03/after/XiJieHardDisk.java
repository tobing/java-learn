package top.tobing.principle.demo03.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:04
 * @Description 希捷硬盘
 */
public class XiJieHardDisk implements HardDisk {

    private String data;

    @Override
    public void save(String data) {
        System.out.println("使用希捷硬盘保存数据。");
        this.data = data;
    }

    @Override
    public String get() {
        System.out.println("从希捷硬盘读取数据：" + this.data);
        return data;
    }
}
