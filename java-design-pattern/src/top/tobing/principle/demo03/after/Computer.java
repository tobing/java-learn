package top.tobing.principle.demo03.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:27
 * @Description
 */
public class Computer {
    private CPU cpu;
    private HardDisk hardDisk;
    private Memory memory;

    public CPU getCpu() {
        return cpu;
    }

    public void setCpu(CPU cpu) {
        this.cpu = cpu;
    }

    public HardDisk getHardDisk() {
        return hardDisk;
    }

    public void setHardDisk(HardDisk hardDisk) {
        this.hardDisk = hardDisk;
    }

    public Memory getMemory() {
        return memory;
    }

    public void setMemory(Memory memory) {
        this.memory = memory;
    }

    public void run() {
        this.cpu.run();
        this.memory.save();
        this.hardDisk.save("tobing");
        this.hardDisk.get();
    }
}
