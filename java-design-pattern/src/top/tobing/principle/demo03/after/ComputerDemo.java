package top.tobing.principle.demo03.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:28
 * @Description
 */
public class ComputerDemo {
    public static void main(String[] args) {
        // CPU cpu = new IntelCPU();
        CPU cpu = new AMDCPU();
        Memory memory = new KingstonMemory();
        HardDisk hardDisk = new XiJieHardDisk();

        Computer computer = new Computer();
        computer.setCpu(cpu);
        computer.setHardDisk(hardDisk);
        computer.setMemory(memory);

        computer.run();
    }
}
