package top.tobing.principle.demo03.after;

/**
 * @Author tobing
 * @Date 2020/12/29 22:06
 * @Description Intel CPU
 */
public class IntelCPU implements CPU {
    @Override
    public void run() {
        System.out.println("使用英特尔CPU运行。");
    }
}
