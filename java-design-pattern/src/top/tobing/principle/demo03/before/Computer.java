package top.tobing.principle.demo03.before;

/**
 * @Author tobing
 * @Date 2020/12/29 22:07
 * @Description
 */
public class Computer {
    private IntelCPU cpu;
    private XiJieHardDisk hardDisk;
    private KingstonMemory memory;

    public IntelCPU getCpu() {
        return cpu;
    }

    public void setCpu(IntelCPU cpu) {
        this.cpu = cpu;
    }

    public XiJieHardDisk getHardDisk() {
        return hardDisk;
    }

    public void setHardDisk(XiJieHardDisk hardDisk) {
        this.hardDisk = hardDisk;
    }

    public KingstonMemory getMemory() {
        return memory;
    }

    public void setMemory(KingstonMemory memory) {
        this.memory = memory;
    }

    public void run() {
        this.memory.save();
        this.cpu.run();
        this.hardDisk.save("数据");
        this.hardDisk.get();
    }
}
