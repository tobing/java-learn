package top.tobing.principle.demo03.before;

/**
 * @Author tobing
 * @Date 2020/12/29 22:01
 * @Description 依赖倒转原则
 * 高层模块不应该依赖底层模块，两者都应该依赖其抽象；抽象不应该依赖具体，具体应该依赖抽象。
 * 简单的说就是要求对抽象进行编程，不要对实现进行编程，这样可以降低客户与实现模块间的耦合。
 */
public class ComputerDemo {
    public static void main(String[] args) {
        IntelCPU cpu = new IntelCPU();
        XiJieHardDisk hardDisk = new XiJieHardDisk();
        KingstonMemory memory = new KingstonMemory();

        Computer computer = new Computer();
        computer.setCpu(cpu);
        computer.setHardDisk(hardDisk);
        computer.setMemory(memory);

        computer.run();
    }

    // 以上代码看似没有问题，但是当我们需要换一个品牌的CPU、换一个品牌的内存条的时候该怎么办呢？
}
