package top.tobing.principle.demo06.after;

/**
 * @Author tobing
 * @Date 2020/12/30 16:18
 * @Description
 */
public class Car {
    private Color color;

    public void move() {
        System.out.println("红色汽车移动");
    }
}
