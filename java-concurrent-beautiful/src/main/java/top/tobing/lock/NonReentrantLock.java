package top.tobing.lock;


import java.io.Serializable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;

/**
 * @Author tobing
 * @Date 2021/3/22 16:07
 * @Description
 */
public class NonReentrantLock implements Serializable {

    private static class Sync extends AbstractQueuedSynchronizer {
        // 是否独占锁
        @Override
        protected boolean isHeldExclusively() {
            return true;
        }

        /**
         * 尝试获取锁，即state从0设置为1
         *
         * @param arg 1
         * @return
         */
        @Override
        protected boolean tryAcquire(int arg) {
            assert arg == 1;
            if (compareAndSetState(0, 1)) { // CAS尝试将0设置为1
                setExclusiveOwnerThread(Thread.currentThread());
                return true;
            }
            return false;
        }

        /**
         * 尝试释放锁，将state设置为0
         *
         * @param arg 1
         * @return
         */
        @Override
        protected boolean tryRelease(int arg) {
            assert arg == 1;
            if (getState() == 0) {
                throw new IllegalMonitorStateException();
            }
            setExclusiveOwnerThread(null);  // 置空当前独占线程
            setState(0);
            return true;
        }

        Condition newCondition() {
            return new ConditionObject();
        }
    }

    private final Sync sync = new Sync();

    public void lock() {
        sync.acquire(1); // 尝试将0设置为1
    }

    public boolean tryLock() {
        return sync.tryAcquire(1); // 阐释
    }

    public void unlock() {
        sync.release(1);
    }

    public Condition newCondition() {
        return sync.newCondition();
    }

    public boolean isLocked() {
        return sync.isHeldExclusively();
    }

    public void lockInterruptibly() throws InterruptedException {
        sync.acquireInterruptibly(1);
    }

    public boolean tryLock(long timeout, TimeUnit unit) throws InterruptedException {
        return sync.tryAcquireNanos(1, unit.toNanos(timeout));
    }

}
