package top.tobing;

/**
 * @Author tobing
 * @Date 2021/3/20 23:07
 * @Description 局部性原理
 */
public class Demo10LocalPrinciple {
    static final int LINE_NUM = 10240;   // 一个int4个字节
    static final int COLUM_NUM = 10240;

    public static void main(String[] args) {
        testCache();
        testUnCache();
    }

    private static void testCache() {
        long[][] array = new long[LINE_NUM][COLUM_NUM];
        long start = System.currentTimeMillis();
        for (int i = 0; i < LINE_NUM; i++) {
            for (int j = 0; j < COLUM_NUM; j++) {
                array[i][j] = i * 2 + j;
            }
        }
        long end = System.currentTimeMillis();
        long cacheTime = end - start;
        System.out.println("cache time:" + cacheTime);
    }

    private static void testUnCache() {
        long[][] array = new long[LINE_NUM][COLUM_NUM];
        long start = System.currentTimeMillis();
        for (int i = 0; i < LINE_NUM; i++) {
            for (int j = 0; j < COLUM_NUM; j++) {
                array[j][i] = i * 2 + j;
            }
        }
        long end = System.currentTimeMillis();
        long cacheTime = end - start;
        System.out.println("no cache time:" + cacheTime);
    }

}
