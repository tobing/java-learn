package top.tobing;

/**
 * @Author tobing
 * @Date 2021/3/20 11:27
 * @Description
 */
public class Demo07ThreadLocal {
    private static ThreadLocal<String> threadLocal = new ThreadLocal<>();
    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(() -> {
            threadLocal.set("thread1");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String s = threadLocal.get();
            System.out.println(Thread.currentThread() + " " + s);
        });
        Thread thread2 = new Thread(() -> {
            threadLocal.set("thread2");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            String s = threadLocal.get();
            System.out.println(Thread.currentThread() + " " + s);
        });

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();


    }
}
