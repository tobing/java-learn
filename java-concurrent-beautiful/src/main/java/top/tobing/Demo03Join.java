package top.tobing;

/**
 * @Author tobing
 * @Date 2021/3/20 9:23
 * @Description join的使用场景
 */
public class Demo03Join {
    public static void main(String[] args) throws InterruptedException {
        Thread thread2 = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("一星龙珠找到了");
        });
        Thread thread1 = new Thread(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("二星龙珠找到了");
        });

        thread1.start();
        thread2.start();
        System.out.println("线程1和线程2启动区找龙珠了");

        thread1.join();
        thread2.join();

        System.out.println("两个龙珠都找到了");


    }
}
