package top.tobing;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author tobing
 * @Date 2021/3/22 15:40
 * @Description
 */
public class Demo13Condition {
    public static void main(String[] args) throws InterruptedException {
        // 创建可重入锁
        ReentrantLock lock = new ReentrantLock();
        // 创建条件变量
        Condition condition = lock.newCondition();
        // ThreadA
        new Thread(() -> {
            lock.lock();
            try {
                System.out.println("Begin await");
                condition.await();      // 阻塞挂起
                System.out.println("end await");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }).start();

        // 睡眠1s
        Thread.sleep(1000);

        // ThreadB
        new Thread(() -> {
            lock.lock();
            try {
                System.out.println("Begin signal");
                condition.signal();
                System.out.println("End signal");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }).start();
    }


}
