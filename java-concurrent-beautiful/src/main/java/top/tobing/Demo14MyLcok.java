package top.tobing;

import top.tobing.lock.NonReentrantLock;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Condition;

/**
 * @Author tobing
 * @Date 2021/3/22 16:33
 * @Description
 */
public class Demo14MyLcok {

    private final static NonReentrantLock lock = new NonReentrantLock();
    static Condition notFull = lock.newCondition();
    static Condition notEmpty = lock.newCondition();
    final static Queue<String> queue = new LinkedBlockingQueue<>();
    final static int queueSize = 10;

    public static void main(String[] args) {
        new Thread(()-> {
            lock.lock();
            try {
                while (queue.size() == queueSize) {
                    notEmpty.await();
                }
                // 生产
                queue.add("ele");
                // 唤醒消费
                notFull.signalAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }).start();

        new Thread(()-> {
            lock.lock();
            try {
                while (queue.size() == 0) {
                    notFull.await();
                }
                // 消费
                String remove = queue.remove();
                System.out.println("消费者：" + remove);
                notEmpty.signalAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }).start();


    }
}
