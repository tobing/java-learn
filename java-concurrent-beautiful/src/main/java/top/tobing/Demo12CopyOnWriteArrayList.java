package top.tobing;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @Author tobing
 * @Date 2021/3/21 16:30
 * @Description 写时复制策略的数据弱一致性问题
 */
public class Demo12CopyOnWriteArrayList {
    private static CopyOnWriteArrayList<String> strs = new CopyOnWriteArrayList<>();

    public static void main(String[] args) throws InterruptedException {
        strs.add("tobing");
        strs.add("zenyet");
        strs.add("rongon");
        strs.add("actogo");
        strs.add("a1");
        strs.add("a2");

        // 线程B对strs进行修改
        Thread threadB = new Thread(() -> {
            strs.remove("a1");
            strs.set(4, "test");
        });

        // main线程获取strs的迭代器
        Iterator<String> iterator = strs.iterator();

        // 执行修改
        threadB.start();
        // 等待线程B执行完毕
        threadB.join();

        // 对迭代器进行遍历
        while (iterator.hasNext()) {
            String next = iterator.next();
            System.out.println(next);   // 遍历输出的结果仍然是获取迭代器的结果
        }
    }
}
