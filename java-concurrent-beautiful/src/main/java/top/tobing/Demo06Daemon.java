package top.tobing;

/**
 * @Author tobing
 * @Date 2021/3/20 11:12
 * @Description 守护线程
 */
public class Demo06Daemon {
    public static void main(String[] args) {
        Thread threadA = new Thread(() -> {
            while (true) {

            }
        });
        // 设置为守护线程
        // threadA.setDaemon(true);
        // 启动守护线程
        threadA.start();

        System.out.println(Thread.currentThread() + " is over");
    }
}
