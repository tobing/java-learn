package top.tobing;

/**
 * @Author tobing
 * @Date 2021/3/19 23:02
 * @Description
 */
public class Demo02Notify {
    private static Object obj = new Object();

    public static void main(String[] args) throws InterruptedException {
        // 阻塞
        Thread thread1 = new Thread(() -> {
            synchronized (obj) {
                try {
                    System.out.println("thread1执行wait前");
                    obj.wait();
                    System.out.println("thread1执行wait后");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        // 阻塞
        Thread thread2 = new Thread(() -> {
            synchronized (obj) {
                try {
                    System.out.println("thread2执行wait前");
                    obj.wait();
                    System.out.println("thread2执行wait后");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        // 唤醒
        Thread thread3 = new Thread(() -> {
            synchronized (obj) {
                try {
                    System.out.println("notify");
                    obj.notify();
                    System.out.println("notify");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        // 唤醒
        Thread thread4 = new Thread(() -> {
            synchronized (obj) {
                try {
                    System.out.println("notifyAll");
                    obj.notifyAll();
                    System.out.println("notifyAll");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread1.start();
        thread2.start();
        Thread.sleep(1000);
        thread3.start();
        thread4.start();
    }
}
