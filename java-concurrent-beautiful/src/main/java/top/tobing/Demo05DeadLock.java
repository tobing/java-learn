package top.tobing;

/**
 * @Author tobing
 * @Date 2021/3/20 10:14
 * @Description 死锁
 * 必要条件
 * 互斥、请求保持、不可剥夺、循环等待
 */
public class Demo05DeadLock {
    private static Object lockA = new Object();
    private static Object lockB = new Object();

    public static void main(String[] args) {
        new Thread(()-> {
            synchronized (lockA) {
                System.out.println(Thread.currentThread() + " get lock A.");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lockB) {
                    System.out.println(Thread.currentThread() + " get lock B.");
                }
            }
        }).start();

        new Thread(()-> {
            synchronized (lockB) {
                System.out.println(Thread.currentThread() + " get lock B.");
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                synchronized (lockA) {
                    System.out.println(Thread.currentThread() + " get lock A.");
                }
            }
        }).start();
    }
}
