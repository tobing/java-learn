package top.tobing;

import sun.misc.Unsafe;

import java.lang.reflect.Field;

/**
 * @Author tobing
 * @Date 2021/3/20 21:01
 * @Description 使用Unsafe类
 * 通过反射或Unsafe类
 * 通过Unsafe从硬件的角度修改变量的值
 */
public class Demo09UnsafeUse {
    static Unsafe unsafe;
    static long stateOffset;
    private volatile long state = 0;

    static {
        try {
            // 通过发射获取Unsafe的成员变量
            Field field = Unsafe.class.getDeclaredField("theUnsafe");
            // 设置为可访问
            field.setAccessible(true);
            // 获取改变了的值
            unsafe = (Unsafe) field.get(null);
            // 获取state在Demo09UnsafeUse的偏移量
            stateOffset = unsafe.objectFieldOffset(Demo09UnsafeUse.class.getDeclaredField("state"));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Demo09UnsafeUse unsafeUse = new Demo09UnsafeUse();
        System.out.println("修改前：" + unsafeUse.state);
        boolean flag = unsafe.compareAndSwapLong(unsafeUse, stateOffset, 0L, 2L);
        System.out.println(flag);
        System.out.println("修改之后：" + unsafeUse.state);
    }
}
