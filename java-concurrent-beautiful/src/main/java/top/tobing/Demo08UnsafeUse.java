package top.tobing;

import sun.misc.Unsafe;

/**
 * @Author tobing
 * @Date 2021/3/20 20:30
 * @Description
 */
public class Demo08UnsafeUse {
    // 获取Unsafe类实例
    /**
     * java.lang.ExceptionInInitializerError
     * Caused by: java.lang.SecurityException: Unsafe
     */
    static final Unsafe unsafe = Unsafe.getUnsafe();
    // 记录state在Demo08UnsafeUse的地址偏移
    static long stateOffset;
    // state变量，初始化为0
    private volatile long state = 0;

    static {
        try {
            // 获取Demo08UnsafeUse中state字段的地址偏移
            stateOffset = unsafe.objectFieldOffset(Demo08UnsafeUse.class.getDeclaredField("state"));
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        // 创建实例并获取state的值
        Demo08UnsafeUse demo08UnsafeUse = new Demo08UnsafeUse();
        boolean success = unsafe.compareAndSwapLong(demo08UnsafeUse, stateOffset, 0L, 2L);
        System.out.println(success);

    }
}
