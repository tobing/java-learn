package top.tobing;

/**
 * @Author tobing
 * @Date 2021/3/20 9:54
 * @Description interrup优雅使用
 */
public class Demo04Interrupt {
    public static void main(String[] args) throws InterruptedException {
        // 创建ThreadA
        Thread threadA = new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                System.out.println(Thread.currentThread() + " hello");
            }
        });
        // 启动ThreadA
        threadA.start();

        // 主线程睡眠，让ThreadA充分执行
        Thread.sleep(100);

        // 中断ThreadA
        System.out.println("interrupt threadA");
        threadA.interrupt();

        // 等待子线程执行完毕
        threadA.join();
        System.out.println("main is over");
    }
}
