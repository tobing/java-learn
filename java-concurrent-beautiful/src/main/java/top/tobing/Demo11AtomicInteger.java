package top.tobing;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author tobing
 * @Date 2021/3/21 12:23
 * @Description AtomicInteger的使用示例
 * 使用多线程并发统计两个数组的0的个数
 */
public class Demo11AtomicInteger {
    private static AtomicInteger atomicInteger = new AtomicInteger();
    private static int[] arrOne = new int[]{0, 1, 2, 3, 0, 5, 6, 0, 56, 0};
    private static int[] arrTwo = new int[]{10, 1, 2, 3, 0, 5, 6, 0, 56, 0};

    public static void main(String[] args) throws InterruptedException {
        // 线程A统计arrOne
        Thread threadA = new Thread(() -> {
            for (int i : arrOne) {
                if (i == 0) {
                    atomicInteger.incrementAndGet();
                }
            }
        });
        // 线程B统计arrTwo
        Thread threadB = new Thread(() -> {
            for (int i : arrTwo) {
                if (i == 0) {
                    atomicInteger.incrementAndGet();
                }
            }
        });
        // 启动执行
        threadA.start();
        threadB.start();

        threadA.join();
        threadB.join();

        // 输出结果
        System.out.println("一共有：" + atomicInteger.get());
    }

}
