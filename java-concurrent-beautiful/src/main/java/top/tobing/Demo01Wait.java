package top.tobing;

/**
 * @Author tobing
 * @Date 2021/3/19 22:44
 * @Description wait
 */
public class Demo01Wait {
    static Object obj = new Object();
    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(() -> {
            System.out.println("...begin...");
            try {
                synchronized (obj) {
                    obj.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("...end...");
        });
        thread1.start();

        Thread.sleep(1000);
        System.out.println("开始中断");
        thread1.interrupt();
        System.out.println("中断完成");
    }
}
