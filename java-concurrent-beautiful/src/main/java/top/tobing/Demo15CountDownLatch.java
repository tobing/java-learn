package top.tobing;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author tobing
 * @Date 2021/3/23 19:05
 * @Description CountDownLatch 结合线程池的使用
 */
public class Demo15CountDownLatch {
    // 创建一个CountDownLatch
    private static CountDownLatch countDownLatch = new CountDownLatch(2);

    public static void main(String[] args) throws InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(2);
        // 将线程添加到线程池中。
        threadPool.submit(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
            }
            System.out.println("Thread one over!");
        });

        threadPool.submit(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
            }
            System.out.println("Thread two over!");
        });
        System.out.println("waiting for all threads over!");
        countDownLatch.await();
        System.out.println("all child thread over");
        threadPool.shutdown();

    }
}
