package top.tobing.java_base;

/**
 * @Author tobing
 * @Date 2021/3/31 11:55
 * @Description
 */
public class Demo02Final {
    final void test() {

    }

    private void privateMethodTest() {

    }

    public static void main(String[] args) {

    }

}
final class FinalClass {
    void test() {
        System.out.println("Final Class test...");
    }
}
