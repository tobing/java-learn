package top.tobing.java_base;

/**
 * @Author tobing
 * @Date 2021/3/31 12:11
 * @Description
 */
public class Demo03Finaly {
    public static void main(String[] args) {
        System.out.println(test());
    }

    static int test() {
        int i = 10;
        try {
            return i + 10;
        } finally {
             return i + 20;
        }
        /**
         *  0 bipush 10 // 10放到栈顶
         *  2 istore_0  // 栈顶元素出栈保存到局部变量表0的位置
         *  3 iload_0   // 读取0位置元素
         *  4 bipush 10 // 压入10
         *  6 iadd      // 执行相加，结果压栈
         *  7 istore_1  // 将结果20保存局部变量表1的位置
         *  8 iload_0   // 读取0位置元素
         *  9 bipush 20 // 压入20
         * 11 iadd      // 执行相加，结果压栈
         * 12 ireturn   
         * 13 astore_2
         * 14 iload_0
         * 15 bipush 20
         * 17 iadd
         * 18 ireturn
         */
    }
}
