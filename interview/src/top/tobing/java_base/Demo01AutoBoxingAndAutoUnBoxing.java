package top.tobing.java_base;

/**
 * @Author tobing
 * @Date 2021/3/31 11:37
 * @Description 自动装箱拆箱
 */
public class Demo01AutoBoxingAndAutoUnBoxing {
    public static void main(String[] args) {
        Integer i = 12;     // 自动装箱，将基础数据类型封住为包装类
        int j = i + 1;      // 自定拆先，将包装类中的基础数据类型取出来直接运算
        // 自动装箱本质：<java/lang/Integer.valueOf>
        // 自动拆箱本质：<java/lang/Integer.intValue>
        // 需要注意的是valueOf这个方法会有缓存，范围时 -128 到 +127
        Integer i1 = 127;
        Integer i2 = 127;
        Integer i3 = 128;
        Integer i4 = new Integer(127);
        System.out.println(i1 == i2);   // true
        System.out.println(i1 == i3);   // fasle
        System.out.println(i1 == i4);   // false
    }
}
