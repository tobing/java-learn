package top.tobing.ali;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

/**
 * @Author tobing
 * @Date 2021/6/11 9:06
 * @Description 一段由01组成的字符串形成一个环，环组成的
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String line = sc.next();
        String min = line;
        String max = line;
        for (int i = 0; i < line.length(); i++) {
            String left = line.substring(0, i);
            String right = line.substring(i);
            String str = right + left;
            if (min.compareTo(str) < 0) {
                min = str;
            }
            if (max.compareTo(str) > 0) {
                max = str;
            }
        }

        int maxVal = (int) Long.parseUnsignedLong(max, 2) % 1000000007;
        int minVal = (int) Long.parseUnsignedLong(min, 2) % 1000000007;
        System.out.println((maxVal + minVal) % 1000000007);
    }
}
