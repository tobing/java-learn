import java.util.Scanner;

/**
 * @Author tobing
 * @Date 2021/6/17 19:12
 * @Description
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        double[] numbers = new double[number];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = scanner.nextDouble();
        }
        double avg = 0;
        double std = 0;
        // 计算平均值
        for (int i = 0; i < numbers.length; i++) {
            avg += numbers[i];
        }
        avg /= number;
        // 计算方差
        for (int i = 0; i < numbers.length; i++) {
            std += (numbers[i] - avg) * (numbers[i] - avg);
        }
        std = Math.sqrt(std/number);
        System.out.printf("dAvg=%.2f,dSD=%.2f\n", avg, std);

        // 计算离群点
        for (int i = 0; i < numbers.length; i++) {
            if (Math.abs(numbers[i] - avg) >= std) {
                System.out.printf("%.2f\n", numbers[i]);
            }
        }
        System.out.println();
    }
}
