import java.io.BufferedWriter;
import java.io.FileWriter;
import java.sql.*;

public class OutputData {
    public static void main(String[] args) {
        String connectionUrl = "jdbc:sqlserver://localhost:1433;databaseName=Swpustu;user=sa;password=123456";
        Connection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        System.out.println("正在连接数据库和检索，请等待...");
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(connectionUrl);
            String SQL = "select cNo,vName,cSex,cDepartNo,cMajorNo,iGrade from Student";
            stmt = con.createStatement();
            rs = stmt.executeQuery(SQL);
            StringBuilder sb = new StringBuilder();
            // 写入列名
            sb.append("学号,姓名,性别,学院编号,专业编号,年级\n");
            // 写入列数据
            while (rs.next()) {
                sb.append(rs.getString(1));
                sb.append(",");
                sb.append(rs.getString(2));
                sb.append(",");
                sb.append(rs.getString(3));
                sb.append(",");
                sb.append(rs.getString(4));
                sb.append(",");
                sb.append(rs.getString(5));
                sb.append(",");
                sb.append(rs.getString(6));
                sb.append("\n");
            }
            BufferedWriter out = new BufferedWriter(new FileWriter("student.csv"));
            out.write(sb.toString());
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) try {
                rs.close();
            } catch (Exception e) {
            }
            if (stmt != null) try {
                stmt.close();
            } catch (Exception e) {
            }
            if (con != null) try {
                con.close();
            } catch (Exception e) {
            }
        }
    }
}
