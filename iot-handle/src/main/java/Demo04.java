import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.Executor;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;

/**
 * @Author tobing
 * @Date 2021/6/22 16:01
 * @Description
 */
public class Demo04 {
    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        /**
//         * doubles[0]:P(A|B1)
//         * doubles[1]:P(B1)
//         * doubles[2]:P(A|B2)
//         * doubles[3]:P(B2)
//         * doubles[4]:P(A|B3)
//         * doubles[5]:P(B3)
//         */
//        double[] doubles = new double[6];
//        for (int i = 0; i < 6; i++) {
//            doubles[i] = sc.nextDouble();
//        }
//        double p1A = doubles[0] * doubles[1];
//        double p2A = doubles[2] * doubles[3];
//        double p3A = doubles[4] * doubles[5];
//        double pA = p1A + p2A + p3A;
//        p1A /= pA;
//        p2A /= pA;
//        p3A /= pA;
//        int index = 0;
//        if (p1A > p2A && p1A > p3A) {
//            index = 1;
//        } else if (p2A > p1A && p2A > p3A) {
//            index = 2;
//        } else {
//            index = 3;
//        }
//        System.out.printf("%.3f %.3f %.3f\n%d\n", p1A, p2A, p3A, index);
    }
}
