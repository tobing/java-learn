import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author tobing
 * @Date 2021/6/15 21:33
 * @Description
 */
public class InputData {
    public static void main(String[] args) {
        String url = "jdbc:mysql://localhost:3306/iot_handle?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai";
        String username = "root";
        String password = "root";
        Connection connection = null;
        Statement stmt = null;
        try {
            // 1、初始化数据库信息
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(url, username, password);
            stmt = connection.createStatement();

            // 2、读取dataset.csv文件
            BufferedReader in = new BufferedReader(new FileReader("D:\\dataset.csv"));
            // 读取属性信息【将空格和斜杠转换为下划线】
            String attrsLine = in.readLine().replace(" ", "_").replace("/", "_");
            String[] attrs = attrsLine.split(",");
            // 读取数据信息
            String line;
            List<String> lines = new ArrayList<>();
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
            System.out.println("读取数据数据完毕！");

            // 3、创建数据库表
            StringBuilder sbSql = new StringBuilder();
            sbSql.append("CREATE TABLE tb_data( ");
            for (String attr : attrs) {
                sbSql.append("S");
                sbSql.append(attr);
                sbSql.append(" VARCHAR(128), \n");
            }
            sbSql.append("PRIMARY KEY(");
            sbSql.append("S");
            sbSql.append(attrs[0]);
            sbSql.append("))");
            System.out.println("建表语句：");
            System.out.println(sbSql.toString());
            stmt.executeUpdate(sbSql.toString());
            System.out.println("数据库表创建成功！");

            // 4、插入数据
            // 构造插入语句
            StringBuilder sbInsertSQL = new StringBuilder();
            sbInsertSQL.append("INSERT INTO tb_data VALUES");
            for (int i = 0; i < lines.size(); i++) {
                String[] vals = lines.get(i).split(",");
                sbInsertSQL.append("(");
                for (int i1 = 0; i1 < vals.length; i1++) {
                    sbInsertSQL.append("'");
                    sbInsertSQL.append(vals[i1]);
                    sbInsertSQL.append("'");
                    if (i1 != vals.length - 1) {
                        sbInsertSQL.append(",");
                    }
                }
                sbInsertSQL.append(")");
                if (i != lines.size() - 1) {
                    sbInsertSQL.append(",");
                }
            }
            stmt.executeLargeUpdate(sbInsertSQL.toString());
            System.out.println("数据库插入成功");
        } catch (IOException | ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }
}
