package top.tobing.algorithm.double_pointer.lc_88;

import java.util.Arrays;

/**
 * @Author tobing
 * @Date 2021/5/23 10:23
 * @Description
 */
public class Solution {
    public static void main(String[] args) {
        int[] arra = {2, 0};
        int alen = 1;
        int[] arrb = {1};
        int blen = 1;
        new Solution().merge(arra, alen, arrb, blen);
        System.out.println(Arrays.toString(arra));
    }

    public void merge(int[] nums1, int m, int[] nums2, int n) {
        if (nums1 == null || nums2 == null) {
            return;
        }
        // 不用处理
        if (nums2.length == 0) {
            return;
        }
        int i = m - 1;
        int j = n - 1;
        for (int k = nums1.length - 1; k >= 0; ) {
            if (i >= 0 && j >= 0) {
                nums1[k--] = nums1[i] - nums2[j] >= 0 ? nums1[i--] : nums2[j--];
            } else if (i < 0) {
                nums1[k--] = nums2[j--];
            } else {
                nums1[k--] = nums1[i--];
            }
        }
    }
}
