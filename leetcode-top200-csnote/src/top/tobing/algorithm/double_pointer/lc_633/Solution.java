package top.tobing.algorithm.double_pointer.lc_633;

/**
 * @Author tobing
 * @Date 2021/5/21 8:44
 * @Description
 */
public class Solution {
    /**
     * 双指针解法
     * left = 0;
     * right = Math.sqrt(c);
     * 只需要保证：在left<right且right>=left+1的情况下，(left+1)^2 + (right-1)^2 <= left^2 +right^2，证明如下：
     * (left+1)^2 + (right-1)^2 <= left^2 +right^2
     * =>left^2 + 2left + 1 + right^2 - 2right + 1 <= left^2 + right^2
     * =>2(left-right) + 2 <=0
     * right-left >=1
     * 得证
     */
    public boolean judgeSquareSum(int c) {
        if (c < 0) {
            return false;
        }
        int i = 0;
        int j = (int) Math.sqrt(c);
        while (i <= j) {
            int powSum = i * i + j * j;
            if (powSum < c) {
                i++;
            } else if (powSum > c) {
                j--;
            } else {
                return true;
            }
        }
        return false;
    }
}
