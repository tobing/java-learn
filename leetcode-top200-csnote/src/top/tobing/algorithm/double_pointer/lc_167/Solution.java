package top.tobing.algorithm.double_pointer.lc_167;

/**
 * @Author tobing
 * @Date 2021/5/21 8:45
 * @Description
 */
public class Solution {
    // 双指针解法
    public int[] twoSum(int[] numbers, int target) {
        if (numbers == null || numbers.length == 0) {
            return new int[0];
        }
        int left = 0;
        int right = numbers.length - 1;
        while (left < right) {
            if (numbers[left] + numbers[right] == target) {
                return new int[]{left + 1, right + 1};
            } else if (numbers[left] + numbers[right] < target) {
                left++;
            } else {
                right--;
            }
        }
        return new int[0];
    }
}
