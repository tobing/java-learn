package top.tobing.algorithm.double_pointer.lc_141;

import top.tobing.algorithm.double_pointer.ListNode;

/**
 * @Author tobing
 * @Date 2021/5/24 9:31
 * @Description
 */
public class Solution {

    public boolean hasCycle(ListNode head) {
        // 不存在元素或元素个数为1的时候，不存在环
        if (head == null || head.next == null) {
            return false;
        }
        // 快慢指针，存在环则肯定会追赶上
        ListNode slow = head;
        ListNode quick = head;
        while (slow != null) {
            slow = slow.next;
            if (quick.next == null) {
                quick = head.next;
            } else if (quick.next.next == null) {
                quick = head;
            } else {
                quick = quick.next.next;
            }
            if (slow != null && slow == quick) {
                return true;
            }
        }
        return false;
    }
}
