package top.tobing.algorithm.double_pointer;

/**
 * @Author tobing
 * @Date 2021/5/24 9:31
 * @Description
 */
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int val) {
        this.val = val;
    }
}
