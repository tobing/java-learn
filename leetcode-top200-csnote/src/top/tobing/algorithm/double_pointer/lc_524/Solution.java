package top.tobing.algorithm.double_pointer.lc_524;

import java.util.List;

/**
 * @Author tobing
 * @Date 2021/7/4 19:28
 * @Description
 */
public class Solution {
    public String findLongestWord(String s, List<String> dictionary) {
        String longestWord = "";
        for (String target : dictionary) {
            int l1 = longestWord.length();
            int l2 = target.length();
            if (l1 > l2 || (l1 == l2 && longestWord.compareTo(target) < 0)) {
                continue;
            }
            if (isSubStr(s, target)) {
                longestWord = target;
            }
        }
        return longestWord;
    }

    // 判断一个字符串s是否为目标字符串target的子串
    public boolean isSubStr(String s, String target) {
        int i = 0;
        int j = 0;
        while (i < s.length() & j < target.length()) {
            if (s.charAt(i) == target.charAt(j)) {
                j++;
            }
            i++;
        }
        return j == target.length();
    }
}