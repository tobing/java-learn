package top.tobing.algorithm.double_pointer.lc_345;

/**
 * @Author tobing
 * @Date 2021/5/22 9:09
 * @Description
 */
public class Solution {

    // 双指针
    public String reverseVowels(String s) {
        if (s == null || s.length() == 0) {
            return s;
        }
        int left = 0;
        int right = s.length() - 1;
        char[] chars = s.toCharArray();
        while (left < right) {
            if (isVowels(s.charAt(left)) && isVowels(s.charAt(right))) { // 元音
                char temp = chars[left];
                chars[left] = chars[right];
                chars[right] = temp;
                left++;
                right--;
            } else if (isVowels(s.charAt(left))) {
                right--;
            } else if (isVowels(s.charAt(right))) {
                left++;
            } else {
                left++;
                right--;
            }
        }
        return new String(chars);
    }

    // 此处判断可以用HashSet来提升
    private boolean isVowels(char ch) {
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u'
                || ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U';
    }
}
