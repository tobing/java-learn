package top.tobing.algorithm.dynamic.lc_303;

/**
 * @Author tobing
 * @Date 2021/4/11 9:11
 * @Description 303. 区域和检索 - 数组不可变
 * https://leetcode-cn.com/problems/range-sum-query-immutable/description/
 */
public class Solution {
    class NumArray {

        private int[] nums;
        private int[] sum;

        public NumArray(int[] nums) {
            this.nums = nums;
            sum = new int[nums.length];
            sum[0] = nums[0];
            for (int i = 1; i < sum.length; i++) {
                sum[i] = sum[i - 1] + nums[i];
            }
        }

        public int sumRange(int left, int right) {
            int temp = left == 0 ? 0 : sum[left - 1];
            return sum[right] - temp;
        }
    }

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray obj = new NumArray(nums);
 * int param_1 = obj.sumRange(left,right);
 */

}


