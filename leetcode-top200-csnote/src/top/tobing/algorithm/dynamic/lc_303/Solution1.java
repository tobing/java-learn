package top.tobing.algorithm.dynamic.lc_303;

/**
 * @Author tobing
 * @Date 2021/4/11 9:11
 * @Description 303. 区域和检索 - 数组不可变
 * https://leetcode-cn.com/problems/range-sum-query-immutable/description/
 */
public class Solution1 {
    class NumArray {

        private int[] nums;

        public NumArray(int[] nums) {
            this.nums = nums;
            for (int i = 1; i < nums.length; i++) {
                nums[i] = nums[i - 1] + nums[i];
            }
        }

        public int sumRange(int left, int right) {
            int temp = left == 0 ? 0 : nums[left - 1];
            return nums[right] - temp;
        }
    }

/**
 * Your NumArray object will be instantiated and called as such:
 * NumArray obj = new NumArray(nums);
 * int param_1 = obj.sumRange(left,right);
 */

}


