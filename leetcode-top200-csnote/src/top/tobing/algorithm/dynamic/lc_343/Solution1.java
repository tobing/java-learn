package top.tobing.algorithm.dynamic.lc_343;

/**
 * @Author tobing
 * @Date 2021/4/11 10:15
 * @Description 343. 整数拆分
 * 给定一个正整数 n，将其拆分为至少两个正整数的和，并使这些整数的乘积最大化。 返回你可以获得的最大乘积。
 */
public class Solution1 {

    /**
     * 动态规划
     */
    public int integerBreak(int n) {
        int[] dp = new int[n + 1];
        dp[0] = dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            int max = 0;
            for (int j = 1; j < i; j++) {
                max = Math.max(max, j * dp[i - j]);
                max = Math.max(max, j * (i - j));
            }
            dp[i] = max;
        }
        return dp[n];
    }


}
