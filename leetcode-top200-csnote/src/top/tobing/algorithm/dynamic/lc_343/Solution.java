package top.tobing.algorithm.dynamic.lc_343;

/**
 * @Author tobing
 * @Date 2021/4/11 10:15
 * @Description 343. 整数拆分
 * 给定一个正整数 n，将其拆分为至少两个正整数的和，并使这些整数的乘积最大化。 返回你可以获得的最大乘积。
 */
public class Solution {
    private int[] dp;

    /**
     * 递归+记忆化搜索
     */
    public int integerBreak(int n) {

        if (n == 1) {
            return 1;
        }
        if (dp == null) {
            dp = new int[n + 1];
        }
        if (dp[n] != 0) {
            return dp[n];
        }
        int max = 0;
        for (int i = 1; i < n; i++) {
            int temp = i * integerBreak(n - i);
            max = Math.max(max, i * (n - i));
            max = Math.max(max, temp);
        }
        dp[n] = max;
        return max;
    }


}
