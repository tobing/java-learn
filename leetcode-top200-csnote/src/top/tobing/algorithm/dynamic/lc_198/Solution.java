package top.tobing.algorithm.dynamic.lc_198;

/**
 * @Author tobing
 * @Date 2021/4/10 9:23
 * @Description 198. 打家劫舍
 * https://leetcode-cn.com/problems/house-robber/description/
 */
public class Solution {
    public int rob(int[] nums) {
        // 动态规划
        if (nums == null || nums.length == 0) {
            return -1;
        }
        if (nums.length == 1) {
            return nums[0];
        }
        if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        }
        nums[2] += nums[0];
        for (int i = 3; i < nums.length; i++) {
            nums[i] += Math.max(nums[i - 2], nums[i - 3]);
        }
        return Math.max(nums[nums.length - 1], nums[nums.length - 2]);
    }
}
