package top.tobing.algorithm.dynamic.lc_198;

/**
 * @Author tobing
 * @Date 2021/4/10 9:23
 * @Description 198. 打家劫舍
 * https://leetcode-cn.com/problems/house-robber/description/
 */
public class Solution1 {
    // 打家劫舍简洁写法
    // TODO 待理解
    public int rob(int[] nums) {
        int prev = 0;
        int cur = 0;
        int tmp;
        for (int num : nums) {
            tmp = cur;
            cur = Math.max(prev + num, cur);
            prev = tmp;
        }
        return cur;
    }

}
