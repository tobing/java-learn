package top.tobing.algorithm.dynamic.lc_376;

import java.util.Arrays;

/**
 * @Author tobing
 * @Date 2021/4/18 9:35
 * @Description 376. 摆动序列
 * https://leetcode-cn.com/problems/wiggle-subsequence/description/
 */
public class Solution {
    // 呆瓜辅助数组暴力解法
    public int wiggleMaxLength(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        // 记录以nums[i]为结尾的点的最大摆动序列长度
        int[] dp = new int[nums.length];
        // 记录以nums[i]为结尾的前一个节点是递增(1)，还是递减(-1)，还是首节点(0)
        int[] status = new int[nums.length];
        Arrays.fill(dp, 1);
        status[0] = 0;
        for (int i = 1; i < nums.length; i++) {
            int max = 0;
            for (int j = i - 1; j >= 0; j--) {
                if (nums[i] > nums[j] & status[j] <= 0) {       // ↑
                    if (dp[j] > max) {
                        max = dp[j];
                        status[i] = 1;
                    }
                } else if (nums[i] < nums[j] & status[j] >= 0) {// ↓
                    if (dp[j] > max) {
                        max = dp[j];
                        status[i] = -1;
                    }
                }
            }
            dp[i] += max;
        }
        int max = 0;
        for (int i : dp) {
            max = Math.max(max, i);
        }
        return max;
    }
}
