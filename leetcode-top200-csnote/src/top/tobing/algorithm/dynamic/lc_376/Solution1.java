package top.tobing.algorithm.dynamic.lc_376;

import java.util.Arrays;

/**
 * @Author tobing
 * @Date 2021/4/18 9:35
 * @Description 376. 摆动序列
 * https://leetcode-cn.com/problems/wiggle-subsequence/description/
 */
public class Solution1 {
    // up[i] 表示 nums[0:i] 中最后两个数字递增的最长摆动序列长度
    // down[i] 表示 nums[0:i] 中最后两个数字递减的最长摆动序列长度
    // 注意到 down 和 up 只和前一个状态有关，所以我们可以优化存储，分别用一个变量即可。ttu
    public int wiggleMaxLength(int[] nums) {
        int down = 1;
        int up = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] > nums[i - 1]) {
                up = down + 1;
            } else if (nums[i] < nums[i - 1]) {
                down = up + 1;
            }
        }
        return nums.length == 0 ? 0 : Math.max(up, down);
    }
}
