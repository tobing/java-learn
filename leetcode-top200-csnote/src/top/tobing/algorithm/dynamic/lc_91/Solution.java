package top.tobing.algorithm.dynamic.lc_91;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author tobing
 * @Date 2021/4/13 8:41
 * @Description 91. 解码方法
 * https://leetcode-cn.com/problems/decode-ways/description/
 */
public class Solution {

    private Map<String, Integer> map = new HashMap<>();

    public int numDecodings(String s) {
        if (s == null || s.startsWith("0")) {
            return 0;
        }
        if (s.length() <= 1) {
            return 1;
        }
        if (map.containsKey(s)) {
            return map.get(s);
        }
        int num = Integer.parseInt(s.substring(0, 2));
        if (num <= 26) {
            int count = numDecodings(s.substring(1)) + numDecodings(s.substring(2));
            map.put(s, count);
            return count;
        } else {
            int count = numDecodings(s.substring(1));
            map.put(s, count);
            return count;
        }
    }
}
