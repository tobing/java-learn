package top.tobing.algorithm.dynamic.lc_213;

/**
 * @Author tobing
 * @Date 2021/4/10 9:42
 * @Description 213. House Robber II (Medium)
 * https://leetcode-cn.com/problems/house-robber-ii/description/
 */
public class Solution {
    public int rob(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        if (nums.length == 1) {
            return nums[0];
        }
        // 因为是环形，有了第一个元素，就不能有最后一个元素
        return Math.max(rob(nums, 0, nums.length - 2), rob(nums, 1, nums.length - 1));
    }

    // 动态规划求数组指定范围 nums[left...right]范围内的最大值
    private int rob(int[] nums, int left, int right) {
        int prev2 = 0;
        int prev1 = 0;
        for (int i = left; i <= right; i++) {
            int cur = Math.max(prev1, prev2 + nums[i]);
            prev2 = prev1;
            prev1 = cur;
        }
        return prev1;
    }
}
