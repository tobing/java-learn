package top.tobing.algorithm.dynamic.lc_646;

import java.util.Arrays;
import java.util.Comparator;

/**
 * @Author tobing
 * @Date 2021/4/16 23:01
 * @Description
 */
public class Solution {
    public int findLongestChain(int[][] pairs) {
        if (pairs == null || pairs.length == 0) {
            return 0;
        }
        // 先按照左边界对排序
        Arrays.sort(pairs, (o1, o2) -> o1[0] - o2[0]);
        // 创建dp表，填充 1
        int[] dp = new int[pairs.length];
        Arrays.fill(dp, 1);
        // 遍历所有元素，对每个元素，判断之前的元素是否位于当前元素的左边的部分
        for (int i = 1; i < pairs.length; i++) {
            int max = 0;
            for (int j = i - 1; j >= 0; j--) {
                // 遍历区间位于当前区间前面，
                if (pairs[j][0] > pairs[i][1] && dp[j] > max) {
                    max = dp[j];
                }
                dp[i] += max;
            }
        }
        int max = 1;
        for (int i : dp) {
            max = Math.max(i, max);
        }
        return max;
    }
}
