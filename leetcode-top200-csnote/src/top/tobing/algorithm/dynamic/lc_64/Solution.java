package top.tobing.algorithm.dynamic.lc_64;

/**
 * @Author tobing
 * @Date 2021/4/10 10:07
 * @Description 64. 最小路径和
 * https://leetcode-cn.com/problems/minimum-path-sum/
 */
public class Solution {
    private Integer[][] dp;

    // 递归+记忆化搜索
    public int minPathSum(int[][] grid) {
        dp = new Integer[grid.length][grid[0].length];
        return minPathSum(grid, 0, 0);
    }

    // 求gird[i][j] 到 gird[-1][-1] 的最小值
    private int minPathSum(int[][] gird, int i, int j) {
        // 终点元素，直接返回
        if (j == gird[0].length - 1 && i == gird.length - 1) {
            return gird[i][j];
        }
        //
        if (i >= gird.length - 1) {
            return gird[i][j] + minPathSum(gird, i, j + 1);
        }
        if (j >= gird[0].length - 1) {
            return gird[i][j] + minPathSum(gird, i + 1, j);
        }

        if (dp[i][j] == null) {
            dp[i][j] = gird[i][j] + Math.min(minPathSum(gird, i + 1, j), minPathSum(gird, i, j + 1));
        }
        return dp[i][j];
    }
}
