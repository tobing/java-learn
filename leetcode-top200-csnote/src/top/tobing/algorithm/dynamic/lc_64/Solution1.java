package top.tobing.algorithm.dynamic.lc_64;

/**
 * @Author tobing
 * @Date 2021/4/10 10:07
 * @Description 64. 最小路径和
 * https://leetcode-cn.com/problems/minimum-path-sum/
 */
public class Solution1 {

    // 动态规划
    public int minPathSum(int[][] grid) {
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (i == 0 && j == 0) {
                    continue;
                } else if (i == 0) {
                    grid[i][j] += grid[i][j - 1];
                } else if (j == 0) {
                    grid[i][j] += grid[i - 1][j];
                } else {
                    grid[i][j] += Math.min(grid[i][j - 1], grid[i - 1][j]);
                }
            }
        }
        return grid[grid.length - 1][grid[0].length - 1];
    }
}
