package top.tobing.algorithm.dynamic.lc_279;

/**
 * @Author tobing
 * @Date 2021/4/12 22:33
 * @Description 279. 完全平方数
 * https://leetcode-cn.com/problems/perfect-squares/
 */
public class Solution {

    // 动态规划-超时
    public int numSquares(int n) {
        int[] dp = new int[n + 1];
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            if (isSquare(i)) {  // 当前数时平方数
                dp[i] = 1;
            } else {            // 当前数不是平方数
                int min = Integer.MAX_VALUE;
                for (int j = 1; j <= i; j++) {
                    if (isSquare(j)) {
                        min = Math.min(min, dp[i - j] + 1);
                    }
                }
                dp[i] = min;
            }
        }
        return dp[n];
    }

    private boolean isSquare(int num) {
        int temp = (int) Math.sqrt(Double.valueOf(num));
        return temp * temp == num;
    }
}
