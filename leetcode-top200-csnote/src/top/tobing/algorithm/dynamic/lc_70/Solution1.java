package top.tobing.algorithm.dynamic.lc_70;

/**
 * @Author tobing
 * @Date 2021/4/9 23:01
 * @Description
 */
public class Solution1 {
    // 动态规划秒杀
    // 考虑到只需要用到 ints[i-1] 和 ints[i-2]，用变量代替数组，优化时间复杂度。
    public int climbStairs(int n) {
        if (n == 1 || n == 2) {
            return n;
        }
        int prev1 = 1;
        int prev2 = 2;

        for (int i = 2; i < n; i++) {
            int cur = prev1 + prev2;
            prev1 = prev2;
            prev2 = cur;

        }
        return prev2;
    }
}
