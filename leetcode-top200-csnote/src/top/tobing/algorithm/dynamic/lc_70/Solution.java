package top.tobing.algorithm.dynamic.lc_70;

/**
 * @Author tobing
 * @Date 2021/4/9 23:01
 * @Description
 */
public class Solution {
    // 动态规划秒杀
    public int climbStairs(int n) {
        if (n == 1 || n == 2) {
            return n;
        }
        int[] ints = new int[n + 1];
        ints[1] = 1;
        ints[2] = 2;
        for (int i = 3; i <= n; i++) {
            ints[i] = ints[i - 1] + ints[i - 2];
        }
        return ints[n];
    }
}
