package top.tobing.algorithm.dynamic.lc_416;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.stream.Collectors;

/**
 * @Author tobing
 * @Date 2021/4/20 9:07
 * @Description 416. 分割等和子集
 * https://leetcode-cn.com/problems/partition-equal-subset-sum/description/
 */
public class Solution {
    public static void main(String[] args) {
        boolean b = new Solution().canPartition(new int[]{1, 2, 3, 4, 5, 6, 7});
        System.out.println(b);
    }

    public boolean canPartition(int[] nums) {
        // 求得数组和
        int sum = 0;
        for (int i : nums) {
            sum += i;
        }
        if (sum % 2 != 0) {
            return false;
        }
        // 在数组中寻找和为sum/2的元素组合
        int w = sum / 2;
        boolean[] dp = new boolean[w + 1];
        dp[0] = true;
        for (int num : nums) {
            for (int i = w; i >= num; i--) {
                dp[i] = dp[i] || dp[i - num];
            }
        }
        return dp[w];
    }
}
