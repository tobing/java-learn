package top.tobing.algorithm.dynamic.lc_413;

/**
 * @Author tobing
 * @Date 2021/4/11 9:28
 * @Description 413. 等差数列划分
 * https://leetcode-cn.com/problems/arithmetic-slices/description/
 */
public class Solution {

    // TODO 没掌握
    public int numberOfArithmeticSlices(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        // dp[i] 表示以 A[i] 为结尾的等差递增子区间的个数。
        int[] dp = new int[nums.length];
        for (int i = 2; i < nums.length; i++) {
            if (nums[i] - nums[i - 1] == nums[i - 1] - nums[i - 2]) {
                dp[i] = dp[i - 1] + 1;
            }
        }
        int sum = 0;
        for (int i : dp) {
            sum += i;
        }
        return sum;
    }
}
