package top.tobing.algorithm.dynamic.lc_300;

import java.util.Arrays;

/**
 * @Author tobing
 * @Date 2021/4/16 22:37
 * @Description 300. 最长递增子序列
 * https://leetcode-cn.com/problems/longest-increasing-subsequence/
 */
public class Solution {

    // 用来记录以当前节点为末节点的最长递增子序列
    private int[] dp;

    // 暴力解
    public int lengthOfLIS(int[] nums) {
        dp = new int[nums.length];
        Arrays.fill(dp, 1);
        for (int i = 1; i < nums.length; i++) {
            int max = 0;
            for (int j = i; j >= 0; j--) {
                if (nums[i] > nums[j] && dp[j] > max) {
                    max = dp[j];
                }
            }
            dp[i] += max;
        }
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < dp.length; i++) {
            max = Math.max(max, dp[i]);
        }
        return max;
    }
}
