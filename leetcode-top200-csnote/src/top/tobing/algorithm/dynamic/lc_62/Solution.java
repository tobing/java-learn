package top.tobing.algorithm.dynamic.lc_62;

import java.util.Arrays;

/**
 * @Author tobing
 * @Date 2021/4/10 10:54
 * @Description 62. 不同路径
 * https://leetcode-cn.com/problems/unique-paths/description/
 */
public class Solution {
    // 动态规划，简单秒杀
    public int uniquePaths(int m, int n) {
        int[][] matrix = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 || j == 0) {
                    matrix[i][j] = 1;
                    continue;
                }
                matrix[i][j] = matrix[i - 1][j] + matrix[i][j - 1];
            }
        }
        return matrix[m - 1][n - 1];
    }
}
