package top.tobing.other.lc_198;

/**
 * @Author tobing
 * @Date 2021/3/31 22:42
 * @Description
 */
public class Solution {

    // dp表
     private Integer[] dp;

    // 解法1：递归+dp表
    public int rob01(int[] nums) {
        dp = new Integer[nums.length];
        int i = rob01(nums, 0);
        int j = rob01(nums, 1);
        return Math.max(i, j);
    }

    // 解法1：递归+dp表
    private int rob01(int[] nums, int index) {
        if (index >= nums.length) {
            return 0;
        }
        if (dp[index] != null) {
            return dp[index];
        }
        int i = rob01(nums, index + 2);
        int j = rob01(nums, index + 3);
        int res = Math.max(i, j) + nums[index];
        dp[index] = res;
        return res;
    }

    // 解法2：动态规划
    public int rob02(int[] nums) {
        if (nums.length < 2) {
            return nums[0];
        }
        if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        }
        // dp表
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        dp[1] = nums[1];
        dp[2] = Math.max(nums[0] + nums[2], nums[2]);
        // 填充dp表
        for (int i = 3; i < nums.length; i++) {
            dp[i] = Math.max(dp[i - 2], dp[i - 3]) + nums[i];
        }
        return Math.max(dp[nums.length - 1], dp[nums.length - 2]);
    }

    // 解法2：动态规划2
    public int rob(int[] nums) {
        if (nums.length < 2) {
            return nums[0];
        }
        if (nums.length == 2) {
            return Math.max(nums[0], nums[1]);
        }
        nums[2] += nums[0];
        // 填充dp表
        for (int i = 3; i < nums.length; i++) {
            nums[i] = Math.max(nums[i - 2], nums[i - 3]) + nums[i];
        }
        return Math.max(nums[nums.length - 1], nums[nums.length - 2]);
    }


}
