package top.tobing.datastruct.linkedlist.lc_19;

/**
 * @Author tobing
 * @Date 2021/3/25 9:36
 * @Description 19. 删除链表的倒数第 N 个结点
 * https://leetcode-cn.com/problems/remove-nth-node-from-end-of-list/description/
 * 给你一个链表，删除链表的倒数第 n 个结点，并且返回链表的头结点。
 * <p>
 * 进阶：你能尝试使用一趟扫描实现吗？
 */
public class Solution {
    class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }
//    呆瓜递归法
//    private int n;
//
//    public ListNode removeNthFromEnd(ListNode head, int n) {
//        if (head == null || n < 0) {
//            return null;
//        }
//        this.n = n;
//        return heler(head);
//    }
//
//    private ListNode heler(ListNode node) {
//        if (node == null) {
//            return node;
//        }
//        ListNode next = heler(node.next);
//        node.next = next;
//        if (--n == 0) {
//            return node.next;
//        }
//        return node;
//    }

    // 高阶双指针解法
    public ListNode removeNthFromEnd(ListNode head, int n) {
        // 虚拟头节点
        ListNode dummy = new ListNode(-1);
        dummy.next = head;
        ListNode left = dummy;  // 后行者
        ListNode right = dummy; // 先行者
        for (; n > 0; n--) {
            right = right.next;
        }
        while (right.next != null) {
            left = left.next;
            right = right.next;
        }
        if (left.next != null) {
            left.next = left.next.next;
        }
        return dummy.next;
    }
}
