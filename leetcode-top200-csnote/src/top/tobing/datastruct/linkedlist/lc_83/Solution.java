package top.tobing.datastruct.linkedlist.lc_83;

/**
 * @Author tobing
 * @Date 2021/3/25 9:28
 * @Description 83. 删除排序链表中的重复元素
 * https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list/
 * 给定一个排序链表，删除所有重复的元素，使得每个元素只出现一次。
 * <p>
 * 输入: 1->1->2
 * 输出: 1->2
 */
public class Solution {
    class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public ListNode deleteDuplicates(ListNode head) {
        ListNode temp = head;
        while(temp!=null) {
            if (temp.next!=null && temp.val == temp.next.val) {
                temp.next = temp.next.next;
            } else {
                temp = temp.next;
            }
        }
        return head;
    }
}
