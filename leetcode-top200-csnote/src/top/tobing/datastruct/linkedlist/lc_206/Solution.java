package top.tobing.datastruct.linkedlist.lc_206;

/**
 * @Author tobing
 * @Date 2021/3/24 11:22
 * @Description 206. 反转链表
 * https://leetcode-cn.com/problems/reverse-linked-list/description/
 * 反转一个单链表。
 * 输入: 1->2->3->4->5->NULL
 * 输出: 5->4->3->2->1->NULL
 * 进阶:
 * 你可以迭代或递归地反转链表。你能否用两种方法解决这道题？
 */
public class Solution {

    class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    /**
     * 递归法
     */
    public ListNode reverseList01(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode ret = reverseList01(head.next);  // 递归到最后一个节点，保存最后一个节点为新链表的首节点
        head.next.next = head;                  // 返回的时候将每个节点进行翻转
        head.next = null;                       // 将反转节点的后继置空
        return ret;                             // 返回新链表的首节点
    }

    /**
     * 非递归：头插法
     */
    public ListNode reverseList(ListNode head) {
        ListNode newHead = new ListNode(-1);
        while (head != null) {
             ListNode next = head.next; // 保存当前节点的下一个节点
             head.next = newHead.next;  // 将当前节点指向newHead的下一个节点【头插法】
             newHead.next = head;       // 将newHead的下一个节点设为新添加的节点
             head = next;               // 将当前节点设置为旧链表的下一个节点
        }
        return newHead.next;
    }
}
