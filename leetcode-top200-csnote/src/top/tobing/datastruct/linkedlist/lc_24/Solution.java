package top.tobing.datastruct.linkedlist.lc_24;

/**
 * @Author tobing
 * @Date 2021/3/25 11:59
 * @Description 24. 两两交换链表中的节点
 * https://leetcode-cn.com/problems/swap-nodes-in-pairs/
 * <p>
 * 给定一个链表，两两交换其中相邻的节点，并返回交换后的链表。
 * 你不能只是单纯的改变节点内部的值，而是需要实际的进行节点交换。
 * 输入：head = [1,2,3,4]
 * 输出：[2,1,4,3]
 */
public class Solution {
    class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    // 万能递归法，感觉反转链表变体
    public ListNode swapPairs(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        // 保存后继节点
        ListNode temp = swapPairs(head.next.next);
        // 将当前节点与下一个节点互换
        ListNode cur = head;
        ListNode next = head.next;

        cur.next = temp;
        next.next = cur;
        return next;
    }
}
