package top.tobing.datastruct.linkedlist.lc_445;

import java.util.LinkedList;

/**
 * @Author tobing
 * @Date 2021/3/26 22:36
 * @Description 445. 两数相加 II
 * https://leetcode-cn.com/problems/add-two-numbers-ii/
 * <p>
 * 给你两个 非空 链表来代表两个非负整数。数字最高位位于链表开始位置。它们的每个节点只存储一位数字。将这两数相加会返回一个新的链表。
 * 你可以假设除了数字 0 之外，这两个数字都不会以零开头。
 * <p>
 * 进阶：
 * 如果输入链表不能修改该如何处理？换句话说，你不能对列表中的节点进行翻转。
 * <p>
 * 输入：(7 -> 2 -> 4 -> 3) + (5 -> 6 -> 4)
 * 输出：7 -> 8 -> 0 -> 7
 */
public class Solution {
    class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    /**
     * 时间复杂度O(m+n)
     * 空间复杂度O(m+n)
     */
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        LinkedList<ListNode> stackA = listNodeToStack(l1);
        LinkedList<ListNode> stackB = listNodeToStack(l2);
        ListNode res = new ListNode(-1);
        int ten = 0;
        while (!stackA.isEmpty() || !stackB.isEmpty()) {
            int x = stackA.isEmpty() ? 0 : stackA.pop().val;
            int y = stackB.isEmpty() ? 0 : stackB.pop().val;
            int sum = x + y + ten;
            ten = sum / 10;
            int bit = sum %= 10;
            // 头插法
            ListNode temp = new ListNode(bit);
            temp.next = res.next;
            res.next = temp;
        }
        if (ten == 1) {
            ListNode temp = new ListNode(1);
            temp.next = res.next;
            res.next = temp;
        }
        return res.next;
    }

    /**
     * 将ListNode转换为Stack
     */
    private LinkedList<ListNode> listNodeToStack(ListNode listNode) {
        ListNode cur = listNode;
        LinkedList<ListNode> res = new LinkedList<>();
        while (cur != null) {
            res.push(cur);
            cur = cur.next;
        }
        return res;
    }

}
