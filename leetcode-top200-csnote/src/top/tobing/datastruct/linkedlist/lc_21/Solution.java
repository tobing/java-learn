package top.tobing.datastruct.linkedlist.lc_21;

/**
 * @Author tobing
 * @Date 2021/3/24 12:06
 * @Description 21. 合并两个有序链表
 * https://leetcode-cn.com/problems/merge-two-sorted-lists/description/
 * 将两个升序链表合并为一个新的 升序 链表并返回。新链表是通过拼接给定的两个链表的所有节点组成的。
 */
public class Solution {
    class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode dummy = new ListNode(-1);
        ListNode cur = dummy;
        while (l1 != null && l2 != null) {
            ListNode temp = (l1.val < l2.val) ? l1 : l2;
            cur.next = temp;
            temp = temp.next;
        }
        cur.next = (l1 == null) ? l2 : l1;
        return dummy.next;
    }
}
