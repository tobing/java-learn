package top.tobing.datastruct.linkedlist.lc_160;

/**
 * @Author tobing
 * @Date 2021/3/24 10:52
 * @Description 160. 相交链表
 * @url https://leetcode-cn.com/problems/intersection-of-two-linked-lists/description/
 * 编写一个程序，找到两个单链表相交的起始节点。
 * <p>
 * 注意：
 * 如果两个链表没有交点，返回 null.
 * 在返回结果后，两个链表仍须保持原有的结构。
 * 可假定整个链表结构中没有循环。
 * 程序尽量满足 O(n) 时间复杂度，且仅用 O(1) 内存。
 */
public class Solution {
    // 链表节点
    class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    /**
     * 1、对于存在公共节点c，在a->c->b、b->c->a之后下面的就是公共节点c，此时节点不为null，且相等，退出。
     * 2、对于不存在公共节点，在a->b、b->a之后两个都为null，退出。
     */
    public ListNode getIntersectionNode01(ListNode headA, ListNode headB) {
        ListNode l1 = headA, l2 = headB;
        while (l1 != l2) {
            if (l1 == null) {   // 链表A已经到了尽头、切换到链表B
                l1 = headB; // a+c==>b
            } else {
                l1 = l1.next;
            }
            if (l2 == null) {   // 链表B到了尽头、切换到链表A
                l2 = headA; // b+c==>a
            } else {
                l2 = l2.next;
            }
        }
        return l1;
    }

    /**
     * 上述实现简化版
     */
    public ListNode getIntersectionNode02(ListNode headA, ListNode headB) {
        ListNode l1 = headA, l2 = headB;
        while (l1 != l2) {
            l1 = (l1 == null) ? headB : l1.next;
            l2 = (l2 == null) ? headA : l2.next;
        }
        return l1;
    }
}