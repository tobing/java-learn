package top.tobing.datastruct.linkedlist.lc_725;

/**
 * @Author tobing
 * @Date 2021/3/27 18:00
 * @Description
 */
public class Solution {
    class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public ListNode[] splitListToParts(ListNode root, int k) {
        // 计算size
        int size = 0;
        ListNode cur = root;
        while (cur != null) {
            size++;
            cur = cur.next;
        }

        // 计算segment、mod
        int mod = size % k;
        int segment = size / k;
        ListNode[] ret = new ListNode[k];
        cur = root;
        for (int i = 0; cur != null && i < k; i++) {
            ret[i] = cur;
            // 计算每个segment确保前面的节点个数最多
            int curSize = segment + (mod-- > 0 ? 1 : 0);
            for (int j = 0; j < curSize - 1; j++) {
                cur = cur.next;
            }
            // 分割完一段之后将该段的尾置NULL
            ListNode next = cur.next;
            cur.next = null;
            cur = next;
        }
        return ret;
    }
}
