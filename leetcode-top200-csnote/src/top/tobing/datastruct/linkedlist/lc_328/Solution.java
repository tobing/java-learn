package top.tobing.datastruct.linkedlist.lc_328;

/**
 * @Author tobing
 * @Date 2021/3/28 18:42
 * @Description 328. 奇偶链表
 * https://leetcode-cn.com/problems/odd-even-linked-list/description/
 */
public class Solution {
    class ListNode {
        int val;
        ListNode next;

        public ListNode(int val) {
            this.val = val;
        }
    }

    public ListNode oddEvenList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode tail = null;       // 偶链表最后一个接单
        ListNode cur = head;        // 遍历指针
        ListNode newHead = new ListNode(-1);// 奇链表的虚拟头节点
        ListNode newCur = newHead;              // 奇链表的遍历指针
        while (cur != null && cur.next != null) {   // 对于迭代删除元素，需要同时判断
            ListNode next = cur.next;   // 被移除节点
            cur.next = cur.next.next;   // 将奇节点从链表中移除
            newCur.next = next;         // 将删除节点放到新链表
            newCur = newCur.next;
            // 被删除节点之后没有节点
            if (newCur.next == null) {
                tail = cur;
            } else {
                tail = newCur.next;
            }
            cur = cur.next;
        }
        tail.next = newHead.next;
        newHead.next = null;
        newCur.next = null;
        return head;
    }
}
