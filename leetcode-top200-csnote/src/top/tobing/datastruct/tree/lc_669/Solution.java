package top.tobing.datastruct.tree.lc_669;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/4/4 10:45
 * @Description 669. 修剪二叉搜索树
 * https://leetcode-cn.com/problems/trim-a-binary-search-tree/description/
 */
public class Solution {
    public TreeNode trimBST(TreeNode root, int low, int high) {
        if (root == null) {
            return root;
        }
        if (root.val > high) {      // 对于当前节点值比给定范围大：访问左孩子
            return trimBST(root.left, low, high);
        } else if (root.val < low) {// 对于当前节点值比给定范围小：访问右孩子
            return trimBST(root.right, low, high);
        } else {        // 当前节点在范围内，对其左右孩子依次递归操作
            root.left = trimBST(root.left, low, high);
            root.right = trimBST(root.right, low, high);
            return root;
        }
    }
}
