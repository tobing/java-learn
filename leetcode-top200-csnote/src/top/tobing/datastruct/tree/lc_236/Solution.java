package top.tobing.datastruct.tree.lc_236;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/4/4 12:45
 * @Description 236. 二叉树的最近公共祖先
 */
public class Solution {


    // 递归返回判断的精髓
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || root == p || root == q) {
            return root;
        }
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);
        if (left == null) {
            return right;
        } else {
            if (right == null) {
                return left;
            } else {
                return root;
            }
        }
    }
}
