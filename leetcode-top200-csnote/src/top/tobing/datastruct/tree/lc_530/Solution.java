package top.tobing.datastruct.tree.lc_530;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/4/5 22:20
 * @Description 530. 二叉搜索树的最小绝对差
 * https://leetcode-cn.com/problems/minimum-absolute-difference-in-bst/description/
 */
public class Solution {

    /**
     * 中序遍历 + 全局变量
     * 中序遍历使得节点的遍历时有序的，加上全局变量保存上一个节点，实现相邻节点之间的判断。
     */

    // 节点之间的最短距离
    private int min = Integer.MAX_VALUE;
    // 当前遍历节点的前一个节点
    private TreeNode prev = null;

    public int getMinimumDifference(TreeNode root) {
        helper(root);
        return min;
    }

    private void helper(TreeNode root) {
        if (root == null) {
            return;
        }
        helper(root.left);
        if (prev == null) {
            prev = root;
        } else {
            min = Math.min(min, Math.abs(prev.val - root.val));
            prev = root;
        }
        helper(root.right);
    }
}
