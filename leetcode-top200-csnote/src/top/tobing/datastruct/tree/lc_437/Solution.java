package top.tobing.datastruct.tree.lc_437;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/3/29 12:38
 * @Description 437. 路径总和 III
 * https://leetcode-cn.com/problems/path-sum-iii/description/
 */
public class Solution {

    /**
     * 递归写法、难、自闭
     */
    public int pathSum(TreeNode root, int sum) {    // 相当于外循环，对树的每一个节点进行遍历判断是否存在子树满足路径和
        if (root == null) {
            return 0;
        }

        return dfs(root, sum) + pathSum(root.left, sum) + pathSum(root.right, sum);
    }

    /**
     * 判断以 root 为根的满足总路径和的总数
     *
     * @param root root
     * @param sum  sum
     * @return int
     */
    public int dfs(TreeNode root, int sum) {    // 相当于内循环，判断以root为首的节点，是否存在满足路径和的符号的树
        if (root == null) {
            return 0;
        }

        int count = 0;
        if (root.val == sum) {
            count = 1;
        }
        count += dfs(root.left, sum - root.val);
        count += dfs(root.right, sum - root.val);
        return count;
    }


}
