package top.tobing.datastruct.tree.lc_112;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/3/29 12:13
 * @Description 112. 路径总和
 * https://leetcode-cn.com/problems/path-sum/description/
 */
public class Solution {
    public boolean hasPathSum(TreeNode root, int targetSum) {
        return helper(root, 0, targetSum);
    }

    // 注意是到叶子节点还是到任何节点，遍历，一般。
    private boolean helper(TreeNode root, int pathValue, int targetSum) {
        if (root == null) {
            return false;
        }
        // 运算路径和
        int path = root.val + pathValue;
        // 叶子节点
        if (root.left == null && root.right == null) {
            return path == targetSum;
        }
        boolean left = helper(root.left, path, targetSum);
        boolean right = helper(root.right, path, targetSum);
        return left || right;
    }
}
