package top.tobing.datastruct.tree.lc_235;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/4/4 11:55
 * @Description 235. 二叉搜索树的最近公共祖先
 * https://leetcode-cn.com/problems/lowest-common-ancestor-of-a-binary-search-tree/description/
 * 【注意】
 * 所有节点的值都是唯一的。
 * p、q 为不同节点且均存在于给定的二叉搜索树中。
 */
public class Solution {


    // 利用二叉树的性质
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || p == null || q == null) {   // 特殊情况
            return null;
        }
        if (root.val < Math.min(p.val, q.val)) {        // 当前节点比p、q的最小值还要小，说明公共点在右子树
            return lowestCommonAncestor(root.right, p, q);
        } else if (root.val > Math.max(p.val, q.val)) { // 当前节点比p、q的最大值还要大、说明公共点在左子树
            return lowestCommonAncestor(root.left, p, q);
        } else {        // 剩余的情况就是，当前节点必定在q和p的值之间
            return root;
        }
    }

    /**为什么最后一种情况就是最近公共祖先？有没有可能是当前节点的孩子节点？（为了方便讨论，假设 q.val < p.val）
     * 假设当前节点的左孩子 node.left 是p、q的公共节点
     * 由平衡二叉树原理的，node.val > 所有子节点的val，则node.val > Math.max(p.val, q.val);
     * 右孩子同理
     */
}
