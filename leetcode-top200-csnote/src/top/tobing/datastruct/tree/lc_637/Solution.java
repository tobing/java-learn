package top.tobing.datastruct.tree.lc_637;

import top.tobing.datastruct.tree.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @Author tobing
 * @Date 2021/4/3 9:45
 * @Description 637. 二叉树的层平均值
 * https://leetcode-cn.com/problems/average-of-levels-in-binary-tree/description/
 */
public class Solution {

    /**
     * 层序遍历要保存每一层的数量状态，因此在内循环需要先保存size
     */
    public List<Double> averageOfLevels(TreeNode root) {
        if (root == null) {
            return null;
        }
        List<Double> res = new ArrayList<>();
        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.addFirst(root);
        res.add((double) root.val);
        while (queue.size() != 0) {
            // 遍历每层
            double avg = 0; // 当前层节点数值
            int size = 0;
            for (int i = queue.size(); i > 0; i--) {
                TreeNode treeNode = queue.removeLast();
                if (treeNode.left != null) {
                    queue.addFirst(treeNode.left);
                    avg += treeNode.left.val;
                    size++;
                }
                if (treeNode.right != null) {
                    queue.addFirst(treeNode.right);
                    avg += treeNode.right.val;
                    size++;
                }
            }
            if (size != 0) {
                avg /= size;
                res.add(avg);
            }
        }
        return res;
    }

}
