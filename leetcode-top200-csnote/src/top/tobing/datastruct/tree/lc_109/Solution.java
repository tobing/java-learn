package top.tobing.datastruct.tree.lc_109;

import top.tobing.datastruct.tree.ListNode;
import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/4/5 21:17
 * @Description 109. 有序链表转换二叉搜索树
 * https://leetcode-cn.com/problems/convert-sorted-list-to-binary-search-tree/description/
 */
public class Solution {
    public TreeNode sortedListToBST(ListNode head) {
        if (head == null) {
            return null;
        }
        if (head.next == null) {
            return new TreeNode(head.val);
        }
        ListNode preMid = helper(head); // 中点的前一个节点
        ListNode mid = preMid.next;     // 中点
        preMid.next = null;             // 沿着中点将list分裂
        TreeNode res = new TreeNode(mid.val);   // 生成中点
        res.left = sortedListToBST(head);       // 递归左节点
        res.right = sortedListToBST(mid.next);  // 递归右节点
        return res;

    }

    /**
     * 通过快慢指针寻找list的中点
     */
    private ListNode helper(ListNode head) {
        ListNode slow = head;   // 慢指针；每次一步
        ListNode fast = head;   // 快指针：每次两步
        ListNode pre = head;    // 用来纠正中点
        while (fast != null && fast.next != null) {
            pre = slow;
            slow = slow.next;
            fast = fast.next.next;
        }
        return pre;

        /**
         * 寻找链表的中间点有个小技巧：
         *
         * 快慢指针起初都指向头结点，分别一次走两步和一步，当快指针走到尾节点时，慢指针正好走到链表的中间。断成两个链表，分而治之。
         * 为了断开，我们需要保存慢指针的前一个节点，因为单向链表的结点没有前驱指针。
         */

    }
}
