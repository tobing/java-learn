package top.tobing.datastruct.tree.lc_671;

import top.tobing.datastruct.tree.TreeNode;


/**
 * @Author tobing
 * @Date 2021/4/2 20:57
 * @Description 671. 二叉树中第二小的节点
 * https://leetcode-cn.com/problems/second-minimum-node-in-a-binary-tree/
 * 如果一个节点有两个子节点的话，那么该节点的值等于两个子节点中较小的一个。
 */
public class Solution {
    public int findSecondMinimumValue(TreeNode root) {
        // 无节点情况
        if (root == null) {
            return -1;
        }
        // 只有一个节点或时叶子节点
        if (root.left == null && root.right == null) {
            return -1;
        }
        int leftVal = root.left.val;
        int rightVal = root.right.val;
        if (leftVal == root.val) {      // 左边更小
            leftVal = findSecondMinimumValue(root.left);
        }
        if (rightVal == root.val) {     // 右边更小
            rightVal = findSecondMinimumValue(root.right);
        }
        if (leftVal != -1 && rightVal != -1) {  // 非叶子节点
            return Math.min(leftVal, rightVal);
        }
        return leftVal == -1 ? rightVal : leftVal;
    }
}
