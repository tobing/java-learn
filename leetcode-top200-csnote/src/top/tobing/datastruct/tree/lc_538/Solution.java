package top.tobing.datastruct.tree.lc_538;

import top.tobing.datastruct.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author tobing
 * @Date 2021/4/4 11:28
 * @Description 538. 把二叉搜索树转换为累加树
 * https://leetcode-cn.com/problems/convert-bst-to-greater-tree/description/
 */
public class Solution {
    private List<TreeNode> list = new ArrayList<>();

    // 中序建立，反序链表化
    // 在通过链表累加的方式实现
    public TreeNode convertBST(TreeNode root) {
        if (root == null) {
            return null;
        }
        inOrder(root);
        for (int i = 1; i < list.size(); i++) {
            TreeNode cur = list.get(i);
            TreeNode pre = list.get(i - 1);
            cur.val += pre.val;
        }
        return root;
    }

    // 中序遍历
    private void inOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        inOrder(root.right);
        list.add(root);
        inOrder(root.left);
    }
}
