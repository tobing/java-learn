package top.tobing.datastruct.tree.lc_538;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/4/4 11:28
 * @Description 538. 把二叉搜索树转换为累加树
 * https://leetcode-cn.com/problems/convert-bst-to-greater-tree/description/
 */
public class Solution1 {

    // 全局变量比参数更加灵活
    // 记录已经累加的值
    private int sum = 0;

    public TreeNode convertBST(TreeNode root) {
        convertBST(root);
        return root;
    }

    // 反向中序遍历，先访问右、在访问中、最后访问左
    private void inOrder(TreeNode node) {
        if (node == null) {
            return;
        }
        inOrder(node.right);// 访问当前节点大的节点
        sum += node.val;    // 累积sum
        node.val = sum;     // 更新当前值
        inOrder(node.left);// 访问比当前节点小的节点
    }
}
