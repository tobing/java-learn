package top.tobing.datastruct.tree.lc_513;

import top.tobing.datastruct.tree.TreeNode;

import java.util.LinkedList;

/**
 * @Author tobing
 * @Date 2021/4/3 10:12
 * @Description 513. 找树左下角的值
 * https://leetcode-cn.com/problems/find-bottom-left-tree-value/description/
 * 给定一个二叉树，在树的最后一行找到最左边的值。
 */
public class Solution {

    // 层序遍历，秒杀
    public int findBottomLeftValue01(TreeNode root) {
        if (root == null) {
            return -1;
        }
        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.addFirst(root);
        int left = 0;
        while (queue.size() != 0) {
            left = queue.getLast().val;
            for (int i = queue.size(); i > 0; i--) {
                TreeNode treeNode = queue.removeLast();
                if (treeNode.left != null) {
                    queue.addFirst(treeNode.left);
                }
                if (treeNode.right != null) {
                    queue.addFirst(treeNode.right);
                }
            }
        }
        return left;
    }

    public int findBottomLeftValue(TreeNode root) {
        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.addFirst(root);
        // 不用记录每一层的数据
        // 每一层从右往左遍历，最后一个元素就是最下一层最左边的元素
        while (queue.size() != 0) {
            root = queue.removeFirst();
            if (root.right != null) {
                queue.addLast(root.right);
            }
            if (root.left != null) {
                queue.addLast(root.left);
            }
        }
        return root.val;
    }
}
