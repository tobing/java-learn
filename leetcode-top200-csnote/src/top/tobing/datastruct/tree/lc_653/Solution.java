package top.tobing.datastruct.tree.lc_653;

import top.tobing.datastruct.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author tobing
 * @Date 2021/4/5 22:02
 * @Description 653. 两数之和 IV - 输入 BST
 * https://leetcode-cn.com/problems/two-sum-iv-input-is-a-bst/description/
 */
public class Solution {

    /**
     * 利用中序遍历将BST转换为有序的序列，再用双指针有序列进行求解
     * 时间复杂度O(n)：两轮遍历，一轮遍历用来中序遍历BST、一轮遍历用来求解
     * 空间复杂度O(n)：用长度为n序列来储存数据
     */

    // 存储有序节点
    private List<Integer> list = new ArrayList<>();

    public boolean findTarget(TreeNode root, int k) {
        inOrder(root);
        int start = 0;
        int end = list.size() - 1;
        while (start < end) {
            if (list.get(start) + list.get(end) == k) {
                return true;
            } else if (list.get(start) + list.get(end) > k) {
                end--;
            } else {
                start++;
            }
        }
        return false;
    }

    // 中序遍历获取BST的有序序列
    private void inOrder(TreeNode root) {
        if (root == null) {
            return;
        }
        inOrder(root.left);
        list.add(root.val);
        inOrder(root.right);
    }
}
