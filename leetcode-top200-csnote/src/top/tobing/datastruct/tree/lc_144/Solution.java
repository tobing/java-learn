package top.tobing.datastruct.tree.lc_144;

import top.tobing.datastruct.tree.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @Author tobing
 * @Date 2021/4/3 10:52
 * @Description 144. 二叉树的前序遍
 * https://leetcode-cn.com/problems/binary-tree-preorder-traversal/
 */
public class Solution {

    // 前序遍历-迭代
    public List<Integer> preorderTraversal01(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        LinkedList<TreeNode> stack = new LinkedList<>();
        stack.addFirst(root);

        while (stack.size() != 0) {
            TreeNode treeNode = stack.removeFirst();
            res.add(treeNode.val);
            if (treeNode.right != null) {       // 先将左节点压栈
                stack.addFirst(treeNode.right);
            }
            if (treeNode.left != null) {        // 再将右节点压栈
                stack.addFirst(treeNode.left);
            }
        }
        return res;
    }


    // 前序遍历-递归
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        helper(root, res);
        return res;
    }

    // 递归写法
    private void helper(TreeNode root, List<Integer> list) {
        if (root == null) {
            return;
        }
        list.add(root.val);
        helper(root.left, list);
        helper(root.right, list);
    }
}
