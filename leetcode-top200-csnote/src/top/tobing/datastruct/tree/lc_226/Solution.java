package top.tobing.datastruct.tree.lc_226;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/3/29 12:01
 * @Description 226. 翻转二叉树
 * https://leetcode-cn.com/problems/invert-binary-tree/description/
 */
public class Solution {
    // 后序遍历，简单，秒杀
    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        TreeNode left = invertTree(root.left);
        TreeNode right = invertTree(root.right);
        root.left = right;
        root.right = left;
        return root;
    }
}
