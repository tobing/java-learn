package top.tobing.datastruct.tree.lc_101;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/3/30 9:03
 * @Description 101. 对称二叉树
 * https://leetcode-cn.com/problems/symmetric-tree/description/
 */
public class Solution {
    // 递归法
    public boolean isSymmetric(TreeNode root) {
        if (root == null) {
            return true;
        }
        return helper(root.left, root.right);
    }

    // 递归法
    private boolean helper(TreeNode left, TreeNode right) {
        if (left == null && right == null) {
            return true;
        }
        if (left == null || right == null) {
            return false;
        }
        return left.val == right.val && helper(left.left, right.right) && helper(left.right, right.left);
    }

    // 迭代法【使用队列】
//    public boolean isSymmetric01(TreeNode root) { }
}
