package top.tobing.datastruct.tree.lc_687;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/3/31 21:37
 * @Description 687. 最长同值路径
 * https://leetcode-cn.com/problems/longest-univalue-path/
 * 给定一个二叉树，找到最长的路径，这个路径中的每个节点具有相同值。 这条路径可以经过也可以不经过根节点。
 */
public class Solution {
    // 保存相同值的最大路径
    private int res;

    // 没啥思路
    public int longestUnivaluePath(TreeNode root) {
        helper(root);
        return res;
    }

    private int helper(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int left = helper(root.left);       // 获取左孩子的最大值
        int right = helper(root.right);     // 获取右孩子的最大值
        // 判断当前节点与孩子节点值的关系
        if (root.left != null && root.val == root.left.val) {
            left++;     // 左孩子节点值与当节点相同，更新最大路径
        } else {
            left = 0;   // 不同，直接置零
        }
        if (root.right != null && root.val == root.right.val) {
            right++;    // 左孩子节点值与当前节点相同，更新
        } else {
            right = 0;  // 不同，直接置零
        }
        res = Math.max(res, left + right);   // 判断只有子树的情况
        return Math.max(left, right);        // 返回只有子树的路径情况
    }
}
