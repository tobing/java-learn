package top.tobing.datastruct.tree.lc_572;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/3/30 8:40
 * @Description 572. 另一个树的子树
 * https://leetcode-cn.com/problems/subtree-of-another-tree/description/
 */
public class Solution {
    /**
     * 判断当前节点、当前节点的孩子节点是否存在与t相等的部分
     */
    public boolean isSubtree(TreeNode s, TreeNode t) {  // 相当于外循环变量树的每个节点
        if (s == null) {
            return false;
        }
        return isSubTreeWithRoot(s, t) || isSubtree(s.left, t) || isSubtree(s.right, t);
    }

    /**
     * 判断 s 和 t 是否完全相等【辅助函数】
     */
    private boolean isSubTreeWithRoot(TreeNode s, TreeNode t) { // 相当于内循环判断以每个节点为root的树，是否与t完全相等，即为子树
        if (t == null && s == null) {
            return true;
        }
        if (t == null || s == null) {
            return false;
        }
        if (t.val == s.val) {
            return false;
        }
        return isSubTreeWithRoot(s.left, t.left) && isSubTreeWithRoot(s.right, t.left);
    }
}
