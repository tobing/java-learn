package top.tobing.datastruct.tree.lc_145;

import top.tobing.datastruct.tree.TreeNode;

import java.util.*;

/**
 * @Author tobing
 * @Date 2021/4/3 11:05
 * @Description 145. 二叉树的后序遍历
 * https://leetcode-cn.com/problems/binary-tree-postorder-traversal/description/
 */
public class Solution {

    // 后续遍历非递归实现
    // 前序遍历：root->left->right
    // 后序遍历：left->rigth->root
    // 修改前序遍历的变量方向，实现root->right->left;在对其进行翻转 left->right->root
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        Stack<TreeNode> stack = new Stack<>();
        stack.push(root);
        while (stack.size() != 0) {
            TreeNode treeNode = stack.pop();
            res.add(treeNode.val);
            if (treeNode.left != null) {
                stack.push(treeNode.left);
            }
            if (treeNode.right != null) {
                stack.push(treeNode.right);
            }
        }
        Collections.reverse(res);
        return res;
    }
}
