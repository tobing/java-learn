package top.tobing.datastruct.tree.lc_617;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/3/29 12:07
 * @Description 617. 合并二叉树
 * https://leetcode-cn.com/problems/merge-two-binary-trees/description/
 */
public class Solution {
    // 以root1为主，进行合并，后序遍历，简单秒杀
    public TreeNode mergeTrees(TreeNode root1, TreeNode root2) {
        if (root1 == null) {
            return root2;
        }
        if (root2 == null) {
            return root1;
        }
        TreeNode left = mergeTrees(root1.left, root2.left);
        TreeNode right = mergeTrees(root1.right, root2.right);
        root1.val += root2.val;
        root1.left = left;
        root1.right = right;
        return root1;
    }
}
