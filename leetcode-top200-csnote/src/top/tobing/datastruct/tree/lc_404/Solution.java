package top.tobing.datastruct.tree.lc_404;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/3/30 9:50
 * @Description 404. 左叶子之和
 * https://leetcode-cn.com/problems/sum-of-left-leaves/description/
 */
public class Solution {
    private int sum = 0;

    // 不太优雅的递归写法
    public int sumOfLeftLeaves(TreeNode root) {
        if (root == null) {
            return 0;
        }
        TreeNode left = root.left;
        TreeNode right = root.right;
        if (left != null && left.left == null && left.right == null) {
            sum += left.val;
        }
        sumOfLeftLeaves(root.left);
        sumOfLeftLeaves(root.right);
        return sum;
    }


    public int sumOfLeftLeaves1(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left != null && root.left.left == null && root.left.right == null) { // 当前子节点的左孩子是叶子，需要返回
            return root.left.val + sumOfLeftLeaves1(root.right);
        }
        return sumOfLeftLeaves1(root.left) + sumOfLeftLeaves1(root.right);
    }
}
