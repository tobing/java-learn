package top.tobing.datastruct.tree;

/**
 * @Author tobing
 * @Date 2021/4/5 21:18
 * @Description
 */
public class ListNode {
    public int val;
    public ListNode next;

    public ListNode(int val) {
        this.val = val;
    }
}
