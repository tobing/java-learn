package top.tobing.datastruct.tree.lc_543;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/3/29 11:44
 * @Description 543. 二叉树的直径
 * https://leetcode-cn.com/problems/diameter-of-binary-tree/description/
 */
public class Solution {

    private int max = 0;

    // 利用树的最大深度，简单秒杀
    public int diameterOfBinaryTree(TreeNode root) {
        depth(root);
        return max;
    }

    private int depth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int left = depth(root.left);
        int right = depth(root.right);
        int sum = left + right;
        // 每次对比左右子树的深度和，将其与当前最大距离对比
        max = Math.max(max, sum);
        return Math.max(left, right) + 1;
    }

}
