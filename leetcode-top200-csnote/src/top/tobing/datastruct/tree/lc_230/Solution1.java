package top.tobing.datastruct.tree.lc_230;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/4/4 10:59
 * @Description 230. 二叉搜索树中第K小的元素
 * https://leetcode-cn.com/problems/kth-smallest-element-in-a-bst/description/
 */
public class Solution1 {

    // 记录当前以及访问元素个数
    private int cnt = 0;
    //保存第k个元素
    private int val;

    /**
     * 中序遍历解法
     */
    public int kthSmallest(TreeNode root, int k) {
        inOrder(root, k);
        return val;
    }

    // 中序遍历
    private void inOrder(TreeNode root, int k) {
        if (root == null) {
            return;
        }
        inOrder(root.left, k);
        cnt++;
        if (cnt == k) {
            val = root.val;
            return;
        }
        inOrder(root.right, k);
    }
}
