package top.tobing.datastruct.tree.lc_230;

import top.tobing.datastruct.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author tobing
 * @Date 2021/4/4 10:59
 * @Description 230. 二叉搜索树中第K小的元素
 * https://leetcode-cn.com/problems/kth-smallest-element-in-a-bst/description/
 */
public class Solution {
    // 辅助数组
    private List<Integer> res = new ArrayList<>();

    public int kthSmallest(TreeNode root, int k) {
        helper(root, k);
        return res.get(k - 1);
    }
    // 辅助数组
    public void helper(TreeNode root, int k) {
        if (root == null) {
            return;
        }
        helper(root.left, k);
        res.add(root.val);
        if (res.size() == k) {
            return;
        }
        helper(root.right, k);
    }
}
