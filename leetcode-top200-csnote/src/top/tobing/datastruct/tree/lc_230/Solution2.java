package top.tobing.datastruct.tree.lc_230;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/4/4 11:16
 * @Description
 */
public class Solution2 {

    // 递归法
    // TODO 理解
    public int kthSmallest(TreeNode root, int k) {
        int leftCount = count(root.left);
        if (leftCount == k - 1) return root.val;
        if (leftCount > k - 1) return kthSmallest(root.left, k);
        return kthSmallest(root.right, k - leftCount - 1);
    }

    private int count(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return 1 + count(root.left) + count(root.right);
    }
}
