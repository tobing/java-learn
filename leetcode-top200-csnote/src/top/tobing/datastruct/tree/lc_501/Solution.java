package top.tobing.datastruct.tree.lc_501;

import top.tobing.datastruct.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author tobing
 * @Date 2021/4/5 22:30
 * @Description 501. 二叉搜索树中的众数
 * https://leetcode-cn.com/problems/find-mode-in-binary-search-tree/description/
 */
public class Solution {

    // 当前val的数量
    private int curCnt = 1;
    // 所有val的最大数量
    private int maxCnt = 1;
    // 当前节点的前一个节点
    private TreeNode prev;
    // 保存当前状态的众树
    private List<Integer> list = new ArrayList<>();

    public int[] findMode(TreeNode root) {
        inOrder(root);
        int[] temp = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            temp[i] = list.get(i);
        }
        return temp;
    }

    private void inOrder(TreeNode node) {
        if (node == null) {
            return;
        }
        inOrder(node.left);
        if (prev != null) {
            curCnt = prev.val == node.val ? curCnt + 1 : 1;
        }
        if (curCnt > maxCnt) {
            maxCnt = curCnt;
            list.clear();
            list.add(node.val);
        } else if (curCnt == maxCnt) {
            list.add(node.val);
        }
        prev = node;
        inOrder(node.right);
    }

}
