package top.tobing.datastruct.tree.lc_108;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/4/5 10:05
 * @Description 108. 将有序数组转换为二叉搜索树
 * https://leetcode-cn.com/problems/convert-sorted-array-to-binary-search-tree/description/
 */
public class Solution {


    public TreeNode sortedArrayToBST(int[] nums) {
        return helper(nums, 0, nums.length - 1);
    }

    private TreeNode helper(int[] nums, int startIndx, int endIndx) {
        if (startIndx > endIndx) {
            return null;
        }
        int mid = (startIndx + endIndx) >>> 1;
        TreeNode node = new TreeNode(nums[mid]);
        node.left = helper(nums, startIndx, mid - 1);
        node.right = helper(nums, mid + 1, endIndx);
        return node;
    }


}
