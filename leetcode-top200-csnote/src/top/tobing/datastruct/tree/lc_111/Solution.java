package top.tobing.datastruct.tree.lc_111;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/3/30 9:15
 * @Description 111. 二叉树的最小深度
 * https://leetcode-cn.com/problems/minimum-depth-of-binary-tree/description/
 */
public class Solution {

    // 本题要理解清除叶子节点
    // 左右孩子都为空的时候才是叶子节点，否则都不是
    // 对于一边为空的情况，可以用 left + right + 1来合并，以为一边为空，则其中一个为o
    public int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int left = minDepth(root.left);
        int right = minDepth(root.right);
        if (left == 0 || right == 0) {
            return left + right + 1;
        }
        return Math.min(left, right) + 1;
    }
}
