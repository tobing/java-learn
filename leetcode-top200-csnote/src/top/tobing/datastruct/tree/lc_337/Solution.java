package top.tobing.datastruct.tree.lc_337;

import top.tobing.datastruct.tree.TreeNode;

/**
 * @Author tobing
 * @Date 2021/3/31 22:31
 * @Description 337. 打家劫舍 III
 * https://leetcode-cn.com/problems/house-robber-iii/description/
 */
public class Solution {

    // 暴力解
    public int rob1(TreeNode root) {
        if (root == null) {
            return 0;
        }
        int val1 = root.val;
        if (root.left != null) {
            val1 += rob1(root.left.left) + rob1(root.left.right);
        }
        if (root.right != null) {
            val1 += rob1(root.right.left) + rob1(root.right.right);
        }
        int val2 = rob1(root.left) + rob1(root.right);
        return Math.max(val1, val2);
    }

    // dp表降低复杂度
    public int rob(TreeNode root) {
        int[] res = helper(root);
        return Math.max(res[0], res[1]);
    }

    private int[] helper(TreeNode node) {
        if (node == null) {
            return new int[]{0, 0};
        }
        int[] left = helper(node.left);
        int[] right = helper(node.right);

        int[] dp = new int[2];

        // dp[0]：儿子节点
        // dp[1]：当前节点+孙子节点
        dp[0] = Math.max(left[0], left[1]) + Math.max(right[0], right[1]);
        dp[1] = node.val + left[0] + right[0];
        return dp;
    }


}
