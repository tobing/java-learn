package top.tobing.datastruct.tree.lc_94;

import top.tobing.datastruct.tree.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * @Author tobing
 * @Date 2021/4/4 10:27
 * @Description 94. 二叉树的中序遍历
 * 给定一个二叉树的根节点 root ，返回它的 中序 遍历。
 */
public class Solution {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<>();
        // 特殊情况
        if (root == null) {
            return res;
        }
        // 用来模拟函数栈
        Stack<TreeNode> stack = new Stack<>();
        // 用于遍历的节点、表示当前节点
        TreeNode cur = root;
        while (cur != null || stack.size() != 0) {
            // 先访问左节点
            while (cur != null) {
                stack.push(cur);
                cur = cur.left;
            }
            // 访问当前节点【中序】
            TreeNode node = stack.pop();
            res.add(node.val);
            // 访问右节点
            cur = node.right;
        }
        return res;
    }
}
