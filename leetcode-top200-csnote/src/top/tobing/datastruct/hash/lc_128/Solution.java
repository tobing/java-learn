package top.tobing.datastruct.hash.lc_128;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Author tobing
 * @Date 2021/4/8 9:46
 * @Description 128. 最长连续序列
 * https://leetcode-cn.com/problems/longest-consecutive-sequence/
 */
public class Solution {

    /**
     * 暴力解法
     */
    public int longestConsecutive(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            set.add(num);
        }
        List<Integer> list = set.stream().sorted().collect(Collectors.toList());
        int longest = 0;
        int temp = 1;
        for (int i = 1; i < list.size(); i++) {
            if (list.get(i) - 1 == list.get(i - 1)) {
                temp++;
                longest = Math.max(longest, temp);
            } else {
                temp = 1;
            }
        }
        return longest;
    }
}
