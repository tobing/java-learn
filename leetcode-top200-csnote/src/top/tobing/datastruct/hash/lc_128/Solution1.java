package top.tobing.datastruct.hash.lc_128;

import java.util.*;

/**
 * @Author tobing
 * @Date 2021/4/8 9:46
 * @Description 128. 最长连续序列
 * https://leetcode-cn.com/problems/longest-consecutive-sequence/
 * 进阶：时间复杂度O(n)
 */
public class Solution1 {

    /**
     * HashMap（有点难理解）
     * TODO
     */
    public int longestConsecutive(int[] nums) {
        Map<Integer, Integer> count = new HashMap<>();
        for (int num : nums) {
            count.put(num, 1);
        }
        for (int num : nums) {
            forward(count, num);
        }
        return maxCount(count);
    }

    private int forward(Map<Integer, Integer> count, int num) {
        if (!count.containsKey(num)) {
            return 0;
        }
        Integer cnt = count.get(num);
        if (cnt > 1) {
            return cnt;
        }
        cnt = forward(count, num + 1) + 1;
        count.put(num, cnt);
        return cnt;
    }

    private int maxCount(Map<Integer, Integer> map) {
        int max = 0;
        for (int num : map.keySet()) {
            max = Math.max(max, map.get(num));
        }
        return max;
    }

}
