package top.tobing.datastruct.hash.lc_1;

import java.util.HashMap;

/**
 * @Author tobing
 * @Date 2021/4/8 9:19
 * @Description 1. 两数之和
 */
public class Solution {

    // 用HashMap
    public int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (!map.isEmpty() && map.containsKey(target - nums[i])) {
                return new int[]{i, map.get(target - nums[i])};
            }
            map.put(nums[i], i);
        }
        return new int[0];
    }
}
