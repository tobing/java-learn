package top.tobing.datastruct.hash.lc_217;

import java.util.HashSet;
import java.util.Set;

/**
 * @Author tobing
 * @Date 2021/4/8 9:26
 * @Description 217. 存在重复元素
 * https://leetcode-cn.com/problems/contains-duplicate/
 */
public class Solution1 {
    // Set另外一种方式
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            set.add(num);
        }
        return set.size() < nums.length;
    }
}
