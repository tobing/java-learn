package top.tobing.datastruct.hash.lc_217;

import java.util.HashSet;
import java.util.Set;

/**
 * @Author tobing
 * @Date 2021/4/8 9:26
 * @Description 217. 存在重复元素
 * https://leetcode-cn.com/problems/contains-duplicate/
 */
public class Solution {
    // Set简单秒杀
    public boolean containsDuplicate(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            if (set.contains(nums[i])) {
                return true;
            }
            set.add(nums[i]);
        }
        return false;
    }
}
