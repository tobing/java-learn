package top.tobing.datastruct.hash.lc_594;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author tobing
 * @Date 2021/4/8 9:33
 * @Description 594. 最长和谐子序列
 */
public class Solution {
    public int findLHS(int[] nums) {
        Map<Integer, Integer> count = new HashMap<>();
        for (int num : nums) {
            count.put(num, count.getOrDefault(num, 0) + 1);
        }
        int res = 0;
        for (int num : count.keySet()) {
            if (count.containsKey(num + 1)) {
                res = Math.max(res, count.get(num) + count.get(num + 1));
            }
        }
        return res;
    }
}
