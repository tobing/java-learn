package top.tobing.datastruct.stack_queue.lc_503;

import java.util.Arrays;
import java.util.Stack;

/**
 * @Author tobing
 * @Date 2021/4/8 8:57
 * @Description 503. 下一个更大元素 II
 * https://leetcode-cn.com/problems/next-greater-element-ii/description/
 */
public class Solution {

    // 辅助栈
    public int[] nextGreaterElements(int[] nums) {
        int[] res = new int[nums.length];
        Arrays.fill(res, -1);
        Stack<Integer> pre = new Stack<>();
        for (int i = 0; i < nums.length * 2; i++) {
            int num = nums[i % nums.length];
            // 当前栈不为空且栈顶元素小于当前元素
            while (!pre.isEmpty() && nums[pre.peek()] < num) {
                res[pre.pop()] = num;
            }
            if (i < nums.length) {
                pre.push(i);
            }
        }
        return res;
    }
}
