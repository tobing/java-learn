package top.tobing.datastruct.stack_queue.lc_232;

import java.util.Stack;

/**
 * @Author tobing
 * @Date 2021/4/6 9:57
 * @Description 232. 用栈实现队列
 * https://leetcode-cn.com/problems/implement-queue-using-stacks/description/
 */
class MyQueue {

    private Stack<Integer> in;
    private Stack<Integer> out;

    /**
     * Initialize your data structure here.
     */
    public MyQueue() {
        in = new Stack<>();
        out = new Stack<>();
    }

    /**
     * Push element x to the back of queue.
     */
    public void push(int x) {
        while (!out.isEmpty()) {
            in.push(out.pop());
        }
        in.push(x);
    }

    /**
     * Removes the element from in front of queue and returns that element.
     */
    public int pop() {
        while (!in.isEmpty()) {
            out.push(in.pop());
        }
        return out.pop();
    }

    /**
     * Get the front element.
     */
    public int peek() {
        while (!in.empty()) {
            out.push(in.pop());
        }
        return out.peek();
    }

    /**
     * Returns whether the queue is empty.
     */
    public boolean empty() {
        return in.isEmpty() && out.empty();
    }
}
