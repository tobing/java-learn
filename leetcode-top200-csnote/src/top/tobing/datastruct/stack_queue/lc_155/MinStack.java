package top.tobing.datastruct.stack_queue.lc_155;

import java.util.Stack;

/**
 * @Author tobing
 * @Date 2021/4/7 9:37
 * @Description 155. 最小栈
 * https://leetcode-cn.com/problems/min-stack/description/
 */
public class MinStack {

    // 保存数据
    private Stack<Integer> stack;
    // 维护递减顺序
    private Stack<Integer> min;

    /**
     * initialize your data structure here.
     */
    public MinStack() {
        stack = new Stack<>();
        min = new Stack<>();
    }

    public void push(int val) {
        stack.push(val);
        if (min.isEmpty() || min.peek() >= val) {
            min.push(val);
        }
    }

    public void pop() {
        Integer pop = stack.pop();
        if (!min.isEmpty() && min.peek().equals(pop)) {
            min.pop();
        }
    }

    public int top() {
        return stack.peek();
    }

    public int getMin() {
        return min.peek();
    }
}
