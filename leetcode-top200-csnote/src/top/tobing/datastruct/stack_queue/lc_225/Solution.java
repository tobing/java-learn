package top.tobing.datastruct.stack_queue.lc_225;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @Author tobing
 * @Date 2021/4/6 10:11
 * @Description 225. 用队列实现栈
 * https://leetcode-cn.com/problems/implement-stack-using-queues/description/
 */
class MyStack {
    // 添加元素首选
    private Queue<Integer> queue1;
    // 删除元素首选
    private Queue<Integer> queue2;

    /**
     * Initialize your data structure here.
     */
    public MyStack() {
        queue1 = new LinkedList<>();
        queue2 = new LinkedList<>();
    }

    /**
     * Push element x onto stack.
     */
    public void push(int x) {
        queue1.add(x);
    }

    /**
     * Removes the element on top of the stack and returns that element.
     */
    public int pop() {
        while (queue1.size() > 1) {
            queue2.add(queue1.remove());
        }
        Integer ret = queue1.remove();
        while (!queue2.isEmpty()) {
            queue1.add(queue2.remove());
        }
        return ret;
    }

    /**
     * Get the top element.
     */
    public int top() {
        while (queue1.size() > 1) {
            queue2.add(queue1.remove());
        }
        Integer ret = queue1.peek();
        queue2.add(queue1.remove());
        while (!queue2.isEmpty()) {
            queue1.add(queue2.remove());
        }
        return ret;
    }

    /**
     * Returns whether the stack is empty.
     */
    public boolean empty() {
        return queue1.isEmpty() && queue2.isEmpty();
    }
}
