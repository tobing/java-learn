package top.tobing.datastruct.stack_queue.lc_739;

import java.util.Stack;

/**
 * @Author tobing
 * @Date 2021/4/7 9:53
 * @Description 739. 每日温度
 * https://leetcode-cn.com/problems/daily-temperatures/
 */
public class Solution1 {

    /**
     * 辅助栈法
     */
    public int[] dailyTemperatures(int[] T) {
        Stack<Integer> stack = new Stack<>();
        int[] res = new int[T.length];
        for (int i = 0; i < T.length; i++) {
            while (!stack.isEmpty() && T[i] > T[stack.peek()]) {
                int preIndex = stack.pop();
                res[preIndex] = i - preIndex;
            }
            stack.add(i);
        }
        return res;
    }

    /**
     * 辅助栈维护递减的序列。【注意：保存的是当前天的下标，而不是温度值，通过下标可以访问的温度值，反之不行】
     * 如果遇到比栈顶元素大的元素，说明出现了「比栈内的元素气温高的天」，因此直接弹栈，计算栈顶元素与当前元素的距离；
     * 反之说明还没有遇到「比栈内的元素气温高的天」，继续压栈。
     * 栈内保存元素都是「还没遇到比它温度高的天」
     */
}
