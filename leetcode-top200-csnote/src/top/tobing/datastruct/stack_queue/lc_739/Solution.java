package top.tobing.datastruct.stack_queue.lc_739;

/**
 * @Author tobing
 * @Date 2021/4/7 9:53
 * @Description 739. 每日温度
 * https://leetcode-cn.com/problems/daily-temperatures/
 */
public class Solution {

    /**
     * 暴力解法：O(n^2)
     */
    public int[] dailyTemperatures(int[] T) {
        int[] res = new int[T.length];
        for (int i = 0; i < T.length - 1; i++) {
            boolean hasLarge = false;
            for (int j = i + 1; j < T.length; j++) {
                res[i]++;
                if (T[j] > T[i]) {
                    hasLarge = true;
                    break;
                }
            }
            if (!hasLarge) {
                res[i] = 0;
            }
        }
        return res;
    }
}
