package hot100.lc_2;

import hot100.ListNode;

/**
 * @Author tobing
 * @Date 2021/7/6 18:38
 * @Description 两数相加-新建链表法
 */
public class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }
        // 是否有进位
        int inBit = 0;
        // 创建虚拟头结点
        ListNode resList = new ListNode(-1);
        ListNode cur = resList;
        while (l1 != null || l2 != null) {
            int l1Val = l1 == null ? 0 : l1.val;
            int l2Val = l2 == null ? 0 : l2.val;
            int res = l1Val + l2Val + inBit;
            cur.next = new ListNode(res % 10);
            cur = cur.next;
            inBit = res / 10;
            if (l1 != null) {
                l1 = l1.next;
            }
            if (l2 != null) {
                l2 = l2.next;
            }
        }
        if (inBit == 1) {
            cur.next = new ListNode(1);
        }
        return resList.next;
    }
}
