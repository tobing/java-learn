package hot100.lc_3;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @Author tobing
 * @Date 2021/7/6 19:24
 * @Description 最长无重复子串--滑动窗口解法
 * 维护一个活动窗口，窗口内的数据都是无重复字符串
 * 通过双指针来维护一个窗口，start end
 * end用于前进，当end指向的元素已经存在，表示滑动窗口内存在元素，需要更新start
 * 需要注意此处start与end的定义，此处[start,end)
 */
public class Solution1 {
    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        if (s .length() == 1) {
            return 1;
        }
        Map<Character, Integer> map = new HashMap<>();
        int res = 0;
        int start = 0;
        for (int end = 0; end < s.length(); end++) {
            if (map.containsKey(s.charAt(end))) {
                // 如果map中s.charAt(end)的数据比start大，说明s.charAt(end)旧的滑动窗口内，需要更新滑动窗口
                // 否则不在滑动窗口内，无需更新滑动窗口
                if (map.get(s.charAt(end)) > start ) {
                    start = map.get(s.charAt(end));
                }
            }
            map.put(s.charAt(end), end + 1);
            res = Math.max(res, end - start + 1);
        }
        return res;
    }

    public static void main(String[] args) {
        new Solution1().lengthOfLongestSubstring(" ");
    }

}
