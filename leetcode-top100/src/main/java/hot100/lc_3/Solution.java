package hot100.lc_3;

import java.util.HashSet;
import java.util.Set;

/**
 * @Author tobing
 * @Date 2021/7/6 19:24
 * @Description 最长无重复子串--暴力解法
 */
public class Solution {
    public int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        if (s.length() == 1) {
            return 1;
        }
        int res = 0;
        for (int i = 0; i < s.length(); i++) {
            Set<Character> set = new HashSet<>();
            for (int j = i; j < s.length(); j++) {
                if (set.contains(s.charAt(j))) {
                    break;
                }
                set.add(s.charAt(j));
                res = Math.max(res, set.size());
            }
        }
        return res;
    }

}
