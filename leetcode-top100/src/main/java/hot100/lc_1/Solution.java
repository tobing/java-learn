package hot100.lc_1;


/**
 * @Author tobing
 * @Date 2021/7/6 18:22
 * @Description 两数之和-暴力解
 */
public class Solution {
    public int[] twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] + nums[j] == target) {
                    return new int[]{i, j};
                }
            }
        }
        return new int[0];
    }
}
