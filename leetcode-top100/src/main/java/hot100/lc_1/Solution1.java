package hot100.lc_1;


import java.util.HashMap;
import java.util.Map;

/**
 * @Author tobing
 * @Date 2021/7/6 18:22
 * @Description 两数之和-HashMap法则
 */
public class Solution1 {
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        map.put(nums[0], 0);
        for (int i = 1; i < nums.length; i++) {
            if (map.containsKey(target - nums[i])) {
                return new int[]{i, map.get(target - nums[i])};
            }
            map.put(nums[i], i);
        }
        return new int[0];
    }
}
