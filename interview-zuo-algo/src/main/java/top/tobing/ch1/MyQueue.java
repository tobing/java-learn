package top.tobing.ch1;

import java.util.Stack;

/**
 * @Author tobing
 * @Date 2021/6/13 11:43
 * @Description
 */
public class MyQueue {

    private Stack<Integer> stackPop;
    private Stack<Integer> stackPush;

    /**
     * Initialize your data structure here.
     */
    public MyQueue() {
        this.stackPop = new Stack<>();
        this.stackPush = new Stack<>();
    }

    /**
     * Push element x to the back of queue.
     */
    public void push(int x) {
        while (!stackPop.isEmpty()) {
            stackPush.push(stackPop.pop());
        }
        stackPush.push(x);
    }

    /**
     * Removes the element from in front of queue and returns that element.
     */
    public int pop() {
        if (empty()) {
            throw new RuntimeException("MyQueue is empty!");
        }
        while (!stackPush.isEmpty()) {
            stackPop.push(stackPush.pop());
        }
        return stackPop.pop();
    }

    /**
     * Get the front element.
     */
    public int peek() {
        if (empty()) {
            throw new RuntimeException("MyQueue is empty!");
        }
        while (!stackPush.isEmpty()) {
            stackPop.push(stackPush.pop());
        }
        return stackPop.peek();
    }

    /**
     * Returns whether the queue is empty.
     */
    public boolean empty() {
        return stackPop.isEmpty() && stackPush.isEmpty();
    }
}
