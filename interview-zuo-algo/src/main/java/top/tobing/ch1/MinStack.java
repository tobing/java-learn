package top.tobing.ch1;

import java.util.Stack;

/**
 * @Author tobing
 * @Date 2021/6/13 11:12
 * @Description 最小栈
 * 实现方式1：stackMin条件压入
 */
public class MinStack {

    private Stack<Integer> stackData;
    private Stack<Integer> stackMin;

    public MinStack() {
        this.stackData = new Stack<>();
        this.stackMin = new Stack<>();
    }

    public void push(int val) {
        // 先判断minStack是否压入
        if (stackMin.isEmpty() || stackMin.peek() >= val) {
            stackMin.push(val);
        }
        stackData.push(val);
    }

    public void pop() {
        if (stackData.size() == 0) {
            throw new RuntimeException("Stack is empty!");
        }
        // 判断minStack是否弹出
        if (!stackMin.isEmpty() && stackMin.peek().equals(stackData.peek())) {
            stackMin.pop();
        }
        stackData.pop();

    }

    public int top() {
        if (stackData.size() == 0) {
            throw new RuntimeException("Stack is empty!");
        }
        return stackData.peek();
    }

    public int getMin() {
        if (stackData.size() == 0) {
            throw new RuntimeException("Stack is empty!");
        }
        return stackMin.peek();
    }
}
