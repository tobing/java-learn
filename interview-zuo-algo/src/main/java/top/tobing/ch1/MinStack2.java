package top.tobing.ch1;

import java.util.Stack;

/**
 * @Author tobing
 * @Date 2021/6/13 11:12
 * @Description 最小栈
 * 实现方式1：stackMin完全压入
 */
public class MinStack2 {

    private Stack<Integer> stackData;
    private Stack<Integer> stackMin;

    public MinStack2() {
        this.stackData = new Stack<>();
        this.stackMin = new Stack<>();
    }

    public void push(int val) {
        // 每次压入当前的最小值
        if (stackMin.isEmpty()) {
            stackMin.push(val);
        } else {
            stackMin.push(Math.min(stackMin.peek(), val));
        }
        stackData.push(val);
    }

    public void pop() {
        if (stackData.size() == 0) {
            throw new RuntimeException("Stack is empty!");
        }
        // 无需判断，直接弹出
        stackMin.pop();
        stackData.pop();
    }

    public int top() {
        if (stackData.size() == 0) {
            throw new RuntimeException("Stack is empty!");
        }
        return stackData.peek();
    }

    public int getMin() {
        if (stackData.size() == 0) {
            throw new RuntimeException("Stack is empty!");
        }
        return stackMin.peek();
    }
}
