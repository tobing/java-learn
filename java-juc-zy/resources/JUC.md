# JUC

## 1. Volatile

volatile是Java虚拟机提供的轻量级同步机制。

volatile具有以下3大特点：

+ 保证可见性
+ 不保证原子性
+ 禁止指令重排

说到可见性，首先要了解Java底层的内存模型

### 1.1 JMM

JMM、Java Memory Model、Java内存模型。

JMM是一种抽象概念，本身并不存在，描述的是一组规范。

JMM规定了各个变量的访问方式：

+ 线程解锁前，必须要把共享变量刷新会主内存
+ 线程加锁前，必须从主内存中读取最新数据到工作内存
+ 加锁和解锁是同一把锁。

JVM中运行的实体是线程，而每个线程创建时，JVM都会为其创建私有的工作内存。

Java内存模型规定所有变量存储在主内存中，主内存是可以共享访问的。

由于线程对变量的操作必须在工作内存中进行，因此首先要将变量从主内存拷贝到自己的工作空间进行操作，操作完成再将数据写回到主内存中。

不同线程无法访问对方线程的工作内存，线程间通信必须要通过主内存来完成。

![image-20201109140220037](C:%5CUsers%5Ctobing%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20201109140220037.png)

### 1.2 可见性

从JMM中我们可以知道，各个线程对主内存中共享变量操作时基于变量拷贝来操作，最后才写回内存中的。

这样可能会操作一些问题：

A线程修改了共享变量X的拷贝副本，还未写回到主内存；B线程又对内存中的X进行拷贝。

此时A线程进行对X进行写回，但此时B线程已经读取了X未修改的值，就是说，对于B来说，X的修改是不可见的。

以上共享内存与工作内存同步延迟现象造成了**可见性问题**。

```java
public class Demo01Volatile01 {
    public static void main(String[] args) {
		// 资源对象
        MyData myData = new MyData();
        // 创建新线程
        new Thread(() -> {
            try {
                System.out.println(Thread.currentThread().getName() + " come in.");
                // 【NewThread】停止3s
                TimeUnit.SECONDS.sleep(3);
                // 【newThread】当前线程将值设置为60
                myData.setNumber2new();
                // 【newThread】线程结束
                System.out.println(Thread.currentThread().getName() + " execute finished.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
		// 等待工作内存的变量值改变【Main线程】
        while (myData.number == 0) {}
		// 线程结束【Main】
        System.out.println("number 新值为：" + myData.number);
        System.out.println("Main线程退出.....");
    }
}
// 资源类
class MyData {
    int number = 0;
    public void setNumber2new() {
        number = 60;
    }
}
```

> 以上程序，我们在main线程的基础上创建了一个新线程，通过main线程和新线程来操作资源类的MyData对象。
>
> 由于变量number未做任何处理，所以main函数读取到的number始终是之前的拷贝0
>
> 所以即使newThread修改了变量并将值写回到主线程，main依然始终持有的是先前的拷贝，因此一直处于死循环中。

基于以上的代码，我们对MyData类中的number使用volatile进行修饰。是的number可以在不同线程间可见。

```java
....
// 资源类
class MyData {
    volatile int number = 0;
    public void setNumber2new() {
        number = 60;
    }
}
```

> 使用volatile关键字修饰之后，我们发现main不会阻塞。
>
> 原因是使用volatile修饰的关键字会在共享变量改变时，通知其他持有该变量副本的线程。

### 1.3 原子性

原子性是指一致性，即一系列动作要么同时发生，要么同时失败。

与JMM相比，Volatile并不提供原子性。

```java
public class Demo02Volatile02 {
    public static void main(String[] args) {
        // 资源类
        MyData01 myData01 = new MyData01();
		// 20个线程并发对number进行操作
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    myData01.add();
                }
            }, String.valueOf(i)).start();
        }
        while (Thread.activeCount() > 2) {
            Thread.yield();
        }
        System.out.println(myData01.number);
    }
}
// 资源类
class MyData01 {
    volatile int number = 0;
    public void add() {
        number++;
    }
}
```

> 以上程序中，我们通过20个线程来操作volatile修饰的共享变量number，对其进行++操作。
>
> 我们查看其运行结果发现，操作之后的结果并没有达到我们期望的20000。
>
> 原因是volatile修饰的变量并不保证原子性，number++虽然看起来像是一行代码，但我们通过反编译分析其底层却发现，number++被分成了3个操作。

#### Atomic

以上我们知道volatile修饰的变量并不具有原子性，我们该如何解决该问题呢。

我们可以使用`java.util.concurrent.atomic`包下提供的原子类来进行操作。

atomic包下提供的API使得我们可以原子性来操作一个变量。

```java
public class AtomicInteger extends Number implements Serializable
An int value that may be updated atomically. See the java.util.concurrent.atomic package specification for description of the properties of atomic variables. An AtomicInteger is used in applications such as atomically incremented counters, and cannot be used as a replacement for an Integer. However, this class does extend Number to allow uniform access by tools and utilities that deal with numerically-based classes.
Since: 
1.5 
```

> JDK1.8API中对AtomicInteger类的介绍

| Modifier and Type | Method and Description                             |
| ----------------- | -------------------------------------------------- |
| `int`             | `addAndGet(int delta)`  原子性i+=n、返回加之后的值 |
| `int`             | `getAndAdd(int delta)`  原子性i+=n、返回加之前的值 |
| `int`             | `getAndDecrement()`  原子性i--                     |
| `int`             | `decrementAndGet()` 原子性--i                      |
| `int`             | `getAndIncrement()`  原子性i++                     |
| `int`             | `incrementAndGet()`  原子性++i                     |

下面我们通过AtomicInteger实现程序的原子性

```java
public class Demo03Volatile03 {
    public static void main(String[] args) {
        MyData02 myData02 = new MyData02();
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    myData02.addNumber();
                }
            }).start();
        }
		// 等待其他线程执行完毕main线程才输出结果
        while (Thread.activeCount() > 2) {
            Thread.yield();
        }
        System.out.println("Result : " + myData02.number);
    }
}
// 资源类
class MyData02 {
    //AtomicInteger初始化时默认是0
    AtomicInteger number = new AtomicInteger();
    public void addNumber() {
        // 等同于i++
        number.getAndIncrement();
    }
}
```

### 1.4 指令重排序

计算机在执行程序的时候，为了提高性能，编译器和处理器往往会对**指令进行重排**。

指令重排序又分为下面三种类型：

+ 编译器优化重排

+ 指令并行重排

+ 内存系统重排

```
源代码 --> 编译器优化重排 --> 指令并行重排 --> 内存系统重排 --> 最终执行指令
```

单线程环境下，指令重排序依旧能够保证最终结果与顺序执行的结果一致。

处理器在重排序前必须要考虑指令间的**数据依赖性**。

而在多线程环境下，指令重排可能会导致结果的不可预测。

```java
public void mySort() {
    int x = 11;	// 语句1
    int y = 12;	// 语句2 
    x = x + 5;	// 语句3
    y = x * x;	// 语句4
}
```

> 在单线程环境下，以下顺序都可以顺利执行，因此可能会有以下不同的指令重排
>
> 1 2 3 4
>
> 2 1 3 4
>
> 1 3 2 4
>
> 但是由于数据依赖性，4不能第一条执行。

在多线程环境下，指令重排可能存在一些问题

```java
public class ReSortSeqDemo {
    int a = 0;
    boolean flag = false;
    
    public void method01() {
        a = 1;			// 语句1
        flag = true;	// 语句2
    }
    
    public void method02() {
        if(flag) {
            a = a + 5;	// 语句3
            System.out.println("******* value = " + a " *******");
        }
    }
}
```

> 以上程序中，在method01方法中，在单线程情况下，语句1和语句2顺序发生改变并不会导致结果出现问题。
>
> 但是，在多线程环境下，语句1和语句2顺序如果出现调换。
>
> 线程A在执行到语句2 时，flag设置为true，此时刚好CPU调度method2，判断通过，结果为a = 5
>
> 这就出现和指令不重排结果不一致的情况。
>
> 因此，在并发环境下，我们要禁止指令重排

#### volatile禁止指令重排

volatile实现禁止指令重排优化，从而避免多线程环境下程序出现乱序执行的现象。

volatile利用内存屏障实现禁止指令重排。

内存屏障（Memory Barrier）又称内存栅栏，**是一个CPU指令**，具有两个作用：

+ 保证特定操作执行顺序
+ 保证某些变量的内存可见性（volatile基于此实现内存可见性）

由于编译器和处理器都能执行指令重排序。如果在指令间插入一条**内存屏蔽**，则会告诉编译器和CPU，不管什么指令都不能和这条内存屏蔽指令重排序。

就是说通过插入内存屏蔽禁止内存屏蔽前后的指令执行重排序优化。

内存屏蔽另个作用是强制刷出各种CPU缓存，因此任何CPU上的线程都能读取到数据的最新版本。

![image-20201109211837778](https://i.loli.net/2020/11/09/GMdOtckzmiTFoyr.png)

### 1.5 Volatile使用场景-单例模式

多线程环境下需要用到DCL（Double Check Lock）机制来保证单例模式的正确性。

```java
class SingletonDemo02 {
	// 唯一实例
    private static SingletonDemo02 instance;
	// 定义私有构造方法、不允许外部直接new创建对象
    private SingletonDemo02() {
        System.out.println(Thread.currentThread().getName() + " create the SingletonDemo02.");
    }
	// 暴露getInstance方法来获取唯一的对象
    public static SingletonDemo02 getInstance() {
        // 双重校验来保证创建唯一的对象
        if (instance == null) {
            synchronized (SingletonDemo02.class) {
                if (instance == null) {
                    instance = new SingletonDemo02();
                }
            }
        }
        // 返回唯一对象
        return instance;
    }
}
```

以上代码似乎没有问题，但是事实却并不是这样，要分析出存在的问题，我们需要分析更加底层的代码。

```java
// 这局代码在底层可分成下面3条语句执行
instance = new SingletonDemo02();

memory = allocate();	// 1. 分配对象内存空间
instance(memory);		// 2. 初始化对象
instance=memory;		// 3. 设置instance对象指向分配的内存地址，此时instance!=null

// 上述过程中，步骤2、3并不存在数据依赖关系，在单线程情况下无论是否重排对程序的执行结果都是无影响的
// 因此，步骤2、3是运行被重排的如下

memory = allocate();	// 1. 分配对象内存空间
instance=memory;		// 3. 设置instance对象指向分配的内存地址，此时instance!=null
instance(memory);		// 2. 初始化对象

// 此时便产生一些问题
// Thread A 在执行1 --> 3时，instance语句非null，但是并未初始化完成。
// 此时Thread B调用getInstance是可以顺利拿到return 的 instance。
// 但是拿到的instance时未被初始化完的！！！！！！这样便产生了现场安全问题
// 因此为了避免指令重拍带来的问题，我们需要使用volatile命令来限制cpu对instance进行指令重排

private static volatile  SingletonDemo03 instance;
```

## 2. CAS

CAS，Compare And Swap，即比较并交换。

在多线程操作一个共享变量的时候，我们通过CAS来保证线程安全。

通过比较当前工作内存中的值和主内存中的值，如果相同则执行规定操作，否则直到主内存和工作内存的值一致为止。

下面展示了CAS的简单使用。

```java
public class Demo05CAS01 {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger(0);
        System.out.println(atomicInteger.compareAndSet(0, 2020) + "\t current:" + atomicInteger);
        System.out.println(atomicInteger.compareAndSet(20, 2019) + "\t current:" + atomicInteger);
    }
}
```

> 运行上面的程序我们可以发现：
>
> 第一个Sout输出：true且正确修改atomicInteger变量。
>
> 第二个Sout输出：false且不能修改值。
>
> 原因是，compareAndSet方法在执行时会先拿第一个参数与原有变量值进行比较，如果第一个参数值和原值一致就执行修改，返回true。否则不修改，返回false。

正式通过CAS这一机制，AtomicInteger.getAndIncrement()方法可以在不加锁的情况下仍然能够保证线程安全。

### 2.1 CAS底层原理

到底是什么原因使得CAS可以在不加锁的情况下仍能保持线程安全性呢，接下来我们看看其底层原理。

下面是AtomicInteger.getAndIncrement方法的源码

```java
public final int getAndIncrement() {
    return unsafe.getAndAddInt(this, valueOffset, 1);
}
```

> 从源码可以看出该方法调用了unsafe对象中的getAndAddInt方法

接下来我们再看看**unsafe**以及**valueOffset**是何方神圣

```java
// setup to use Unsafe.compareAndSwapInt for updates
private static final Unsafe unsafe = Unsafe.getUnsafe();
private static final long valueOffset;

static {
    try {
        valueOffset = unsafe.objectFieldOffset(AtomicInteger.class.getDeclaredField("value"));
    } catch (Exception ex) { throw new Error(ex); }
}

private volatile int value;
```

> 从源码中，我们可以看到，unsafe是一个Unsafe类的一个对象，通过Unsafe.getUnsafe静态方法拿到
>
> 而valueOffset是一个long类型的变量，实质上记录了AtomicInteger中value变量相当于对象的内存偏移，可以理解为变量的内存地址。

#### Unsafe类

从上面我们可以知道AtomicInteger.getAndIncrement方法是通过Unsafe类来实现的，接下来我们在认识一下Unsafe类。

+ Unsafe类是CAS的核心类，由于Java方法无法直接访问底层，需要通过本地方法（native method）来访问。而Unsafe类封装了很多native方法，基于该类可以直接操作特定内存数据。

+ Unsafe类位于sun.misc包中，内部方法操作可以像C的指针一样直接操作内存，因为Java中CAS操作的执行依赖于Unsafe类的方法。
+ Unsafe类中native方法表明，这些方法不是由Java自己实现，而是调用操作底层资源执行。

从上面我们还看得了ValueOffset，是该变量在内存中的偏移地址，因为Unsafe是根据内存偏移地址来获取数据的。

而value被volatile修饰保证了value的可见性。

下面我们来看Unsafe的getAndAddInt()方法

```java
// getAndAddInt()
// var1 --> AtomicInteger对象本身，被传入this
// var2 --> 对象值引用的地址偏移
// var4 --> 需要变动的值
// var5 --> 通过var1、var2去内存中看到的值
public final int getAndAddInt(Object var1, long var2, int var4) {           
    int var5;   
    // 通过判断内存拿到的值以及传进来的值，比较
    // 如果比较一致，则调用交换
    do {                                                                    
        var5 = this.getIntVolatile(var1, var2);                             
    } while(!this.compareAndSwapInt(var1, var2, var5, var5 + var4));        

    return var5;                                                            
}

// compareAnSwapInt() 
// native方法
// 实质上比较并交换的方法
// 该方法是一个本地方法，实现位于unsafe.cpp中
// 该方法底层是使用了操作系统的原语来保证执行原子性
public final native boolean compareAndSwapInt(Object var1, long var2, int var4, int var5);
```

为什么需要通过读以上的方式来保证数据能够得出正确的运算结果呢？我们来看下面一种情况

```
1. A、B线程同时拿到了value的副本拷贝。【value = 5】
2. A线程对value进行操作，运算完毕正要写回共享内存，但此时刚好CPU调度运行线程B【value = value + 1】
3. B线程又对value操作，运行完毕，顺利的写回了共享内存【value = 6】
4. CPU再次调度A，执行到compareAndSwapInt发现值已经不一样了，因此不能执行修改，再次回到do内部，获取新的值，再判断。
```

### 2.2 CAS缺点

尽管CAS机制可以使得变量在不加锁的情况下仍然可以本多个线程并发操纵而且保证结果的正确性，但是CAS仍然具有一些缺点。

+ 循环时间长开销很大：由于使用do、while已经，如果CAS长时间不成功，将会给CPU带来很大开销。
+ 只能保证一个共享变量的原子性：对于多个共享变量，CAS无法保证操作的原子性。
+ 引入了ABA问题

## 3. ABA问题

上面我们提到CAS会引入ABA问题，那么到底什么是ABA问题呢？

CAS算法实现的一个重要前提是，需要取出内存中某个时刻的数据并与当前的数据进行并比较并替换。

这样就会引出一个问题，在这个时间间隔内，值被改变了若干次，但最终的结果却是与改变前的值保持一致。

这时候CAS并没有知道数据是否被改变，只是比较该值仍然一致，仍然能够成功执行CAS操作。

**因此，尽管线程A的CAS操作成功，但是不代表过程是没有问题的。**

> 这里可能会有一些疑惑，ABA问题一定会产生危害吗？
>
> 这其实取决于我们是否关注过程，如果我们只需要结果一致而不关注过程，我们便可以忽略ABA问题；
>
> 但是如果我们对过程有要求，我们就需要解决ABA问题。

### 3.1 原子引用

在处理ABA问题前，我们先引入原子引用的概念。

之前我们用的都是AtomicInteger等基本数据类型的原子类，但是如果我们需要将我们自己定义的类定义为原子类，我们该如何定义呢？

我们想到的问题Java工程师也替我们想到了，我们可以使用AtomicReference来对我们自定义的类进行包装。

```java
// 自定义类
class User {
    private String name;
    private Integer age;

    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}

// 自定义原子包装类
public class Demo06AtomicReference {
    public static void main(String[] args) {
        
        User user1 = new User("tobing", 12);
        User user2 = new User("zenyet", 13);
        
        AtomicReference<User> userAtomicReference = new AtomicReference<>();
        userAtomicReference.set(user1);
        boolean isChange1 = userAtomicReference.compareAndSet(user1, user2);
        System.out.println(Thread.currentThread().getName() + " : " + isChange1 + " " + userAtomicReference.get());
        boolean isChange2 = userAtomicReference.compareAndSet(user1, user2);
        System.out.println(Thread.currentThread().getName() + " : " + isChange2 + " " + userAtomicReference.get());
    }
}
```

### 3.2 基于时间戳的原子引用

上面我们提到了ABA问题，为了解决ABA问题，Java工程师为我们提供了基于时间戳的原子引用AtomicStampedReference。

AtomicStampedReference的原理是，每次对引用进行修改的时候，添加一个类似版本标记的时间戳，当修改一次，版本号改变。

这时，即使当我们就是把之前的值经过若干次修改之后，修改回来到原来的值，我们仍然能够通过时间戳来判断是否已经被修改过。

下面演示了普通原子引用以及基于时间戳的原子引用的不同

```java
public class Demo07AtomicStampedReference {
    /**
     * 原子引用
     */
    private static AtomicReference<Integer> atomicReference =
            new AtomicReference<>(100);
    /**
     * 基于时间戳的原子引用
     */
    private static AtomicStampedReference<Integer> stampedReference =
            new AtomicStampedReference<>(100, 1);


    public static void main(String[] args) {
        System.out.println("===================ABA问题演示==================");
        new Thread(() -> {
            atomicReference.compareAndSet(100, 101);
            atomicReference.compareAndSet(101, 100);
        }).start();

        new Thread(() -> {
            // 睡眠1s、确保上面线程已经执行了ABA操作
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean flag = atomicReference.compareAndSet(100, 6666);
            Integer res = atomicReference.get();
            System.out.println(Thread.currentThread().getName() + "执行是否成功: " + flag + "\t 执行结果为：" + res);
        }).start();

        System.out.println("===================ABA解决演示==================");
        new Thread(() -> {
            // 睡眠1s，确保下面线程拿到的是初始的版本号
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 执行ABA操作
            stampedReference.compareAndSet(100, 101,
                    stampedReference.getStamp(), stampedReference.getStamp() + 1);
            stampedReference.compareAndSet(101, 100,
                    stampedReference.getStamp(), stampedReference.getStamp() + 1);
        }).start();

        new Thread(() -> {
            int stamp = stampedReference.getStamp();
            // 睡眠1s、确保上面线程已经执行了ABA操作
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean flag = stampedReference.compareAndSet(100, 9999,
                    stamp, stamp + 1);
            Integer res = stampedReference.getReference();
            System.out.println(Thread.currentThread().getName() + " 版本号："
                    + stampedReference.getStamp() + " 是否修改：" + flag + " 最终结果：" + res);
        }).start();
    }
}
```

最终结果

```bash
===================ABA问题演示==================
===================ABA解决演示==================
Thread-1执行是否成功: true	 执行结果为：6666
Thread-3 版本号：3 是否修改：false 最终结果：100

Process finished with exit code 0
```

## 4. 不安全集合

集合在我们日常开发中可谓是无处不见，但是我们常用的集合框架类（如ArrayList、HashSet、HashMap等）考虑到性能问题都是设计为线程非安全的。这就引出一个问题，在多线程环境下我们应该如何高效使用这些集合呢？

### 4.1 ArrayList

1. 多线程环境下使用ArrayList将会出现异常。

   ```java
   // 线程操纵资源类
   List<Integer> list = new ArrayList<>();
   for (int i = 0; i < 30; i++) {
       new Thread(() -> {
           for (int j = 0; j < 5; j++) {
               list.add(1);
               System.out.println(list);
           }
       }).start();
   }
   ```

   > 运行以上程序将出现并发修改异常：java.util.ConcurrentModificationException

2. 解决方法

   1）使用老式线程安全集合替代：Vector

   ```java
   // List<Integer> list = new ArrayList<>();
   // 用一下替代上面代码
   List<Integer> list = new Vector<>();
   ```

   2）使用Collections集合工具类：Collections.synchronizedAsList()

   ```java
   // List<Integer> list = new ArrayList<>();
   // 用一下替代上面代码
   List<Integer> list = Collections.synchronizedList(new ArrayList<>());
   ```

   3）使用juc包下提供的新集合类：CopyOnWriteArrayList

   ```java
   // List<Integer> list = new ArrayList<>();
   // 用一下替代上面代码
   List<Integer> list = new CopyOnWriteArrayList<>();
   ```

3. 解决方法原理

   1）Vector这个不用说，用来synchronized修饰方法，我们知道这样在多线程环境下是很低效的，因此我们不推荐使用该方式。

   2）Collections.synchronizedAsList我们暂且不提

   3）CopyOnWriteArrayList包是juc包提供，我们主要的使用对象

   接下来我们看看CopyOnWriteArrayList是如何在不加synchronized下保证线程安全的

   ```java
   public boolean add(E e) {
       // 可重入锁加锁
       final ReentrantLock lock = this.lock;
       lock.lock();
       try {
           // 拿到存储数据的数组
           Object[] elements = getArray();
           int len = elements.length;
           // 在存储数据的数组上长度+1，创建新数组，并将旧数组中的值复制到新数组
           Object[] newElements = Arrays.copyOf(elements, len + 1);
           // 在新增的位置辅助
           newElements[len] = e;
           // 把新的数组设置回去
           setArray(newElements);
           return true;
       } finally {
           // 解锁
           lock.unlock();
       }
   }
   ```

   > 以上是CopyOnWriteArrayList中add方法的源码

   通过分析add方法的源码我们可以大概知道有这几点：

   + add通过轻量级可重入锁ReentrantLock来加锁；
   + add通过写时新复制一份的方式，在复制的基础上加上新加的部分；
   + add最后将新加了元素的数组重新设置到ArrayList中，实现更新。

### 4.2 HashSet

与ArrayList大同小异

### 4.3 HashMap

与ArrayList大同小异，注意的是juc中提供的是ConcurrentHashMap



## 5. 各种锁

公平锁、非公平锁、可重入锁、递归锁、自旋锁......

### 5.1 公平锁与非公平锁

#### 公平锁

是指多个线程按照申请锁的顺序来获取锁，类似排队，先到先得。

#### 非公平锁

是指多个线程获取锁的顺序不一定按照申请锁的顺序，有可能后申请锁的线程能够比先申请锁的线程先拿到锁。

并发并公平锁高，但可能发生优先级反转或者饥饿现象。

#### 联系与区别

公平锁：并发环境下每个线程在获取锁的时候会先查看对应**锁维护的等待队列**。如果为空，或当前线程是等待线程的第一个，就占有锁，否则就加入到等待队列中，以后安装FIFO的方式从队列中取出自己。

非公平锁：上来就直接尝试申请锁，如果失败采用公平锁的方式。

synchronized采用的是非公平锁。

### 5.2 可重入锁

可重入锁也称递归锁。指的是统一线程外层函数获得锁之后，内层递归函数仍然能够获取该锁的代码。

同一个线程在外层方法获取锁之后，进入内层方法会自动获取锁。

**即线程可以进入任何一个它已经拥有的锁所同步着的代码块。**

可重入锁最大的作用就是避免死锁。

资源类

```java
class Phone {

    public synchronized void sendMSM() {
        System.out.println(Thread.currentThread().getName() + " : sendMSM....");
        sendEmail();
    }

    public synchronized void sendEmail() {
        System.out.println(Thread.currentThread().getName() + " : sendEmail....++++++");
    }
    
    public void doubleLock() {
        // 加锁2次
        lock.lock();
        lock.lock();
        try {
            System.out.println("Double Lock.");
        } finally {
            // 解锁2次
            // 如果只解锁一次，程序便会阻塞于此
            lock.unlock();
            lock.unlock();
        }
    }
}
```

> 同一把锁可以lock多次，但是lock多少次就需要unlock多少次。

线程调用资源类

```java
public class Demo11ReentrantLock {
    public static void main(String[] args) {
        Phone phone = new Phone();
        new Thread(() -> {
            phone.sendMSM();
        }, "T1").start();
        new Thread(() -> {
            phone.sendMSM();
        }, "T2").start();
    }
}
```

### 5.4 自旋锁

自旋锁是指获取锁的线程不会立即阻塞，而是采用循环的方式获取锁，这样的好处是减少线程上下文的切换，缺点是循环会消耗CPU资源。

JDK中Unsafe类中getAndAddInt方法对自旋锁的实现：

```java
public final int getAndAddInt(Object var1, long var2, int var4) {
    int var5;
    do {
        var5 = this.getIntVolatile(var1, var2);
    } while(!this.compareAndSwapInt(var1, var2, var5, var5 + var4));
    return var5;
}
```

自实现的自旋锁

```java
// 自定义自旋锁
class MySpinLock {
    AtomicReference<Thread> atomicReference = new AtomicReference<>();
    public void lock() {
        // 拿到当前进程
        Thread cur = Thread.currentThread();
        // 如果null，则将value更新为cur
        // 当value=cur时，其他进程进来是，肯定是非null，只能循环等待
        while (!atomicReference.compareAndSet(null, cur)) {
        }
    }
    public void unlock() {
        // 解锁时将value设置为null，允许其他线程跳出循环。
        Thread cur = Thread.currentThread();
        atomicReference.compareAndSet(cur, null);
    }
}
```

使用自定义自旋锁

```java
public class Demo12SpinLockDemo {
    public static void main(String[] args) {
        MySpinLock spinLock = new MySpinLock();

        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() +" : come in.");
            // 上锁
            spinLock.lock();
            // 睡眠2s等待T2申请锁
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " : doing.........");
            System.out.println(Thread.currentThread().getName() + " : Finish.");
        }, "T1").start();

        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() +" : come in.");
            // 上锁
            spinLock.lock();
            // 睡眠3s模拟执行
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " : doing.........");
            System.out.println(Thread.currentThread().getName() + " : Finish.");
        }, "T2").start();

    }
}
```

### 5.5 独占锁与共享锁

独占锁：指的是该锁一次只能被一个线程占有。ReentrantLock与synchronized都是独占锁。

 共享锁：指的是锁可以被多个线程持有。

ReentrantReadWriteLock中读锁是共享锁，写锁是独占锁。

### 5.6 读写锁

多线程并发操纵一个资源类，为了满足并发量，允许读取共享资源你应该可以同时进行，但也有一个写共享资源应该是排他的。

读写锁实现缓存，使得MyCache中可以共享get，排他set。

```java
class MyCache {

    private volatile Map<String, Object> map = new HashMap<>();
    ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public void set(String key, Object value) {
        readWriteLock.writeLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + " : 正在写入" + key);
            // 睡眠300ms 模拟网络阻塞
            try {
                TimeUnit.MILLISECONDS.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            map.put(key, value);
            System.out.println(Thread.currentThread().getName() + " : 写入完成" + key);
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    public void get(String key) {
        readWriteLock.readLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + " : 正在读取" + key);
            // 睡眠300ms 模拟网络阻塞
            try {
                TimeUnit.MILLISECONDS.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Object o = map.get(key);
            System.out.println(Thread.currentThread().getName() + " : 读取完成" + key);
        } finally {
            readWriteLock.readLock().unlock();
        }
    }
}
```

> 使用自定义缓存MyCache

```java
public class Demo13ReadWriteLock {
    public static void main(String[] args) {
        MyCache myCache = new MyCache();
        for (int i = 0; i < 10; i++) {
            final int temp = i;
            new Thread(() -> {
                myCache.set(temp + "", temp + "");
            }, i + "").start();
        }

        for (int i = 0; i < 10; i++) {
            final int temp = i;
            new Thread(() -> {
                myCache.get(temp + "");
            }, i + "").start();
        }
    }
}
```

## 6. CCS

### 6.1 CountDownLatch 

CountDownLatch可以实现让一些线程阻塞直到另外一些完成之后才唤醒。

+ 当一个或多个线程调用await方法时，调用线程会被阻塞；

+ 其他线程调用countDown方法计数器-1；
+ 当计数器变为0，因调用await方法而被阻塞的线程会被唤醒，继续执行。

班长关门案例：班上有6个人、班长必须等待最后一个同学坐了才能够锁门

```java
private static void closeDoor() throws InterruptedException {
    CountDownLatch countDownLatch = new CountDownLatch(6);
    for (int i = 0; i < 6; i++) {
        final int temp = i;
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "同学走了。");
            countDownLatch.countDown();
        },String.valueOf(i)).start();
    }
    countDownLatch.await();
    System.out.println("班长关门");
}
```



### 6.2 CyclicBarrier

CyclicBarrier，可循环使用屏障。

+ 实现让一组线程到达一个屏障（同步点）时被阻塞；
+ 直到所有线程都到达屏障点，所有被屏障点拦截的线程都可以继续执行下去。

召唤龙珠案例：必须要集齐7颗龙珠，才能召唤出神龙。

```java
public static void collectDragonBall() {
    CyclicBarrier cyclicBarrier = new CyclicBarrier(7, () -> {
        System.out.println("出来吧！神龙！");
    });
    for (int i = 0; i < 7; i++) {
        final int temp = i + 1;
        new Thread(() -> {
            System.out.println("第" + temp + "颗龙珠找到。");
            try {
                cyclicBarrier.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }
        }, "Thread-name").start();
    }
}
```



### 6.3 Semaphore

信号量。

+ 用于共享线程的互斥使用；
+ 用于并发资源数的控制。

抢车位案例：一共6个车位，第7辆车要想进来必须要等待里面的一辆或多辆出来。

```java
private static void getCarSpace() {
    // 最高并发数一共只有6个
    Semaphore semaphore = new Semaphore(6);
    // 6个线程并发争抢资源
    for (int i = 0; i < 12; i++) {
        new Thread(() -> {
            // 拿到共享资源-》车位
            try {
                semaphore.acquire();
                System.out.println(Thread.currentThread().getName() + "停车了");
                // 停车1s
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(Thread.currentThread().getName() + "停留了1s，离开了");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                // 释放共享资源
                semaphore.release();
            }
        }, String.valueOf(i)).start();
    }
}
```

## 7. 阻塞队列

BlockQueue

阻塞队列本质就是一个队列，但是符合以下条件：

+ 队列满时，从队列中添加元素的操作会被阻塞
+ 队列空时，从队列中删除元素的操作会被阻塞

### 7.1 好处

多线程环境下，某些情况下需要将线程挂起（阻塞），一旦某些条件满足，被挂起的线程将会自动唤醒。

在上面我们提到的例子中，我们很多时候都是通过自己定义await、singal的逻辑。这时候就要我们兼顾效率和线程安全，给我们开发带来了不便。

因此，Java为了便利开发，为我们提供了阻塞队列，对线程的阻塞、唤醒等操作进行了封装。

### 7.2 核心方法

BlockQueue核心有6个方法，6个方法两两对应，可以运用于不同场景。

| 方法类型     | 插入               | 删除            | 检查      |
| ------------ | ------------------ | --------------- | --------- |
| **抛出异常** | add()              | remove()        | element() |
| **特殊值**   | offer()            | poll()          | peek()    |
| **阻塞**     | put()              | take()          | 无        |
| **超时**     | offer(e,time,unit) | poll(time,unit) | 无        |

1. 抛出异常

   队列满时添加元素抛出异常

   队列空时删除元素抛出异常

2. 特殊值

   队列满时添加元素返回false，表示添加失败

   队列空时删除元素返回null，表示删除失败

3. 一直阻塞

   队列满时添加元素阻塞

   队列空时删除元素阻塞

4. 超时

   队列满时添加元素阻塞，到指定时间还不成功就返回false

   队列空时删除元素阻塞，到指定时间还不成功就返回null

### 7.3 架构

![image-20201112171107703](https://i.loli.net/2020/11/12/JwQFogaBb8OcAtH.png)

```java
/**
 * BlockQueue
 * ----LinkedTransferQueue      链表结构组成的无解阻塞队列
 * ----LinkedBlockingDeque      链表结构组成的双向阻塞队列
 * ----SynchronousQueue       * 不存储元素的阻塞队列，即单个元素队列
 * ----ArrayListBlockingQueue * 数组结构组成的有界阻塞队列
 * ----LinkedBlockingQueue    * 链表结构组成的有界阻塞队列（size=Integer.MAX_VALUE）
 * ----DelayQueue               优先级队列实现的延迟队列
 * ----PriorityBlockingQueue    支持优先级排序的无界阻塞队列
 */
```

> *表示常用

### 7.4 应用

阻塞队列实现生产者消费者模式

资源类

```java
class MyDate {
	// 全局控制
    private volatile boolean FLAG = true;
    private AtomicInteger atomicInteger = new AtomicInteger();
    private BlockingQueue<String> blockingQueue;
    public MyDate(BlockingQueue<String> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }
    // 生产
    public void myProd() throws InterruptedException {
        String data = null;
        boolean isFinish;
        while (FLAG) {
            data = atomicInteger.incrementAndGet() + "";
            isFinish = blockingQueue.offer(data, 2L, TimeUnit.SECONDS);
            if (isFinish) {
                System.out.println(Thread.currentThread().getName() + "\t 插入数据" + data + "成功");
            } else {
                System.out.println(Thread.currentThread().getName() + "\t 插入数据" + data + "失败");
            }
            TimeUnit.SECONDS.sleep(1);
        }
    }
	// 消费
    public void myConsumer() throws InterruptedException {
        String res = null;
        while (FLAG) {
            res = blockingQueue.poll(2L, TimeUnit.SECONDS);
            if (null == res || "".equals(res)) {
                FLAG = false;
                System.out.println(Thread.currentThread().getName() + "\t 超过2S没取到消息，消费退出");
                System.out.println();
                System.out.println();
                return;
            }
            System.out.println(Thread.currentThread().getName() + "消费队列" + res + "成功");
        }
    }
    // 停止
    public void stop() {
        this.FLAG = false;
    }
}
```

> 多线程并发调用

```java
public class Demo22ProdConsumerBlockQueue {
    public static void main(String[] args) throws InterruptedException {
        MyDate myDate = new MyDate(new ArrayBlockingQueue<>(5));
        new Thread(() -> {
            try {
                System.out.println(Thread.currentThread().getName() + "\t 生产进程启动");
                myDate.myProd();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "Thread-name").start();

        new Thread(() -> {
            try {
                System.out.println(Thread.currentThread().getName() + "\t 消费进程启动");
                myDate.myConsumer();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "Thread-name").start();

        TimeUnit.SECONDS.sleep(10);
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("时间到，停止活动");
        myDate.stop();
    }
}
```

## 8. 线程池

线程池主要做的工作是控制运行线程的数量。

处理过程中将任务加入队列中，然后在线程创建后启动这些任务。

如果线程超过最大数量、超出的线程将会排队等候，等待其他线程执行完毕，再从队列中取出任务执行。

### 8.1 线程池的特点

1. **降低资源消耗。**通过重复利用已经创建的线程，降低线程创建和销毁造成的损耗。
2. **提高响应速度。**当任务下达时，任务不需要等待线程创建就能直接执行。
3. **提高线程可管理性。**线程是稀缺资源，如果无限创建不仅会消耗系统资源，还会降低系统稳定性。使用线程池可以统一分配、调优和监控。

### 8.2 线程池的架构

Java中线程池是通过Executor框架实现，该框架主要用到：Executor、ExecutorService、ThreadPoolExecutor、Executors这几个类。

![image-20201114092231857](https://i.loli.net/2020/11/14/93nTP8aoAcH7XGq.png)

### 8.3 线程池的简单使用

Java中的Executors线程池工具类为我们提供了3种创建线程的方法：

```java
//执行一个长期的任务,性能好很多
ExecutorService threadPool1 = Executors.newFixedThreadPool(5);

// 一个任务一个线程执行的任务场景
ExecutorService threadPool2 = Executors.newSingleThreadExecutor();

// 适用于：执行很多短期异步的小程序或者负载较轻的服务器
ExecutorService threadPool3 = Executors.newCachedThreadPool();
```

尽管Executors为我们提供了3种创建线程池的方式。

但是这三种方式在生产环境下并不使用。【基于阿里巴巴手册】

下面我们通过源码分析其存在的隐患

```java
// FixedThreadPool
public static ExecutorService newFixedThreadPool(int nThreads) {
    return new ThreadPoolExecutor(nThreads, nThreads,
                                  0L, TimeUnit.MILLISECONDS,
                                  new LinkedBlockingQueue<Runnable>());
}

// SingleThreadExecutor
public static ExecutorService newSingleThreadExecutor() {
    return new FinalizableDelegatedExecutorService
        (new ThreadPoolExecutor(1, 1,
                                0L, TimeUnit.MILLISECONDS,
                                new LinkedBlockingQueue<Runnable>()));
}

// CacheThreadPool
public static ExecutorService newCachedThreadPool() {
    return new ThreadPoolExecutor(0, Integer.MAX_VALUE,
                                  60L, TimeUnit.SECONDS,
                                  new SynchronousQueue<Runnable>());
}
```

我们发现，这三个方法底层都直接或间接依赖ThreadPoolExecutor来实现线程池的创建。

因此在分析之前我们先简单介绍ThreadPoolExexutor的几个参数

1. 核心线程数【常驻】
2. 最大线程数
3. 延时等待时间
4. 延时等待时间单位
5. 阻塞队列

在这些参数中，我们先别管其他，先看第5个参数：阻塞队列。

阻塞队列是：在线程数大于核心线程数，新来的线程会在阻塞队列中等待。

但是由于LinkedBlockingQueue队列的长度默认是21亿多，如果过多线程进来，容易导致OOM。

### 8.4 自定义线程池

由上面我们可以知道，Executors方式创建的线程池在实践中容易导致OOM，因此实践中往往使用自定义线程池。

在自定义线程池之前，我们先了解创建线程池的核心类：**ThreadPoolExecutor**

```java
public ThreadPoolExecutor(int corePoolSize,
                          int maximumPoolSize,
                          long keepAliveTime,
                          TimeUnit unit,
                          BlockingQueue<Runnable> workQueue,
                          ThreadFactory threadFactory,
                          RejectedExecutionHandler handler)
```

上面展示了ThreadPoolExecutor的构造方法，接下来我们了解这个7大参数

+ corePoolSize：线程池常驻核心线程数
+ maximumPoolSize：线程池能够容纳同时执行的最大线程数

+ keepAliveTime：多余的空闲线程存活时间、当空闲时间达到keepAliveTime，多余线程会被销毁直到剩下corePoolSize
+ unit：keepAliveTime单位
+ workQueue：任务队列，提交但未执行的任务。
+ threadFactory：线程池中工作线程的线程工厂，一般采用默认即可
+ handler：拒绝策略。当线程队列满并且工作线程大于线程池最大线程数，handler将指定任何拒绝新来的线程。

在使用ThreadPoolExecutor前，我们先通过图解方式讲解线程池底层执行的原理：

![image-20201114094735529](https://i.loli.net/2020/11/14/UVDLHZGlF6hsIkv.png)

1. 创建线程池后，等待提交过来的线程任务
2. 在调用execut()方法添加一个任务请求，线程池会做如下判断：
   + 如果正在运行的线程数量小于corePoolSize，则马上创建线程运行这个任务。
   + 如果正在运行的线程数量大于corePoolSize，则在**workQueue中等待**。
   + 如果workQueue队列已满，且正在运行的线程数量还小于maximumPoolSize，则创建**非核心线程**立即执行这个任务。
   + 如果workQueue队列已满，且正在运行的线程数量大于或等于maximumPoolSize，则会使用**拒绝策略**执行拒绝。
3. 当一个线程完成任务，会从workQueue中取出一个人任务执行。
4. 当一个线程空闲时间超过keepAliveTime，线程池会判断：
   + 如果当前运行的线程数大于corePoolSize，那么线程会被停用

下面我们通过自定义ThreadPoolExecutor创建自定义线程池：

```java
ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
    2,
    5,
    1,
    TimeUnit.SECONDS,
    new LinkedBlockingQueue<Runnable>(4),
    Executors.defaultThreadFactory(),
    // new ThreadPoolExecutor.AbortPolicy());    // 该方式会在无法接受线程的时候直接抛出异常
    // new ThreadPoolExecutor.CallerRunsPolicy()); // 该方式会在无法接受线程的时候抛会给调用者
    // new ThreadPoolExecutor.DiscardOldestPolicy());  // 该方式将处理不来的不执行
    new ThreadPoolExecutor.DiscardPolicy());
```

### 8.5 拒绝策略

当**等待队列已满且线程池最大线程数也满**的时候，无法继续为新任务提供服务，这时候，需要拒绝策略机制合理处理这个问题。

JDK中为我们提供了4种拒绝策略：

+ AbortPolicy：默认策略，直接**抛出异常**阻止系统正常运行。
+ CallerRunPolicy：“调用者运行”机制，不会抛弃任务，也不会抛出异常，而是给调用线程池的线程执行。
+ DiscardOldestPolicy：抛弃队列中等待最久的。
+ DiscardPolicy：直接丢弃任务，不做任务响应。（如果允许任务丢失，将会是最好的拒绝策略）。

### 8.6自定义线程池参数配置

实际工程中我们该如何配置我们ThreadPoolExecutor的参数呢？

我们需要区分不同类型的业务场景：**CPU密集型和IO密集型**。

#### CPU密集型

CPU密集型指的是，任务需要大量的运算而没有阻塞（如IO阻塞），CPU一直在全力运算。

CPU密集型任务只有在正在的多核CPU上才能得到加锁。

> 可以通过Runtime.getRuntime().availableProcessors()拿到CPU核心数。

CPU密集型任务配置尽可能少的线程数量：线程数=CPU核数+1

#### IO密集型

由于IO密集型，线程常常是在等待IO操作而阻塞，因此应该配置尽量多的线程。

线程数量=CPU核心数*2

## 9. 死锁

### 9.1 概念

死锁是指两个或两个以上进程在执行过程中，因争夺资源而造成**相互等待的现象**，若无外力干涉那它们都将无法推进下去。

### 9.2 演示

运行下面代码将会产生死锁

```java
public class Demo27ThreadDeadLock {
    public static void main(String[] args) {
        new Thread(new MyDeadLock("AAA","BBB")).start();
        new Thread(new MyDeadLock("BBB","AAA")).start();
    }
}

class MyDeadLock implements Runnable {

    private String lockA;
    private String lockB;

    public MyDeadLock(String lockA, String lockB) {
        this.lockA = lockA;
        this.lockB = lockB;
    }

    @Override
    public void run() {
        synchronized (lockA) {
            System.out.println(Thread.currentThread() + "拿到了锁" + lockA + "尝试去申请" + lockB);
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (lockB) {
                System.out.println(Thread.currentThread() + "拿到了锁" + lockB + "尝试去申请" + lockA);
            }
        }
    }
}
```



### 9.3 排查

#### jps定位进程编号

```bash
F:\code\learn\java-learn>jps -l
14912 sun.tools.jps.Jps
15696 top.tobing.zy.interview.juc.Demo27ThreadDeadLock
5408
9040 org.jetbrains.jps.cmdline.Launcher
```

> 上面可以看到只有一个进程是top.tobing开头的。

#### jstack找到死锁

```bash
F:\code\learn\java-learn>jstack 15696
...
"Thread-1":
        at top.tobing.zy.interview.juc.MyDeadLock.run(Demo27ThreadDeadLock.java:37)
        - waiting to lock <0x0000000780eca678> (a java.lang.String)
        - locked <0x0000000780eca6a8> (a java.lang.String)
        at java.lang.Thread.run(Thread.java:748)
"Thread-0":
        at top.tobing.zy.interview.juc.MyDeadLock.run(Demo27ThreadDeadLock.java:37)
        - waiting to lock <0x0000000780eca6a8> (a java.lang.String)
        - locked <0x0000000780eca678> (a java.lang.String)
        at java.lang.Thread.run(Thread.java:748)

Found 1 deadlock.
```

> 已经找到死锁。



