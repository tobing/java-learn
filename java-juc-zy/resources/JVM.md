#  JVM

## 1. JVM体系结构

JVM是运行在操作系统之上的，并没有与硬件直接打交道。

下图展示了JVM体系结构图：

![image-20201115122400987](https://i.loli.net/2020/11/15/OoveIpiH4gC9lLc.png)

> 从图中我们可以看出JVM体系结构中主要包含 9部分：
>
> ClassLoader类装载器、Method Area方法区、Heap堆、Java栈、本地方法栈、程序计数器、执行引擎、本地方法接口、本地方法库。

### 1.1 ClassLoader

类装载器主要**负责加载.class文件**，将class文件字节码内容加载到内存中，并将这些内容转换成方法区中运行时数据结构。

ClassLoader只负责class文件的加载，至于class文件是否可运行有执行引擎（Execution Engine）决定。

> 注意：ClassLoader加载class会检查其文件内容头部是否符合标准

#### 类加载器类型

在Java中提供了3种类装载器

1. 启动类装载器（Bootstrap）

   底层C++、主要用于加载`$JAVA_HOME$/jre/lib/rt.jar`下的类

2. 扩展类装载器（Extension）

   底层Java、主要用于加载`$JAVA_HOME$/jre/lib/ext/*.jar`下的类

3. 应用程序类装载器（App）

   主要加载`$CLASSPATH$`下的类

#### 双亲委派

双亲委派有点像啃老，有事先找我爹去。即：

当一个类收到类加载的请求，首先先把请求委派给父类去完成。

每层的类加载器都是如此，如果父类加载器反馈无法完成这个请求，子类加载器才会产生加载。

正是双亲委派的方式，使得我们每加载的java.lang.Object都是属于rt.jar中的。

### 1.2 Execution Engine

执行引擎，负责解析命令，提交给OS执行。

### 1.3 Native Interface

本地接口。

作用是融合不同的语言为Java所用。在Java诞生早起，C/C++还是主流开发语言。为了兼容复用C/C++程序，Java在内存中专门开辟一块区域用于处理native修饰的代码，具体做法是在Native Method Stack中登记native方法，在Execution Engine执行时加载native libraries。

随着技术的发展，使用该方式越来越少，通常通过Socket通信或者WebService方式调用异构的领域。

### 1.4 Native Method Stack

本地方法栈，用于登记native，在Execution Engine执行时加载本地方法库。

### 1.5 PC Register

程序计数寄存器。

每个线程都要一个**私有**的程序计数寄存器，是一个指针，指向方法区中的方法字节码。

PC寄存器是当前线程执行的字节码的行号指示器，执行引擎通过该指针可以读取下一条指令。

PC寄存器占用的空间很小，几乎可以忽略不计。

PC寄存器用于完成分支、循环、跳转、异常处理、线程恢复等基础功能。 不会发生OOM

如果执行到是一个native方法，则计数器是空的。

### 1.6 Method Area

方法区，提供个线程共享的时内存区域。

存储了每个类的结构信息，如：运行时常量池、字段方法数据、构造函数和普通方法的字节码内容。

方法区在不同虚拟机中实现不一样，最典型的就是永久代【JDK7】元空间【JDK8】

### 1.7 Java Stack

栈，主管Java程序的运行，线程创建时创建，随着线程的生命周期存在。

栈不存在垃圾回收问题，是线程私有的。

8种基本数据类型的变量、对象引用变量、实例方法都是在函数的栈内存中分配。

栈主要保存3类数据：

+ 本地变量：输入输出参数以及方法内的变量
+ 栈操作：入栈出栈操作
+ 栈帧数据：类文件、方法等

栈中数据已栈帧的格式存在，栈帧是一个内存取悦，是一个数据集，是一个关于方法和运行期数据的数据集。

当一个方法被调用时就会产生一个栈帧，并被压如栈。

每个方法执行的同时会创建一个栈帧，用于存储局部变量表、操作数栈、动态链接、方法出口等信息。

每个方法调用和完成都伴随栈帧在虚拟机的入栈和出栈的操作。

栈的大小和具体的JVM实现有关，通常在256K~756K之间。

### 1.8 Heap

堆，一个JVM实例只存在一个堆内存，堆内存的大小是可以调节的。

类加载器读取类文件后，需要把：类、方法、常变量放到堆内存中，保存所有引用类型的真实信息，以方便执执行。

堆分为以下三部分：

+ 新生区、Young Generation Space
+ 养老区、Tenure Generation Space
+ 永久区、Permanent Space/Mate Space

![image-20201115145824990](C:%5CUsers%5Ctobing%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20201115145824990.png)



#### 新生区

新生区是类诞生、成长、消亡的区域。类从这里诞生、最后被垃圾回收器收集、结束生命。

新生区又分为3部分：伊甸区、幸存者0区、幸存者1区

+ 伊甸区（Eden Space）
+ 幸存者0区（Survivor Space）
+ 幸存者1区（Survivor Space)

Minor GC第一次发生时，伊甸区中幸存的对象进入作为From区的幸存者0区，同时幸存者1区变为TO区。

Minor GC第二次发生时，伊甸区和From区的幸存者进入TO区，同时情况伊甸园和From区，TO区和From区互换。

#### Minor GC详细过程

1. Eden、SurvivorFrom复制到SurvivorTO，年龄+1
2. Eden、SurvivorFrom清空
3. SurvivorFrom与SurvivorTo互换

```
复制 ---> 清空 ---> 互换
```

## 2. GC

### 2.1 垃圾回收

JVM中，垃圾就是内存中已经不再使用的对象就是垃圾。

GC操作主要对堆内存以及方法区里面的资源进行回收。

如何判断一个对象是垃圾呢？

#### 引用计数法

Java中引用和对象是关联的，操作对象必须要通过引用操作。

因此，可以通过引用计数的方式来判断对象是否可以回收。

即给对象添加一个引用计数器：

如果有一个地方引用它，计数器+1；

如果有一个地方引用失效，计数器-1。

当计数器引用为0，对象就不在可用，可以被回收。

但是这种方式很难解决对象之间相互依赖引用的问题。因此JVM并没采用该方式。

#### GC Roots可达性分析

上面提到引用计数法无法解决引用相互依赖的问题。为了解决该问题，Java使用了可达性分析。

**所谓GC Roots就是一组必须活跃的引用。**

GC Roots可达性分析的基本思路是：以GC Roots中的对象为起点向下搜索，如果对象到GC Roots没有任何引用链接，说明对象不可用。

![image-20201115121830424](https://i.loli.net/2020/11/15/auOsioXHSkJWIYU.png)

Java中可以作为GC Roots对象的有以下：

+ 局部变量【虚拟机栈】
+ 类静态属性引用的对象【方法区】
+ 常量引用的对象【方法区】
+ native方法引用的对象【本地方法栈】



## 3. JVM参数调优

Java中可以看为具有3种参数：标配参数、X参数和XX参数。

### 3.1 标配参数、X参数

对与标配参数、X参数我们只需要了解即可

```java
// 标配函数
java -version
java -help
java -showversion
// X参数
java -Xint -version
java -Xcomp -version
java -Xmixed -version
```

注重关注XX参数

### 3.2 XX参数

XX参数是指指定的时候在前面加上`-XX:`来指定。

XX参数可以细分为两种类型：Boolean类型、KV类型

Boolean类型

```bash
## Boolean中使用 “+”表示开启、“-”关闭
-XX: +PrintGCDetails	## 开启GC日志打印
-XX: -PrintGCDetails	## 关闭GC日志打印
-XX: +UseSerialGC		## 使用串行垃圾收集器
-XX: -UseSserialGC		## 关闭串行垃圾收集器
```

KV类型

```bash
-XX: MetaspaceSize=120m 		## 指定元空间大小
-XX: MaxTenuringThreshold=15	## 指定进入养老区需要建立的代数
```

> 值得注意的是：Xms、Xmx是属于XX参数，如此使用是为了简化。
>
> Xms==>-XX:InitalHeapSize
>
> Xmx==>-XX:MaxHeapSize

### 3.3 查看系统参数

对于正在运行的程序，如果我们想要查看其参数，可以通过jinfo命令查看指定进程的参数。

为了查看进程号，通常要使用jps命令结合使用

```bash
F:\code\learn\java-learn>jps -l			## 查看进程号
3200 org.jetbrains.jps.cmdline.Launcher
12596 sun.tools.jps.Jps
15032
2808 top.tobing.zy.interview.jvm.Demo02JVMParams

F:\code\learn\java-learn>jinfo -flag PrintGCDetails 2808 ## 查看是否开启PrintGCDetail
-XX:-PrintGCDetails

F:\code\learn\java-learn>jinfo -flags 2808	## 查看所有参数
Attaching to process ID 2808, please wait...
Debugger attached successfully.
Server compiler detected.
JVM version is 25.191-b12
Non-default VM flags: -XX:CICompilerCount=4 -XX:InitialHeapSize=199229440 -XX:MaxHeapSize=3179282432 -XX:MaxNewSize=1059586048 -XX:MinHeapDeltaBytes=524288 -XX:NewSize=66060288 -XX:OldSize=133169152 -XX:+UseCompressedClassPointers -
XX:+UseCompressedOops -XX:+UseFastUnorderedTimeStamps -XX:-UseLargePagesIndividualAllocation -XX:+UseParallelGC
Command line:  -javaagent:D:\Program Files\JetBrains\IntelliJ IDEA 2020.2\lib\idea_rt.jar=59332:D:\Program Files\JetBrains\IntelliJ IDEA 2020.2\bin -Dfile.encoding=UTF-8

## 查看初始化默认值
F:\code\learn\java-learn>java -XX:+PrintFlagsFinal
[Global flags]
     intx ActiveProcessorCount                      = -1                                  {product}
    uintx AdaptiveSizeDecrementScaleFactor          = 4                                   {product}
	....

     intx CICompilerCount                          := 4                                   {product}
	....
## 我们可以看到一些事“=”、一些是“:=”
## =是没有被改变、:=是指被修改的
```

### 3.4 常用的配置

在配置JVM参数之前，我们应该参考服务器的硬件参数。

```java
System.out.println("总核心数：" + Runtime.getRuntime().availableProcessors());
long maxMemory = Runtime.getRuntime().maxMemory();
long totalMemory = Runtime.getRuntime().totalMemory();
System.out.println("总内存数：" + totalMemory / 1024 / 1204 + "MB");
System.out.println("最大内存数：" + maxMemory / 1024 / 1204 + "MB");
```

通过我们当前主机的参数，接下来我们可以使用参数进行调整。

```java
-Xms: ==> -XX:InitalHeapSize=10m		// 初始堆空间大小
-Xmx: ==> -XX:MaxHeapSize=10m			// 最大堆空间大小
-Xss: ==> -XX:ThreadStackSize=10m		// 栈大小
-Xmn: ==>  								// 设置年轻代大小

-XX：MatespaceSize=1024m		// 设置元空间大小
-XX:+PrintGCDetails			// 是否开启GC日志打印
-XX:SurvivoRatio=8			// Eden:Survivo	= 8:2
-XX:NewRatio=4				// Old:New = 1:4
-XX:MaxTenuringThreshold=15	// 到老年区的存活次数
```

## 4. 四种引用

Java体系中将引用分为四种：强引用、软引用、弱引用、虚引用。

![image-20201115173014250](https://i.loli.net/2020/11/15/gHwT7cDriIGuXK2.png)

### 4.1 强引用

强引用是我们最常见的引用，我们平常创建的引用就是强引用。

只要还有一个强引用指向一个对象，就能表明对象还活着，GC不会对这些对象进行处理，即使是OOM也不会。

强引用是造成Java内存泄漏的最重要原因之一。

如果一个对象没有引用关系，一般认为是可以被GC回收。

```java
Object o1 = new Object();
Object o2 = o1;
o1 = null;
System.gc();
System.out.println(o2);
```

> 不解释

### 4.2 软引用

软引用是对强引用的一种弱化，需要通过java.lang.ref.SoftReference类实现，可以让对象豁免部分垃圾回收。

对于软引用的对象：

+ 系统内存充足，不会被回收

+ 系统内存不足，会被回收

基于这种机制，软引用常**用于高速缓存**，内存够用的时候保留，不够就将其回收。

```java
Object o1 = new Object();
SoftReference<Object> softReference = new SoftReference<>(o1);
System.out.println(o1);
System.out.println(softReference.get());

System.out.println("======================");
o1 = null;			
System.out.println(o1);							// 输出null、毋庸置疑
System.out.println(softReference.get());		// 输出非null，因为空间足够，还未被GC

// -Xms10m -Xmx10m -XX:+PrintGCDetails
// 刻意制造Heap不足，触发GC
System.out.println("======================");
try {
    byte[] temp = new byte[50 * 1024 * 1024];	
} catch (Throwable t) {
    t.printStackTrace();
} finally {
    System.out.println(o1);						// 不解释
    System.out.println(softReference.get());	// 创建temp前会GC，已经被回收，返回null
}
```



### 4.3 弱引用

弱引用需要通过java.lang.ref.WeakReference类实现，比软引用生存期短。

对于弱引用的对象：只有GC发生，立即被回收。

```java
Object o1 = new Object();
WeakReference<Object> weakReference = new WeakReference<>(o1);
System.out.println(o1);
System.out.println(weakReference.get());

System.out.println("===================");
o1 = null;
System.out.println(o1);						// null
System.out.println(weakReference.get());	// 非null、未触发gc

System.gc();
System.out.println("===================");
System.out.println(o1);						// null
System.out.println(weakReference.get());	// 非null、已经触发gc
```



### 4.4 虚引用

虚引用，顾名思义，形同虚设，虚引用不会决定对象的生命周期。

如果一个对象仅仅持有虚引用，那么**它和没有任何引用一样，任何时候都可能被GC回收**。

虚引用需要java.lang.ref.PhantomReference类实现且使用必须配合引用队列（ReferenceQueue）

虚引用主要用于跟踪对象被GC回收的状态。仅仅是提供一种确保对象被finalize之后做出一些通知。

```java
Object o1 = new Object();
ReferenceQueue<Object> referenceQueue = new ReferenceQueue<>();
// 必须配合ReferenceQueue使用
PhantomReference<Object> phantomReference = new PhantomReference<>(o1, referenceQueue);
System.out.println(o1);							// 非null
System.out.println(phantomReference.get());		// null、虚的、拿不到

o1= null;
System.out.println("===================");
System.out.println(o1);							// null
System.out.println(phantomReference.get());		// null
System.out.println(referenceQueue.poll());		// null、还未被回收、没进队列

System.gc();
System.out.println("===================");
System.out.println(o1);
System.out.println(phantomReference.get());
System.out.println(referenceQueue.poll());		// 非null、手动gc、被回收进入队列
```



### 4.5 引用队列

ReferenceQueue，可以将被回收的引用保存在其中。