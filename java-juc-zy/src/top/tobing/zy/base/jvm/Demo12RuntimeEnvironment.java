package top.tobing.zy.base.jvm;

/**
 * @Author tobing
 * @Date 2020/11/15 10:33
 * @Description
 */
public class Demo12RuntimeEnvironment {
    public static void main(String[] args) {
        System.out.println("总核心数：" + Runtime.getRuntime().availableProcessors());
        long maxMemory = Runtime.getRuntime().maxMemory();
        long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("总内存数：" + totalMemory / 1024 / 1204 + "MB");
        System.out.println("最大内存数：" + maxMemory / 1024 / 1204 + "MB");
    }
}
