package top.tobing.zy.base.jvm;

import java.util.UUID;

/**
 * @Author tobing
 * @Date 2020/11/14 22:44
 * @Description JVM参数调优
 * <p>
 * <p>
 * VM参数： -Xms1024m -Xmx1024m -XX:+PrintGCDetails
 * Xms：初始分配内存、默认为物理内存1/16
 * Xmx：最大分配内存、默认为物理内存1/4
 * -XX:+PrintGCDetails：输出详细的GC处理日志
 */
public class Demo11JVMParams {
    public static void main(String[] args) {

        // 将Xms、Xmx社会为10m、同时省去50M的数组，制造OOM
        // VM参数： -Xms1024m -Xmx1024m -XX:+PrintGCDetails
        // Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
        // 该种方式一次性省去，看不到GC过程、不推荐使用该方式
        // byte[] cache = new byte[500*1024*1024];

        // 不断创建字符串
        String s = "top.tobing.oom";
        while (true) {
            // 指数增长创建字符串、使得Heap快速被填满
            s += s + UUID.randomUUID().toString().substring(0, 10) + UUID.randomUUID().toString().substring(0, 20);
        }

        // 普通GC日志
        // [GC (Allocation Failure) [PSYoungGen: 2528K->495K(2560K)] 2840K->1051K(9728K), 0.0011574 secs] [Times: user=0.00 sys=0.00, real=0.00 secs]
        // GC发生（由于堆空间分配失败）[新生代：回收前占用->回收后占用（总内存2560K]堆空间回收前->堆空间回收后（堆空间总大小）

        // FullGC日志
        // [Full GC (Ergonomics) [PSYoungGen: 64K->0K(2048K)]
        // [ParOldGen: 6458K->4968K(7168K)] 6522K->4968K(9216K),
        // [Metaspace: 3779K->3779K(1056768K)], 0.0052344 secs]
        // [Times: user=0.00 sys=0.00, real=0.01 secs]

        // FUll GC发生 [新生代:回收前->回收后（总大小）][老年代:回收前->回收后（总大小）] 堆空间：回收前->回收后（总大小）
        // 元空间：回收前->回收后（总大小）

    }
}
