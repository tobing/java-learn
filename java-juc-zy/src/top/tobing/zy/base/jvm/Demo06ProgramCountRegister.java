package top.tobing.zy.base.jvm;

/**
 * @Author tobing
 * @Date 2020/11/14 16:53
 * @Description PC寄存器
 */
public class Demo06ProgramCountRegister {
    /**
     * 【每个线程】都有一个程序计数器，线程私有，是一个指针。
     * 指向方法区中的方法字节码，有执行引擎读取下一条指令，是一个非常小的内存空间，几乎可以忽略不计。
     * 这块内存区域很小、是当前线程所执行的字节码文件的行号指示器，自己买解析器。
     * 通过执行native方法，则计数器是空的。
     * 完成：分支、循环、异常处理、线程恢复等功能。
     * 不会发生OOM
     */
}
