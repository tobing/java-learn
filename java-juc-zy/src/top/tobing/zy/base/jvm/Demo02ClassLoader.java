package top.tobing.zy.base.jvm;


/**
 * @Author tobing
 * @Date 2020/11/14 16:36
 * @Description
 */
public class Demo02ClassLoader {
    public static void main(String[] args) {

        // Bootstrap 类加载器
        // 由于底层是C++，因此输出null
        Object object = new Object();
        System.out.println(object.getClass().getClassLoader());



        // App 类加载器
        MyObject myObject = new MyObject();
        System.out.println(myObject.getClass().getClassLoader());

        // Extension 类装载器
        System.out.println(myObject.getClass().getClassLoader().getParent());
        System.out.println(myObject.getClass().getClassLoader().getParent().getParent());
    }
}

class MyObject {

}
