package top.tobing.zy.base.jvm;

/**
 * @Author tobing
 * @Date 2020/11/14 16:41
 * @Description 双亲委派机制实现沙箱安全
 * 【双亲委派】
 * 当一个类收到类的加载请求，不会产生自己加载，而是委派给父类完成。
 * 父类也是如此，先委派父类的父类，当父类反馈自己无法加载，最终
 * 才会让子类尝试加载。
 * 【例如】
 * 自己创建java.lang.String运行时会抛出以下异常。
 *
 * 错误: 在类 java.lang.String 中找不到 main 方法, 请将 main 方法定义为:
 *    public static void main(String[] args)
 * 否则 JavaFX 应用程序类必须扩展javafx.application.Application
 */
public class Demo03ParentsDelegate {
}


