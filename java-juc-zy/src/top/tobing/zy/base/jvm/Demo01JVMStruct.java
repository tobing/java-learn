package top.tobing.zy.base.jvm;

/**
 * @Author tobing
 * @Date 2020/11/14 16:27
 * @Description JVM结构
 */
public class Demo01JVMStruct {
    /**总概
     * xxx.class--> ClassLoader
     * -------------------------
     * 方法区  | Java栈 本地方法栈
     * 堆     | 程序计数器
     * -------------------------
     * 执行器   本地方法接口   <-----本地方法库
     */

    /**
     * ClassLoader
     * 类装载器，负责加载class。
     * 将class字节码内容加载到内存，并将这些内容转换成方法区中的运行时数据结构。
     * 类装载器只负责class文件袋加载，至于是否可以运行有Execution Engine决定。
     * （class文件在文件开头有特定的文件标识）
     *
     * 虚拟机自带的类装载器主要有3种：
     * 启动类加载器：Bootstrap、C++、java.xxx.xxx
     * 扩展类加载器：Extension、Java、javax.xxx.xxx
     * 应用程序类加载器：org.xxx
     *
     */
}
