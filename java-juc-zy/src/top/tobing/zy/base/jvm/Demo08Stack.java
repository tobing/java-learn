package top.tobing.zy.base.jvm;

/**
 * @Author tobing
 * @Date 2020/11/14 16:58
 * @Description 栈
 */
public class Demo08Stack {
    /**
     * 栈管运行、堆管存储
     * 不会被GC、线程私有
     * 8基本数据、对象引用变量、示例方法等在栈内存分配
     * 栈主要保存：
     * 本地变量：输入输出参数、方法内变量
     * 栈操作：入栈出栈操作
     * 栈帧数据：类文件、方法
     *
     */
}
