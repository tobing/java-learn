package top.tobing.zy.base.jvm;

/**
 * @Author tobing
 * @Date 2020/11/14 16:57
 * @Description 方法区
 */
public class Demo07MethodArea {
    /**
     * 各线程共享运行时内存区域。
     * 【存储了每个类的结构信息】
     * 常量池、字段、方法数据、构造函数、普通方法的字节码内容。
     */
}
