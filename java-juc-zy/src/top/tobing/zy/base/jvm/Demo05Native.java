package top.tobing.zy.base.jvm;

/**
 * @Author tobing
 * @Date 2020/11/14 16:47
 * @Description 本地接口和本地方法栈
 */
public class Demo05Native {
    /**
     * 【本地接口】
     * 本地接口作用是为了融合不同编程语言为Java使用。
     * 初衷是融合C、C++程序，Java诞生是C和C++横行，先要顺利活下去必须要调用C的代码，
     * 因此在内存中专门创建了一块区域，处理标记为Native的方法。
     * 在Native Method Stack登记的native方法在Execution Engine执行时加载Native Libraries
     *
     * 目前该方式越来越少，除非是硬件相关。
     * 现在常用Socket、Web  Service等代替
     */

    /**
     * Native Method Stack
     * native方法登记在这里
     */
}
