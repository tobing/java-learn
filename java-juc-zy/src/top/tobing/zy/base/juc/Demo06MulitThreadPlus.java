package top.tobing.zy.base.juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author tobing
 * @Date 2020/11/9 11:24
 * @Description 多生产者、多消费模式
 * 【步骤】
 * 1. 高内聚、低耦合前提下、线程操纵资源类
 * 2. 判断、干活、通知
 * 3. 防止虚假唤醒、while
 * 4. 新版Lock、Condition
 * <p>
 * 【需求】
 * 1. 线程A执行10次、B执行20次、C执行30次
 * 2. 线程A执行10次、B执行20次、C执行30次
 * 3. 重复以上步骤10次
 */
public class Demo06MulitThreadPlus {
    public static void main(String[] args) {
        Printer printer = new Printer();
        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    printer.print10();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "AAA").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    printer.print20();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "BBB").start();

        new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                try {
                    printer.print30();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "CCC").start();
    }
}

class Printer {

    /**
     * 标志位
     * 0-A
     * 1-B
     * 2-C
     */
    private int flag = 0;

    /**
     * 可重入锁
     */
    private Lock lock = new ReentrantLock();
    /**
     * 钥匙 1 - 3
     * 1 控制 A
     * 2 控制 B
     * 3 控制 C
     */
    private Condition c1 = lock.newCondition();
    private Condition c2 = lock.newCondition();
    private Condition c3 = lock.newCondition();

    /**
     * c1 -> c2 -> c3 -> c1
     */

    public void print10() throws InterruptedException {
        lock.lock();
        try {
            // 判断
            while (flag != 0) {
                c1.await();
            }
            // 干活
            for (int i = 0; i < 10; i++) {
                System.out.println(Thread.currentThread().getName() + " : " + i);
            }
            // 更新状态位、通知
            flag = 1;
            c2.signal();
        } finally {
            lock.unlock();
        }
    }

    public void print20() throws InterruptedException {
        lock.lock();
        try {
            // 判断
            while (flag != 1) {
                c2.await();
            }
            // 干活
            for (int i = 0; i < 20; i++) {
                System.out.println(Thread.currentThread().getName() + " : " + i);
            }
            // 更新状态位、通知
            flag = 2;
            c3.signal();
        } finally {
            lock.unlock();
        }
    }

    public void print30() throws InterruptedException {
        lock.lock();
        try {
            // 判断
            while (flag != 2) {
                c3.await();
            }
            // 干活
            for (int i = 0; i < 30; i++) {
                System.out.println(Thread.currentThread().getName() + " : " + i);
            }
            // 更新状态位、通知
            flag = 0;
            c1.signal();
        } finally {
            lock.unlock();
        }
    }
}
