package top.tobing.zy.base.juc;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @Author tobing
 * @Date 2020/11/7 20:27
 * @Description 并发操纵集合类
 * 1、故障现象
 *      多线程并发读写常规集合类时，会出现结果与预期不符、而且每次结果不可控。
 *      java.util.ConcurrentModificationException
 * 2、导致原因
 *      多线程并发操纵共享资源时，由于并发具有随机性，如果不对操纵进行控制，
 *      可能会出现各种与预期不符合的现象。
 * 3、解决方法
 *      1）使用线程安全集合，如：Vector
 *      2）使用Collections提供的synchronizedXXX方法
 *      3）使用java.util.concurrent下的并发集合
 * 4、优化建议
 *
 *
 */
public class Demo03Concurrent {
    public static void main(String[] args) {

        // 1. 线程安全集合
        // List<String> list = new Vector<>();
        // 2. 使用Collections提供的方法
        // List<String> list = Collections.synchronizedList(new ArrayList<>());
        // 3. 使用juc包下的并发集合
        List<String> list = new CopyOnWriteArrayList<>();

        /**    写时复制源码分析：
         *     写时复制，顾名思义就是在write操作的时候拷贝一份出来写。
         *     下面通过注释源码的方式来解说如何通过实现写时复制。
         *     正是通过写时复制的方式，我们可以实现多线程进行读操作，
         *     由于Lock比synchronized定义锁轻量，可以提供性能。
         *
         *     public boolean add(E e) {
         *         （1）创建一把轻量级可重入锁
         *         final ReentrantLock lock = this.lock;
         *         （2）将当前操作加锁
         *         lock.lock();
         *         try {
         *             （3）保存list中也有的数据
         *             Object[] elements = getArray();
         *             int len = elements.length;
         *             （4）复制list的数据，并在原有的基础上进行扩容（size+1）
         *             Object[] newElements = Arrays.copyOf(elements, len + 1);
         *             newElements[len] = e;
         *             （5）将新数据覆盖原有数据
         *             setArray(newElements);
         *             return true;
         *         } finally {
         *             lock.unlock();
         *         }
         *     }
         */


        // 多个线程并发争抢资源
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                list.add(UUID.randomUUID().toString().substring(0, 9));
                System.out.println(list);
            }, String.valueOf(i)).start();
        }
    }
}
