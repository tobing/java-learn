package top.tobing.zy.base.juc;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @Author tobing
 * @Date 2020/11/9 11:42
 * @Description Callable
 * 有返回值的任务Callable
 */
public class Demo07Callable {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<Integer> task = new FutureTask<Integer>(new MyThread());

        FutureTask<String> stringFutureTask = new FutureTask<>(() -> {
            System.out.println("正在执行任务。。。。。。");
            return "tobing";
        });

        new Thread(task, "AAA").start();
        new Thread(stringFutureTask, "BBB").start();
        // 拿到返回值
        Integer integer = task.get();
        System.out.println(integer);
        String res = stringFutureTask.get();
        System.out.println(res);
    }
}


class MyThread implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        System.out.println("Exec........");
        return 1024;
    }
}


