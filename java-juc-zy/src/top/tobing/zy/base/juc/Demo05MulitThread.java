package top.tobing.zy.base.juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author tobing
 * @Date 2020/11/9 10:57
 * @Description 多个线程交替执行任务（通信）
 * 2个线程操作同一个变量。
 * A线程对该变量加1
 * B线程对该变量减1
 * A、B两个线程要保持交替运行，是的变量始终保持0、1。
 * <p>
 * 1. 高内聚、低耦合的前提下，线程操作资源类。
 * 2. 判断、干活、通知。
 * 3. 防止虚假唤醒。(while)
 * 4. 新版干活 Lock、Condition
 */
public class Demo05MulitThread {
    public static void main(String[] args) {
        Airconditioner airconditioner = new Airconditioner();
        new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                try {
                    airconditioner.increment();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "A").start();

        new Thread(() -> {
            for (int i = 0; i < 100; i++) {
                try {
                    airconditioner.decrement();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, "B").start();
    }
}

/**
 * 资源类
 */
class Airconditioner {
    /**
     * 共享变量
     */
    private int temp = 0;
    /**
     * 并发操作资源的时候需要加锁
     */
    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    /**
     * 加1
     *
     * @throws InterruptedException
     */
    public void increment() throws InterruptedException {
        lock.lock();
        try {
            // 判断
            while (temp != 0) {
                condition.await();
            }
            // 干活
            temp++;
            System.out.println(Thread.currentThread().getName() + " ： " + temp);
            // 通知
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }

    /**
     * 减1
     *
     * @throws InterruptedException
     */
    public void decrement() throws InterruptedException {
        lock.lock();
        try {
            // 判断
            while (temp != 1) {
                condition.await();
            }
            // 干活
            temp--;
            System.out.println(Thread.currentThread().getName() + " ： " + temp);
            // 通知
            condition.signalAll();
        } finally {
            lock.unlock();
        }

    }
}
