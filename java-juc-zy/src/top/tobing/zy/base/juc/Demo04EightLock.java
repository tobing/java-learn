package top.tobing.zy.base.juc;

import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2020/11/8 18:33
 * @Description 八种锁演示
 * 1. 同一对象，正常访问
 * 先Email、再SMS。
 * 由于是同一个对象，synchronized的拿到的锁是同一把，
 * 由于锁被Email持有，因此SMS并不能拿到锁
 * 2. 同一对象，在其中一个同步方法中睡眠
 * 先Email、再SMS。
 * 由于是同一个对象，synchronized的拿到的锁是同一把，
 * 由于锁被Email持有，因此SMS并不能拿到锁
 * 3. 同一对象，新增普通方法
 * 先SMS、再Email
 * 一个加锁、一个不加锁、不加锁的方法在被调度时不会被阻塞。
 * 4. 不同对象，同步方法加锁
 * 先SMS、在Email
 * 不同锁，因此不能影响
 * 5. 不同对象，两静态方法
 * 先Email、再SMS
 * 相同锁
 * 6. 相同对象，两静态方法
 * 先Email、再SMS
 * 相同锁
 * 7. 不同对象，一静态、一普通
 * 先SMS、再Email
 * 一个加锁、一个不加锁、不加锁的方法在被调度时不会被阻塞。
 * 8. 相同对象，一静态、一普通
 * 先SMS、再Email
 * 一个加锁、一个不加锁、不加锁的方法在被调度时不会被阻塞。
 */
public class Demo04EightLock {
    public static void main(String[] args) throws Exception {
        Phone phone1 = new Phone();
        Phone phone2 = new Phone();

        new Thread(() -> {
            phone1.sendEmail();
        }).start();

        // 睡眠100ms
        TimeUnit.MILLISECONDS.sleep(100);

        new Thread(() -> {
            phone2.send();
        }).start();
    }
}


/**
 * 资源类
 */
class Phone {

    /**
     * 同步方法
     */
    public static synchronized void sendEmail() {
        try {
            // 睡眠1s
            TimeUnit.SECONDS.sleep(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Send Email.......");
    }

    /**
     * 同步方法
     */
    public static synchronized void sendSMS() {
        System.out.println("Send MSM........");
    }

    /**
     * 普通方法
     */
    public void send() {
        System.out.println("Send.........");
    }
}
