package top.tobing.zy.base.juc;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author tobing
 * @Date 2020/11/7 18:05
 * @Description zy的juc学习源码
 * 实现企业级多线程买票程序
 * 步骤：
 * 1）高内聚低耦合的前提下，【线程】操纵【资源类】
 * 2）创建资源类
 * 3）在访问共享变量的方法中加锁
 * 4）多线程调用方法
 */
public class Demo01TicketBuy {
    public static void main(String[] args) {
        Ticket ticket = new Ticket();
        new Thread(() -> {
            for (int i = 0; i < 50; i++) ticket.saleTickets();
        }, "tobing").start();
        new Thread(() -> {
            for (int i = 0; i < 50; i++) ticket.saleTickets();
        }, "zenyet").start();
        new Thread(() -> {
            for (int i = 0; i < 50; i++) ticket.saleTickets();
        }, "rongon").start();
    }
}

class Ticket {
    private int tickets = 50;

    Lock lock = new ReentrantLock();

    public void saleTickets() {
        // 加锁
        lock.lock();
        try {
            if (tickets > 0) {
                System.out.println(Thread.currentThread().getName()
                        + "销售出了第" + (50 - tickets) + "张票，还剩下" + tickets + "张票。");
                tickets--;
            }
        } finally {
            // 释放锁
            lock.unlock();
        }
    }
}
