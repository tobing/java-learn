package top.tobing.zy.base.juc;

/**
 * @Author tobing
 * @Date 2020/11/7 19:35
 * @Description Lambda表达式
 * 1）定义函数式接口
 * 2）new接口
 */
public class Demo02Lambda {
    public static void main(String[] args) {
        Foo foo = (int a, int b) ->{
            System.out.println("return a + b;");
            return a + b;
        };
        int add = foo.add(10, 20);
        int div = Foo.div(20, 10);
        int mulit = foo.multi(20, 10);
        System.out.println(add);
        System.out.println(div);
        System.out.println(mulit);
    }
}

/**
 * 函数式接口：
 * 1、FunctionInterface
 * 2、default
 * 3、static
 */
@FunctionalInterface
interface Foo {
    public int add(int a, int b);

    public default int multi(int a, int b) {
        return a * b;
    }

    public static int div(int a, int b) {
        return a / b;
    }
}
