package top.tobing.zy.juc;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.LockSupport;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author tobing
 * @Date 2021/1/18 16:13
 * @Description 等待唤醒机制
 */
public class Demo02WaitNotifyMech {
    public static void main(String[] args) {
        objectWaitNotify();
    }

    /**
     * 传统唤醒机制：Object
     */
    public static void objectWaitNotify() {
        // A:等待
        new Thread(() -> {
//            synchronized (Demo02WaitNotifyMech.class) {
            try {
                System.out.println("Wait");
                Demo02WaitNotifyMech.class.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Get Lock.");
//            }
        }, "A").start();

        // B:唤醒
        new Thread(() -> {
            synchronized (Demo02WaitNotifyMech.class) {
                Demo02WaitNotifyMech.class.notify();
                System.out.println("Notify");
            }
        }, "B").start();
    }

    static ReentrantLock lock = new ReentrantLock();
    static Condition condition = lock.newCondition();

    /**
     * 传统唤醒机制：ReentrantLock
     */
    public static void reentrantLockUnLock() {
        // A:阻塞等待
        new Thread(() -> {
            lock.lock();
            try {
                System.out.println("Await");
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
            System.out.println("Unlock");
        }, "A").start();
        // B:唤醒
        new Thread(() -> {
            lock.lock();
            try {
                condition.signal();
                System.out.println("Signal");
            } finally {
                lock.unlock();
            }
        }, "B").start();
    }

    public static void lockSupportParkUnpark() {
        // A:Park
        Thread threadA = new Thread(() -> {
            // 阻塞等待
            System.out.println("Park");
            LockSupport.park();
            System.out.println("Unpark");
        }, "A");
        threadA.start();

        // B:Unpark
        Thread threadB = new Thread(() -> {
            // 唤醒
            LockSupport.unpark(threadA);
            System.out.println("Unpark Now");
        }, "B");
        threadB.start();
    }
}
