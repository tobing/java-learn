package top.tobing.zy.juc;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author tobing
 * @Date 2021/1/18 14:24
 * @Description 可重入锁演示
 */
public class Demo01ReentrantLock {
    static ReentrantLock lock = new ReentrantLock();

    public static void main(String[] args) {
        // synchronized代码块
        new Thread(() -> {
            synchronized (Demo01ReentrantLock.class) {
                System.out.println("======================第一次获取锁==================");
                synchronized (Demo01ReentrantLock.class) {
                    System.out.println("======================第二次获取锁==================");
                    synchronized (Demo01ReentrantLock.class) {
                        System.out.println("======================第三次获取锁==================");
                    }
                }
            }
        }, "Thread-1").start();
        // synchronized方法
        new Thread(() -> {
            m1();
        }, "Thread-2").start();
        // ReentrantLock
        new Thread(() -> {
            lock.lock();
            try {
                System.out.println("ReentrantLock：第一次获得锁");
                lock.lock();
                try {
                    System.out.println("ReentrantLock：第二次获取锁");
                } finally {
                    lock.unlock();
                }
            } finally {
                lock.unlock();
            }
        }, "Thread-name").start();

    }

    public static synchronized void m1() {
        System.out.println("======================第一次获取锁==================");
        m2();
    }

    public static synchronized void m2() {
        System.out.println("======================第二次获取锁==================");
        m3();
    }

    public static synchronized void m3() {
        System.out.println("======================第三次获取锁==================");
    }


}
