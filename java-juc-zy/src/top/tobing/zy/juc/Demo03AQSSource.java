package top.tobing.zy.juc;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author tobing
 * @Date 2021/1/18 22:09
 * @Description AQS源码解读：以ReentrantLock为例
 */
public class Demo03AQSSource {
    static ReentrantLock lock = new ReentrantLock();


    public static void main(String[] args) {
        // 线程A
        new Thread(() -> {
            lock.lock();
            try {
                System.out.println("A in.");
            } finally {
                lock.unlock();
            }
        }, "Thread-A").start();
        // 线程B
        new Thread(() -> {
            lock.lock();
            try {
                System.out.println("B in.");
            } finally {
                lock.unlock();
            }
        }, "Thread-B").start();
        // 线程C
        new Thread(() -> {
            lock.lock();
            try {
                System.out.println("C in.");
            } finally {
                lock.unlock();
            }
        }, "Thread-C").start();
    }
}
