package top.tobing.zy.interview.others;

/**
 * @Author tobing
 * @Date 2020/11/17 21:36
 * @Description
 */
public class Demo01String {
    public static void main(String[] args) {
        String str1 = new StringBuilder("ali").append("baba").toString();
        System.out.println(str1);
        System.out.println(str1.intern());
        // 下面输出true
        System.out.println(str1 == str1.intern());
        System.out.println();

        String str2 = new StringBuilder("ja").append("va").toString();
        System.out.println(str2);
        System.out.println(str2.intern());
        // 下面输出false
        System.out.println(str2 == str2.intern());


        /**
         * String intern是一个本地方法，作用是如果字符串常量池中已经包含一个等于此String对象的字符串，则返回代表池中字符串对象的引用
         * 否则会将String对象包含的字符串添加到常量池中，并且返回String对象的引用。
         *
         * 由于str1在main方法中创建执行的时候，常量池中并没有alibaba这个字符串，因此会创建该字符串。
         * str1调用intern方法的时候，将该对象的引用添加的字符串常量中，并返回该引用。
         *
         * 而str2在main方法中创建执行时，由于System初始化时会调用sun.misc.Version.init();进行初始化
         * 而Version在包含了private static final String launcher_name = "java";
         * 此时会将java加载到字符串常量池中。
         * 所以在str2.intern的时候返回Version初始化时产生的字符串常量。
         * 而str2是new出来，势必与str2.intern引用对不上
         */
    }
}
