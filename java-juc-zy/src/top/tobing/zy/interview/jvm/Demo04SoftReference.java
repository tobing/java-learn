package top.tobing.zy.interview.jvm;

import java.lang.ref.SoftReference;

/**
 * @Author tobing
 * @Date 2020/11/15 17:46
 * @Description 软引用
 */
public class Demo04SoftReference {
    public static void main(String[] args) {
        Object o1 = new Object();
        SoftReference<Object> softReference = new SoftReference<>(o1);
        System.out.println(o1);
        System.out.println(softReference.get());

        System.out.println("======================");
        o1 = null;
        System.out.println(o1);
        System.out.println(softReference.get());

        // -Xms10m -Xmx10m -XX:+PrintGCDetails
        // 刻意制造Heap不足，触发GC
        System.out.println("======================");
        try {
            byte[] temp = new byte[50 * 1024 * 1024];
        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            System.out.println(o1);
            System.out.println(softReference.get());
        }
    }
}
