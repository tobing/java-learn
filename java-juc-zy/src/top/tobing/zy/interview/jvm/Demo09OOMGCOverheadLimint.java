package top.tobing.zy.interview.jvm;

/**
 * @Author tobing
 * @Date 2020/11/15 20:49
 * @Description OutOfMemoryError:GC Overhead limit exceeded.
 * GC回收花费的时间过长。
 * 当超过98%的时间用来GC，只剩下2%时间用于运行，抛出该异常。
 * 因为如果不抛出该异常会导致CPU持续100%占用而无任何成果。
 */
public class Demo09OOMGCOverheadLimint {

}
