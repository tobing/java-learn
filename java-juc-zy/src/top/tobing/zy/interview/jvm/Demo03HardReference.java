package top.tobing.zy.interview.jvm;

/**
 * @Author tobing
 * @Date 2020/11/15 17:45
 * @Description
 */
public class Demo03HardReference {
    public static void main(String[] args) {
        Object o1 = new Object();
        Object o2 = o1;
        o1 = null;
        System.gc();
        System.out.println(o2);
    }
}
