package top.tobing.zy.interview.jvm;

import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2020/11/15 21:01
 * @Description 线程创建达到上限【Linux】
 * 【导致原因】
 * 1. 应用创建线程太多，一个应用创建的线程超过系统承载。
 * 2. 系统现在创建如此多线程。
 * 【解决办法】
 * 1. 现在应用创建线程数
 * 2. 调宽系统对线程数的限制
 */
public class Demo11UnableToCreateNewNativeThread {
    public static void main(String[] args) {
        int i = 0;
        // Exception in thread "main" java.lang.OutOfMemoryError: unable to create new native thread
        while (true) {
            System.out.println("----------->" + (++i));
            new Thread(() -> {
                try {
                    TimeUnit.SECONDS.sleep(Integer.MAX_VALUE);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }
}
