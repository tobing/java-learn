package top.tobing.zy.interview.jvm;

/**
 * @Author tobing
 * @Date 2020/11/15 20:45
 * @Description Heap Space
 */
public class Demo08OOMHeapSpace {
    public static void main(String[] args) {
        // 将堆空间调小
        // -Xms10m -Xmx10m -XX:+PrintGCDetails
        // 申请50M空间
        byte[] cache = new byte[50 * 1024 * 1024];
        // Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
    }
}
