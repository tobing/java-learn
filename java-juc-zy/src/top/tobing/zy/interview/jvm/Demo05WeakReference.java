package top.tobing.zy.interview.jvm;

import java.lang.ref.WeakReference;

/**
 * @Author tobing
 * @Date 2020/11/15 17:51
 * @Description 弱引用
 */
public class Demo05WeakReference {
    public static void main(String[] args) {
        Object o1 = new Object();
        WeakReference<Object> weakReference = new WeakReference<>(o1);
        System.out.println(o1);
        System.out.println(weakReference.get());

        System.out.println("===================");
        o1 = null;
        System.out.println(o1);
        System.out.println(weakReference.get());

        System.gc();
        System.out.println("===================");
        System.out.println(o1);
        System.out.println(weakReference.get());

    }
}
