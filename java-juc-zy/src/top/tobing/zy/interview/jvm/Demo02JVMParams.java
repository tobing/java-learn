package top.tobing.zy.interview.jvm;

import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2020/11/15 15:06
 * @Description JVM参数
 */
public class Demo02JVMParams {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Main methods!");
        TimeUnit.SECONDS.sleep(Integer.MAX_VALUE);
    }
}
