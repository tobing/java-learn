package top.tobing.zy.interview.jvm;

import sun.misc.VM;

import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2020/11/15 20:52
 * @Description OOMDirectBufferMemory
 * NIO程序检测使用ByteBuffer读取或写入数据，只是一种基于通道和缓冲区的I/O方式，
 * 可以使用Native函数库直接分配堆外内存，任何通过一个储存在Java堆内的DirectByteBuffer对象作为这块内存的引用进行操作。
 * 这样能在一些场景中显著提高性能，因为避免了Java堆中和Native堆中复制数据。
 * ByteBuffer.allocate(cap) 分配JVM堆内存、属于GC管辖范围
 * ByteBuffer.allocateDirect(cap) 分配OS本地内存，不属于GC管辖范围，不需要内存间拷贝速度快。
 */
public class Demo10OOMDirectBufferMemory {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("配置的DirectMemory：" + (VM.maxDirectMemory() / 1024.0 / 1024));
        TimeUnit.SECONDS.sleep(3);
        // -XX:MaxDirectMemorySize=5m
        ByteBuffer buffer = ByteBuffer.allocateDirect(6 * 1024 * 1024);
        // Exception in thread "main" java.lang.OutOfMemoryError: Direct buffer memory
    }
}
