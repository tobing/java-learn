package top.tobing.zy.interview.jvm;

/**
 * @Author tobing
 * @Date 2020/11/15 20:43
 * @Description 栈溢出
 */
public class Demo07StackOverflow {
    public static void main(String[] args) {
        // Exception in thread "main" java.lang.StackOverflowError
        stackOverflow();
    }

    public static void stackOverflow() {
        stackOverflow();
    }
}
