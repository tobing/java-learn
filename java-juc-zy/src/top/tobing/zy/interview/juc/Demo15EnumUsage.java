package top.tobing.zy.interview.juc;

import java.util.concurrent.CountDownLatch;

/**
 * @Author tobing
 * @Date 2020/11/12 12:07
 * @Description 枚举的使用
 */
public class Demo15EnumUsage {
    public static void main(String[] args) throws InterruptedException {
        CountDownLatch downLatch = new CountDownLatch(6);
        for (int i = 0; i < 6; i++) {
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + "，灭亡!");
                downLatch.countDown();
            }, CountryEnum.forEach(i + 1).getName()).start();
        }

        // 6国灭亡、秦国统一
        downLatch.await();
        System.out.println("秦统一天下！");
    }
}
