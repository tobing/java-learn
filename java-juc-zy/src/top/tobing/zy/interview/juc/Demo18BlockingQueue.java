package top.tobing.zy.interview.juc;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * @Author tobing
 * @Date 2020/11/12 15:17
 * @Description 阻塞队列
 * 阻塞队列是一个队列，在该队列中：
 * 如果队列为空，则从队列中【取出】数据将会被阻塞。
 * 如果队列为慢，则从队列中【添加】数据将会被阻塞。
 * <p>
 * 使用阻塞队列我们不需要关系如何对线程状态进行控制。由Java帮我们实现。
 */
public class Demo18BlockingQueue {
    public static void main(String[] args) throws InterruptedException {
        // addAndRemove();
        // offerAndPoll();
        // putAndTake();
    }

    private static void putAndTake() throws InterruptedException {
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);
        for (int i = 0; i < 3; i++) {
            blockingQueue.put(String.valueOf(i));
        }
        // 满添加-阻塞
        // blockingQueue.put("5");

        for (int i = 0; i < 3; i++) {
            System.out.println(blockingQueue.take());
        }
        // 删除空-阻塞
        System.out.println(blockingQueue.take());
    }

    private static void offerAndPoll() {
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);
        for (int i = 0; i < 3; i++) {
            System.out.println(blockingQueue.offer(String.valueOf(i)));
        }
        // 慢添加-返回false
        System.out.println(blockingQueue.offer("5"));

        for (int i = 0; i < 3; i++) {
            System.out.println(blockingQueue.poll());
        }
        // 删除空-返回null
        System.out.println(blockingQueue.poll());
    }

    private static void addAndRemove() {
        // 创建容量为3的阻塞队列
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);
        for (int i = 0; i < 3; i++) {
            System.out.println(blockingQueue.add(String.valueOf(i)));
        }
        // 超过容量，抛出异常
        // java.lang.IllegalStateException Queue full
        // System.out.println(blockingQueue.add("5"));

        for (int i = 0; i < 3; i++) {
            System.out.println(blockingQueue.remove());
        }
        // 没有元素时，删除会抛出异常
        // java.util.NoSuchElementException
        // System.out.println(blockingQueue.remove());
    }


}
