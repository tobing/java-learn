package top.tobing.zy.interview.juc;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author tobing
 * @Date 2020/11/10 15:13
 * @Description CAS简单使用
 */
public class Demo05CAS01 {
    public static void main(String[] args) {
        AtomicInteger atomicInteger = new AtomicInteger();
        System.out.println(atomicInteger.compareAndSet(0, 2020) + "\t current:" + atomicInteger);
        System.out.println(atomicInteger.compareAndSet(20, 2019) + "\t current:" + atomicInteger);
    }
}
