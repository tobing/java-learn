package top.tobing.zy.interview.juc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @Author tobing
 * @Date 2020/11/10 21:47
 * @Description 不安全集合
 * ArrayList
 * 方法论：
 * 1. 故障现象   java.util.ConcurrentModificationException
 * 2. 导致原因   ArrayList不是线程安全的集合，多线程并发修改操纵ArrayList会产生线程不安全问题
 * 3. 解决方法
 * （1）Vector 最呆瓜的做法
 * （2）Collections.synchronizedAsList()
 * （3）CopyOnWriteArrayList提到ArrayList
 * 4. 优化建议
 */
public class Demo08UnsafeList {
    public static void main(String[] args) {
        // 线程操纵资源类
        // List<Integer> list = new ArrayList<>();
        // List<Integer> list = new Vector<>();
        // List<Integer> list = Collections.synchronizedList(new ArrayList<>());
        List<Integer> list = new CopyOnWriteArrayList<>();
        for (int i = 0; i < 30; i++) {
            new Thread(() -> {
                for (int j = 0; j < 5; j++) {
                    list.add(1);
                    System.out.println(list);
                }
            }).start();
        }
    }
}
