package top.tobing.zy.interview.juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author tobing
 * @Date 2020/11/11 16:09
 * @Description 可重入锁
 * 可重入锁（递归锁）
 * 指的是同一个线程在外层函数获得锁之后，内存递归函数仍能自动获得该锁。
 * 在同一个线程在外层方法获取锁的时候，在进入内存方法自动获取锁
 * <p>
 * 也就是说，线程可以进入任何一个他【已经拥有锁所同步着的代码块】。
 * ReentrantLock是一把典型的可重入锁
 */
public class Demo11ReentrantLock {
    public static void main(String[] args) {
        Phone phone = new Phone();
        new Thread(() -> {
            phone.sendMSM();
        }, "T1").start();
        new Thread(() -> {
            phone.sendMSM();
        }, "T2").start();

        // 睡眠1s
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();

        new Thread(() -> {
            phone.go();
        }, "T3").start();

        new Thread(() -> {
            phone.go();
        }, "T4").start();

        new Thread(() -> {
            phone.doubleLock();
        }, "T5").start();
    }
}

class Phone {

    /**
     * 可重入锁默认就是一把非公平锁。
     * 可重入锁可以在构造方法中传递一个boolean变量指定是否是公平锁
     * 公平锁：先到先得
     * 非公平锁：优先级高者先得，不同线程优先级可以变化
     */
    private Lock lock = new ReentrantLock();

    public synchronized void sendMSM() {
        System.out.println(Thread.currentThread().getName() + " : sendMSM....");
        sendEmail();
    }

    public synchronized void sendEmail() {
        System.out.println(Thread.currentThread().getName() + " : sendEmail....++++++");

    }

    public void go() {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + " : gogogogogo....");
            look();
        } finally {
            lock.unlock();
        }
    }

    public void look() {
        lock.lock();
        try {
            System.out.println(Thread.currentThread().getName() + " : loooooooooook......");
        } finally {
            lock.unlock();
        }
    }

    /**
     * 加锁N次就要对应解锁N次
     */
    public void doubleLock() {
        // 加锁2次
        lock.lock();
        lock.lock();
        try {
            System.out.println("Double Lock.");
        } finally {
            // 解锁2次
            // 如果只解锁一次，程序便会阻塞于此
            lock.unlock();
            lock.unlock();
        }
    }
}
