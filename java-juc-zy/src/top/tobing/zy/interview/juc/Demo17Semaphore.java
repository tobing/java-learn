package top.tobing.zy.interview.juc;

import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2020/11/12 12:44
 * @Description 信号量
 * 1. 用于多个共享资源的互斥访问
 * 2. 控制并发资源数
 */
public class Demo17Semaphore {
    public static void main(String[] args) {
        getCarSpace();
    }

    private static void getCarSpace() {
        // 最高并发数一共只有6个
        Semaphore semaphore = new Semaphore(6);

        // 6个线程并发争抢资源
        for (int i = 0; i < 12; i++) {
            new Thread(() -> {
                // 拿到共享资源-》车位
                try {
                    semaphore.acquire();
                    System.out.println(Thread.currentThread().getName() + "停车了");
                    // 停车1s
                    try {
                        TimeUnit.SECONDS.sleep(3);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(Thread.currentThread().getName() + "停留了1s，离开了");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    // 释放共享资源
                    semaphore.release();
                }
            }, String.valueOf(i)).start();
        }
    }
}
