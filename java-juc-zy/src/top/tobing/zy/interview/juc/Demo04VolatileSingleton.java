package top.tobing.zy.interview.juc;

/**
 * @Author tobing
 * @Date 2020/11/10 12:18
 * @Description 多线程下实现单例模式
 */
public class Demo04VolatileSingleton {
    public static void main(String[] args) {
        // 单线程版
//        for (int i = 0; i < 10; i++) {
//            SingletonDemo01 singletonDemo01 = SingletonDemo01.getInstance();
//        }
        // 多线程版
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
//                SingletonDemo01 instance = SingletonDemo01.getInstance();
                SingletonDemo02 instance = SingletonDemo02.getInstance();
            }).start();
        }

    }
}

/**
 * 单线程单例设计模式
 */
class SingletonDemo01 {

    private static SingletonDemo01 instance;

    private SingletonDemo01() {
        System.out.println(Thread.currentThread().getName() + " create the SingletonDemo01.");
    }

    public static SingletonDemo01 getInstance() {
        if (instance == null) {
            instance = new SingletonDemo01();
        }
        return instance;
    }
}

/**
 * DCL(Double Check Lock)
 * 双重校验锁机制实现单例设计模式
 * 尽管使用了双重校验锁机制，但由于存在指令重排
 * new SingletonDemo02创建的步骤可能会发生顺序问题，
 * 因此需要使用volatile关键字对SingletonDemo02就行修饰
 * 使得创建SingletonDemo02的过程不能被拆分。
 */
class SingletonDemo02 {
    private static SingletonDemo02 instance;

    private SingletonDemo02() {
        System.out.println(Thread.currentThread().getName() + " create the SingletonDemo02.");
    }

    public static SingletonDemo02 getInstance() {
        if (instance == null) {
            synchronized (SingletonDemo02.class) {
                if (instance == null) {
                    instance = new SingletonDemo02();
                }
            }
        }
        return instance;
    }
}

/**
 *
 */
class SingletonDemo03 {
    private static volatile SingletonDemo03 instance;

    private SingletonDemo03() {
        System.out.println(Thread.currentThread().getName() + " create the SingletonDemo02.");
    }

    public static SingletonDemo03 getInstance() {
        if (instance == null) {
            synchronized (SingletonDemo02.class) {
                if (instance == null) {
                    instance = new SingletonDemo03();
                }
            }
        }
        return instance;
    }
}
