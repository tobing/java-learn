package top.tobing.zy.interview.juc;

import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2020/11/13 22:52
 * @Description 线程死锁现象演示
 */
public class Demo27ThreadDeadLock {
    public static void main(String[] args) {
        new Thread(new MyDeadLock("AAA","BBB")).start();
        new Thread(new MyDeadLock("BBB","AAA")).start();
    }
}

class MyDeadLock implements Runnable {

    private String lockA;
    private String lockB;

    public MyDeadLock(String lockA, String lockB) {
        this.lockA = lockA;
        this.lockB = lockB;
    }

    @Override
    public void run() {
        synchronized (lockA) {
            System.out.println(Thread.currentThread() + "拿到了锁" + lockA + "尝试去申请" + lockB);
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            synchronized (lockB) {
                System.out.println(Thread.currentThread() + "拿到了锁" + lockB + "尝试去申请" + lockA);
            }
        }
    }
}
