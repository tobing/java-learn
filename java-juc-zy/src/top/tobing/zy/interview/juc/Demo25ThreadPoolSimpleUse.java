package top.tobing.zy.interview.juc;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author tobing
 * @Date 2020/11/13 21:50
 * @Description 线程池API的简单使用
 * 3种常用的创建线程的方式
 */
public class Demo25ThreadPoolSimpleUse {
    public static void main(String[] args) {
        // 通过指定固定线程数的方式来创建线程池
        // 如果线程超过线程池的数量、将会进入到LinkedBlockingQueue阻塞队列中等待
        ExecutorService threadPool1 = Executors.newFixedThreadPool(5);

        // 创建容量为1的线程
        // 但是由于使用了LinkedBlockQueue，因此理论上可以有21亿个等待线程
        // 因此在实际使用中通常使用该方式，可能会导致OOM
        ExecutorService threadPool2 = Executors.newSingleThreadExecutor();

        // 弹性创建线程的方式
        // 等待的队列会被存储在SynchronousQueue中【该阻塞队列只会存储1个线程】
        ExecutorService threadPool3 = Executors.newCachedThreadPool();

        for (int i = 0; i < 30; i++) {
            threadPool3.execute(() -> {
                System.out.println(Thread.currentThread().getName() + "\t is Running........");
            });
        }
    }
}
