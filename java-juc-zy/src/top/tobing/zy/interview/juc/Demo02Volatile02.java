package top.tobing.zy.interview.juc;

/**
 * @Author tobing
 * @Date 2020/11/9 13:28
 * @Description volatile不保证原子性
 * 1. 线程操纵资源类
 * 2.
 */
public class Demo02Volatile02 {
    public static void main(String[] args) {
        // 资源类
        MyData01 myData01 = new MyData01();
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    myData01.add();
                }
            }, String.valueOf(i)).start();
        }
        while (Thread.activeCount() > 2) {
            Thread.yield();
        }
        System.out.println(myData01.number);
    }
}


class MyData01 {
    volatile int number = 0;
    public void add() {
        number++;
    }
}