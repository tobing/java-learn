package top.tobing.zy.interview.juc;

/**
 * @Author tobing
 * @Date 2020/11/12 15:30
 * @Description BlockQueue架构
 */
public class Demo19BlockQueueStructure {
    /**
     * BlockQueue
     * ----LinkedTransferQueue      链表结构组成的无解阻塞队列
     * ----LinkedBlockingDeque      链表结构组成的双向阻塞队列
     * ----SynchronousQueue       * 不存储元素的阻塞队列，即单个元素队列
     * ----ArrayListBlockingQueue * 数组结构组成的有界阻塞队列
     * ----LinkedBlockingQueue    * 链表结构组成的有界阻塞队列（size=Integer.MAX_VALUE）
     * ----DelayQueue               优先级队列实现的延迟队列
     * ----PriorityBlockingQueue    支持优先级排序的无界阻塞队列
     */
}
