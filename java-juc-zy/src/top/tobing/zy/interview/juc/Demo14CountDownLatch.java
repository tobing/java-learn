package top.tobing.zy.interview.juc;

import java.util.concurrent.CountDownLatch;

/**
 * @Author tobing
 * @Date 2020/11/12 9:37
 * @Description CountDownLatch使用
 * CountDownLatch让一些线程阻塞到直到另外一些完成后才唤醒。
 * CountDownLatch主要有2个方法。
 * 当一个或多个线程调用await方法时，调用线程会被阻塞，其他线程调用countDownLatch方法计数器-1，
 * 当计数器值变为0是，因调用await方法被阻塞的线程会被唤醒，进行执行。
 */
public class Demo14CountDownLatch {


    public static void main(String[] args) throws InterruptedException {
        closeDoor();
    }

    /**
     * 假如教室一共有6个同学+班长。
     * 班长要等到6位同学离开之后才能够关门
     */
    private static void closeDoor() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(6);
        for (int i = 0; i < 6; i++) {
            final int temp = i;
            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + "同学走了。");
                countDownLatch.countDown();
            },String.valueOf(i)).start();
        }
        countDownLatch.await();
        System.out.println("班长关门");
    }
}
