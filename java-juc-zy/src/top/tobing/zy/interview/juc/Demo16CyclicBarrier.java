package top.tobing.zy.interview.juc;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @Author tobing
 * @Date 2020/11/12 12:37
 * @Description CyclicBarrier
 * 可循环。
 * 使用的屏障，让一组线程达到一个屏障（同步点）时被阻塞，直到最后一个线程到达屏障时，
 * 使用被拦截的线程才会干活，线程进入屏障通过CyclicBarrier的await方法
 */
public class Demo16CyclicBarrier {
    public static void main(String[] args) {
        collectDragonBall();
    }

    /**
     * 集齐7颗龙珠，召唤神龙
     */
    public static void collectDragonBall() {

        CyclicBarrier cyclicBarrier = new CyclicBarrier(7, () -> {
            System.out.println("出来吧！神龙！");
        });

        for (int i = 0; i < 7; i++) {
            final int temp = i + 1;
            new Thread(() -> {
                System.out.println("第" + temp + "颗龙珠找到。");
                try {
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }, "Thread-name").start();
        }
    }
}
