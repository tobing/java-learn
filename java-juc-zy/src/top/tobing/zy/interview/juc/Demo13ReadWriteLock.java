package top.tobing.zy.interview.juc;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @Author tobing
 * @Date 2020/11/11 20:08
 * @Description 读写锁
 * 多线程并发操纵一个资源类，为了满足并发量
 * 读取共享资源你应该可以同时进行
 * 但也有一个写共享资源应该是排他的
 * 即:
 * 读 读能够共享
 * 读 写不能共享
 * 写 写不能共享
 * 写操作 ： 原子性+独占
 */
public class Demo13ReadWriteLock {
    public static void main(String[] args) {
        MyCache myCache = new MyCache();
        for (int i = 0; i < 10; i++) {
            final int temp = i;
            new Thread(() -> {
                myCache.set(temp + "", temp + "");
            }, i + "").start();
        }

        for (int i = 0; i < 10; i++) {
            final int temp = i;
            new Thread(() -> {
                myCache.get(temp + "");
            }, i + "").start();
        }
    }
}


class MyCache {

    private volatile Map<String, Object> map = new HashMap<>();
    ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public void set(String key, Object value) {
        readWriteLock.writeLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + " : 正在写入" + key);
            // 睡眠300ms 模拟网络阻塞
            try {
                TimeUnit.MILLISECONDS.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            map.put(key, value);
            System.out.println(Thread.currentThread().getName() + " : 写入完成" + key);
        } finally {
            readWriteLock.writeLock().unlock();
        }
    }

    public void get(String key) {
        readWriteLock.readLock().lock();
        try {
            System.out.println(Thread.currentThread().getName() + " : 正在读取" + key);
            // 睡眠300ms 模拟网络阻塞
            try {
                TimeUnit.MILLISECONDS.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Object o = map.get(key);
            System.out.println(Thread.currentThread().getName() + " : 读取完成" + key);
        } finally {
            readWriteLock.readLock().unlock();
        }
    }
}
