package top.tobing.zy.interview.juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @Author tobing
 * @Date 2020/11/11 19:56
 * @Description 自旋锁
 * 产生获取锁的线程不会立即阻塞，而是【采用循环的方式尝试获取锁】
 * 这样方式的好处是减少线程上下文切换的消耗，缺点是循环会消耗CPU
 */
public class Demo12SpinLockDemo {
    public static void main(String[] args) {
        MySpinLock spinLock = new MySpinLock();

        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() +" : come in.");
            // 上锁
            spinLock.lock();
            // 睡眠2s等待T2申请锁
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " : doing.........");
            System.out.println(Thread.currentThread().getName() + " : Finish.");
        }, "T1").start();

        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() +" : come in.");
            // 上锁
            spinLock.lock();
            // 睡眠3s模拟执行
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " : doing.........");
            System.out.println(Thread.currentThread().getName() + " : Finish.");
        }, "T2").start();

    }
}

/**
 * 自定义自旋锁
 */
class MySpinLock {

    AtomicReference<Thread> atomicReference = new AtomicReference<>();

    public void lock() {
        // 拿到当前进程
        Thread cur = Thread.currentThread();
        // 如果null，则将线程
        while (!atomicReference.compareAndSet(null, cur)) {
        }
    }

    public void unlock() {
        Thread cur = Thread.currentThread();
        atomicReference.compareAndSet(cur, null);
    }

}
