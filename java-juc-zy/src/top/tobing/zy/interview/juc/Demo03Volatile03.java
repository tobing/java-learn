package top.tobing.zy.interview.juc;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author tobing
 * @Date 2020/11/9 19:54
 * @Description Atomic 结合 volatile
 * 线程操纵资源类
 */
public class Demo03Volatile03 {
    public static void main(String[] args) {
        MyData02 myData02 = new MyData02();
        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                for (int j = 0; j < 1000; j++) {
                    myData02.addNumber();
                }
            }).start();
        }

        while (Thread.activeCount() > 2) {
            Thread.yield();
        }

        System.out.println("Result : " + myData02.number);
    }
}

class MyData02 {

    /**
     * AtomicInteger初始化时默认是0
     */
    AtomicInteger number = new AtomicInteger();
    public void addNumber() {
        // 等同于i++
        number.getAndIncrement();
    }
}
