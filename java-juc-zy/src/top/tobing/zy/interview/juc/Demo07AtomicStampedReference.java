package top.tobing.zy.interview.juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicStampedReference;

/**
 * @Author tobing
 * @Date 2020/11/10 17:08
 * @Description 基于时间戳的原子引用
 */
public class Demo07AtomicStampedReference {
    /**
     * 原子引用
     */
    private static AtomicReference<Integer> atomicReference =
            new AtomicReference<>(100);
    /**
     * 基于时间戳的原子引用
     */
    private static AtomicStampedReference<Integer> stampedReference =
            new AtomicStampedReference<>(100, 1);


    public static void main(String[] args) {
        System.out.println("===================ABA问题演示==================");
        new Thread(() -> {
            atomicReference.compareAndSet(100, 101);
            atomicReference.compareAndSet(101, 100);
        }).start();

        new Thread(() -> {
            // 睡眠1s、确保上面线程已经执行了ABA操作
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean flag = atomicReference.compareAndSet(100, 6666);
            Integer res = atomicReference.get();
            System.out.println(Thread.currentThread().getName() + "执行是否成功: " + flag + "\t 执行结果为：" + res);
        }).start();

        System.out.println("===================ABA解决演示==================");
        new Thread(() -> {
            // 睡眠1s，确保下面线程拿到的是初始的版本号
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 执行ABA操作
            stampedReference.compareAndSet(100, 101,
                    stampedReference.getStamp(), stampedReference.getStamp() + 1);
            stampedReference.compareAndSet(101, 100,
                    stampedReference.getStamp(), stampedReference.getStamp() + 1);
        }).start();

        new Thread(() -> {
            int stamp = stampedReference.getStamp();
            // 睡眠1s、确保上面线程已经执行了ABA操作
            try {
                TimeUnit.SECONDS.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            boolean flag = stampedReference.compareAndSet(100, 9999,
                    stamp, stamp + 1);
            Integer res = stampedReference.getReference();
            System.out.println(Thread.currentThread().getName() + " 版本号："
                    + stampedReference.getStamp() + " 是否修改：" + flag + " 最终结果：" + res);
        }).start();
    }
}
