package top.tobing.zy.interview.juc;

import java.util.concurrent.atomic.AtomicReference;

/**
 * @Author tobing
 * @Date 2020/11/10 16:57
 * @Description
 */
public class Demo06AtomicReference {
    public static void main(String[] args) {

        User user1 = new User("tobing", 12);
        User user2 = new User("zenyet", 13);
        AtomicReference<User> userAtomicReference = new AtomicReference<>();
        userAtomicReference.set(user1);
        boolean isChange1 = userAtomicReference.compareAndSet(user1, user2);
        System.out.println(Thread.currentThread().getName() + " : " + isChange1 + " " + userAtomicReference.get());
        boolean isChange2 = userAtomicReference.compareAndSet(user1, user2);
        System.out.println(Thread.currentThread().getName() + " : " + isChange2 + " " + userAtomicReference.get());
    }
}

class User {
    private String name;
    private Integer age;

    public User(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
