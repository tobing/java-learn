package top.tobing.zy.interview.juc;

/**
 * @Author tobing
 * @Date 2020/11/13 21:38
 * @Description 线程池的简单使用
 * 线程池主要用于控制运行线程的数量，处理过程中将任务加入队列，然后在线程创建启动这些任务
 * 如果线程超过最大数量，超出数量的线程将在阻塞队列中排队等待，等其他线程执行完毕，再从队列中
 * 取出任务执行。
 * <p>
 * 线程池主要有以下优点：
 * 1. 降低资源消耗，通过重复利用自己创建的线程从而降低线程创建和销毁造成的消耗
 * 2. 提高线程的响应速度，当任务到达时，可以不用等待线程创建的时间
 * 3. 提高线程的可管理性，线程是稀缺资源，无限创建不仅会消耗资源，
 * 还会降低系统稳定性，使用吸纳吃吃可以就行统一分配、调优和调度。
 *
 * 线程池结构：
 * 线程池主要有以下的几个类：
 * Executor
 * ExecutorService
 * AbstractExecutorService、ScheduleExecutorService
 * ThreadPoolExecutor
 * ScheduleThreadPoolExecutor
 *
 * Executors
 * 线程池常用API：创建线程的3大方式
 * 1. Executors.newFixedThreadPool(int)     执行一个长期的任务，性能很好
 * 2. Executors.newSingleThreadExecutor()   一个任务一个线程执行的任务场景
 * 3. Executors.newCachedThreadPool()       执行很多短期异步的小程序或者负载较轻的服务器
 *
 */
public class Demo24ThreadPoolStart {
}
