package top.tobing.zy.interview.juc;

/**
 * @Author tobing
 * @Date 2020/11/12 12:08
 * @Description 枚举的特殊用法
 */
public enum CountryEnum {

    ONE(1, "齐国"),
    TWO(2, "楚国"),
    THREE(3, "燕国"),
    FOUR(4, "赵国"),
    FIVE(5, "魏国"),
    SIX(6, "韩国");

    private Integer code;
    private String name;

    CountryEnum(Integer code, String name) {
        this.name = name;
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static CountryEnum forEach(int index) {
        CountryEnum[] countries = CountryEnum.values();
        for (CountryEnum country : countries) {
            if (index == country.code) {
                return country;
            }
        }
        return null;
    }
}
