package top.tobing.zy.interview.juc;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author tobing
 * @Date 2020/11/12 15:45
 * @Description 生产者消费者模式传统版实现-Lock
 * 1. 高内聚低耦合的前提下，线程操作资源类
 * 2. 判断、干活、通知
 * 3. 防止虚假唤醒while
 */
public class Demo21ProdConsumerTradition {
    public static void main(String[] args) {
        ShareData shareData = new ShareData();

        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                shareData.produce();
            }, String.valueOf(i)).start();
        }

        for (int i = 0; i < 5; i++) {
            new Thread(() -> {
                shareData.consumer();
            }, String.valueOf(i)).start();
        }
    }
}

class ShareData {
    private int number = 0;
    private Lock lock = new ReentrantLock();
    private Condition condition = lock.newCondition();

    public void produce() {
        lock.lock();
        try {
            // 判断
            while (number != 0) {
                condition.await();
            }
            // 干活
            number++;
            System.out.println(Thread.currentThread().getName() + "\t 生产");
            // 更新状态位、通知
            condition.signalAll();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    public void consumer() {
        lock.lock();
        try {
            // 判断
            while (number != 1) {
                condition.await();
            }
            // 干活
            number--;
            System.out.println(Thread.currentThread().getName() + "\t 消费");
            TimeUnit.SECONDS.sleep(2);
            // 更新状态位、通知
            condition.signalAll();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }
}
