package top.tobing.zy.interview.juc;

import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2020/11/9 12:27
 * @Description 演示Volatile的可见性
 */
public class Demo01Volatile01 {
    public static void main(String[] args) {
        MyData myData = new MyData();

        // 创建新线程
        new Thread(() -> {
            try {
                System.out.println(Thread.currentThread().getName() + " come in.");
                // 停止3s
                TimeUnit.SECONDS.sleep(3);
                // 当前线程将值设置为60
                myData.setNumber2new();
                System.out.println(Thread.currentThread().getName() + " execute finished.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();

        while (myData.number == 0) {

        }

        System.out.println("number 新值为：" + myData.number);
        System.out.println("Main线程退出.....");
    }
}

class MyData {
    volatile int number = 0;

    public void setNumber2new() {
        number = 60;
    }
}

