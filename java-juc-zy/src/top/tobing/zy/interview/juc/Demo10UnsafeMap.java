package top.tobing.zy.interview.juc;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author tobing
 * @Date 2020/11/10 23:48
 * @Description 不安全Map
 * 1. 故障现象
 * 2. 导致问题
 * 3. 问题来源
 * 4. 优化方案
 */
public class Demo10UnsafeMap {
    public static void main(String[] args) {
        // 线程操纵资源类
        // Map<String, String> map = new HashMap<>();
        // Map<String ,String > map = new Hashtable<>();
        // Map<String, String> map = Collections.synchronizedMap(new HashMap<>());
        Map<String, String> map = new ConcurrentHashMap<>();
        for (int i = 0; i < 40; i++) {
            new Thread(() -> {
                for (int j = 0; j < 100; j++) {
                    map.put(UUID.randomUUID().toString().substring(0, 9), " ");
                    System.out.println(map);
                }
            }).start();
        }


    }
}
