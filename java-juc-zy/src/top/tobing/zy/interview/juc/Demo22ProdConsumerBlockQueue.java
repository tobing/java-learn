package top.tobing.zy.interview.juc;

import jdk.nashorn.internal.ir.Flags;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author tobing
 * @Date 2020/11/12 15:58
 * @Description 生产者消费者-BlockQueue实现
 */
public class Demo22ProdConsumerBlockQueue {
    public static void main(String[] args) throws InterruptedException {
        MyDate myDate = new MyDate(new ArrayBlockingQueue<>(5));
        new Thread(() -> {
            try {
                System.out.println(Thread.currentThread().getName() + "\t 生产进程启动");
                myDate.myProd();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "Thread-name").start();

        new Thread(() -> {
            try {
                System.out.println(Thread.currentThread().getName() + "\t 消费进程启动");
                myDate.myConsumer();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "Thread-name").start();

        TimeUnit.SECONDS.sleep(10);
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println("时间到，停止活动");
        myDate.stop();
    }
}

class MyDate {
    /**
     * 全局控制
     * true：可生产可消费
     * false:啥都不干
     */
    private volatile boolean FLAG = true;

    private AtomicInteger atomicInteger = new AtomicInteger();

    private BlockingQueue<String> blockingQueue;

    public MyDate(BlockingQueue<String> blockingQueue) {
        this.blockingQueue = blockingQueue;
    }

    public void myProd() throws InterruptedException {
        String data = null;
        boolean isFinish;
        while (FLAG) {
            data = atomicInteger.incrementAndGet() + "";
            isFinish = blockingQueue.offer(data, 2L, TimeUnit.SECONDS);
            if (isFinish) {
                System.out.println(Thread.currentThread().getName() + "\t 插入数据" + data + "成功");
            } else {
                System.out.println(Thread.currentThread().getName() + "\t 插入数据" + data + "失败");
            }
            TimeUnit.SECONDS.sleep(1);
        }
    }


    public void myConsumer() throws InterruptedException {
        String res = null;
        while (FLAG) {
            res = blockingQueue.poll(2L, TimeUnit.SECONDS);
            if (null == res || "".equals(res)) {
                FLAG = false;
                System.out.println(Thread.currentThread().getName() + "\t 超过2S没取到消息，消费退出");
                System.out.println();
                System.out.println();
                return;
            }
            System.out.println(Thread.currentThread().getName() + "消费队列" + res + "成功");
        }
    }

    public void stop() {
        this.FLAG = false;
    }
}
