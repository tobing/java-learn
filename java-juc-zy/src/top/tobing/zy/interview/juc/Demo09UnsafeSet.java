package top.tobing.zy.interview.juc;

import java.util.*;
import java.util.concurrent.CopyOnWriteArraySet;

/**
 * @Author tobing
 * @Date 2020/11/10 23:28
 * @Description 不安全集合
 * UnsafeSet
 * 1. 故障现象 java.util.ConcurrentModificationException
 * 2. 导致问题 多线程并发访问Set，由于Set不是线程安全，因此会跑出线程修改异常
 * 3. 解决方法
 * 4. 优化方案
 */
public class Demo09UnsafeSet {
    public static void main(String[] args) {
        // 线程操纵资源类
        // Set<String> set = new HashSet<>();
        // Set<String> set = Collections.synchronizedSet(new HashSet<>());
        Set<String> set = new CopyOnWriteArraySet<>();
        for (int i = 0; i < 40; i++) {
            new Thread(() -> {
                for (int j = 0; j < 100; j++) {
                    set.add(UUID.randomUUID().toString().substring(0, 9));
                    System.out.println(set);
                }
            }).start();
        }
    }
}
