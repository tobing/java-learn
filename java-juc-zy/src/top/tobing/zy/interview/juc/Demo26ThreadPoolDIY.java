package top.tobing.zy.interview.juc;

import java.util.concurrent.*;

/**
 * @Author tobing
 * @Date 2020/11/13 22:08
 * @Description 自定义线程池
 * 之前我们通过的Executors创建线程的3种方式，在实际应用中并不使用。
 * 实际中我们通过ThreadPoolExecutor直接指定参数来创建
 */
public class Demo26ThreadPoolDIY {
    public static void main(String[] args) {
        /**
         *     public ThreadPoolExecutor(int corePoolSize,                  核心工作数
         *                               int maximumPoolSize,               最大工作池数
         *                               long keepAliveTime,                其他线程保持时间
         *                               TimeUnit unit,                     保持单位
         *                               BlockingQueue<Runnable> workQueue, 阻塞队列类型
         *                               ThreadFactory threadFactory,       ThreadFactory【采用默认】
         *                               RejectedExecutionHandler handler)  拒绝执行策略
         */
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
                2,
                5,
                1,
                TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>(4),
                Executors.defaultThreadFactory(),
                // new ThreadPoolExecutor.AbortPolicy());    // 该方式会在无法接受线程的时候直接抛出异常
                // new ThreadPoolExecutor.CallerRunsPolicy()); // 该方式会在无法接受线程的时候抛会给调用者
                // new ThreadPoolExecutor.DiscardOldestPolicy());  // 该方式将处理不来的不执行
                new ThreadPoolExecutor.DiscardPolicy());

        try {
            for (int i = 0; i < 10; i++) {
                threadPool.execute(() -> {
                    System.out.println(Thread.currentThread().getName() + "\t 正在执行任务...");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 该步骤很重要，记得要释放资源
            threadPool.shutdown();
        }
    }
}

