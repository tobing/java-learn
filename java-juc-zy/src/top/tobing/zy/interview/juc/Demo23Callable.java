package top.tobing.zy.interview.juc;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/**
 * @Author tobing
 * @Date 2020/11/13 11:39
 * @Description 线程创建的第三种方式----实现Callable接口
 * Callable与Runnable的区别：
 * 1）实现的接口不一样：Callable、Runnable
 * 2）实现的方法不一样：call、run
 * 3）方法返回值不一样：call带返回值、run不带返回值
 * 4）方法的异常不一样：call抛出异常、run不抛出异常
 * <p>
 * 由于Thread类的构造方法中没有直接Callable的参数，
 * 因此我们需要通过Runnable的子接口FutureTask作为
 * 中间人，将Callable间接交给Thread执行。
 */
public class Demo23Callable {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // 将1+...+100拆分给两个线程执行运行，最终将结果合并
        FutureTask<Integer> task1 = new FutureTask<>(new MyThread());
        FutureTask<Integer> task2 = new FutureTask<>(new MyThread2());
        // 记得启动线程进行执行
        new Thread(task1).start();
        new Thread(task2).start();


        // 分别拿到两个运算结果、合并
        // 如果还没运算完将会一直阻塞
//        Integer res1 = task1.get();
//        System.out.println("线程1运算完毕，结果为：" + res1);
//        Integer res2 = task2.get();
//        System.out.println("线程2运算完毕，结果为：" + res2);

        // 采用循环的方式替换阻塞的方式
        while (!task1.isDone()) {
            System.out.println("waiting.....");
        }
        Integer res1 = task1.get();
        System.out.println("线程1运算完毕，结果为：" + res1);
        while (!task2.isDone()) {
        }
        Integer res2 = task2.get();
        System.out.println("线程2运算完毕，结果为：" + res2);


        int sum = res1 + res2;
        System.out.println("运算结果为：" + sum);
    }
}

class MyThread implements Callable<Integer> {

    /**
     * 类似于Runnable中的run方法，将要执行的任务放在这里面执行
     *
     * @return res
     * @throws Exception Ex
     */
    @Override
    public Integer call() throws Exception {
        int res = 0;
        // 运算 1 + .... + 50
        for (int i = 0; i < 50; i++) {
            res += i;
        }
        // 睡眠2s，模拟运行时间
        TimeUnit.SECONDS.sleep(2);
        return res;
    }
}

class MyThread2 implements Callable<Integer> {

    @Override
    public Integer call() throws Exception {
        int res = 0;
        for (int i = 51; i < 100; i++) {
            res += i;
        }
        // 睡眠3秒模拟运行时间
        TimeUnit.SECONDS.sleep(3);
        return res;
    }
}


